#!/usr/bin/env python3
#
# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

'''
Used to be OleToy. Respawned in py3/gi
'''

import faulthandler; faulthandler.enable()

import cairo
import importlib
import json
import math
import os
import re
import struct
import sys
import tempfile
import traceback
import time
import zipfile
import gi

gi.require_version("Gtk", "3.0")
gi.require_version('GtkSource', '4')
from gi.repository import GLib, Gio, Gtk, Gdk, GtkSource, GObject

from console import Console
from config import ConfigWindow
import formats.utils
import gui
import hv3
import formats.mf
import utils
import widgets
from utils import debug

APPNAME = "limerest"

@Gtk.Template(filename="ui/page.ui")
class AppPage(Gtk.ScrolledWindow):
    __gtype_name__ = "AppPage"

    main_tview = Gtk.Template.Child()
    hv_paned = Gtk.Template.Child()
    hv_toc = Gtk.Template.Child()
    hv_disp = Gtk.Template.Child()
    toc_tview = Gtk.Template.Child()
    toc_model = Gtk.Template.Child()
    toc_cr = Gtk.Template.Child()
    toc_tvc_cr = Gtk.Template.Child()
    tvc_path = Gtk.Template.Child()
    cr_path = Gtk.Template.Child()
    tvc_length = Gtk.Template.Child()
    cr_length = Gtk.Template.Child()

    def __init__(self, path, app, *args, **kwargs):
        super().__init__()
        self.path = path
        self.app = app
        self.kwargs = kwargs
        self.model = self.main_tview.get_model()
        self.main_tview.connect("key-press-event", gui.on_row_keypress, self.app)
        self.main_tview.connect("key-release-event", self.on_row_keyrelease)
        self.main_tview.connect("row-activated", self.on_row_activated)
        self.search_tables = {}  # FIXME! Move to doc_data?
        self.doc_data = {} # for format parser to store anything interesting
        self.hv_paned.set_position(200)
        self.toc_tview.connect("key-press-event", gui.on_row_keypress)
        self.toc_tview.connect("key-release-event", self.on_toc_row_keyrelease)
        self.toc_tview.connect("row-activated", self.on_toc_row_activated)
        self.toc_cr.connect('edited', self.toc_edited_cb)
        self.toc_hl = None
        self.tvc_path.set_cell_data_func(self.cr_path, self.cell_func_path)
        self.tvc_length.set_cell_data_func(self.cr_length, self.cell_func_path)
        self.project = None
        self.load_file(**kwargs)
        for c in self.main_tview.get_columns():
            c.set_sizing(0)

    def cell_func_path(self, column, cell, model, iter, *args):
        if cell == self.cr_path:
            cell.set_property("text", model.get_string_from_iter(iter))
        else:
            cell.set_property("text", "%s" % len(model[iter][3]))

    def add_progressbar(self):
        pb = Gtk.ProgressBar.new()
        self.kwargs["progressbar"] = pb
        tablabel = self.app.windows["main"].main_nb.get_tab_label(self)
        self.kwargs["tablabel"] = tablabel
        lbl = tablabel.label
        lbltxt = lbl.get_text()
        pb.set_text(lbltxt)
        pb.set_show_text(True)
        tablabel.pack_start(pb, False, False, 0)
        tablabel.reorder_child(pb, 0)
        pb.show()
        lbl.hide()
        return pb

    def pgi_add(self, **kwargs):
        '''Adds iter to main_tview model'''
        # name, type, data
        pgi = [kwargs.get("name"), kwargs.get("type"), kwargs.get("options"), kwargs.get("data") or b""]
        itr = self.model.append(kwargs.get("parent"), pgi)
        return itr

    def pgi_ins(self, **kwargs):
        '''Insert iter after sibling'''
        pgi = [kwargs.get("name"), kwargs.get("type"), kwargs.get("options"), kwargs.get("data") or b""]
        parent = kwargs.get("parent")
        sibling = kwargs.get("sibling")
        itr = self.model.insert_after(parent, sibling, pgi)
        return itr

    def pgi_mod(self, itr, **kwargs):
        pos = {"name": 0, "type": 1, "options": 2}
        for k, v in kwargs.items():
            if k in pos:
                self.model.set_value(itr, pos[k], v)

    def load_file(self, **kwargs):
        buf = kwargs.get("data")
        if kwargs.get("type"):
            action = None
            parent = self.pgi_add(name="File", type=kwargs.get("type"), data=buf)
        else:
            if buf is None:
                with open(self.path, "rb") as f:
                    buf = f.read()
            self.app.reload_action.set_enabled(True)
            size = len(buf)
            self.fmt, kw, action = formats.utils.detect(buf, size)
            ver = kw.get("version") if kw else None
            ftype = "%s%s" % (self.fmt, ":v%s" % ver if ver else "")
            print("Detected: %s (%s)" % (ftype, self.path))
            parent = self.pgi_add(name="File", type=ftype, data=buf)
            self.kwargs["type"] = ftype
        if "hexview" not in self.__dict__:
            self.hexview = hv3.HexView(self, self.app.settings, parent)
            self.hv_paned.add(self.hexview)
        if action:
            self.kwargs["offset"] = 0
            GLib.idle_add(action, self, parent, buf, kw)
        elif "scroll_onload" in self.kwargs:
            del self.kwargs["scroll_onload"]


    def toc_edited_cb(self, cell, path, value):
        item = self.toc_model[path][2]
        try:
            fmt = item["fmt"]
            if len(fmt) == 2 and fmt[0] in "<>" and fmt[1] in "iIhHbBlLqQdf":
                if value[0] == value[-1] and value[0] in '"\'':
                    # string
                    if len(value) - 2 > struct.calcsize(fmt):
                        value = value[1:-1]
                    else:
                        value = value[1:-1] + '\x00' * max(0, len(value) - 2 - struct.calcsize(fmt))
                    new = bytes(value, "utf-8")
                    value = struct.unpack(fmt, new)
                    self.toc_model[path][1] = "%d" % value
                else:
                    rpz = value.strip()[:2] == "0x"
                    value = eval(value)
                    new = struct.pack(fmt, value)
                    self.toc_model[path][1] = "%#x" % value if rpz else "%g" % value
                data = b""
                if item["offset"] > 0:
                    data = self.hexview.data[:item["offset"]]
                data += new
                data += self.hexview.data[item["offset"] + item["length"]:]
                self.hexview.data = data
                self.model.set_value(self.hexview.data_iter, 3, data)
                nitr = self.toc_model[path].next
                if nitr:
                    self.on_toc_row_activated(self.toc_tview, nitr.path, None)
                    self.toc_tview.set_cursor(nitr.path, None, False)

            else:
                print("Format %s is not yet supported" % fmt)
        except Exception as e:
            print("Failed to modify data", e)

    def on_toc_row_activated(self, tv, path, tvc):
        data = self.toc_model[path][2]
        if self.toc_hl and self.toc_hl in self.hexview.highlights:
            self.hexview.highlights.remove(self.toc_hl)
        self.toc_hl = (1, 1, 0, .3), data["offset"], data["length"] - 1
        self.hexview.highlights.append(self.toc_hl)
        self.hexview.scroll_to(data["offset"], redraw=True, force=False)

    def on_load_complete(self):
        self.main_tview.set_reorderable(True)
        self.main_tview.thaw_child_notify()
        pb = self.kwargs.get("progressbar")
        if pb:
            self.kwargs["tablabel"].remove(pb)
            self.kwargs["tablabel"].label.show()
            del self.kwargs["progressbar"]
        sol = self.kwargs.get("scroll_onload")
        if sol:
            self.main_tview.expand_to_path(sol)
            self.main_tview.set_cursor(sol, None, False)
            self.main_tview.row_activated(sol, self.main_tview.get_column(0))
            del self.kwargs["scroll_onload"]
        proj = self.kwargs.get("apply_project")
        if proj:
            utils.apply_project(self, proj)
            del self.kwargs["apply_project"]

    def on_row_activated(self, tv, path, tvc):
        img = self.hv_disp.get_children()
        if img:
            self.hv_disp.remove(img[0])
            self.hv_disp.hide()
        iter1 = self.model.get_iter(path)
        if self.hexview.data != self.model.get_value(iter1, 3):
            self.toc_model.clear()
            # need to redraw
            lines = None
            if self.model[path][2] and "hv3" in self.model[path][2] and "lines" in self.model[path][2]["hv3"]:
                lines = self.model[path][2]["hv3"]["lines"]
            self.hexview.highlights = []
            self.hexview.set_data(iter1, lines=lines)
            self.hexview.set_size()
            ft = self.model[path][1].split("/") if self.model[path][1] else []
            if len(ft) > 1:
                # data for TOC tview
                fmt, rtype = ft[:2]
                fmtpath = os.path.join(self.app.execpath, "formats", "%s.py" % fmt)
                if fmt and os.path.exists(fmtpath) and fmt not in self.app.formats:
                    try:
                        mod = importlib.import_module("formats.%s" % fmt)
                        self.app.formats[fmt] = mod
                    except Exception as e:
                        #print("Failed to import module", fmt, e)
                        return
                if self.model[path][2] and "parse" in self.model[path][2] and "func" in self.model[path][2]["parse"]:
                    rtype = self.model[path][2]["parse"]["func"]
                if rtype in self.app.formats[fmt].__dict__:
                    try:
                        self.app.formats[fmt].__dict__[rtype](self, self.toc_model, self.model[path][2], self.model[path][3])  # model, options and data
                    except Exception as e:
                        debug("FAILED in %s:%s\n" % (fmt, rtype), e)
                        traceback.print_exc()
                    self.toc_tview.columns_autosize()
        else:
            # need this otherwise editing after jumping (eg. w mouse) between records with identical content
            # will have interesting side effects
            self.hexview.data_iter = iter1

    def update_stbar_proj(self):
        self.app.saveproj_action.set_enabled(True)
        markup = "<b>File:</b> %s\n<b>Description:</b>" % self.project["file"]
        self.app.windows["main"].proj_title.set_markup(markup)
        self.app.windows["main"].proj_title.set_halign(1)
        markup = "%s" % self.project["desc"]
        self.app.windows["main"].proj_desc.get_buffer().set_text(markup)
        self.app.windows["main"].proj_desc.set_halign(1)

        pmodel = self.app.windows["main"].proj_tview.get_model()
        pmodel.clear()
        for p, c in self.project["paths"].items():
            v  = self.model[p][0]
            pmodel.append([p, v, c])

    def on_toc_row_keyrelease(self, w, e):
        selection = w.get_selection()
        _, itr = selection.get_selected()
        if itr:
            itr_path = self.toc_model.get_path(itr)
            self.on_toc_row_activated(None, itr_path, None)

    def on_row_keyrelease(self, w, e):
        selection = w.get_selection()
        _, itrs = selection.get_selected_rows()
        if itrs:
            self.on_row_activated(None, itrs[0], None)

class Application(Gtk.Application):
    def __init__(self, *args, **kwargs):
        super().__init__(
            *args,
            application_id="re-lab.limerest",
            flags=Gio.ApplicationFlags.HANDLES_OPEN,
            **kwargs
        )
        self.appname = APPNAME
        self.execpath = os.path.dirname(os.path.abspath(__file__))
        self.windows = {}
        self.clp = None
        self.hvclp = None
        self.formats = {}
        self.connect("open", self.open_files)

    def do_startup(self):
        Gtk.Application.do_startup(self)

        actions = {
            "open": self.on_open,
            "close": self.on_close,
            "new": self.on_new, # show dialog to select one of the supported formats
            "console": self.on_console,
            "about": self.on_about,
            "manual": self.on_manual,
            "quit": self.on_quit,
            "add-record": self.on_add_record,
            "saveproj": self.on_save_project,
            "loadproj": self.on_load_project,
            "reload": self.on_file_reload,
            "dump": self.on_file_dump,
            "prefs": self.on_preferences,
        }
        self.config()

        for k,v in actions.items():
            action = Gio.SimpleAction.new(k, None)
            action.connect("activate", v)
            self.add_action(action)

        self.add_rec_action = self.lookup_action("add-record")
        self.add_rec_action.set_enabled(False)
        self.saveproj_action = self.lookup_action("saveproj")
        self.saveproj_action.set_enabled(False)
        self.reload_action = self.lookup_action("reload")
        self.reload_action.set_enabled(False)
        self.dump_action = self.lookup_action("dump")
        self.dump_action.set_enabled(False)

        self.builder = Gtk.Builder.new_from_file("ui/app.ui")
        self.set_menubar(self.builder.get_object("menubar"))
        self.activate()

    def config(self):
        # default settings
        c1, c2 = Gdk.RGBA(), Gdk.RGBA()
        c1.parse("#4CFF4C")
        c2.parse("#4C4CFF")
        self.settings = {
            "main/files_path": {"type": "dir", "value": os.path.expanduser("~"), "name": "Main/Base path to files"},
#            "main/tree_fontsize": {"type": "int", "value": 20, "name": "Main/Tree font size"},
            "hexview/fontsize": {"type": "int", "value": 20, "name": "Hexview/Font size"},
            "hexview/cur_clr": {"type": "rgb", "value": c1, "name": "Hexview/Positive selection color"},
            "hexview/cur_clr2": {"type": "rgb", "value": c2, "name": "Hexview/Negative selection color"},
            "hexview/hexlen": {"type": "int", "value": 16, "name": "Hexview/Line length"},
            "hexview/scrolling_speed": {"type": "float", "value": 2, "name": "Hexview/Scrolling speed"},
            "hexview/line_interval": {"type": "float", "value": 1.4, "name": "Hexview/Line interval"},
            "console/fontsize": {"type": "int", "value": 20, "name": "Console/Font size"},
            "statusbar/show_le": {"type": "bool", "value": True, "name": "Statusbar/Show LE value"},
            "statusbar/show_be": {"type": "bool", "value": True, "name": "Statusbar/Show BE value"},
            "statusbar/show_midi": {"type": "bool", "value": False, "name": "Statusbar/Show MIDI value"},
            "statusbar/show_color": {"type": "bool", "value": True, "name": "Statusbar/Show color"},
            "statusbar/div_only": {"type": "bool", "value": False, "name": "Statusbar/Show divided values only"},
#            "statusbar/divs": {"type": "list:float", "value": [], "name": "Statusbar/List of divisors"},
            "statusbar/divs": {"type": "txt", "value": "", "name": "Statusbar/List of divisors"},
            "statusbar/show_txt": {"type": "bool", "value": False, "name": "Statusbar/Show text"},
            "statusbar/show_ipv4": {"type": "bool", "value": False, "name": "Statusbar/Show IPv4"},
            "statusbar/encoding": {"type": "txt", "value": "utf-8", "name": "Statusbar/Text encoding"},
            "hexview/show_keypress": {"type": "bool", "value": False, "name": "Hexview/Show key presses"},
            "hexview/msg_delay": {"type": "int", "value": 600, "name": "Keypress message delay"},
        }
        self.basepath = os.path.join(GLib.get_user_config_dir(), self.appname)
        self.cfgpath = os.path.join(self.basepath, "config.xml")
        self.execpath = os.path.dirname(os.path.abspath(__file__))
        ok, msg = utils.read_config(self.cfgpath, self.settings, self.basepath)
        if not ok:
            print(msg)
        if not os.path.exists(self.basepath):
            os.makedirs(self.basepath)

    def do_activate(self):
        # We only allow a single window and raise any existing ones
        if "main" not in self.windows:
            # Windows are associated with the application
            # when the last one is closed the application shuts down
            self.windows["main"] = gui.AppWindow(AppPage, application=self, title=self.appname)
        self.windows["main"].present()
        self.msg_win = None
        self.msg_timeout = None
        self.stbar_timeout = None

    def set_stbar_msg(self, msg, delay):
        if self.stbar_timeout:
            GLib.source_remove(self.stbar_timeout)
        self.windows["main"].stbar1.set_markup(msg)
        self.stbar_timeout = GLib.timeout_add_seconds(delay, self.unset_stbar_msg)

    def unset_stbar_msg(self):
        self.windows["main"].stbar1.set_markup("")

    def on_msg(self):
        if self.msg_win:
            self.msg_win.present()
        else:
            self.msg_win = Gtk.Window()
            self.msg_da = Gtk.DrawingArea()
            self.msg_da.connect("draw", self.draw_msg)
            self.msg_win.add(self.msg_da)
            self.msg_win.set_gravity(Gdk.Gravity.SOUTH)
            self.msg_win.move(1200, 1200)
            self.msg_win.set_decorated(False)
            self.msg_win.set_can_focus(False)
            self.msg_win.set_app_paintable(True)
            self.msg_win.set_keep_above(True)
            self.msg_win.set_visual(self.msg_win.get_screen().get_rgba_visual())
            self.msg_win.show_all()

    def draw_msg(self, widget, ctx):
        ctx = widget.get_window().cairo_create()
        ctx.select_font_face("Monospace", cairo.FONT_SLANT_NORMAL, cairo.FONT_WEIGHT_NORMAL)
        ctx.set_source_rgb(1, 0, 0)
        ctx.set_font_size(128)
        ctx.move_to(20, 148)
        ctx.show_text(self.msg_text)

    def unset_msg(self):
        self.msg_text = ""
        self.msg_timeout = None
        self.msg_win.hide()
        return False

    def update_msg(self, msg):
        if not self.msg_win:
            self.on_msg()
        if self.msg_timeout:
            GLib.source_remove(self.msg_timeout)
        self.msg_text = msg
        self.draw_msg(self.msg_da, None)
        self.msg_timeout = GLib.timeout_add(self.settings["hexview/msg_delay"]["value"], self.unset_msg)
        self.msg_win.show()

    def open_files(self, app, files, nfiles, hint):
        for f in files:
            page = self.windows["main"].add_file(f.get_path())
            page.main_tview.columns_autosize()
        self.windows["main"].present()
        return 0

    def on_file_dump(self, action, param):
        pn = self.windows["main"].main_nb.get_current_page()
        if pn < 0:
            return
        page = self.windows["main"].main_nb.get_nth_page(pn)
        ftype = page.kwargs.get("type").lower()
        try:
            dialog = Gtk.FileChooserDialog(
                title="Please choose a file", parent=self.windows["main"], action=Gtk.FileChooserAction.SAVE
            )
            dialog.add_buttons(
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_SAVE, Gtk.ResponseType.OK,
            )
            fltr = Gtk.FileFilter()
            fltr.add_pattern("*.%s" % ftype)
            dialog.set_filter(fltr)
            dialog.set_filename(page.path)
            response = dialog.run()
            filename = dialog.get_filename()
            dialog.destroy()
            if response == Gtk.ResponseType.OK:
                self.formats[ftype].file_save(filename, page.model)
                page.path = filename
                self.windows["main"].change_page_name(page, os.path.split(filename)[1])
        except Exception as e:
            debug("FAILED to dump %s\n" % ftype, e)
            traceback.print_exc()

    def on_file_reload(self, action, param):
        pn = self.windows["main"].main_nb.get_current_page()
        if pn < 0:
            return
        page = self.windows["main"].main_nb.get_nth_page(pn)
        if not os.path.exists(page.path):
            return
        if "progressbar" in page.kwargs:
            # do not reload if not yet loaded
            return
        selection = page.main_tview.get_selection()
        model, itrs = selection.get_selected_rows()
        if itrs:
            path = itrs[0]
        else:
            path = model.get_path(model.get_iter_from_string("0"))
        page.model.clear()
        page.doc_data = {}
        page.kwargs["scroll_onload"] = path
        page.load_file()

    def on_save_project(self, action, param):
        pn = self.windows["main"].main_nb.get_current_page()
        if pn < 0:
            return
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file", parent=self.windows["main"], action=Gtk.FileChooserAction.SAVE
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_SAVE, Gtk.ResponseType.OK,
        )
        fltr = Gtk.FileFilter()
        fltr.add_pattern("*.lrp")
        dialog.set_filter(fltr)

        page = self.windows["main"].main_nb.get_nth_page(pn)
        if not page.project.get("proj"):
            page.project["proj"] = page.project["file"] + ".lrp"
        cur_fname = os.path.split(page.project["proj"])[1]
        dialog.set_current_name(cur_fname)
        if not page.project["sha"]:
            with open(page.project["file"], "rb") as f:
                page.project["sha"] = utils.calchash(f)
        fsz = os.stat(page.path).st_size
        lbl = Gtk.Label()
        proj_fname = os.path.relpath(os.path.normpath(os.path.join(self.settings["main/files_path"]["value"], page.project["file"])), self.settings["main/files_path"]["value"])
        markup = "<b>Main file:</b> %s\n<b>Size:</b> %s\n<b>SHA:</b> %s\n<b>Description:</b>" % (proj_fname, fsz, page.project["sha"])
        lbl.set_markup(markup)
        lbl.set_halign(1)
        desc = Gtk.TextView()
        desc.set_editable(True)
        desc.set_size_request(-1, 200)
        desc.get_buffer().set_text(page.project.get("desc") or "")
        gs = Gtk.Separator.new(Gtk.Orientation.HORIZONTAL)
        chkb = Gtk.CheckButton.new_with_label("Add main file")
        proj_box = Gtk.Box.new(Gtk.Orientation.VERTICAL, 0)
        proj_box.pack_start(lbl, False, False, 0)
        proj_box.pack_start(desc, False, False, 0)
        proj_box.pack_start(gs, False, False, 2)
        proj_box.pack_start(chkb, False, False, 0)
        ca = dialog.get_content_area()
        ca.set_orientation(Gtk.Orientation.HORIZONTAL)
        ca.pack_end(proj_box, True, True, 8)
        ca.show_all()
        response = dialog.run()
        filename = dialog.get_filename()
        tb = desc.get_buffer()
        dialog.destroy()
        if response == Gtk.ResponseType.OK:
            desc_txt = tb.get_text(tb.get_start_iter(), tb.get_end_iter(), False)
            m = page.model
            poi = {}
            for path, cmnt in page.project["paths"].items():
                if m[path][2]:
                    poi[path] = (cmnt, m[path][2]["hv3"]["lines"])
            if poi:
                tmp = tempfile.mkstemp()[1]
                with open(tmp, "w") as f:
                    poi = json.dumps(poi, separators=(",", ":"))
                    f.write('{"file":"%s","sha":"%s","size":"%s","desc":"%s","poi":%s' % (proj_fname, page.project["sha"], fsz, desc_txt, poi))
                    f.write("}")

                with zipfile.ZipFile(filename, "w", compression=zipfile.ZIP_BZIP2) as z:
                    z.write(tmp, "project")
                    if chkb.get_active():
                        z.write(page.path, "basefile")
                os.remove(tmp)

    def on_load_project(self, action, param):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a project file", parent=self.windows["main"], action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
        )
        fltr = Gtk.FileFilter()
        fltr.add_pattern("*.lrp")
        dialog.set_filter(fltr)

        response = dialog.run()
        files = [dialog.get_filename()]
        dialog.destroy()
        if response == Gtk.ResponseType.OK:
            # currently dialog is set for a single file
            fpath = files[0]
            zdata = False
            try:
                with zipfile.ZipFile(fpath, "r") as z:
                    zproj = z.read("project")
                    if "basefile" in z.namelist():
                        zdata = True
            except:
                dlg = Gtk.MessageDialog(
                    transient_for=self.windows["main"],
                    flags=0,
                    message_type=Gtk.MessageType.ERROR,
                    buttons=Gtk.ButtonsType.OK,
                    text="Project is not loaded",
                )
                dlg.format_secondary_markup("<b>This is not a limerest project file</b>")
                dlg.run()
                dlg.destroy()
                return

            obj = json.loads(zproj)
            obj["proj"] = fpath
            page_not_found = True
            pn = self.windows["main"].main_nb.get_current_page()
            num_pgs = self.windows["main"].main_nb.get_n_pages()
            pages = []
            if num_pgs > 1:
                pages = list(range(num_pgs))
                pages.remove(pn)
            if pn > -1:
                pages = [pn,] + pages
            # start with current page
            for pi in pages:
                page = self.windows["main"].main_nb.get_nth_page(pi)
                with open(page.path, "rb") as h:
                    sha = utils.calchash(h)
                if obj["sha"] == sha:
                    utils.apply_project(page, obj)
                    self.windows["main"].stbar_proj.show()
                    self.windows["main"].main_nb.set_current_page(pi)
                    page_not_found = False
                    break
            while page_not_found:
                dlg = Gtk.Dialog(title="Base file is not opened", transient_for=self.windows["main"], flags=0)
                buttons = ["Cancel", Gtk.ResponseType.CANCEL, "Open", Gtk.ResponseType.OK]
                if zdata:
                    buttons += ["Load", Gtk.ResponseType.ACCEPT]
                dlg.add_buttons(*buttons)
                dlg.set_default_size(150, 100)

                label = Gtk.Label()
                markup = "The project needs a base file named <b><i>%s</i></b> with a size of <b><i>%s</i></b> bytes.\n\n" % (obj["file"], obj["size"])
                if zdata:
                    markup += "You can load base file embedded in this project or open one from your storage.\n"
                else:
                    markup += "Do you want to open a base file?"
                label.set_markup(markup)
                ca = dlg.get_content_area()
                ca.add(label)
                ca.show_all()
                res = dlg.run()
                dlg.destroy()
                if res == Gtk.ResponseType.ACCEPT:
                    # FIXME! need to offer to save 'basefile'
                    try:
                        with zipfile.ZipFile(fpath, "r") as z:
                            zdata = z.read("basefile")
                        normfile = os.path.normpath(os.path.join(self.settings["main/files_path"]["value"], obj["file"]))
                        page = self.windows["main"].add_file("%s" % normfile, **{"data":zdata, "apply_project": obj})
                        self.windows["main"].stbar_proj.show()
                        page_not_found = False
                        break
                    except:
                        dlg = Gtk.MessageDialog(
                            transient_for=self.windows["main"],
                            flags=0,
                            message_type=Gtk.MessageType.ERROR,
                            buttons=Gtk.ButtonsType.OK,
                            text="Base file is not loaded",
                        )
                        dlg.format_secondary_markup("<b>Failed to extract a base file from the archive</b>")
                        dlg.run()
                        dlg.destroy()

                elif res == Gtk.ResponseType.OK:
                    # user tries to open a file
                    dialog = Gtk.FileChooserDialog(
                        title="Please choose a base file (like %s)" % obj["file"], parent=self.windows["main"], action=Gtk.FileChooserAction.OPEN
                    )
                    dialog.add_buttons(
                        Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                        Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
                    )
                    while True:
                        response = dialog.run()
                        files = [dialog.get_filename()]
                        if response == Gtk.ResponseType.OK:
                            with open(files[0], "rb") as h:
                                sha = utils.calchash(h)
                            if sha != obj["sha"]:
                                dlg = Gtk.MessageDialog(
                                    transient_for=self.windows["main"],
                                    flags=0,
                                    message_type=Gtk.MessageType.ERROR,
                                    buttons=Gtk.ButtonsType.OK,
                                    text="Hash doesn't match",
                                )
                                dlg.format_secondary_markup("<b>This base file hash doesn't match with one in the project file</b>")
                                dlg.run()
                                dlg.destroy()
                            else:
                                # load file, apply project
                                dialog.destroy()
                                try:
                                    page = self.windows["main"].add_file(files[0], **{"apply_project": obj})
                                    self.windows["main"].stbar_proj.show()
                                except Exception as e:
                                    dlg = Gtk.MessageDialog(
                                        transient_for=self.windows["main"],
                                        flags=0,
                                        message_type=Gtk.MessageType.ERROR,
                                        buttons=Gtk.ButtonsType.OK,
                                        text="Something failed...",
                                    )
                                    markup = "<b>Exception happened while opening a file or project.</b>\n\n"
                                    markup += e
                                    dlg.format_secondary_markup(markup)
                                    dlg.run()
                                    dlg.destroy()

                                page_not_found = False
                                break
                        else:
                            dialog.destroy()
                            break
                else: # don't want to load project
                    break

    def on_close(self, action, param):
        pn = self.windows["main"].main_nb.get_current_page()
        if pn > -1:
            self.windows["main"].stbar_proj.hide()
            self.windows["main"].main_nb.remove_page(pn)

    def on_new(self, action, param):
        dialog = Gtk.Dialog(title="Select format", transient_for=self.windows["main"], flags=0)
        dialog.add_buttons( Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL, Gtk.STOCK_OK, Gtk.ResponseType.OK)
        dialog.set_default_size(150, 100)
        formats = ["EMF", "WMF"]
        combo = Gtk.ComboBoxText()
        combo.set_entry_text_column(0)
        for fmt in formats:
            combo.append_text(fmt)
        combo.set_active(0)
        box = dialog.get_content_area()
        box.add(combo)
        dialog.show_all()
        resp = dialog.run()
        choice = None
        if resp == Gtk.ResponseType.OK:
            choice = formats[combo.get_active()]
        dialog.destroy()
        if choice:
            page = self.windows["main"].add_file("%s File" % choice, **{"data":b"", "type":choice})
            page.main_tview.columns_autosize()
            page.main_tview.set_reorderable(True)
            self.add_rec_action.set_enabled(True)

    def on_window_destroy(self, w, e, name):
        del self.windows[name]

    def on_add_record(self, action, param):
        pn = self.windows["main"].main_nb.get_current_page()
        if pn < 0:
            return
        page = self.windows["main"].main_nb.get_nth_page(pn)

        if page.kwargs.get("type") in ["EMF", "WMF"]:
            if page.kwargs.get("type") == "EMF":
                wname = "add_record_emf"
                m, v = formats.mf.emf_gentree()
            elif page.kwargs.get("type") == "WMF":
                wname = "add_record_wmf"
                m, v = formats.mf.wmf_gentree()
            if wname not in self.windows:
                self.windows[wname] = Gtk.Window()
                self.windows[wname].connect("delete-event", self.on_window_destroy, wname)
                scr = Gtk.ScrolledWindow()
                scr.set_size_request(300, 480)
                v.connect("key-press-event", gui.on_row_keypress)
                v.connect("row-activated", formats.mf.on_row_activated, self.windows["main"].main_nb, wname)
                scr.add(v)
                self.windows[wname].add(scr)
                self.windows[wname].show_all()
            self.windows[wname].present()

    def on_open(self, action, param):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file", parent=self.windows["main"], action=Gtk.FileChooserAction.OPEN
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
        )
        response = dialog.run()
        files = [dialog.get_filename()]
        dialog.destroy()
        if response == Gtk.ResponseType.OK:
            for fpath in files:
                self.windows["main"].add_file(fpath)

    def save_config(self):
        utils.write_config(self.cfgpath, self.settings, APPNAME)

    def save(self, data):
        dialog = Gtk.FileChooserDialog(
            title="Please choose a file", parent=self.windows["main"], action=Gtk.FileChooserAction.SAVE
        )
        dialog.add_buttons(
            Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
            Gtk.STOCK_OPEN, Gtk.ResponseType.OK,
        )
        response = dialog.run()
        filename = dialog.get_filename()
        dialog.destroy()
        if response == Gtk.ResponseType.OK:
            with open(filename, "wb") as f:
                f.write(data)

    def on_console(self, action, param):
        if "console" not in self.windows:
            self.windows["console"] = Console(self)
        self.windows["console"].present()

    def on_preferences(self, action, param):
        if "prefs" not in self.windows:
            self.windows["prefs"] = ConfigWindow(self)
        self.windows["prefs"].present()

    def on_manual(self, action, param):
        with open(os.path.join(self.execpath, 'docs/manual.txt'), "r") as f:
            help_text = f.read()

        label = Gtk.TextView()
        tb = label.get_buffer()
        si = tb.get_start_iter()
        tb.insert_markup(si, help_text, -1)
        label.set_editable(False)
        d = Gtk.Window()
        d.set_title("limerest: manual")
        sw = Gtk.ScrolledWindow()
        sw.add(label)
        sw.set_margin_start(24)
        d.set_size_request(1200, 800)
        d.add(sw)
        d.show_all()

    def on_about(self, action, param):
        builder = Gtk.Builder.new_from_file("ui/about.ui")
        about_dialog = builder.get_object("about")
        about_dialog.set_transient_for(self.windows["main"])
        about_dialog.present()

    def on_quit(self, action, param):
        self.quit()

if __name__ == "__main__":
    try:
        app = Application()
        app.run(sys.argv)
    except KeyboardInterrupt:
        app.quit()

# END
