# Requirements
- python3.7+
- pygobject
- pycairo
- lxml
- gtksourceview4

## Installation on Ubuntu

```bash
sudo apt install python3-pip libgtksourceview-4-common libgtksourceview-4-dev python3-lxml python3-cairo python3-gi-cairo libgsf-bin
```

## Installation on macOS

### Install pip

- Download pip by running the following command:

```bash
$ curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
```

- Install downloaded package
```bash
$ python3 get-pip.py
```

### Install Homebrew

```bash
$ /bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"
```

### Install requirements

```bash
$ pip3 install lxml pycairo pygobject
$ brew install gtksourceview4
```