# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

from collections import defaultdict
from functools import lru_cache
import inspect
import hashlib
import lxml.etree as ET
import os
import re
import struct
import time

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gdk, Gtk

import formats.midi

def add_iter(model, name, value, offset, length, fmt, parent=None):
    # toc_tview iter
    itr = model.append(parent, [name, str(value), {"offset": offset, "length":length, "fmt": fmt}])
    return itr

def calchash(fh):
    h  = hashlib.sha1()
    b  = bytearray(64*1024)
    mv = memoryview(b)
    for n in iter(lambda : fh.readinto(mv), 0):
        h.update(mv[:n])
    return h.hexdigest()

def debug(*args, **kwargs):
    cf = inspect.currentframe().f_back

    fi = inspect.getframeinfo(cf)
    filename = os.path.split(fi.filename)[1]
    lineno = cf.f_lineno
    funcname = fi.function
    print("%s:%s (%s)" % (filename, lineno, funcname), *args, **kwargs)

def str2clr(txt):
    rgb = Gdk.RGBA()
    rgb.parse(txt)
    return (rgb.red, rgb.green, rgb.blue)

def apply_project(page, obj):
    def set_entry(page, p, d):
        mod4 = page.model[p][2] or {}
        if not "hv3" in mod4:
            mod4["hv3"] = {}
        mod4["hv3"]["lines"] = d[1]
        page.model[p][2] = mod4

    tmp = []
    if not page.project:
        page.project = {"paths":{}}

    for p, d in obj["poi"].items():
        if p not in page.project["paths"]:
            # new project entry
            set_entry(page, p, d)
        elif page.model[p][2]["hv3"]["lines"] != d[1]:
            # updated project entry
            tmp.append((p, d))
    if tmp:
        # need to confirm entries update
        dlg = Gtk.Dialog(title="Project update conflict", transient_for=page.app.windows["main"], flags=0)
        buttons = ["No", Gtk.ResponseType.CANCEL, "Yes", Gtk.ResponseType.OK]
        dlg.add_buttons(*buttons)
        dlg.set_default_size(150, 100)
        label = Gtk.Label()
        markup = "The project that you are loading\nhas differences with entries in your local project.\n\n"
        markup += "Do you want to update those?"
        label.set_markup(markup)
        ca = dlg.get_content_area()
        ca.add(label)
        ca.show_all()
        res = dlg.run()
        dlg.destroy()
        if res == Gtk.ResponseType.OK:
            for p, d in tmp:
                set_entry(page, p, d)

    paths = {k:v[0] for k, v in obj["poi"].items()}
    page.project["paths"].update(paths)
    page.project.update({"file":obj["file"], "sha":obj["sha"], "desc":obj["desc"], "proj":obj["proj"]})
    page.update_stbar_proj()
    gtp = Gtk.TreePath.new_from_string(list(page.project["paths"])[0])
    page.hexview.data = ""
    page.main_tview.expand_to_path(gtp)
    page.main_tview.set_cursor(gtp, None, False)
    page.main_tview.row_activated(gtp, page.main_tview.get_column(0))

def data2status(data, options):
    '''
    options: dict with params influencing selection and pointer(s) to 'search_tables' if any
    '''
    divs = options.get("divs")
    div_only = options.get("div_only")
    if not divs:
        divs = [1,]
    else:
        divs = [float(x) for x in divs.split(",")]
        if not div_only and 1 not in divs:
            divs = [1, *divs]

    markup = ""
    if len(data) == 1:
        markup += "%d " % data[0]
        if options.get("midi"):
            if data[0] in formats.midi.pitches:
                markup += formats.midi.pitches[data[0]]
            if data[0] in formats.midi.controllers:
                markup += "\t[" + formats.midi.controllers[data[0]]+"]"
    elif len(data) == 2:
        markup = ""
        if options.get("le"):
            markup += "<b>LE:</b> %d  %d " % (struct.unpack("<H", data)[0], struct.unpack("<h", data)[0])
        if options.get("be"):
            markup += "<b>BE:</b> %d %d" % (struct.unpack(">H", data)[0], struct.unpack(">h", data)[0])
    elif len(data) == 3 and options.get("clr"):
        markup = "<span background='#%02x%02x%02x' foreground='#%02x%02x%02x'>RGB</span>" % (data[0], data[1], data[2], 255 - data[0], 255 - data[1], 255 - data[2])
        markup += "<span background='#%02x%02x%02x' foreground='#%02x%02x%02x'>BGR</span>" % (data[2], data[1], data[0], 255 - data[2], 255 - data[1], 255 - data[0])
    elif len(data) == 4:
        if options.get("le"):
            lef = struct.unpack("<f", data)[0]
            lei = struct.unpack("<i", data)[0]
            markup = "<b>LE:</b> %d" % lei
            if lei < 0:
                leI = struct.unpack("<I", data)[0]
                markup += " ["
                for d in divs:
                    markup += " %g" % (leI / d)
                markup += " ]"
            if abs(lef) > .00001 and abs(lef) < 100000000:
                for d in divs:
                    markup += " <b>: </b>%g " % (lef / d)
        if options.get("be"):
            bef = struct.unpack(">f", data)[0]
            bei = struct.unpack(">i", data)[0]
            markup += "  <b>BE:</b> %d" % bei
            if bei < 0:
                beI = struct.unpack(">I", data)[0]
                markup += " ["
                for d in divs:
                    markup += " %g" % (beI / d)
                markup += " ]"
            if abs(bef) > .00001 and abs(bef) < 100000000:
                for d in divs:
                    markup += " <b>:</b> %g " % (bef / d)
        if options.get("clr"):
            r, g, b = formats.utils.cmyk2rgb(data[0]/255, data[1]/255, data[2]/255, data[3]/255)
            markup += "  <span background='#%02x%02x%02x' foreground='#%02x%02x%02x'>CMYK</span>" % (r, g, b, 255 - r, 255 - g, 255 - b)
        if options.get("ipv4"):
            markup += " IPv4: %d.%d.%d.%d" % (data[0], data[1], data[2], data[3])
    elif len(data) == 8:
        if options.get("le"):
            led = struct.unpack("<d", data)[0]
            if led == 0:
                markup += "<b>LE:</b> 0 "
            elif abs(led) > .00001 and abs(led) < 10000000:
                markup += " <b>LE:</b> ["
                for d in divs:
                    markup += " %g" % (led / d)
                markup += " ] "
        if options.get("be"):
            bed = struct.unpack(">d", data)[0]
            if bed == 0:
                markup += "<b>BE:</b> 0 "
            elif abs(bed) > .00001 and abs(bed) < 10000000:
                markup += "<b>BE:</b> ["
                for d in divs:
                    markup += " %g" % (bed / d)
                markup += " ] "
        xt_le = struct.unpack("<Q", data[4:8]+data[:4])[0]
        xt_be = struct.unpack(">Q", data)[0]
        if options.get("date"):
            delta = options.get("date_delta") or 0
            xtl, xtb = "", ""
            try:
                xtl = time.ctime(xt_le/10000000 - delta)
                if time.strptime(xtl).tm_year <= time.gmtime().tm_year and time.strptime(xtl).tm_year > 1970:
                    markup += " | %s" % xtl
            except: pass
            try:
                xtb = time.ctime(xt_be/10000000 - delta)
                if time.strptime(xtb).tm_year <= time.gmtime().tm_year and time.strptime(xtb).tm_year > 1970:
                    markup += " | %s" % xtb
            except: pass
    elif len(data) > 8:
        t = divmod(len(data), 3)
        q = divmod(len(data), 4)
        o = divmod(len(data), 8)
        markup = "TQO: %s %s %s" % (t[0] if not t[1] else "-", q[0] if not q[1] else "-" , o[0] if not o[1] else "-")

    # "global" search tables
    stbls = options.get("ds_tables") or []
    if len(data) in stbls:
        stl = stbls[len(data)]
        for st in stl:
            if data in stl[st]:
                markup += " # %s %s " % (st, stl[st][data])
    # "document" search tables
    stbls = options.get("ps_tables") or []
    if len(data) in stbls:
        stl = stbls[len(data)]
        for st in stl:
            if data in stl[st]:
                markup += " # %s %s " % (st, stl[st][data])

    txt = None
    if len(data) > 1:
        show_txt = options.get("txt")
        txt_enc = options.get("txt_enc") or "utf-8"
        if show_txt:
            try:
                txt = data.decode(txt_enc)
            except: pass

    return markup, txt


def indent(elem, level=0):
    i = "\n" + level * "  "
    if len(elem):
        if not elem.text or not elem.text.strip():
            elem.text = i + "  "
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
        for elem in elem:
            indent(elem, level + 1)
        if not elem.tail or not elem.tail.strip():
            elem.tail = i
    else:
        if level and (not elem.tail or not elem.tail.strip()):
            elem.tail = i
    return elem

def dib2bmp(data):
    [flag] = struct.unpack("<I", data[:4])
    if flag != 0x28:
        return
    size = len(data) + 14
    bpp = data[14]
    if bpp == 1:
        bsize = size - 0x3e
    else:
        [bsize] = struct.unpack("<I", data[0x14:0x18])
    return b"BM" + struct.pack("<I", size) + b"\x00" * 4 + struct.pack("<I", size - bsize) + data

def d2asc(data, ln=0, rch=b"\xC2\xB7"):
    asc = b""
    for i in range(len(data)):
        ch = data[i]
        if ch < 32 or ch > 126:
            asc += rch
        else:
            asc += bytes([ch])
        if ln != 0 and i > 0 and (i + 1) % ln == 0:
            asc += "\n"
    return asc.decode("utf-8")

def d2hex(data, space="", ln=0):
    s = ""
    for i in range(len(data)):
        s += "%02x%s"%(data[i], space)
        if ln != 0 and i > 0 and (i + 1) % ln == 0:
            s += "\n"
    return s

def hex2d(data):
    res = b''
    data = data.replace(" ", "")
    for i in range(len(data) // 2):
        num = int(data[i * 2:i * 2 + 2], 16)
        res += struct.pack("B", num)
    return res

def key2txt(key, data, txt="Unknown"):
    if key in data:
        return data[key]
    else:
        return txt

def bflag2txt(flag, data, txt=""):
    flags = []
    i = 1
    while flag >= i:
        if flag & i:
            flags.append(key2txt(i, data, ""))
        i <<= 1
    return txt + "/".join(flags)


def read_config(cfg, settings, basepath=None, execpath=None):
    '''
    cfg      - full path to config file
    settings - dict to update
    basepath - base path to use for relative paths, if none -- split from cfg
    execpath - path for substitution of {{EXEC}}
    '''
    if not os.path.exists(cfg):
        return False, "Config file doesn't exist"
    if not basepath:
        basepath = os.path.split(cfg)[0]
    if not execpath:
        execpath = os.path.dirname(os.path.abspath(__file__))
    exp_user = os.path.expanduser("~")
    try:
        tree = ET.parse(cfg)
        root = tree.getroot()
        for optkey, optdict in settings.items():
            option = root.find(optkey)
            if option is not None:
                try:
                    if optdict["type"] in ["txt", "font"]:
                        settings[optkey]["value"] = option.text
                    elif optdict["type"] == "bool":
                        settings[optkey]["value"] = bool(int(option.text))
                    elif optdict["type"] == "int":
                        settings[optkey]["value"] = int(option.text)
                    elif optdict["type"] == "float":
                        settings[optkey]["value"] = float(option.text)
                    elif optdict["type"] == "rgb":
                        c = Gdk.RGBA()
                        c.parse(option.text)
                        settings[optkey]["value"] = c
                    elif optdict["type"] in ["dir", "file"]:
                        opt_fname = option.text
                        opt_fname = opt_fname.replace("{{EXEC}}", execpath)
                        if opt_fname[:2] == "~/":
                            opt_fname = os.path.join(exp_user, opt_fname[2:])
                        elif opt_fname[:2] == "./":
                            opt_fname = os.path.join(basepath, opt_fname[2:])
                        if (optdict["type"] == "file" and os.path.isfile(opt_fname)) or os.path.isdir(opt_fname):
                            settings[optkey]["value"] = opt_fname
                    #elif optdict["type"][:5] == "list:":
                        # part after "list:" describes format, presumably one of int/float/dir
                        # as others do not seem to be very useful in such a context

                    # elif optdict["type"][:5] == "enum:":
                        # FIXME! text value should be one of the available options
                except:
                    debug("Failed to read config option %s" % optkey)
        return True, "Configuration file loaded."
    except Exception as e:
        return False, "Failed to parse configuration file! (%s)" % e


def write_config(cfg, settings, rootname, basepath=None, execpath=None):
    '''
    cfg      - full path to config file
    settings - dict to update
    rootname - name for the root element
    basepath - base path to use for relative paths, if none -- split from cfg
    execpath - path for substitution of {{EXEC}}
    '''
    if not basepath:
        basepath = os.path.split(cfg)[0]
    if not execpath:
        execpath = os.path.dirname(os.path.abspath(__file__))
    exp_user = os.path.expanduser("~")
    try:
        root = ET.Element(rootname)
        parents = {}
        # FIXME! Dryer needed...
        for optkey, optdict in settings.items():
            if not optdict["value"]:
                continue
            r, c = root, optkey
            if "/" in optkey:
                p, c = optkey.split("/")
                if p not in parents:
                    r = ET.SubElement(r, p)
                    parents[p] = r
                else:
                    r = parents[p]
            opt = ET.SubElement(r, c)
            if optdict["type"] in ["txt", "font"]:
                opt.text = optdict["value"]
            elif optdict["type"] == "bool":
                opt.text = "1" if optdict["value"] else "0"
            elif optdict["type"] in ["int", "float"]:
                opt.text = "%g" % optdict["value"]
            elif optdict["type"] == "rgb":
                opt.text = optdict["value"].to_string()
            elif optdict["type"] in ["dir", "file"]:
                opt_fname = optdict["value"]
                pfx_exec = os.path.commonprefix((opt_fname, execpath))
                pfx_base = os.path.commonprefix((opt_fname, basepath))
                pfx_user = os.path.commonprefix((opt_fname, exp_user))
                if pfx_exec == execpath:
                    opt_fname = "{{EXEC}}/" + os.path.relpath(opt_fname, execpath)
                elif pfx_base == basepath:
                    opt_fname = "." + os.path.relpath(opt_fname, basepath)
                elif pfx_user == exp_user:
                    opt_fname = "~/" + os.path.relpath(opt_fname, exp_user)
                opt.text = opt_fname
        indent(root)
        tree = ET.ElementTree(root)
        tree.write(cfg)
        return True, "Configuration file saved."
    except Exception as e:
        return False, "Failed to save configuration file! (%s)" % e


def int16(x):
    return int(x, 16)

def rlstr(x):
    return x

rlformats = [
    (re.compile(r"{(\d*)}"), r"([+-]?\d+)", int),
    (re.compile(r"{(\d*):.*f}"), r"([-+]?(?:\d+(?:\.\d*)?|\.\d+)(?:[eE][-+]?\d+)?)", float),
    (re.compile(r"{(\d*):(\d+)x}"), r"([-+]?(?:0[xX])?[\dA-Fa-f]{%s})", int16),
    (re.compile(r"{(\d*):x}"), r"([-+]?(?:0[xX])?[\dA-Fa-f]+)", int16),
    (re.compile(r"{(\d*):s}"), r"(\S+)", rlstr)
    ]

def unformat(reprfmt, uinput):
    '''
    'Like scanf but for str.format'.
    Incompatible with scanf.
    Cruelly tailored to limerest needs from https://github.com/joshburnett/scanf/blob/master/scanf.py.
    Likely useless outside of limerest.
    '''
    @lru_cache(maxsize=1000)
    def compile_fmt(reprfmt):
        pattern = ""
        params = []
        i = 0
        length = len(reprfmt)
        while i < length:
            found = None
            for fmt, patt, conv in rlformats:
                found = fmt.match(reprfmt, i)
                if found:
                    groups = found.groups()
                    params.append((int(groups[0]), conv))
                    if len(groups) > 1:
                        patt = patt % groups[1:]
                    pattern += patt
                    i = found.end()
                    break
            if not found:
                char = reprfmt[i]
                pattern += "\\%c" % char if char in "|^$()[]-.+*?{}<>\\" else char
                i += 1
        pattern = re.compile(re.sub(r'\s+', r'\\s+', pattern))
        return pattern, params
    pattern, params = compile_fmt(reprfmt)
    found = pattern.search(uinput)
    if found:
        groups = found.groups()
        return {params[i][0]:params[i][1](groups[i]) for i in range(len(groups))}


def rldecode(data, offset, packfmt, reprfmt, conv=None):
    '''
    data: bytes
    offset: int [0:len(data)]
    packfmt: struct-compatible format description
    reprfmt: str.format-compatible string
    conv: optional list of tuples index_of_value/string_to_eval
        in the string_to_eval 'x' will represent a value
        eg. ((0, "5*x/9 - 17.78"),) to convert first value as F to C
        converted values are added to the list of values used in formatted output
        e.g. if packfmt is "BB" and the first byte is temperature in F
        "F:{0} C:{2} {1}" would show first byte in F and C, then the first byte
        Using list of tuples instead of dict to have a predictable positions of converted values.

        Read uint32 for IPv4 and uint32 for mask, show in x.x.x.x/nn format
        rldecode(b'\x02\x01\xa8\xc0\x00\xff\xff\xff', 0, "<BBBBI", "{3}.{2}.{1}.{0}/{5}", ((4,"int(32 - math.log2(2**32 - x))"),))
        ((2, 1, 168, 192, 4294967040), '192.168.1.2/24')

    returns: tuple of unpacked values and string of conversion/formating result
    '''

    values = struct.unpack(packfmt, data[offset:offset + struct.calcsize(packfmt)])
    rvalues = [*values]
    for i, c in conv:
        x = rvalues[i]
        rvalues.append(eval(c))
    return values, str.format(reprfmt, *rvalues)


def rlencode(uinput, values, packfmt, reprfmt, unconv=None):
    '''
    uinput: string from user
    values: from rldecode
    packfmt: struct-compatible format description
    reprfmt: str.format-compatible string
    unconv: optional list of tupples of index_of_value/string_to_eval
        in the string_to_eval 'x' will represent a value
        string_to_eval is invert to that of rldecode

    returns: user input extracted with reprfmt, optionally unconverted,
        placed over rldecoded values and packed according to packfmt
    '''

    uf = unformat(reprfmt, uinput)
    shift = len(values)
    for i, (o, c) in enumerate(unconv):
        x = uf[i + shift]
        uf[o] = eval(c)
    values.update(uf)
    data = struct.pack(packfmt, *values)
    return data

def add_toc_iter(model, name, data, offset, packfmt, reprfmt, hints=None, conv=None, unconv=None, parent=None):
    # toc_tview iter
    values, rvalue = rldecode(data, offset, packfmt, reprfmt, conv)
    itr = model.append(parent,
            [name, str(rvalue),
                {"offset": offset,                  # for highlight and inject on editing
                "length": struct.calcsize(packfmt), # for highlight
                "pfmt": packfmt, "rfmt": reprfmt,   # to pack back on editing
                "conv": conv, "unconv": unconv,     # to unconv on editing
                "hints": hints}                     # color, enum etc
            ])
    return itr

# END
