#!/usr/bin/env python3

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk

import gui
import widgets
from utils import debug


@Gtk.Template(filename="ui/config.ui")
class ConfigWindow(Gtk.Window):
    __gtype_name__ = "ConfigWindow"

    cfg_tview = Gtk.Template.Child()
    tvc2 = Gtk.Template.Child()
    cr11 = Gtk.Template.Child()
    cr21 = Gtk.Template.Child()
    cr22 = Gtk.Template.Child()
    save_btn = Gtk.Template.Child()
    apply_btn = Gtk.Template.Child()

    def __init__(self, app, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.app = app
        self.set_transient_for(app.windows["main"])
        css_provider = Gtk.CssProvider()
        css_provider.load_from_data(b'#cfg_tview {border-top-color: #aaa}')
        context = Gtk.StyleContext()
        screen = Gdk.Screen.get_default()
        context.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION)
        self.connect("delete-event", self.on_destroy)
        self.cfg_tview.connect("row-activated", self.on_row_activated)
        self.cfg_tview.connect("key-press-event", gui.on_row_keypress)
        self.cr11.set_fixed_size(-1, 48)
        self.cr23 = widgets.CellRendererColorBox()
        self.cr23.set_property("column", 1)
        self.cr23.set_property("key", "value")
        self.tvc2.pack_start(self.cr23, True)
        self.tvc2.set_cell_data_func(self.cr21, self.cell_func, "text")
        self.tvc2.set_cell_data_func(self.cr22, self.cell_func, "toggle")
        self.tvc2.set_cell_data_func(self.cr23, self.cell_func, "color")
        self.cr21.connect('edited', self.on_cell_edited)
        self.cr22.connect('toggled', self.on_cell_toggled)
        self.save_btn.connect("clicked", self.on_save)
        self.apply_btn.connect("clicked", self.on_apply)
        self.read_settings()
        self.show_all()

    def on_destroy(self, *args):
        del self.app.windows["prefs"]

    def read_settings(self):
        sas = self.app.settings
        model = self.cfg_tview.get_model()
        for opt in sorted(sas):
            opt_value = sas[opt]["value"]
            opt_type = sas[opt]["type"]
            opt_name = sas[opt]["name"]
            crtype = "text"
            data = {}
            if opt_type in ["rgb", "rgba"]:
                crtype = "color"
            elif opt_type == "bool":
                crtype = "toggle"
            elif opt_type == "bool":
                data["fmt"] = "%g"
            elif opt_type == "font":
                crtype = "font"
            elif opt_type == "dir":
                crtype = "dir"

            data["crtype"] = crtype
            data["value"] = opt_value
            data["sas"] = opt
            model.append(None, [opt_name, data])

    def apply_settings(self):
        sas = self.app.settings
        model = self.cfg_tview.get_model()
        for o in model:
            data = o[1]
            if "sas" in data and data["sas"] in sas:
                sas[data["sas"]]["value"] = data["value"]

    def on_apply(self, w):
        self.apply_settings()
        win = self.app.windows["main"]
        win.apply_settings()
        for n in range(win.main_nb.get_n_pages()):
            page = win.main_nb.get_nth_page(n)
            page.hexview.apply_settings()
            page.hexview.redraw()
            bw = page.hexview.body.get_window()
            if bw:
                bw.invalidate_rect(None, True)

    def on_save(self, w):
        self.apply_settings()
        self.app.save_config()
        self.hide()

    def cell_func(self, column, cell, model, iter, *args):
        data = model.get_value(iter, 1)
        if args[0] == data["crtype"] or (args[0] == "text" and data["crtype"] in ["font", "dir"]):
            cell.set_property("visible", True)
            if args[0] == "toggle":
                cell.set_property("active", data["value"])
            elif args[0] == "color":
                cell.set_property("value", data["value"])
            else: # text, font
                cell.set_property("editable", data["crtype"] not in ["font", "dir"])
                fmt = data["fmt"] if "fmt" in data else "%s"
                cell.set_property("text", fmt % data["value"])
        else:
            cell.set_property("visible", False)

    def on_cell_edited(self, cell, path, value):
        data = self.cfg_tview.get_model()[path][1]
        if type(data["value"]) in [int, float]:
            try:
                value = eval(value)
                if ("min" in data and value < data["min"]) or ("max" in data and value > data["max"]):
                    return
            except:
                return
        self.cfg_tview.get_model()[path][1]["value"] = value

    def on_cell_toggled(self, cell, path):
        self.cfg_tview.get_model()[path][1]["value"] = not self.cfg_tview.get_model()[path][1]["value"]
        return True

    def on_row_activated(self, tv, path, tvc):
        model = tv.get_model()
        data = model[path][1]
        if data["crtype"] == "font":
            dlg = Gtk.FontChooserDialog(title="Select font", parent=self)
            dlg.set_font(data["value"])
        elif data["crtype"] in ["dir", "file"]:
            action=Gtk.FileChooserAction.OPEN
            if data["crtype"] == "dir":
                action=Gtk.FileChooserAction.SELECT_FOLDER
            dlg = Gtk.FileChooserDialog(title="Select %s" % data["crtype"], parent=self, action=action)
            dlg.add_buttons(
                Gtk.STOCK_CANCEL, Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OK, Gtk.ResponseType.OK,
            )
            dlg.set_filename(data["value"])
        else:
            return False
        res = dlg.run()
        if res and res == -5: # "Select/OK"
            if data["crtype"] == "font":
                new_value = dlg.get_font()
            else:
                new_value = dlg.get_filename()
            model[path][1]["value"] = new_value
        dlg.destroy()

# END
