# Copyright (C) 2007,2010,2011,2021,2024 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import datetime
import struct
import sys

import formats.inflate
from utils import *


chunknoshift = {
    0x15: 'Page',
    0x18: 'FontList',
    0x1a: 'StyleSheets',
    0x46: 'PageSheet',
    0x47: 'ShapeType="Group"',
    0x48: 'ShapeType="Shape"',
    0x4a: 'StyleSheet',
    0x4d: 'ShapeType="Guide"',
    0x4e: 'ShapeType="Foreign"',
    0x4f: 'DocSheet'
}

chunklist = {
    0x0d: 'OleList',
    0x2c: 'NameList',
    0x64: 'ScratchList',
    0x65: 'ShapeList',
    0x66: 'FieldList',
    0x67: 'UserDefList',
    0x68: 'PropList',
    0x69: 'CharList',
    0x6a: 'ParaList',
    0x6b: 'TabsDataList',
    0x6c: 'GeomList',
    0x6d: 'CustPropsList',
    0x6e: 'ActIdList',
    0x6f: 'LayerList',
    0x70: 'CtrlList',
    0x71: 'CPntsList',
    0x72: 'CnnectList',
    0x73: 'HypelLnkList',
    0x76: 'SmartTagList',
    0xcb: "0xCB list"
}

chunktype = {
    0x0a: 'Prompt',
    0x0c: 'FrgnData',
    0x0d: 'OLE_List',
    0x0e: 'Text IX',
    0x10: 'Data1',
    0x11: 'Data2',
    0x12: 'Data3',
    0x14: 'Trailer',
    0x15: 'Page',
    0x16: 'Colors',
    0x18: 'FontList',
    0x19: 'Font',
    0x1a: 'StyleSheets',
    0x1d: 'Masters',
    0x1e: 'Master',
    0x1f: 'OLE_Data',
    0x23: 'Icon',
    0x27: 'Pages',
    0x28: 'Shape Stencil',
    0x29: 'Windows',
    0x2a: 'Window',
    0x2c: 'NameList',
    0x2d: 'Name',
    0x2e: 'EventList',
    0x2f: 'EventItem',
    0x31: 'Document',
    0x32: 'NameList',
    0x33: 'Name',
    0x34: 'NameIDX(v123)',
    0x42: 'UniqueID',
    0x46: 'PageSheet',
    0x47: 'ShapeType="Group"',
    0x48: 'ShapeType="Shape"',
    0x4a: 'StyleSheet',
    0x4d: 'ShapeType="Guide"',
    0x4e: 'ShapeType="Foreign"',
    0x4f: 'DocSheet',
    0x64: 'ScratchList',
    0x65: 'ShapeList',
    0x66: 'FieldList',
    0x67: 'UserDefList',
    0x68: 'PropList',
    0x69: 'CharList',
    0x6a: 'ParaList',
    0x6b: 'TabsDataList',
    0x6c: 'GeomList',
    0x6d: 'CustPropsList',
    0x6e: 'ActIdList',
    0x6f: 'LayerList',
    0x70: 'CtrlList',
    0x71: 'CPntsList',
    0x72: 'CnnectList',
    0x73: 'HypelLnkList',
    0x76: 'SmartTagLst',
    0x83: 'ShapeID',
    0x84: 'Event',
    0x85: 'Line',
    0x86: 'Fill',
    0x87: 'TextBlock',
    0x88: 'Tabs Data',
    0x89: 'Geometry',
    0x8a: 'MoveTo',
    0x8b: 'LineTo',
    0x8c: 'ArcTo',
    0x8d: 'InfinLine',
    0x8f: 'Ellipse',
    0x90: 'EllpArcTo',
    0x92: 'PageProps',
    0x93: 'StyleProps',
    0x94: 'Char IX',
    0x95: 'ParaIX',
    0x96: 'Tabs Data',
    0x97: 'Tabs Data',
    0x98: 'FrgnType',
    0x99: 'ConnectPts',
    0x9b: 'XForm',
    0x9c: 'TxtXForm',
    0x9d: 'XForm1D',
    0x9e: 'Scratch',
    0xa0: 'Protection',
    0xa1: 'TextFields',
    0xa2: 'Control',
    0xa3: 'Help',
    0xa4: 'Misc',
    0xa5: 'SplineStart',
    0xa6: 'SplineKnot',
    0xa7: 'LayerMem',
    0xa8: 'LayerIX',
    0xa9: 'Act ID',
    0xaa: 'Control',
    0xb4: 'User-defined',
    0xb5: 'Tabs Data',
    0xb6: 'CustomProps',
    0xb7: 'RulerGrid',
    0xb9: 'ConnectionPnts',
    0xba: 'ConnectionPnts',
    0xbb: 'ConnectionPnts',
    0xbc: 'DocProps',
    0xbd: 'Image',
    0xbe: 'Group',
    0xbf: 'Layout',
    0xc0: 'PageLayout',
    0xc1: 'PolylineTo',
    0xc3: 'NURBSTo',
    0xc4: 'Hyperlink',
    0xc5: 'Reviewer',
    0xc6: 'Annotation',
    0xc7: 'SmartTagDef',
    0xc8: 'PrintProps',
    0xc9: 'NameIDX',
    0xd1: 'Shape Data',
    0xd7: 'FaceName',
    0xd8: 'FaceNames'
}


names75 = {
    0x00:"PinX",0x01:"PinY",0x02:"Width",0x03:"Height",
    0x04:"LocPinX",0x05:"LocPinY",0x06:"Angle",0x07:"FlipX",0x08:"FlipY",
    0x09:"ResizeMode",0x0A:"BeginX",0x0B:"BeginY",0x0C:"EndX",0x0D:"EndY",
    0x0E:"LineWeight",0x0F:"LineColor",0x10:"LinePattern",0x11:"FillForegnd",
    0x12:"FillBkgnd",0x13:"FillPattern",0x14:"TextDirection",0x15:"TextContainer",
    0x16:"TextGeometry",
    0x17:'unkn_0x17',0x18:'unkn_0x18',0x19:'unkn_0x19',0x1A:'unkn_0x1A',
    0x1B:'unkn_0x1B',0x1C:'unkn_0x1C',
    0x1D:"TxtPinX",0x1E:"TxtPinY",0x1F:"TxtWidth",0x20:"TxtHeight",
    0x21:"TxtLocPinX",0x22:"TxtLocPinY",0x23:"TxtAngle",0x24:"TxtFlipX",
    0x25:"TxtFlipY",0x26:"ImageOffsetX",0x27:"ImageOffsetY",0x28:"ImageWidth",
    0x29:"ImageHeight",
    0x2A:'unkn_0x2A',0x2B:'unkn_0x2B',0x2C:'unkn_0x2C',
    0x2D:"BeginArrow",0x2E:"EndArrow",0x2F:"EndArrowSize",0x30:"Rounding",
    0x31:"VerticalAlign",0x32:"ShdwBkgnd",0x33:"BottomMargin",0x34:"LeftMargin",
    0x35:"RightMargin",0x36:"TextMaxDepth",0x37:"LockWidth",0x38:"LockHeight",
    0x39:"LockMoveX",0x3A:"LockMoveY",0x3B:"LockAspect",0x3C:"LockDelete",
    0x3D:"LockBegin",0x3E:"LockEnd",0x3F:"LockRotate",0x40:"LockCrop",
    0x41:"LockVtxEdit",0x42:"LockTextEdit",0x43:"LockFormat",0x44:"LockGroup",
    0x45:"LockCalcWH",0x46:"LockSelect",
    0x47:'unkn_0x47',0x48:'unkn_0x48',0x49:'unkn_0x49',0x4A:'unkn_0x4A',
    0x4B:"ShdwPattern",0x4C:"Sharpen",0x4D:"EventDblClick",0x4E:"EventXFMod",
    0x4F:"EventDrop",
    0x50:'unkn_0x50',
    0x51:"DrawingScale",0x52:"PageScale",0x53:"PageWidth",0x54:"PageHeight",
    0x55:"ShdwOffsetX",0x56:"ShdwOffsetY",0x57:"NoObjHandles",
    0x58:"NonPrinting",0x59:"NoCtlHandles",0x5A:"NoAlignBox",
    0x5B:"UpdateAlignBox",0x5C:"HideText",0x5D:"DrawingSizeType",
    0x5E:"DrawingScaleType",
    0x5F:'unkn_0x5F',0x60:'unkn_0x60',0x61:'unkn_0x61',
    0x62:"LineCap",0x63:"DynFeedback",0x64:"GlueType",0x65:"WalkPreference",
    0x66:"BegTrigger",0x67:"EndTrigger",0x68:"XRulerDensity",
    0x69:"YRulerDensity",0x6A:"XRulerSpacing",0x6B:"YRulerSpacing",
    0x6C:"XRulerOrigin",0x6D:"YRulerOrigin",0x6E:"XGridDensity",
    0x6F:"YGridDensity",0x70:"XGridSpacing",0x71:"YGridSpacing",
    0x72:"XGridOrigin",0x73:"YGridOrigin",0x74:"HelpTopic",
    0x75:"Copyright",0x76:"LayerMember",0x77:"ObjType",
    0x78:'unkn_0x78',0x79:'unkn_0x79',0x7A:'unkn_0x7A',0x7B:'unkn_0x7B',
    0x7C:'unkn_0x7C',0x7D:'unkn_0x7D',0x7E:'unkn_0x7E',0x7F:'unkn_0x7F',
    0x80:'unkn_0x80',0x81:'unkn_0x81',0x82:'unkn_0x82',0x83:'unkn_0x83',
    0x84:"InhibitSnap",0x85:"NoLiveDynamics",0x86:"OutputFormat",
    0x87:"PreviewQuality",0x88:"Gamma",0x89:"Contrast",0x8A:"Brightness",
    0x8B:"TextBkgnd",0x8C:"Blur",0x8D:"Denoise",0x8E:"Transparency",
    0x8F:"CompressionLevel",0x90:"ConFixedCode",0x91:"SelectMode",
    0x92:"DisplayMode",0x93:"IsDropTarget",0x94:"IsSnapTarget",
    0x95:"IsTextEditTarget",0x96:"EventTextOverflow",0x97:"ShapeTabStop",
    0x98:"Comment",0x99:"BeginArrowSize",0x9A:"DefaultTabStop",
    0x9B:"ShdwForegnd",0x9C:"TopMargin",0x9D:"TheData",
    0x9E:"TheText",0x9F:"ShapePermeableX",0xA0:"ShapePermeableY",
    0xA1:"ShapePermeablePlace",0xA2:"ShapeFixedCode",0xA3:"ShapePlowCode",
    0xA4:"ShapeRouteStyle",0xA5:"ShapePlaceStyle",0xA6:"CompressionType",
    0xA7:"ConLineJumpCode",0xA8:"ConLineJumpStyle",0xA9:"ShapePlaceDepth",
    0xAA:"ResizePage",0xAB:"EnableGrid",0xAC:"DynamicsOff",
    0xAD:"CtrlAsInput",0xAE:"PlaceStyle",0xAF:"RouteStyle",
    0xB0:"PlaceDepth",0xB1:"PlowCode",0xB2:"LineJumpCode",
    0xB3:"LineJumpStyle",0xB4:"LineToNodeX",0xB5:"LineToNodeY",
    0xB6:"BlockSizeX",0xB7:"BlockSizeY",0xB8:"AvenueSizeX",
    0xB9:"AvenueSizeY",0xBA:"LineToLineX",0xBB:"LineToLineY",
    0xBC:"LineJumpFactorX",0xBD:"LineJumpFactorY",0xBE:"Metric",
    0xBF:"HideForApply",0xC0:"IsDropSource",0xC1:"PreviewScope",
    0xC2:"PageLineJumpDirX",0xC3:"PageLineJumpDirY",0xC4:"ConLineJumpDirX",
    0xC5:"ConLineJumpDirY",0xC6:"LockPreview",0xC7:"DontMoveChildren",
    0xC8:"LineAdjustFrom",0xC9:"LineAdjustTo",0xCA:"EnableLineProps",
    0xCB:"EnableFillProps",0xCC:"EnableTextProps",
    0xCD:'unkn_0xCD',0xCE:'unkn_0xCE',0xCF:'unkn_0xCF',0xD0:'unkn_0xD0',
    0xD1:'unkn_0xD1',0xD2:'unkn_0xD2',0xD3:'unkn_0xD3',0xD4:'unkn_0xD4',
    0xD5:'unkn_0xD5',0xD6:'unkn_0xD6',0xD7:'unkn_0xD7',0xD8:'unkn_0xD8',
    0xD9:'unkn_0xD9',0xDA:'unkn_0xDA',0xDB:'unkn_0xDB',0xDC:'unkn_0xDC',
    0xDD:'unkn_0xDD',0xDE:'unkn_0xDE',0xDF:'unkn_0xDF',0xE0:'unkn_0xE0',
    0xE1:'unkn_0xE1',0xE2:'unkn_0xE2',0xE3:'unkn_0xE3',0xE4:'unkn_0xE4',
    0xE5:'unkn_0xE5',0xE6:'unkn_0xE6',0xE7:'unkn_0xE7',0xE8:'unkn_0xE8',
    0xE9:'unkn_0xE9',0xEA:'unkn_0xEA',0xEB:'unkn_0xEB',0xEC:'unkn_0xEC',
    0xED:'unkn_0xED',0xEE:'unkn_0xEE',0xEF:'unkn_0xEF',0xF0:'unkn_0xF0',
    0xF1:'unkn_0xF1',0xF2:'unkn_0xF2',0xF3:'unkn_0xF3',0xF4:'unkn_0xF4',
    0xF5:'unkn_0xF5',0xF6:'unkn_0xF6',0xF7:'unkn_0xF7',0xF8:'unkn_0xF8',
    0xF9:'unkn_0xF9',0xFA:'unkn_0xFA',0xFB:'unkn_0xFB',0xFC:'unkn_0xFC',
    0xFD:'unkn_0xFD',0xFE:'unkn_0xFE',0xFF:'unkn_0xFF'
}

fnames7ab = {
    0x00:'_NYI', 0x01:'_ADD(num1;num2)',
    0x02:'_SUB(num1;num2)', 0x03:'UNKNOWN_3',
    0x04:'_DIV(num1;num2)', 0x05:'_UPLUS(num1)',
    0x06:'_UMINUS(num1)', 0x07:'_PCT(num1)',
    0x08:'SUM(num1;num2;...)', 0x09:'MAX(num1;num2;...)',
    0x0a:'MIN(num1; num2;...)', 0x0b:'PNT(x_num; y_num)',
    0x0c:'PNTX(num)', 0x0d:'PNTY(num)',
    0x0e:'LOC(num)', 0x0f:'ABS(num)',
    0x10:'POW(num; exp)', 0x11:'SQRT(num)',
    0x12:'ATAN2(num1;num2)', 0x13:'PI()',
    0x14:'RAD(ang)', 0x15:'DEG(num)',
    0x16:'UNKNOWN_22', 0x17:'_FLT(num1;num2)',
    0x18:'_FLE(num1;num2)', 0x19:'_FEQ(num1;num2)',
    0x1a:'_FGE(num1;num2)', 0x1b:'_FGT(num1;num2)',
    0x1c:'_FNE(num1;num2)', 0x1d:'AND()',
    0x1e:'OR()', 0x1f:'NOT(num)',
    0x20:'BITAND(num1;num2)', 0x21:'BITOR(num1;num2)',
    0x22:'BITOXR(num1;num2)', 0x23:'BITNOT(num)',
    0x24:'COS(num)', 0x25:'COSH(num)',
    0x26:'SIN(num)', 0x27:'SINH(num)',
    0x28:'TAN(angle)', 0x29:'TANH(angle)',
    0x2a:'LN(num)', 0x2b:'LOG10(num)',
    0x2c:'RAND()', 0x2d:'TEXTWIDTH(text;num)',
    0x2e:'TEXTHEIGHT(text;num)', 0x2f:'_GLUELOC(num1;num2;num3;num4)',
    0x30:'_GLUEPAR(num1;num2;num3;num4)', 0x31:'REF()',
    0x32:'_MARKER(num)', 0x33:'PAR(point)',
    0x34:'_ELLIPSE_THETA(n1;n2;n3;n4;w;h)',0x35:'_ELLIPSE_ECC(n1;n2;n3;n4;w;h;_ellipse_theta)',
    0x36:'_UMARKER(num1;num2)', 0x37:'EVALTEXT(text)',
    0x38:'_GLUELOCPCT(num1;num2;num3)', 0x39:'_GLUEPARPCT(num1;num2;num3)',
    0x3a:'DATE(year;month;day)', 0x3b:'TIME(hour;minute;second)',
    0x3c:'NOW()', 0x3d:'INT(num)',
    0x3e:'_MOD(num1;num2)', 0x3f:'ROUND(num;numdigits)',
    0x40:'TRUNC(num;numdigits)', 0x41:'GUARD(num)',
    0x42:'MAGNITUDE(constA;A;constB;B)', 0x43:'_ELT(num1;num2)',
    0x44:'_ELE(num1;num2)', 0x45:'_EEQ(num1;num2)',
    0x46:'_EGE(num1;num2)', 0x47:'_EGT(num1;num2)',
    0x48:'_ENE(num1;num2)', 0x49:'_CAT(num1;num2)',
    0x4a:'NA()', 0x4b:'DEFAULTEVENT()',
    0x4c:'OPENTEXTWIN()', 0x4d:'OPENGROUPWIN()',
    0x4e:'OPENSHEETWIN()', 0x4f:'DOOLEVERB(num)',
    0x50:'GOTOPAGE(page)', 0x51:'RUNADDON(name)',
    0x52:'HELP(topic)', 0x53:'ISERROR(cellref)',
    0x54:'ISERR(cellref)', 0x55:'ISERRNA(cellref)',
    0x56:'ISERRVALUE(cellref)', 0x57:'OPENPAGE(page)',
    0x58:'ACOS(num)', 0x59:'ASIN(num)',
    0x5a:'ATAN(num)', 0x5b:'SIGN(num; fuzz)',
    0x5c:'INTUP(num)', 0x5d:'ANG360(num)',
    0x5e:'FLOOR(num1;num2)', 0x5f:'CEILING(num1;num2)',
    0x60:'GRAVITY(num1;num2;num3)', 0x61:'RECTSECT(w;h;x;y;option)',
    0x62:'MODULUS(number; divisor)', 0x63:'LOTUSNOTES(lotusname)',
    0x64:'USERUI(state;dflt_exp;user_exp)',0x65:'_UCON_C1(num1;num2;num3;num4;num5)',
    0x66:'_UCON_C2(num1;num2)', 0x67:'_UCON_D1(num1;num2;num3;num4;num5)',
    0x68:'_UCON_D2(num1;num2)', 0x69:'_UCON_X1(num1;num2;num3;num4;num5;num6;num7;num8;num9;num10)',
    0x6a:'_UCON_X2(num1;num2;num3;num4;num5;num6;num7;num8)',
    0x6b:'_UCON_Y1(num1;num2;num3;num4;num5;num6;num7;num8;num9;num10)',
    0x6c:'_UCON_Y2(num1;num2;num3;num4;num5;num6;num7;num8)',
    0x6d:'_UCON_SIMPLE(num1;num2;num3;num4;num5;num6;num7;num8)',
    0x6e:'_UCON_BEGTYP(num1;num2;num3;num4;num5)',
    0x6f:'_UCON_ENDTYP(num1;num2;num3;num4;num5)',
    0x70:'_WALKGLUE(num1,num2,num3)', 0x71:'_SHAPEMIN(num1)',
    0x72:'_SHAPEMAX(num1)', 0x73:'_XFTRIGGER(cell)',
    0x74:'_UCON_C3(num1;num2)', 0x75:'_UCON_D3(num1;num2)',
    0x76:'_UCON_X3(num1;num2;num3;num4;num5;num6;num7;num8;num9)',
    0x77:'_UCON_Y3(num1;num2;num3;num4;num5;num6;num7;num8;num9)',
    0x78:'_UCON_GEOTYP(num1;num2)', 0x79:'RUNADDONWARGS(name;args)',
    0x7a:'DEPENDSON(num1;num2)', 0x7b:'OPENFILE()',
    0x7c:'FORMAT(num1;num2)', 0x7d:'CHAR(num)',
    0x7e:'SETF(cell;formula)', 0x7f:'LOOKUP(key;list;delim_opt)',
    0x80:'INDEX(idx;list;delim_opt;error_opt)',
    0x81:'PLAYSOUND(filename;is_alias;beep_on_fail;sync)',
    0x82:'DOCMD(cmd)', 0x83:'RGB(red;green;blue)',
    0x84:'HSL(hue;sat;lum)', 0x85:'RED(num)',
    0x86:'GREEN(num)', 0x87:'BLUE(num)',
    0x88:'HUE(num)', 0x89:'SAT(num)',
    0x8a:'LUM(num)', 0x8b:'USE(mastername;opt1;opt2;opt3)',
    0x8c:'DATEVALUE(num;locale)', 0x8d:'TIMEVALUE(time;locale_opt)',
    0x8e:'DATETIME(num;locale)', 0x8f:'HOUR(num;locale)',
    0x90:'MINUTE(datetime;locale_opt)', 0x91:'SECOND(num;locale)',
    0x92:'YEAR(datetime;locale_opt)', 0x93:'MONTH(datetime;locale_opt)',
    0x94:'DAY(num;locale)', 0x95:'WEEKDAY(datetime;locale_opt)',
    0x96:'DAYOFYEAR(num;locale)', 0x97:'CY(num1;country_code)',
    0x98:'UPPER(string)', 0x99:'LOWER(string)',
    0x9a:'FORMATEX(num1;num2;num3;num4)', 0x9b:'CALLTHIS(...)',
    0x9c:'HYPERLINK(link;sublink;info;new_win;frame)',
    0x9d:'INTERSECTX(x1;y1;ang1;x2;y2;ang2)',
    0x9e:'INTERSECTY(x1;y1;ang1;x2;y2;ang2)',
    0x9f:'POLYLINE(xType;yType;x1; y1;...)',
    0xa0:'_POLYARC', 0xa1:'NURBS(knotLast;degree;xType;yType;x1;y1;knot1;weight1;...)',
    0xa2:'LOCTOLOC(srcPoint;srcRef;dstRef)',0xa3:'LOCTOPAR(srcPoint;srcRef;dstRef)',
    0xa4:'ANGLETOLOC(srcAng;srcRef;dstRef)',0xa5:'ANGLETOPAR(srcAng,srcRef,dstRef)',
    0xa6:'DocCreation()', 0xa7:'DocLastPrint()',
    0xa8:'DocLastEdit()', 0xa9:'DocLastSave()',
    0xaa:'PageCount()', 0xab:'Creator()',
    0xac:'Description()', 0xad:'Directory()',
    0xae:'Filename()', 0xaf:'Keywords()',
    0xb0:'Subject()', 0xb1:'Title()',
    0xb2:'Manager()', 0xb3:'Company()',
    0xb4:'Category()', 0xb5:'HyperlinkBase()',
    0xb6:'BkgPageName(name)', 0xb7:'PageName(lcid)',
    0xb8:'PageNumber()', 0xb9:'Data1()',
    0xba:'Data2()', 0xbb:'Data3()',
    0xbc:'ListSep()', 0xbd:'ID()',
    0xbe:'Type()', 0xbf:'TypeDesc()',
    0xc0:'Name(lcid_opt)', 0xc1:'MasterName(lcid_opt)',
    0xc2:'FieldPicture(num)', 0xc3:'StrSame(str1;str2;ignore_case)',
    0xc4:'StrSameEx(str1;str2;localeID;flags)',
    0xc5:'ShapeText(text;flags_opt)', 0xc6:'DecimalSep()',
    0xC7:'RUNMACRO(...)', 0xC8:'FORMULAEXISTS(cel_ref)',
    0xC9:'LOCALFORMULAEXISTS(cel_ref)', 0xCA:'FIND(str,str,pos,flag)',
    0xCB:'LEFT(text,pos)', 0xCC:'LEN(text)',
    0xCD:'MID(...)', 0xCE:'REPLACE(...)',
    0xCF:'REPT(...)', 0xD0:'RIGHT(...)',
    0xD1:'TRIM(...)', 0xD2:'QUEUEMARKEREVENT(...)',
    0xD3:'BOUND', 0xD4:'SUBSTITUTE(...)',
    0xD5:'BLOB(...)', 0xD6:'UNICHAR(...)',
    0xD7:'REWIDEN(...)', 0xD8:'SETATREF(...)',
    0xD9:'SETATREFEXPR(...)', 0xDA:'SETATREFVAL(...)',
    0xDB:'THEME', 0xDC:'TINT(...)',
    0xDD:'SHADE', 0xDE:'TONE(...)',
    0xDF:'LUMDIFF', 0xE0:'SATDIFF(...)',
    0xE1:'HUEDIFF(clr1,clr2)', 0xE2:'BLEND(clr1,clr2,mix)',
    0xE3:'THEMEGUARD()', 0xE4:'CELLISTHEMED(...)',
    0xE5:'THEMERESTORE', 0xE6:'EVALLCELL(...)',
    0xE7:'ARG(...)', 0xE8:'FONTTOID(font)',
    0xE9:'SHEETREF(...)', 0xEA:'BOUNDINGBOXRECT(...)',
    0xEB:'BOUNDINGBOXDIST(...)', 0xEC:'MSOTINT(...)',
    0xED:'MSOSHADE(...)', 0xEE:'PATHLENGTH(...)',
    0xEF:'POINTALONGPATH(...)', 0xF0:'ANGELALONGPATH(...)',
    0xF1:'NEARESTPOINTONPATH(...)', 0xF2:'DISTTOPATH(ref,num1,num2)',
    0xF3:'UNKNOWN_0xf3', 0xF4:'LISTMEMBERCOUNT()',
    0xF5:'LISTORDER(...)', 0xF6:'CONTAINERCOUNT()',
    0xF7:'CONTAINERMEMBERCOUNT()', 0xF8:'UNKNOWN_0xf8',
    0xF9:'CALLOUTCOUNT(...)', 0xFA:'UNKNOWN_0xfa',
    0xFB:'HASCATEGORY(text)', 0xFC:'UNKNOWN_0xfc',
    0xFD:'IS1D()', 0xFE:'UNKNOWN_0xfe',
    0xFF:'UNKNOWN_0xff', 0x10A:'SEGMENTCOUNT(...)',
    0x10B:'PATHSEGMENT(...)', 0x10C:'VERSION()'
}

sl_opnames = {
    0x3:'+', 0x4:'-', 0x5:'*', 0x6:'/', 0x7:'^', 0x8:'&', 0x9:'<', 0xa:'<=',
    0xb:'=', 0xc:'>=', 0xd:'>', 0xe:'!=', 0x13:'neg.sign', 0x14:'%', 0xe4:'()'
}

sl_vars72 = {0:"X", 1:"Y"}

sl_objs72 = {
    1:"Sheet", 2:"Member", 3:"Char", 4:"Para", 5:"Tabs", 6:"Scratch",
    7:"Connections", 8:"TextFields", 9:"Controls", 0xF0: "Actions",
    0xF1:"Layer", 0xF2:"User", 0xF3:"Prop", 0xF4:"Hyperlink"
}

sl_logops = {0xa0:'AND()', 0xa1:'OR()', 0xa2:'IF()'}


def sl_8bytes(page, model, data, shift, offset, blk_off):
    value = struct.unpack("<d", data[offset + blk_off:offset + blk_off + 8])[0]
    add_iter(model, "\tieee754", value, shift + offset + blk_off, 8, "<d")
    return blk_off + 8


def sl_2bytes(page, model, data, shift, offset, blk_off):
    value = struct.unpack("<h", data[offset + blk_off:offset + blk_off + 2])[0]
    add_iter(model, "\tword", value, shift + offset + blk_off, 2, "<h")
    return blk_off + 2


def sl_1byte(page, model, data, shift, offset, blk_off):
    value = data[offset + blk_off]
    add_iter(model, "\tbyte", value, shift + offset + blk_off, 1, "<b")
    return blk_off + 1


def sl_funcs7b(page, model, data, shift, offset, blk_off):
    nargs = struct.unpack("<I", data[offset +blk_off:offset + blk_off + 4])[0]
    fid = struct.unpack("<h", data[offset + blk_off + 4:offset+blk_off + 6])[0]
    nm_str = "# of args: %i " % nargs
    if fid in fnames7ab:
        nm_str += "(" + fnames7ab[fid] + ")" 
    else:
        nm_str += "%02x" % fid
    add_iter(model, "\tfunc7b", nm_str, shift + offset + blk_off, 8, "<d")    
    return blk_off + 8


def sl_funcs7a(page, model, data, shift, offset, blk_off):
    if page.doc_data["version"] > 5:
        fid = struct.unpack("<h", data[offset + blk_off:offset + blk_off + 2])[0]
    else:
        fid = data[offset + blk_off]
    if fid in fnames7ab:
        nm_str = fnames7ab[fid]
    else:
        nm_str = "%02x" % fid
    add_iter(model, "\tfunc7a", nm_str, shift + offset + blk_off, 4, "<I")
    if page.doc_data["version"] > 5:
        return blk_off + 4
    else:
        return blk_off + 2


def sl_str(page, model, data, shift, offset, blk_off):
    slen = data[offset + blk_off]
    if page.doc_data["version"] == 11:
        txt = data[offset + blk_off + 1:offset + blk_off + 3 + slen * 2].decode("utf-16")
        tlen = slen * 2 + 3
    else:
        txt = data[offset + blk_off + 1:offset + blk_off + 2 + slen]
        tlen = slen + 2
    add_iter(model, "\tstring", txt, shift + offset + blk_off, tlen, "txt")
    return blk_off + tlen


def sl_ops(page, model, data, shift, offset, blk_off):
    op = data[offset + blk_off]
    if op in sl_opnames:
        op_txt = sl_opnames[op]
    else:
        op_txt = "%02x" % op
    add_iter(model, "\tops", op_txt, shift + offset + blk_off, 1, "<b")
    return blk_off + 1


def sl_names70(page, model, data, shift, offset, blk_off):
    length = 11
    v1,v2,v3,v4 = 0,0,"","" # TypeError: %d format: a real number is required, not str
    if page.doc_data["version"] < 6: # TODO Add unpacking for later versions
        length = 7
        v1 = struct.unpack("<H", data[offset + blk_off:offset + blk_off + 2])[0]
        v2 = struct.unpack("<H", data[offset + blk_off + 2:offset + blk_off + 4])[0]
        try:
            v3 = sl_objs72[data[offset + blk_off + 4]]
        except:
            v3 = "unkn_0x%d" % data[offset + blk_off + 4]
        try:
            v4 = sl_vars72[struct.unpack("<H", data[offset + blk_off + 5:offset + blk_off + 7])[0]]
        except:
            v4 = "unkn_0x%d" % (struct.unpack("<H", data[offset + blk_off + 5:offset + blk_off + 7])[0])
    add_iter(model, "\tnames70 [%d %d : %s: %s]" % (v1, v2, v3, v4), "", shift + offset + blk_off, length, "txt")
    return blk_off + length


def sl_names72(page, model, data, shift, offset, blk_off):
    idx = struct.unpack("<I", data[offset + blk_off:offset + blk_off + 4])[0]
    obj = data[offset + blk_off + 4]
    var = data[offset + blk_off + 5]
    if obj > 9 and obj < 0xf0:
        obj_name = "Geometry%d" % (obj - 9)
    elif obj in sl_objs72:
        obj_name = sl_objs72[obj]
    else:
        obj_name = "Obj%02x" % obj
    if var in sl_vars72:
        var_name = sl_vars72[var]
    else:
        var_name = "Var%02x_" % var
    add_iter(model, "\tnames72", obj_name + "." + var_name + "%d" % idx, shift + offset + blk_off, 7, "txt")
    return blk_off + 7


def sl_names74(page, model, data, shift, offset, blk_off):
    var = struct.unpack("<h", data[offset + blk_off:offset + blk_off + 2])[0]
    idx = struct.unpack("<I", data[offset + blk_off + 4:offset + blk_off + 8])[0]
    if var in names75:
        var_name = names75[var]
    else:
        var_name = "Var%02x" % var
    add_iter(model, "\tnames74", "Sheet.%d!" % idx + var_name, shift + offset + blk_off, 8, "txt")
    return blk_off + 8


def sl_names75(page, model, data, shift, offset, blk_off):
    value = data[offset + blk_off]
    nm_str = "%02x" % value
    if value in names75:
        nm_str = names75[value]
    l, t = 4, "<I"
    if page.doc_data["version"] < 6:
        l, t = 2, "<H"
    add_iter(model, "\tnames75", nm_str, shift + offset + blk_off, l, t)
    return blk_off + l


def sl_names76(page, model, data, shift, offset, blk_off):
    # FIXME, just skipping at the moment
    add_iter(model, "\tnames76", "", shift + offset + blk_off, 17, "txt")
    return blk_off + 17


def sl_logfunc(page, model, data, shift, offset, blk_off):
    # FIXME, just skipping at the moment
    op = data[offset + blk_off]
    if op in sl_logops:
        op_txt = sl_logops[op]
    else:
        op_txt = "%02x" % op
    length = struct.unpack("<I", data[offset + blk_off + 1:offset + blk_off + 5])[0]
    add_iter(model, "\tlog.func", op_txt, shift + offset + blk_off + 1, length - 1, "txt")
    return blk_off + length


def sl_nurbs(page, model, data, shift, offset, blk_off):
    # the same as "NURBS Data (type 0x82)"
    add_iter(model, "\tknotLast", "%.2f" % struct.unpack("<d", data[offset + blk_off:offset + blk_off + 8]), shift + offset + blk_off, 8, "<d")
    add_iter(model, "\tdegree", "%.2f" % struct.unpack("<h", data[offset + blk_off + 8:offset + blk_off + 10]), shift + offset + blk_off, 2, "<h")

    xType = data[offset + blk_off + 10]
    yType = data[offset + blk_off + 11]
    add_iter(model, "\txType", xType, shift + offset + blk_off + 10, 1, "<b")
    add_iter(model, "\tyType", yType, shift + offset + blk_off + 11, 1, "<b")
    
    [num_pts] = struct.unpack("<I", data[offset + blk_off + 12:offset + blk_off + 16])
    hd.model.set (iter1, 0, "\t# of pts", 1, num_pts,2,shift+offset+blk_off+12,3,4,4,"<I")
    add_iter(model, "\t# of pts", num_pts, shift + offset + blk_off + 12, 4, "<I")
    
    for i in range(num_pts):
        add_iter(model, "--- x%d" % (i + 1), "%.2f" % struct.unpack("<d", data[offset + blk_off + 16 + i * 32:offset + blk_off + 24 + i * 32]), shift + offset + blk_off + 16 + i * 32, 8, "<d")
        add_iter(model, "\t y%d" % (i + 1), "%.2f" % struct.unpack("<d", data[offset + blk_off + 24 + i * 32:offset + blk_off + 32 + i * 32]), shift + offset + blk_off + 24 + i * 32, 8, "<d")
        add_iter(model, "\t knot%d" % (i + 1), "%.2f" % struct.unpack("<d", data[offset + blk_off + 32 + i * 32:offset + blk_off + 40 + i * 32]), shift + offset + blk_off + 32 + i * 32, 8, "<d")
        add_iter(model, "\t weight%d" % (i + 1), "%.2f" % struct.unpack("<d", data[offset + blk_off + 40 + i * 32:offset + blk_off + 48 + i * 32]), shift + offset + blk_off + 40 + i * 32, 8, "<d")

    return blk_off + 40 + num_pts * 32


def get_slice (page, model, data, shift, offset, blk_off):
    blk_func = data[offset + blk_off]
    if blk_func < 0x20 or blk_func == 0xe4:
        blk_off = sl_ops(page, model, data, shift, offset, blk_off)
    elif blk_func > 0x19 and blk_func < 0x60:
        blk_off = sl_8bytes(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x60:
        blk_off = sl_str(page, model, data, shift, offset, blk_off + 1)  #may require 'version' to check for 6 vs 11
    elif blk_func == 0x61:
        blk_off = sl_1byte(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x62:
        blk_off = sl_2bytes(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x70:
        blk_off = sl_names70(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x72:
        blk_off = sl_names72(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x74:
        blk_off = sl_names74(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x75:
        blk_off = sl_names75(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x76:
        blk_off = sl_names76(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x7a or blk_func == 0x80:
        blk_off = sl_funcs7a(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x7b or blk_func == 0x81:
        blk_off = sl_funcs7b(page, model, data, shift, offset, blk_off + 1)
    elif blk_func == 0x8a:
        blk_off = sl_nurbs(page, model, data, shift, offset, blk_off + 1)
    elif blk_func > 0x9f and blk_func < 0xa4:
        blk_off = sl_logfunc(page, model, data, shift, offset, blk_off)
    else:
        blk_off += 1 # bad idea, but just to skip unknowns
    return blk_off


def parse_block(page, model, value, shift):
    # offset -- where we are in the buffer
    # shift -- what to add to hd iter
    # blk_off -- offset inside the block
    offset = 0
    blk_id = 0
    data = value[shift:]
    while offset < len(data):
        blk_len = struct.unpack("<I", data[offset:offset + 4])[0]
        if blk_len == 0:
            blk_len = 4  # 4 bytes "trailer" at the end of chunk
        else:
            add_iter(model, "Blk #%d Length" % blk_id, blk_len, shift + offset, 4, "<I")
            blk_id += 1
            blk_off = 4
            blk_type = data[offset + blk_off]
            add_iter(model, "\tBlk Type", blk_type, shift + offset + blk_off, 1, "<b")
            blk_off = 5
            blk_idx = data[offset + blk_off] # which cell in the shapesheet this formula is for
            add_iter(model, "\tBlk IDX", blk_idx, shift + offset + blk_off, 1, "<b")
            blk_off = 6
            if blk_type == 2:
                while blk_off < blk_len:
                    blk_off = get_slice(page, model, data, shift, offset, blk_off)
        offset += blk_len

def parse5_block(page, model, value, shift):
    offset = 0
    blk_id = 0
    data = value[shift:]
    
    while offset < len(data) - 2:
        blk_len = struct.unpack("<H", data[offset:offset + 2])[0]
        if blk_len == 0:
            blk_len = 2  # 2 bytes "trailer" at the end of chunk
        else:
            add_iter(model, "Blk #%d Length" % blk_id, blk_len, shift + offset, 2, "<H")
            blk_id += 1
            blk_off = 2
            blk_type = data[offset + blk_off]
            add_iter(model, "\tBlk Type", blk_type, shift + offset + blk_off, 1, "<b")
            blk_off = 3
            blk_idx = data[offset + blk_off] # which cell in the shapesheet this formula is for
            add_iter(model, "\tBlk IDX", blk_idx, shift + offset + blk_off, 1, "<b")
            blk_off = 4
            if blk_type == 2:
                while blk_off < blk_len:
                    blk_off = get_slice(page, model, data, shift, offset, blk_off)
        offset += blk_len


class Chunk:
    def __init__(self):
        type = 0	# dword
        IX = 0		# dword
        list = 0	# dword
        length = 0	# dword
        level = 0	# word
        unkn3 = 0	# byte
        hdrlen = 0
        data = ''

    def read_hdr (self, data, offset, version):
        if version > 5:
            self.type, self.IX, self.list, self.length, self.level, self.unkn3 = struct.unpack('<LLLLHB', data[offset:offset + 19])
            self.hdrlen = 19
        else:
            self.type, self.IX, self.level, self.unkn3, self.list, self.length = struct.unpack('<HHBBHL', data[offset:offset + 12])
            self.hdrlen = 12
        if self.IX == 0xffffffff:
            self.IX = -1

    def get_size (self, data, offset, version):
        trailer = 0
        if self.list != 0 or self.type in chunklist:
            trailer = 8
        if(11 == version): #/* separators were found only in Visio2k3 atm.  trailer means that there is a separator too. */
            if self.list or (2 == self.level and 0x55 == self.unkn3) or\
                (2 == self.level and 0x54 == self.unkn3 and 0xaa == self.type) or\
                (3 == self.level and 0x50 != self.unkn3) or\
                self.type in (0x64, 0x65, 0x66, 0x69, 0x6a, 0x6b, 0x6f, 0x71, 0xa9, 0xb4, 0xb6, 0xb9, 0xc7) or\
                (0x2c == self.type and self.unkn3 in (0x50, 0x54)):
                trailer = trailer + 4
        if 11 == version and self.type in (0x1f, 0xc9, 0x2d, 0xd1):
            trailer = 0
        return self.hdrlen + trailer + self.length


class Pointer:
    def __init__(self):
        type = 0
        address = 0
        offset = 0
        length = 0
        format = 0
        shift = 0
        data = ''
        hex = ''

    def read(self, data, ptrdata, offset, version):
        if version < 6:
            self.type, self.format, self.address, self.offset, self.length = struct.unpack('<hhLLL', ptrdata[offset:offset + 16])
            self.hex = ptrdata[offset:offset + 16]
        else:
            self.type, self.address, self.offset, self.length, self.format = struct.unpack('<LLLLh', ptrdata[offset:offset + 18])
            self.hex = ptrdata[offset:offset + 18]
        if self.format & 2 == 2 : #compressed
            res = formats.inflate.inflate(self, data)
            self.shift = 4
        else:
            res = data[self.offset:self.offset + self.length]
            self.shift = 0
        self.data = res
        # print "rp: %x %x %x %x %x"%(self.type,self.address,self.offset,self.length,self.format)

    def parse_str80(self, page, data, parent):
        off = 0
        iter1 = ""
        iter2 = ""
        iter3 = ""
        while off < len(self.data) - 19:
            ch = Chunk()
            ch.read_hdr(self.data, off, page.doc_data["version"])
            size = ch.get_size(self.data, off, page.doc_data["version"])
            if ch.level == 0:
                name = key2txt(ch.type, chunktype, "%02x" % ch.type)
                iter1 = page.pgi_add(parent=parent, name= name + "\t%02x" % ch.IX, type="vsd/" + name, options={"parse": {"func": "vsdchunk", "type": name}}, data=self.data[off:off + size])
            elif ch.level == 1:
                name = key2txt(ch.type, chunktype, "%02x" % ch.type)
                iter2 = page.pgi_add(parent=iter1, name= name + "\t%02x" % ch.IX, type="vsd/" + name, options={"parse": {"func": "vsdchunk", "type": name}}, data=self.data[off:off + size])
            elif ch.level == 2:
                name = key2txt(ch.type, chunktype, "%02x" % ch.type)
                iter3 = page.pgi_add(parent=iter2, name= name + "\t%02x" % ch.IX, type="vsd/" + name, options={"parse": {"func": "vsdchunk", "type": name}}, data=self.data[off:off + size])
            elif ch.level == 3:
                name = key2txt(ch.type, chunktype, "%02x" % ch.type)
                page.pgi_add(parent=iter3, name= name + "\t%02x" % ch.IX, type="vsd/" + name, options={"parse": {"func": "vsdchunk", "type": name}}, data=self.data[off:off + size])
            off += size
            # if len(self.data) - off < 19:
                # print "Bang",ch.level,page.model.get_path(parent),page.model.get_path(iter1)

    def parse_str40(self, page, data, parent):
        off = 0
        if self.format & 2:
            off = 4
        iter1 = page.pgi_add(parent=parent, name=key2txt(self.type, chunktype, "Unkn %02x" % self.type) + " (ptr)", type="vsd/str40ptr", data=self.hex)
        #page.model.set_value(iter1, 7, "%02x" % self.format)
        if len(self.data) > 0:
            name = key2txt(self.type, chunktype, "%02x" % self.type)
            iter2 = page.pgi_add(parent=iter1, name= name + " (data)", type="vsd/" + name, options={"parse": {"func": "vsdchunk", "type": name}}, data=self.data)
        if self.format & 0x10:
            chnklen = struct.unpack("<I", self.data[off:off + 4])[0]
            listlen = struct.unpack("<I", self.data[off + chnklen - 4:off + chnklen])[0]
            name = key2txt(self.type, chunktype, "%02x" % self.type)
            page.pgi_add(parent=iter2, name= "Hdr " + name, type="vsd/" + name, options={"parse": {"func": "vsdchunk", "type": name}}, data=self.data[off:off + chnklen])
            off += chnklen
            numptr = struct.unpack("<I", self.data[off:off + 4])[0]
            off += 8
            for i in range(numptr):
                ptr = Pointer()
                ptr.read(data, self.data, off, page.doc_data["version"])
                ptr.parse(page, data, iter2)
                if page.doc_data["version"] > 5:
                    off += 18
                else:
                    off += 16

    def get_colors(self, page, data, parent):
        clrnum = data[6]
        for i in range(clrnum):
            r = data[8 + i * 4]
            g = data[9 + i * 4]
            b = data[10 + i * 4]
            a = data[11 + i * 4]
            clr = "#%02x%02x%02x" % (r, g, b)
            lum = 0.2126 * r + 0.7152 * g + 0.0722 * b
            fgclr = "black" if lum > 127 else "white"
            txt = "Color #%02x: <span background='%s' foreground='%s'>%02x%02x%02x</span> %02x" % (i, clr, fgclr, r, g, b, a)
#            model.set (iter1, 0, txt, 1, ("vsd", "clr"), 2, 4, 3, data[8 + i * 4:12 + i * 4], 5, clr, 6, model.get_string_from_iter(iter1))
            page.pgi_add(parent=parent, name=txt, type="vsd/clr", data=data[8 + i * 4:12 + i * 4])

    def parse(self, page, data, parent):
        # 0x80 -- chunk + list + collection of chunks
        # 0x40 -- one chunk
        # 0x20 -- empty?
        # 0x10 -- array of pointers
        # 0x0* -- depends on ptr type
        if self.format & 0x20 == 0x20:
            return
        elif self.type == 0x16: #colors
            name = key2txt(self.type, chunktype, "Unkn %02x" % self.type)
            iter1 = page.pgi_add(parent=parent, name=name + " (ptr)", type="vsd/" + name , data=self.data)
            self.get_colors (page, self.data, iter1)
        elif self.format&0x80 == 0x80:
            name = key2txt(self.type, chunktype, "Unkn %02x" % self.type)
            iter1 = page.pgi_add(parent=parent, name=name + " (ptr)", type="vsd/" + name , data=self.data)
            self.parse_str80(page, data, iter1)  # don't need data
        elif self.format & 0x40 == 0x40:
            self.parse_str40(page, data, parent)
        elif self.format != 0:
            # need to parse as 4,5 or 8 based on pointer.type
            if self.type in chunklist:
                self.format += 0x10
            # print '!!!',page.model.get_path(parent),chunklist[self.type],"123","%02x"%self.format,chunklist[self.type]
            self.parse_str40(page, data, parent)


def hdr(page, model, options, data):
    # display pat of header in hd_view
    add_iter(model, "Sig", data[0:0x12].decode(), 0, 0x12, "txt")
    add_iter(model, "Version", "%d" % data[0x1a], 0x1a, 1, "<B")
    add_iter(model, "Size", "0x%02x" % struct.unpack("<I", data[0x1c:0x20]), 0x1c, 4, "<I")


def ActId(page, model, options, data, off=19):
    if len(data) > off + 57:
        parse_block(page, model, data, off + 57)


def ArcTo(page, model, options, data, off=19):
    add_iter(model, "X", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "A", "%.2f" % struct.unpack("<d", data[off + 19:off + 27]), off + 19, 8, "<d")
    if len(data) > off + 29: # both 6 and 11
        parse_block(page, model, data, off + 29)


def Char(page, model, options, data, off=19):
    add_iter(model, "Num of Chars", "%d" % struct.unpack("<I", data[off:off + 4]), off, 4, "<I")
    add_iter(model, "FontID", "0x%02x" % struct.unpack("<H", data[off + 4:off + 6]), off + 4, 2, "<H")
    add_iter(model, "ColorID", "0x%02x" % data[off+6], off + 6, 1, "B")
    add_iter(model, "Color", "%02x%02x%02x" % (data[off + 7], data[off + 8], data[off + 9]), off + 7, 3, "clr")
    add_iter(model, "Transparency", "%d%%" % (data[off + 10] * 100 / 256), off + 10, 1, "B")

    flags1 = data[off + 11]
    ftxt = ""
    if flags1 & 1 == 1:
        ftxt += "bold "
    if flags1 & 2== 2:
        ftxt += "italic "
    if flags1 & 4 == 4:
        ftxt += "undrline "
    if flags1 & 8 == 8:
        ftxt += "smcaps "
    add_iter(model, "Font Mods1", ftxt, off + 11, 1, "txt")

    flags2 = data[off + 12]
    ftxt = ""
    if flags2 & 1 == 1:
        ftxt += "allcaps "
    if flags2 & 2 == 2:
        ftxt += "initcaps "
    add_iter(model, "Font Mods2", ftxt, off + 12, 1, "txt")
    
    flags3 = data[off + 13]
    ftxt = ""
    if flags3 & 1 == 1:
        ftxt += "superscript "
    if flags3 & 2 == 2:
        ftxt += "subscript "
    add_iter(model, "Font Mods3", ftxt, off + 13, 1, "txt")
    add_iter(model, "Scale", "%d%%" % (struct.unpack("<h", data[off + 14:off + 16])[0]/100.), off + 14, 2, "<h")
    add_iter(model, "FontSize", "%.2f pt" % (72 * struct.unpack("<d", data[off + 18:off + 26])[0]),off + 18, 8, "<d")

    flags4 = data[off + 26]
    ftxt = ""
    if flags4 & 1 == 1:
        ftxt += "dblunder "
    if flags4 & 2 == 2:
        ftxt += "overline "
    if flags4 & 20 == 20:
        ftxt += "dblstrike "
    add_iter(model, "Font Mods4", ftxt, off + 26, 1, "txt")
    add_iter(model, "Spacing", "%d pt" % (struct.unpack("<h", data[off + 27:off + 29])[0]/200.), off + 27, 2, "<h")
    if page.doc_data["version"] == 11:
        add_iter(model, "AsianFont", "%d" % data[off + 37],off + 37, 1, "B")
        add_iter(model, "ComplexScriptFont","%d" % data[off + 39],off + 39, 1, "B")
        add_iter(model, "LocalizeFont", "%d" % data[off + 41], off + 41, 1, "B")
        add_iter(model, "ComplexScriptSize", "%d%%" % (struct.unpack("<d", data[off + 43:off + 51])[0] * 100), off + 43, 8, "<d")
        add_iter(model, "LangID", "%d" % struct.unpack("<I", data[off + 69:off + 73]), off + 69, 4, "<I")
        if len(data) > off + 88:
            parse_block(page, model, data, off + 88)
    elif page.doc_data["version"] == 6 and len(data) > off + 35:
        parse_block(page, model, data, off + 35)


def ConnPts(page, model, options, data, off=19):
    add_iter(model, "X", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "var1", "%.2f" % struct.unpack("<d", data[off + 19:off + 27]), off + 19, 8, "<d")
    add_iter(model, "var2", "%.2f" % struct.unpack("<d", data[off + 28:off + 36]), off + 28, 8, "<d")
    if len(data) > off + 0x30: # 11
        parse_block(page, model, data, off + 0x30)


def Control(page, model, options, data, off=19):
    add_iter(model, "X", "%.3f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.3f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "XDyn", "%.3f" % struct.unpack("<d", data[off + 19:off + 27]), off + 19, 8, "<d")
    add_iter(model, "YDyn", "%.3f" % struct.unpack("<d", data[off + 28:off + 36]), off + 28, 8, "<d")
    add_iter(model, "XCon", data[off + 36], off + 36, 1, "<B")
    add_iter(model, "YCon", data[off + 37], off + 37, 1, "<B")
    add_iter(model, "CanGlue", data[off + 38], off + 38, 1, "<B")
    if len(data) > off + 47: # 11
        parse_block(page, model, data, off + 47)


def EllArcTo(page, model, options, data, off=19):
    add_iter(model, "X", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "A", "%.2f" % struct.unpack("<d", data[off + 19:off + 27]), off + 19, 8, "<d")
    add_iter(model, "B", "%.2f" % struct.unpack("<d", data[off + 28:off + 36]), off + 28, 8, "<d")
    add_iter(model, "C", "%.2f" % struct.unpack("<d", data[off + 37:off + 45]), off + 37, 8, "<d")
    add_iter(model, "D", "%.2f" % struct.unpack("<d", data[off + 46:off + 54]), off + 46, 8, "<d")
    if len(data) > off + 56: # both 6 and 11
        parse_block(page, model,  data, off + 56)


def Ellipse(page, model, options, data, off=19):
    add_iter(model, "Center X", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "Center Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "Right X", "%.2f" % struct.unpack("<d", data[off + 19:off + 27]), off + 19, 8, "<d")
    add_iter(model, "Right Y", "%.2f" % struct.unpack("<d", data[off + 28:off + 36]), off + 28, 8, "<d")
    add_iter(model, "Top X", "%.2f" % struct.unpack("<d", data[off + 37:off + 45]), off + 37, 8, "<d")
    add_iter(model, "Top Y", "%.2f" % struct.unpack("<d", data[off + 46:off + 54]), off + 46, 8, "<d")
    if len(data) > off + 56: # both 6 and 11
        parse_block(page, model, data, off + 56)


def Event(page, model, options, data, off=19):
    if len(data) > off + 17:
        parse_block(page, model, data, off + 17)


def Fill(page, model, options, data, off=19):
    add_iter(model, "FillFG", "%2x" % data[off], off, 1, "B")
    add_iter(model, "FillFGClr", "%02x%02x%02x" % (data[off + 1], data[off + 2], data[off + 3]), off + 1, 3, "clr")
    add_iter(model, "FillFGXparency", "%d %%" % (data[off+4] / 2.55), off + 4, 1, "B")
    add_iter(model, "FillBG", "%2x" % data[off + 5], off + 5, 1, "B")
    add_iter(model, "FillBGClr", "%02x%02x%02x" % ( data[off + 6], data[off + 7], data[off + 8]), off + 6, 3, "clr")
    add_iter(model, "FillBGXparency","%d %%" % (data[off + 9] / 2.55),off + 9, 1, "B")
    add_iter(model, "FillPattern", "%2x" % data[off + 10], off + 10, 1, "B")
    add_iter(model, "ShdwFG", "%2x" % data[off + 11], off + 11, 1, "B")
    add_iter(model, "ShdwFGClr", "%02x%02x%02x" % (data[off + 12], data[off + 13], data[off + 14]), off + 12, 3, "clr")
    add_iter(model, "ShdwFGXparency", "%d %%" % (data[off + 15] / 2.56), off + 15, 1, "B")
    add_iter(model, "ShdwBG", "%2x" % data[off + 16], off + 16, 1, "B")
    add_iter(model, "ShdwBGClr", "%02x%02x%02x" % (data[off + 17], data[off+18], data[off + 19]), off + 19, 3, "clr")
    add_iter(model, "ShdwBGXparency", "%d %%" % (data[off + 20] / 2.55), off + 20, 1, "B")
    add_iter(model, "ShdwPattern", "%2x" % data[off+21], off + 21, 1, "B")
    add_iter(model, "ShdwType", "%2x" % data[off + 22], off + 22, 1, "B")
    if page.doc_data["version"]  == 11:
        add_iter(model, "ShdwOffsetX", "%.2f" % struct.unpack("<d", data[off + 24:off + 32]), off + 24, 8, "<d")
        add_iter(model, "ShdwOffsetY", "%.2f" % struct.unpack("<d", data[off + 33:off + 41]), off + 33, 8, "<d")
        add_iter(model, "ShdwObliqueAngle", "%.2f" % struct.unpack("<d", data[off + 42:off + 50]), off + 42, 8, "<d")
        add_iter(model, "ShdwScaleFactor", "%.2f" % struct.unpack("<d", data[off + 50:off + 58]), off + 50, 8, "<d")
        if len(data) > off + 61:
            parse_block(page, model, data, off + 61)
    if page.doc_data["version"]  == 6 and len(data) > off + 25:
        parse_block(page, model, data, off + 25)


def Font(page, model, options, data, off=19):
    charset = data[off + 2]
    chtxt = key2txt(charset, ms_charsets, "%02x" % charset)
    add_iter(model, "Charset", chtxt, off + 2, 1, "<B")
    fontname = unicode(data[off + 6:])
    add_iter(model, "Font name", fontname, off + 6, len(fontname), "txt")


def FrgnType(page, model, options, data, off=19):
    add_iter(model, "ImgOffsetX", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "ImgOffsetY", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "ImgWidth", "%.2f" % struct.unpack("<d", data[off + 19:off + 27]), off + 19, 8, "<d")
    add_iter(model, "ImgHeight", "%.2f" % struct.unpack("<d", data[off + 28:off + 36]), off + 28, 8, "<d")
    ftype_map = {0: 'Metafile', 1: 'Bitmap', 2: 'OLE object', 4: 'Metafile'}
    ftype = struct.unpack("<h", data[off + 36:off + 38])[0]
    add_iter(model, "Type", key2txt(ftype, ftype_map), off + 36, 2, "<h")
    add_iter(model, "MapMode", "%d" % struct.unpack("<h", data[off + 38:off + 40]), off + 38, 2, "<h")
    if ftype == 1:
        fmt_map = {0: 'BMP', 1: 'JPEG', 2: 'GIF', 3: 'TIFF', 4: 'PNG'}
        fmt = struct.unpack("<I", data[off + 49:off + 53])[0]
        add_iter(model, "Bitmap format", key2txt(fmt, fmt_map), off + 49, 4, "<I")
    elif ftype == 4:
        add_iter(model, "ExtentX", "%d" % struct.unpack("<h", data[off + 40:off + 42]), off + 40, 2, "<h")
        add_iter(model, "ExtentY", "%d" % struct.unpack("<h", data[off + 42:off + 44]), off + 42, 2, "<h")
    if len(data) > off + 62:
        parse(page, model, data, off + 62)


def Geometry(page, model, options, data, off=19):
    bits = {1:'noFill',2:'noLine',4:'noShow',8:'noSnap',32:'noQuickDrag'}

    flags = data[off]
    for i in (1, 2, 4, 8, 32):
        res = 'No'
        if flags & i:
            res = 'Yes'
        add_iter(model, bits[i], res, off, 1, "txt")
    if len(data) > off + 3:
        parse_block(page, model, data, off + 3)


def InfLine(page, model, options, data, off=19):
    add_iter(model, "X", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "A", "%.2f" % struct.unpack("<d", data[off + 19:off + 27]), off + 19, 8, "<d")
    add_iter(model, "B", "%.2f" % struct.unpack("<d", data[off + 28:off + 36]), off + 28, 8, "<d")
    if len(data) > off + 38: # both 6 and 11 ???
        parse_block(page, model, data, off + 38)


def LayerIX(page, model, options, data, off=19):
    add_iter(model, "ClrID", "0x%02x" % data[off + 8], off + 8, 1, "<B")
    if data[off + 8] != 255:
        add_iter(model, "Colour", d2hex(data[off + 9:off + 12]), off + 9, 3, "clr")
    add_iter(model, "Transparency", "%d%%" % (data[off + 12] * 100 / 255), off + 12, 1, "<B")
    add_iter(model, "Visible","%d" % data[off + 14], off + 14, 1, "<B")
    add_iter(model, "Print","%d" % data[off + 15], off + 15, 1, "<B")
    add_iter(model, "Active","%d" % data[off + 16], off + 16, 1, "<B")
    add_iter(model, "Lock","%d" % data[off + 17], off + 17, 1, "<B")
    add_iter(model, "Snap","%d" % data[off + 18], off + 18, 1, "<B")
    add_iter(model, "Glue","%d" % data[off + 19], off + 19, 1, "<B")
    if len(data) > off + 33: # both 6 and 11
        parse_block(page, model, data, off + 33)


def Line(page, model, options, data, off=19):
    linecaps = {0:"Round (SVG: Round)", 1:"Square (SVG: Butt)", 2:"Extended (SVG: Square)"}
    
    add_iter(model, "Weight", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "LineClrID", "%2x" % data[off + 9], off + 9, 1, "B")
    add_iter(model, "LineClr", "%02x%02x%02x" % (data[off + 10], data[off + 11], data[off + 12]), off + 10, 3, "clr")
    add_iter(model, "LineXparency", "%d %%" % (data[off + 13] / 2.55), off + 13, 1, "B")
    add_iter(model, "LinePatternID", "%2x" % data[off + 14], off + 14, 1, "B")
    add_iter(model, "Rounding", "%.2f" % struct.unpack("<d", data[off + 16:off + 24]), off + 16, 8, "<d")
    add_iter(model, "EndArrSize", "%2x" % data[off + 24], off + 24, 1, "B")
    add_iter(model, "BeginArrow", "%2x" % data[off + 25], off + 25, 1, "B")
    add_iter(model, "EndArrow", "%2x" % data[off + 26], off + 26, 1, "B")
    lc = data[off + 27]
    lc_txt = "%2x " % lc
    if lc in linecaps:
        lc_txt += linecaps[lc]
    add_iter(model, "LineCap", lc_txt, off + 27, 1, "txt")
    add_iter(model, "BeginArrSize", "%2x" % data[off + 28], off + 28, 1, "B")
    if len(data) > off + 35: # both 6 and 11
        parse_block(page, model, data, off + 35)


def Misc(page, model, options, data, off=19):
    '''
    "IsDropSource"
    "Comment" -- in blocks only?
    "LocalizeMerge"
    '''
    bits0x3c = {1: "NoObjHandles",
            2: "NonPrinting",
            4: "NoCtrlHandles",
            8: "NoAlignBox",
            0x10: "UpdateAlignBox",
            0x20: "HideText",
            }
    bits0x3d = {1: "DynFeedback"}
    bits0x3e = {4: "NoLiveDynamics"}

    #0x19 - ObjType
    add_iter(model, "Calendar", "%d" % data[off + 17], off + 17, 1, "B")
    add_iter(model, "LangID", "%d" % struct.unpack("<H", data[off + 18:off + 20]), off + 18, 2, "<H")
    # FIXME It is often failing with error: "struct.error: unpack requires a buffer of 8 bytes"
    add_iter(model, "DropOnPageScale", "%.2f%%" % (100 * struct.unpack("<d", data[off + 24:off + 32])[0]), off + 24, 8, "<d")
    
    if len(data) > off + 45: # 11, probably v6 too
        parse_block(page, model, data, off + 45)

def MoveTo(page, model, options, data, off=19):
    add_iter(model, "X", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    if len(data) > off + 20: # both 6 and 11
        parse_block(page, model, data, off + 20)


def NURBS(page, model, options, data, off=19):
    add_iter(model, "X", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "Knot", "%.2f" % struct.unpack("<d", data[off + 18:off + 26]), off + 18, 8, "<d")
    add_iter(model, "Weight", "%.2f" % struct.unpack("<d", data[off + 26:off + 34]), off + 26, 8, "<d")
    add_iter(model, "KnotPrev", "%.2f" % struct.unpack("<d", data[off + 34:off + 42]), off + 34, 8, "<d")
    add_iter(model, "WeightPrev", "%.2f" % struct.unpack("<d", data[off + 42:off + 50]), off + 42, 8, "<d")
    sdflag =  data[off + 51:off + 55]
    sdtext = 'No'
    if sdflag == '\x8a\x02\x00\x00':
        sdtext = 'Yes'
    add_iter(model, "Use Shape Data", sdtext, off + 51, 4, "<d")
    add_iter(model, "ShapeData Id", "%02x" % struct.unpack("<I", data[off + 55:off + 59]), off + 55, 4, "<I")
    if len(data) > off + 61:
        parse_block(page, model, data, off + 61)


def NameID(page, model, options, data, off=19):
    numofrec = struct.unpack("<I", data[off:off + 4])[0]
    add_iter(model, "#ofRecords", "%2x" % numofrec, off, 4, "<I")
    for i in range(numofrec):
        n1 = struct.unpack("<I", data[off + 4 + i * 13:off + 8 + i * 13])[0]
        n2 = struct.unpack("<I", data[off + 8 + i * 13:off + 12 + i * 13])[0]
        n3 = struct.unpack("<I", data[off + 12 + i * 13:off + 16 + i * 13])[0]
        flag = data[off + 16 + i * 13]
        add_iter(model, "Rec #%d" % i, "%2x %2x %2x %2x" % (n1, n2, n3, flag), off + 4 + i * 13, 13, "txt")


def Page(page, model, options, data, off=19):
    #List (model,  size,  data, off)
    add_iter(model, "BG Page", "%x" % struct.unpack("<I", data[off + 8:off + 12])[0], off + 8, 4, "<I")
    add_iter(model, "ViewScale?", struct.unpack("<d", data[off + 26:off + 34])[0],off + 26, 8, "<d")
    add_iter(model, "ViewCntrX", struct.unpack("<d", data[off + 34:off + 42])[0],off + 34, 8, "<d")
    add_iter(model, "ViewCntrY", struct.unpack("<d", data[off + 42:off + 50])[0],off + 42, 8, "<d")


def PageLayout(page, model, options, data, off=19):
    add_iter(model, "LineToNodeX", "%.3f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "LineToNodeY", "%.3f" % struct.unpack("<d", data[off + 19:off + 27]), off + 19, 8, "<d")
    add_iter(model, "BlockSizeX", "%.3f" % struct.unpack("<d", data[off + 28:off + 36]), off + 28, 8, "<d")
    add_iter(model, "BlockSizeY", "%.3f" % struct.unpack("<d", data[off + 37:off + 45]), off + 37, 8, "<d")
    add_iter(model, "AvenueSizeX", "%.3f" % struct.unpack("<d", data[off + 46:off + 54]), off + 46, 8, "<d")
    add_iter(model, "AvenueSizeY", "%.3f" % struct.unpack("<d", data[off + 55:off + 63]), off + 55, 8, "<d")
    add_iter(model, "LineToLineX", "%.3f" % struct.unpack("<d", data[off + 64:off + 72]), off + 64, 8, "<d")
    add_iter(model, "LineToLineY", "%.3f" % struct.unpack("<d", data[off + 73:off + 81]), off + 73, 8, "<d")
    add_iter(model, "LineJumpFactorX", "%.3f" % struct.unpack("<d", data[off + 81:off + 89]), off + 81, 8, "<d")
    add_iter(model, "LineJumpFactorY", "%.3f" % struct.unpack("<d", data[off + 89:off + 97]), off + 89, 8, "<d")


def PageProps(page, model, options, data, off=19):
    add_iter(model, "PageWidth", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "PageHeight", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    add_iter(model, "ShdwOffsetX", "%.2f" % struct.unpack("<d", data[off + 19:off + 27]), off + 19, 8, "<d")
    add_iter(model, "ShdwOffsetY", "%.2f" % struct.unpack("<d", data[off + 28:off + 36]), off + 28, 8, "<d")
    add_iter(model, "PageScale", "%.2f" % struct.unpack("<d", data[off + 37:off + 45]), off + 37, 8, "<d")
    add_iter(model, "DrawingScale", "%.2f" % struct.unpack("<d", data[off + 46:off + 54]), off + 46, 8, "<d")
    add_iter(model, "DrawingSizeType", "%2x" % data[off + 54], off + 54, 1, "B")
    add_iter(model, "DrawingScaleType", "%2x" % data[off + 55], off + 55, 1, "B")
    add_iter(model, "ShdwObliqueAngle", "%.2f" % struct.unpack("<d", data[off + 78:off + 86]), off + 78, 8, "<d")
    add_iter(model, "ShdwScaleFactor", "%.2f" % struct.unpack("<d", data[off + 86:off + 94]), off + 86, 8, "<d")
    if len(data) > off + 136:
        parse_block(page, model, data, off + 136)


def Para(page, model, options, data, off=19):
    add_iter(model, "Num of Chars", "%d" % struct.unpack("<I", data[off:off + 4]), off, 4, "<I")
    add_iter(model, "IndFirst", "%.2f" % struct.unpack("<d", data[off + 5:off + 13]), off + 5, 8, "<d")
    add_iter(model, "IndLeft", "%.2f" % struct.unpack("<d", data[off + 14:off + 22]), off + 14, 8, "<d")
    add_iter(model, "IndRight", "%.2f" % struct.unpack("<d", data[off + 23:off + 31]), off + 23, 8, "<d")
    add_iter(model, "SpLine", "%d%%" % (struct.unpack("<d", data[off + 32:off + 40])[0] * 100), off + 32, 8, "<d")
    add_iter(model, "SpBefore", "%d pt" % round(struct.unpack("<d", data[off + 41:off + 49])[0] * 72), off + 41, 8, "<d")
    add_iter(model, "SpAfter", "%d pt" % round(struct.unpack("<d", data[off + 50:off + 58])[0] * 72), off + 50, 8, "<d")
    add_iter(model, "HAlign", "%d" % data[off + 58], off + 58,1,"B")
    add_iter(model, "Bullet", "%d" % data[off + 59], off + 59,1,"B")
    if page.doc_data["version"] == 11:
        add_iter(model, "BulletFont", "%d" % struct.unpack("<H", data[off + 64:off + 66]), off + 64, 2, "<H")
        add_iter(model, "LocBulletFont", "%d" % data[off + 66], off + 66, 1, "B")
        add_iter(model, "BulletSize", "%d%%" % (struct.unpack("<d", data[off + 68:off + 76])[0] * 100), off + 68, 8, "<d")
        add_iter(model, "TxtPosAfterBullet", "%.2f" % struct.unpack("<d", data[off + 77:off + 85]), off + 77, 8, "<d")
        add_iter(model, "Flags", "%d" % struct.unpack("<I", data[off + 85:off + 89]), off + 85, 4, "<I")
        if len(data) > off + 123:
            parse_block(page, model, data, off + 123)
    else:
        add_iter(model, "Flags","%d" % struct.unpack("<I", data[off + 64:off + 68]),off + 64, 4, "<I")
        if page.doc_data["version"] == 6 and len(data) > off + 73:
            parse_block(page, model, data, off + 73)


def Polyline(page, model, options, data, off=19):
    add_iter(model, "X", "%.2f" % struct.unpack("<d", data[off + 1:off + 9]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 18]), off + 10, 8, "<d")
    sdflag =  data[off + 19:off + 23]
    sdtext = 'No'
    if sdflag == '\x8b\x02\x00\x00':
        sdtext = 'Yes'
    add_iter(model, "Use Shape Data", sdtext, off + 19, 4, "<I")
    add_iter(model, "ShapeData Id", "%02x" % struct.unpack("<I", data[off + 23:off + 27]), off + 23, 4, "<I")
    if len(data) > off + 29:
        parse_block(page, model, data, off + 29)

def PrintProps(page, model, options, data, off=19):
    '''
    0x14 <d PageLeftMargin
    0x1d <d PageRightMargin
    0x26 <d PageTopMargin
    0x2f <d PageBottomMargin
    0x37 <d ScaleX
    0x3f <d ScaleY
    0x47 <H ? PagesX
    0x49 <H ? PagesY
    0x4b -- flags for CenterX/Y, OnPage, PrintGrid
    0x4d <H ? PaperKind
    0x4f ? PaperSource

    '''
    if len(data) > off + 72:
        parse_block(page, model, data, off + 72)


def Shape(page, model, options, data, off=19):
    # List (model,  size,  data, off)
    add_iter(model, "Parent", "%2x" % struct.unpack("<I", data[off + 10:off + 10 + 4])[0], off + 10, 4, "<I")
    add_iter(model, "Master", "%2x" % struct.unpack("<I", data[off + 18:off + 18 + 4])[0], off + 18, 4, "<I")
    add_iter(model, "MasterShape", "%2x" % struct.unpack("<I", data[off + 26:off + 26 + 4])[0], off + 26, 4, "<I")
    add_iter(model, "FillStyle", "%2x" % struct.unpack("<I", data[off + 34:off + 34 + 4])[0], off + 34, 4, "<I")
    add_iter(model, "LineStyle", "%2x" % struct.unpack("<I", data[off + 42:off + 42 + 4])[0], off + 42, 4, "<I")
    add_iter(model, "TextStyle", "%2x" % struct.unpack("<I", data[off + 50:off + 50 + 4])[0], off + 50, 4, "<I")


def ShapeData(page, model, options, data, off=19):
    nd_types = {0x80:"Polyline", 0x82:"NURBS"}

    nd_type = data[off]
    nd_str = "%02x "%nd_type
    if nd_type in nd_types:
        nd_str += "(" + nd_types[nd_type] + ")"
    add_iter(model, "Type", nd_str, off, 1, "b")
    if nd_type == 0x80:
        xType = data[off + 16]
        yType = data[off + 17]
        add_iter(model, "xType", xType, off + 16, 1, "b")
        add_iter(model, "yType", yType, off + 17, 1, "b")
        num_pts = struct.unpack("<I", data[off + 18:off + 18 + 4])[0]
        add_iter(model, "# of pts", num_pts, off + 18, 4, "<I")
        for i in range(num_pts):
            add_iter(model, "x%d"%(i + 1), "%.2f" % struct.unpack("<d", data[off + 22 + i * 16:off + 22 + 8+i*16]), off + 22 + i * 16, 8, "<d")
            add_iter(model, "y%d"%(i + 1), "%.2f" % struct.unpack("<d", data[off + 30 + i * 16:off + 30 + 8+i*16]), off + 30 + i * 16, 8, "<d")
    if nd_type == 0x82:
        add_iter(model, "knotLast", "%.2f" % struct.unpack("<d", data[off + 16:off + 16 + 8]), off + 16, 8, "<d")
        add_iter(model, "degree", "%.2f" % struct.unpack("<h", data[off + 24:off + 24 + 2]), off + 24, 2, "<h")
        xType = data[off + 26]
        yType = data[off + 27]
        add_iter(model, "xType", xType, off + 26, 1, "b")
        add_iter(model, "yType", yType, off + 27, 1, "b")
        num_pts = struct.unpack("<I", data[off + 28:off + 28 + 4])[0]
        add_iter(model, "# of pts", num_pts, off + 28, 4, "<I")
        for i in range(num_pts):
            add_iter(model, "x%d"%(i + 1), "%.2f" % struct.unpack("<d", data[off + 32 + i * 32:off + 32 + 8 + i * 32]), off + 32 + i * 32, 8, "<d")
            add_iter(model, "y%d"%(i + 1), "%.2f" % struct.unpack("<d", data[off + 40 + i * 32:off + 40 + 8 + i * 32]), off + 40 + i * 32, 8, "<d")
            add_iter(model, "knot%d"%(i + 1), "%.2f" % struct.unpack("<d", data[off + 48 + i * 32:off + 48 + 8 + i * 32]), off + 48 + i * 32, 8, "<d")
            add_iter(model, "weight%d"%(i + 1), "%.2f" % struct.unpack("<d", data[off + 56 + i * 32:off + 56 + 8 + i * 32]), off + 56 + i * 32, 8, "<d")


def ShapeStencil(page, model, options, data, off=19):
    add_iter(model, "ID?", "%02x" % struct.unpack("<I", data[off:off + 4]), off, 4, "<I")
    add_iter(model, "var1?", "%.2f" % struct.unpack("<d", data[off + 4:off + 4+8]), off + 4, 8, "<d")
    add_iter(model, "var2?", "%.2f" % struct.unpack("<d", data[off + 12:off + 12 + 8]), off + 12, 8, "<d")
    add_iter(model, "var3?", "%.2f" % struct.unpack("<d", data[off + 20:off + 20 + 8]), off + 20, 8, "<d")
    add_iter(model, "var4?", "%.2f" % struct.unpack("<d", data[off + 28:off + 28 + 8]), off + 28, 8, "<d")


def SplineKnot(page, model, options, data, off=19):
    add_iter(model, "X", "%.2f" % struct.unpack("<d", data[off + 1:off + 1+8]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 10 + 8]), off + 10, 8, "<d")
    add_iter(model, "Knot", "%.2f" % struct.unpack("<d", data[off + 18:off + 18 + 8]), off + 18, 8, "<d")
    if len(data) > off + 28:
        parse_block(page, model, data, off + 28)


def SplineStart(page, model, options, data, off=19):
    add_iter(model, "X", "%.2f" % struct.unpack("<d", data[off + 1:off + 1+8]), off + 1, 8, "<d")
    add_iter(model, "Y", "%.2f" % struct.unpack("<d", data[off + 10:off + 10 + 8]), off + 10, 8, "<d")
    add_iter(model, "Knot", "%.2f" % struct.unpack("<d", data[off + 18:off + 18 + 8]), off + 18, 8, "<d")
    add_iter(model, "Knot2", "%.2f" % struct.unpack("<d", data[off + 26:off + 26 + 8]), off + 26, 8, "<d")
    add_iter(model, "Knot3", "%.2f" % struct.unpack("<d", data[off + 34:off + 34 + 8]), off + 34, 8, "<d")
    add_iter(model, "Degree", "%d" % struct.unpack("<h", data[off + 42:off + 42 + 2]), off + 42, 2, "<h")
    if len(data) > off + 46:
        parse_block(page, model, data, off + 46)


def StyleProps(page, model, options, data, off=19):
    add_iter(model, "Use Line", data[off], off, 1, "<B")
    add_iter(model, "Use Fill", data[off + 1], off + 1, 1, "<B")
    add_iter(model, "Use Text", data[off + 2], off + 2, 1, "<B")
    add_iter(model, "Hidden", data[off + 3], off + 3, 1, "<B")
    if len(data) > off + 7:
        parse_block(page, model, data, off + 7)


def Text(page, model, options, data, off=19):
    # no support for LangID for v.6
    if page.doc_data["version"] == 11:
        txt = data[off + 8:].decode('utf-16').encode('utf-8')
        fmt = "utxt"
        add_iter(model, "Text", txt, off + 8, len(txt) - 8, fmt)
    else:
        # Avoid fail to set text from markup due to error parsing markup by '&'
        txt =  data[0x1b:].replace(b'&',b'&amp;')
        fmt = "txt"
        for index, line in enumerate(txt.split(b'\n')):
            fixed_line = line.replace(b'&',b'&amp;')
            add_iter(model, "Text{0}".format(index), fixed_line, 0x1b, len(data[0x1b:]) - 0x1b, fmt)


def TextBlock(page, model, options, data, off=19):
    add_iter(model, "LeftMargin", "%.2f" % round(struct.unpack("<d", data[off + 1:off + 9])[0] * 72), off + 1, 8, "<d")
    add_iter(model, "RightMargin", "%.2f" % round(struct.unpack("<d", data[off + 10:off + 18])[0] * 72), off + 10, 8, "<d")
    add_iter(model, "TopMargin", "%.2f" % round(struct.unpack("<d", data[off + 19:off + 27])[0] * 72), off + 19, 8, "<d")
    add_iter(model, "BottomMargin", "%.2f" % round(struct.unpack("<d", data[off + 28:off + 36])[0] * 72), off + 28, 8, "<d")
    add_iter(model, "VAlign", "%d" % data[off + 36], off + 36, 1, "B")
    add_iter(model, "TxtBG CLR Id", "%d" % data[off + 37], off + 37, 1, "B")
    add_iter(model, "TxtBG Trans", "%d" % round(data[off + 41] * 100 / 256.), off + 41, 1, "B")
    add_iter(model, "DefTabStop", "%.2f" % struct.unpack("<d", data[off + 43:off + 43 + 8]), off + 43, 8, "<d")
    add_iter(model, "TxtDirection", "%d" % data[off + 63], off + 63, 1, "B")
    if len(data) > off + 92 and page.doc_data["version"] == 11:
        parse_block(page, model, data, off + 92)
    elif page.doc_data["version"] == 6 and len(data) > off + 71:
        parse_block(page, model, data, off + 71)


def TextFields(page, model, options, data, off=19):
    fmt = data[off + 7]
    add_iter(model, "Celltype", "%d" % fmt, off + 7, 1, "B")
    tdiff = struct.unpack("<d", data[off + 8:off + 16])[0]
    dlen = 8
    dfmt = "<d"
    if fmt == 0x28:
        dt = datetime.datetime(1899, 12, 30) + datetime.timedelta(tdiff)
        dname = "Date"
    elif fmt == 0xe8:
        dt = struct.unpack("<I", data[off + 8:off + 12])[0]
        dname = "Name ID"
        dlen = 4
        dfmt = "<I"
    else:
        dt = "%.2f" % tdiff
        dname = " data"
    fmtidtype = data[off + 17]
    if fmtidtype == 0xe8:
        fmtid = struct.unpack("<I", data[off + 18:off + 18 + 4])[0]
        fmtname = "Format ID"
        fmtlen = 4
        fmtfmt = "<I"
    add_iter(model, dname, dt, off + 8, dlen, dfmt)
    add_iter(model, fmtname, fmtid, off + 18, fmtlen, fmtfmt)
    dtype = struct.unpack("<H", data[off + 26:off + 26 + 2])[0]
    add_iter(model, "Type", dtype, off + 26, 2, "<H")
    add_iter(model, "UICat", "0x%02x" % data[off + 28], off + 28, 1, "B")
    add_iter(model, "UICod", "0x%02x" % data[off + 29], off + 29, 1, "B")
    add_iter(model, "UIFmt", "0x%02x" % data[off + 30], off + 30, 1, "B")
    if len(data) > off + 54 and page.doc_data["version"] == 11:
        parse_block(page, model, data, off + 54)
    elif page.doc_data["version"] == 6 and len(data) > off + 36:
        parse_block(page, model, data, off + 36)


def TxtXForm(page, model, options, data, off=19):
    add_iter(model, "TxtPinX", "%.2f" % struct.unpack("<d", data[off + 1:off + 1+8]), off + 1, 8, "<d")
    add_iter(model, "TxtPinY", "%.2f" % struct.unpack("<d", data[off + 10:off + 10 + 8]), off + 10, 8, "<d")
    add_iter(model, "TxtWidth", "%.2f" % struct.unpack("<d", data[off + 19:off + 19 + 8]), off + 19, 8, "<d")
    add_iter(model, "TxtHeight", "%.2f" % struct.unpack("<d", data[off + 28:off + 28 + 8]), off + 28, 8, "<d")
    add_iter(model, "TxtLocPinX", "%.2f" % struct.unpack("<d", data[off + 37:off + 37 + 8]), off + 37, 8, "<d")
    add_iter(model, "TxtLocPinY", "%.2f" % struct.unpack("<d", data[off + 46:off + 46 + 8]), off + 46, 8, "<d")
    add_iter(model, "TxtAngle", "%.2f" % struct.unpack("<d", data[off + 55:off + 55 + 8]), off + 55, 8, "<d")
    if len(data) > off + 69: # both 6 and 11
        parse_block(page, model, data, off + 69)


def XForm(page, model, options, data, off=19):
    add_iter(model, "PinX", "%.2f" % struct.unpack("<d", data[off + 1:off + 1 +8]), off + 1, 8, "<d")
    add_iter(model, "PinY", "%.2f" % struct.unpack("<d", data[off + 10:off + 10 + 8]), off + 10, 8, "<d")
    add_iter(model, "Width", "%.2f" % struct.unpack("<d", data[off + 19:off + 19 + 8]), off + 19, 8, "<d")
    add_iter(model, "Height", "%.2f" % struct.unpack("<d", data[off + 28:off + 28 + 8]), off + 28, 8, "<d")
    add_iter(model, "LocPinX", "%.2f" % struct.unpack("<d", data[off + 37:off + 37 + 8]), off + 37, 8, "<d")
    add_iter(model, "LocPinY", "%.2f" % struct.unpack("<d", data[off + 46:off + 46 + 8]), off + 46, 8, "<d")
    add_iter(model, "Angle", "%.2f" % struct.unpack("<d", data[off + 55:off + 55 + 8]), off + 55, 8, "<d")
    add_iter(model, "FlipX", "%2x" % data[off + 63], off + 63, 1, "<I")
    add_iter(model, "FlipY", "%2x" % data[off + 64], off + 64, 1, "<I")
    add_iter(model, "ResizeMode", "%2x" % data[off + 65], off + 65, 1, "<I")
    if len(data) > off + 69: # both 6 and 11
        parse_block(page, model, data, off + 69)


def XForm1D(page, model, options, data, off=19):
    add_iter(model, "BeginX", "%.2f" % struct.unpack("<d", data[off + 1:off + 1+8]), off + 1, 8, "<d")
    add_iter(model, "BeginY", "%.2f" % struct.unpack("<d", data[off + 10:off + 10 + 8]), off + 10, 8, "<d")
    add_iter(model, "EndX", "%.2f" % struct.unpack("<d", data[off + 19:off + 19 + 8]), off + 19, 8, "<d")
    add_iter(model, "EndY", "%.2f" % struct.unpack("<d", data[off + 28:off + 28 + 8]), off + 28, 8, "<d")
    if len(data) > off + 38: # both 6 and 11
        parse_block(page, model, data, off + 38)


def vsdchunk(page, model, options, data):
    funcs = {
            "Act ID": ActId,
            "ArcTo": ArcTo,
            "Char IX": Char,
            "ConnectPts": ConnPts, 
            "Control": Control,
            "EllpArcTo": EllArcTo,
            "Ellipse": Ellipse,
            "Event": Event,
            "Fill": Fill, 
            "Font": Font,
            "FrgnType": FrgnType,
            "Geometry": Geometry,
            "InfLine": InfLine,
            "LayerIX": LayerIX,
            "Line": Line,
            "LineTo": MoveTo,
            "Misc": Misc,
            "MoveTo": MoveTo,
            "NURBS": NURBS,
            "NameIDX": NameID,
            "Page": Page,
            "PageLayout": PageLayout,
            "PageProps": PageProps,
            "ParaIX": Para,
            "Polyline": Polyline,
            "PrintProps": PrintProps,
            "Shape": Shape,
            
            "PageSheet": Shape,
            "ShapeType=\"Group\"": Shape,
            "ShapeType=\"Shape\"": Shape,
            "StyleSheet": Shape,
            "ShapeType=\"Guide\"": Shape,
            "ShapeType=\"Foreign\"": Shape,
            "DocSheet": Shape,
            
            "ShapeData": ShapeData,
            "Shape Stencil": ShapeStencil,
            "SplineKnot": SplineKnot,
            "SplineStart": SplineStart,
            "StyleProps": StyleProps,
            "Text IX": Text,
            "TextBlock": TextBlock,
            "TextFields": TextFields,
            "TxtXForm": TxtXForm,
            "XForm": XForm,
            "XForm1D": XForm1D
            }
    ftype = options["parse"]["type"]
    if ftype in funcs: 
        funcs[ftype](page, model, options, data)


def parse(page, parent, data, kw):
    ver_offset = 0x1a
    size_offset = 0x1c
    trlr_offset = 0x24

    version = data[ver_offset]
    kw['version'] = version
    page.doc_data["version"] = version

    page.pgi_add(parent=parent, name="Header", type="vsd/hdr", data=data[:0x24])

    if version > 6:
        lenhdr2 = 74
    else:
        lenhdr2 = 4
    page.pgi_add(parent=parent, name="Header part2", type="vsd/hdr2", data=data[0x36:0x36 + lenhdr2])

    tr_pntr = Pointer()
    tr_pntr.read(data, data, trlr_offset, version)
    tr_pntr.parse(page, data, parent)


# END
