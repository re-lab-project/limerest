# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import struct
import sys
from formats.mf import *
from utils import *

UnitType = {0: "World", 1: "Display", 2: "Pixel", 3: "Point", 4: "Inch", 5: "Document", 6: "Millimeter"}

TextRenderingHint = {0: "SystemDefault", 1: "SingleBitPerPixelGridFit", 2: "SingleBitPerPixel",
                     3: "AntialiasGridFit", 4: "Antialias", 5: "ClearTypeGridFit"}

SmoothingMode = {0: "Default", 1: "HighSpeed", 2: "SmoothingModeHighQuality", 3: "None",
                 4: "AntiAlias8x4", 5: "AntiAlias8x8"}

ObjectType = {0: "Invalid", 1: "Brush", 2: "Pen", 3: "Path", 4: "Region", 5: "Image", 6: "Font", 7: "StringFormat",
              8: "ImageAttributes", 9: "CustomLineCap"}

PenDataFlags = {0: "Transform", 1: "StartCap", 2: "EndCap", 3: "Join", 4: "MiterLimit", 5: "LineStyle",
                6: "DashedLineCap", 7: "DashedLineOffset", 8: "DashedLine", 9: "NonCenter", 10: "CompoundLine",
                11: "CustomStartCap", 12: "CustomEndCap"}

BrushDataFlags = {0: "Path", 1: "Transform", 2: "PresetColors", 3: "BlendFactorsH", 4: "BlendFactorsV",
                    5: "FocusScales", 6: "IsGammaCorrected", 7: "DoNotTransform"}

BrushType = {0: "SolidColor", 1: "HatchFill", 2: "TextureFill", 3: "PathGradient", 4: "LinearGradient"}

HatchStyle = {0: "Horizontal", 1: "Vertical", 2: "ForwardDiagonal", 3: "BackwardDiagonal", 4: "LargeGrid",
                5: "DiagonalCross", 6: "05Percent", 7: "10Percent", 8: "20Percent", 9: "25Percent", 10: "30Percent",
                11: "40Percent", 12: "50Percent", 13: "60Percent", 14: "70Percent", 15: "75Percent", 16: "80Percent",
                17: "90Percent", 18: "LightDownwardDiagonal", 19: "LightUpwardDiagonal", 20: "DarkDownwardDiagonal",
                21: "DarkUpwardDiagonal", 22: "WideDownwardDiagonal", 23: "WideUpwardDiagonal", 24: "LightVertical",
                25: "LightHorizontal", 26: "NarrowVertical", 27: "NarrowHorizontal", 28: "DarkVertical",
                29: "DarkHorizontal", 30: "DashedDownwardDiagonal", 31: "DashedUpwardDiagonal", 32: "DashedHorizontal",
                33: "DashedVertical", 34: "SmallConfetti", 35: "LargeConfetti", 36: "ZigZag", 37: "Wave",
                38: "DiagonalBrick", 39: "HorizontalBrick", 40: "Weave", 41: "Plaid", 42: "Divot", 43: "DottedGrid",
                44: "DottedDiamond", 45: "Shingle", 46: "Trellis", 47: "Sphere", 48: "SmallGrid", 49: "SmallCheckerBoard",
                50: "LargeCheckerBoard", 51: "OutlinedDiamond", 52: "SolidDiamond"}

WrapMode = {0: "Tile", 1: "TileFlipX", 2: "TileFlipY", 3: "TileFlipXY", 4: "Clamp"}

PathPointType = {0: "Start", 1: "Line", 3: "Bezier"}

PathPointTypeFlags = {0: "DashMode", 1: "PathMarker", 2: "RLE", 3: "CloseSubpath"} # Added RLE and translated to power for high 4 bits

RegionNodeDataType = {1: "And", 2: "Or", 3: "Xor", 4: "Exclude", 5: "Complement", 0x10000000: "Rect",
                        0x10000001: "Path", 0x10000002: "Empty", 0x10000003: "Infinite"}

ImageType = {0: "Unknown", 1: "Bitmap", 2: "Metafile"}

MetafileType = {0: "Wmf", 1: "WmfPlaceable", 2: "Emf", 3: "EmfPlusOnly", 4: "EmfPlusDual"}

FontStyleFlags = {0: "Bold", 1: "Italic", 2: "Underline", 3: "Strikeout"}

StrFmtFlagNames = {0: "DirectionRightToLeft", 1: "DirectionVertical", 2: "NoFitBlackBox", 3: "DisplayFormatControl",
                   4: "NoFontFallback", 5: "MeasureTrailingSpaces", 6: "NoWrap", 7: "LineLimit", 8: "NoClip", 9: "BypassGDI"}

StrFmtFlagValues = {0:0x1, 1:0x2, 2:0x4, 3:0x20, 4:0x400, 5:0x800, 6:0x1000, 7:0x2000, 8:0x4000, 9:0x80000000}

LangIDnames = {0x0000: "LANG_NEUTRAL", 0x0004: "zh-CHS", 0x007F: "LANG_INVARIANT", 0x0400: "LANG_NEUTRAL_USER_DEFAULT",
            0x0401: "ar-SA", 0x0402: "bg-BG", 0x0403: "ca-ES", 0x0404: "zh-CHT", 0x0405:"cs-CZ", 0x0406: "da-DK",
            0x0407: "de-DE", 0x0408: "el-GR", 0x0409: "en-US", 0x040A: "es-Tradnl-ES", 0x040B:"fi-FI", 0x040C: "fr-FR",
            0x040D: "he-IL", 0x040E: "hu-HU", 0x040F: "is-IS", 0x0410: "it-IT", 0x0411: "ja-JA", 0x0412: "ko-KR",
            0x0413: "nl-NL", 0x0414: "nb-NO", 0x0415: "pl-PL", 0x0416: "pt-BR", 0x0417: "rm-CH", 0x0418: "ro-RO",
            0x0419: "ru-RU", 0x041A: "hr-HR", 0x041B: "sk-SK", 0x041C: "sq-AL", 0x041D: "sv-SE", 0x041E: "th-TH",
            0x041F: "tr-TR", 0x0420: "ur-PK", 0x0421: "id-ID", 0x0422: "uk-UA", 0x0423: "be-BY", 0x0424: "sl-SI",
            0x0425: "et-EE", 0x0426: "lv-LV", 0x0427: "lt-LT", 0x0428: "tg-TJ", 0x0429: "fa-IR", 0x042A: "vi-VN",
            0x042B: "hy-AM", 0x042C: "az-Latn-AZ", 0x042D: "eu-ES", 0x042E: "wen-DE", 0x042F: "mk-MK", 0x0430: "st-ZA",
            0x0432: "tn-ZA", 0x0434: "xh-ZA", 0x0435: "zu-ZA", 0x0436: "af-ZA", 0x0437: "ka-GE", 0x0438: "fa-FA",
            0x0439: "hi-IN", 0x043A: "mt-MT", 0x043B: "se-NO", 0x043C: "ga-GB", 0x043E: "ms-MY", 0x043F: "kk-KZ",
            0x0440: "ky-KG", 0x0441: "sw-KE", 0x0442: "tk-TM", 0x0443: "uz-Latn-UZ", 0x0444: "tt-Ru", 0x0445: "bn-IN",
            0x0446: "pa-IN", 0x0447: "gu-IN", 0x0448: "or-IN", 0x0449: "ta-IN", 0x044A: "te-IN", 0x044B: "kn-IN",
            0x044C: "ml-IN", 0x044D: "as-IN", 0x044E: "mr-IN", 0x044F: "sa-IN", 0x0450: "mn-MN", 0x0451: "bo-CN",
            0x0452: "cy-GB", 0x0453: "km-KH", 0x0454: "lo-LA", 0x0456: "gl-ES", 0x0457: "kok-IN", 0x0459: "sd-IN",
            0x045A: "syr-SY", 0x045B: "si-LK", 0x045D: "iu-Cans-CA", 0x045E: "am-ET", 0x0461: "ne-NP",0x0462: "fy-NL",
            0x0463: "ps-AF",0x0464: "fil-PH", 0x0465: "div-MV", 0x0468: "ha-Latn-NG", 0x046A:"yo-NG",0x046B: "quz-BO",
            0x046C: "nzo-ZA", 0x046D: "ba-RU", 0x046E: "lb-LU", 0x046F: "kl-GL", 0x0470: "ig-NG", 0x0477: "so-SO",
            0x0478: "ii-CN", 0x047A: "arn-CL",0x047C: "moh-CA", 0x047E: "br-FR", 0x0480: "ug-CN",0x0481: "mi-NZ",
            0x0482: "oc-FR", 0x0483: "co-FR",0x0484: "gsw-FR", 0x0485: "sah-RU", 0x0486: "qut-GT", 0x0487: "rw-RW",
            0x0488: "wo-SN", 0x048C: "gbz-AF", 0x0800: "LANG_NEUTRAL_SYS_DEFAULT", 0x0801: "ar-IQ",0x0804: "zh-CN",
            0x0807: "de-CH", 0x0809: "en-GB",0x080A: "es-MX", 0x080C: "fr-BE",0x0810: "it-CH",  0x0812: "ko-Johab-KR",
            0x0813: "nl-BE", 0x0814: "nn-NO", 0x0816: "pt-PT", 0x081A: "sr-Latn-SP", 0x081D: "sv-FI", 0x0820: "ur-IN",
            0x0827: "lt-C-LT", 0x082C: "az-Cyrl-AZ", 0x082E: "wee-DE", 0x083B:"se-SE", 0x083C: "ga-IE", 0x083E: "ms-BN",
            0x0843: "uz-Cyrl-UZ", 0x0845: "bn-BD", 0x0850: "mn-Mong-CN", 0x0859:"sd-PK", 0x085D: "iu-Latn-CA",
            0x085F: "tzm-Latn-DZ", 0x086B: "quz-EC", 0x0C00: "LANG_NEUTRAL_CUSTOM_DEFAULT", 0x0C01: "ar-EG",
            0x0C04: "zh-HK", 0x0C07: "de-AT", 0x0C09: "en-AU", 0x0C0A: "es-ES", 0x0C0C:"fr-CA", 0x0C1A: "sr-Cyrl-CS",
            0x0C3B: "se-FI",0x0C6B: "quz-PE", 0x1000: "LANG_NEUTRAL_CUSTOM", 0x1001:"ar-LY", 0x1004: "zh-SG",
            0x1007: "de-LU", 0x1009: "en-CA", 0x100A: "es-GT", 0x100C: "fr-CH", 0x101A:"hr-BA", 0x103B: "smj-NO",
            0x1400: "LANG_NEUTRAL_CUSTOM_DEFAULT_MUI", 0x1401:"ar-DZ", 0x1404: "zh-MO", 0x1407: "de-LI", 0x1409: "en-NZ",
            0x140A: "es-CR", 0x140C: "fr-LU", 0x141A: "bs-Latn-BA", 0x143B: "smj-SE", 0x1801: "ar-MA", 0x1809: "en-IE",
            0x180A: "es-PA", 0x180C: "ar-MC", 0x181A: "sr-Latn-BA", 0x183B: "sma-NO", 0x1C01: "ar-TN", 0x1C09: "en-ZA",
            0x1C0A: "es-DO", 0x1C1A: "sr-Cyrl-BA", 0x1C3B: "sma-SE", 0x2001: "ar-OM", 0x2008: "el-2-GR", 0x2009: "en-JM",
            0x200A: "es-VE", 0x201A: "bs-Cyrl-BA", 0x203B: "sms-FI", 0x2401: "ar-YE", 0x2409: "ar-029", 0x240A: "es-CO",
            0x243B: "smn-FI", 0x2801: "ar-SY", 0x2809: "en-BZ", 0x280A: "es-PE", 0x2C01: "ar-JO", 0x2C09: "en-TT",
            0x2C0A: "es-AR", 0x3001: "ar-LB", 0x3009: "en-ZW", 0x300A: "es-EC", 0x3401: "ar-KW", 0x3409: "en-PH",
            0x340A: "es-CL", 0x3801: "ar-AE", 0x380A: "es-UY", 0x3C01: "ar-BH", 0x3C0A: "es-PY", 0x4001: "ar-QA",
            0x4009: "en-IN", 0x400A: "es-BO", 0x4409: "en-MY", 0x440A: "es-SV", 0x4809: "en-SG", 0x480A: "es-HN",
            0x4C0A: "es-NI", 0x500A: "es-PR", 0x540A: "es-US", 0x7C04: "zh-Hant"}

StrAlign = {0: "Near", 1: "Center", 2: "Far"}

StrDgtSubs = {0: "User", 1: "None", 2: "National", 3: "Traditional"}

HotPfx = {0: "None", 1: "Show", 2: "Hide"}

StrTrim = {0: "None", 1: "Character", 2: "Word", 3: "EllipsisCharacter", 4: "EllipsisWord", 5: "EllipsisPath"}

ClampMode = {0: "Rect", 1: "Bitmap"}

CustLCapDataType ={0: "Default", 1: "AdjustableArrow"}

DrvStrOptFlags = {0: "CmapLookup", 1: "Vertical", 2: "RealizedAdvance", 3: "LimitSubpixel"}

CombineMode = {0: "Replace", 1: "Intersect", 2: "Union", 3: "XOR", 4: "Exclude", 5: "Complement"}

InterpolationMode = {0: "Default", 1: "LowQuality", 2: "HighQuality", 3: "Bilinear", 4: "Bicubic",
                     5: "NearestNeighbor", 6: "HighQualityBilinear", 7: "HighQualityBicubic"}

PixelOffsetMode = {0: "Default", 1: "HighSpeed", 2: "HighQuality", 3: "None", 4: "Half"}

CompositingQuality = {0: "Default", 1: "HighSpeed", 2: "HighQuality", 3: "GammaCorrected", 4: "AssumeLinear"}

CompositingMode = {0: "Over", 1: "Copy"}

def PointType(data, offset):
    ptype = data[offset]
    ptft = ""
    ptf = (ptype&0xF0) // 16
    for j in range(4):
        if ptf & 1:
            ptft = PathPointTypeFlags[j]
        ptf //= 2
    ptt = ptype & 0xF
    pttt = "unknown"
    if ptt in PathPointType:
        pttt = PathPointType[ptt]
    return ptft, pttt

def PointTypeRLE(data, offset):
    rle = data[offset]
    b = rle & 1
    runcnt = rle // 4
    ptft, pttt = PointType(data, offset + 1)
    return b, runcnt, ptft, pttt

def RGBA(page, model, data, offset, i=""):
    clr = "%02X%02X%02X%02X" % (data[offset + 2], data[offset + 1], data[offset], data[offset + 3])
    add_iter(model, "%s RGBA" % i, clr, offset, 4, "clr/bgra")

def XFormMtrx(page, model, data, offset):
    add_iter(model, "m11", struct.unpack("<f", data[offset:offset + 4])[0], offset, 4, "<f")
    add_iter(model, "m12", struct.unpack("<f", data[offset + 4:offset + 8])[0], offset + 4, 4, "<f")
    add_iter(model, "m21", struct.unpack("<f", data[offset + 8:offset + 12])[0], offset + 8, 4, "<f")
    add_iter(model, "m22", struct.unpack("<f", data[offset + 12:offset + 16])[0], offset + 12, 4, "<f")
    add_iter(model, "Dx", struct.unpack("<f", data[offset + 16:offset + 20])[0], offset + 16, 4, "<f")
    add_iter(model, "Dy", struct.unpack("<f", data[offset + 20:offset + 24])[0], offset + 20, 4, "<f")

def Record_Header(page, model, options, data):
    add_iter(model, "Type", struct.unpack("<H", data[:2])[0], 0, 2, "<H")
    add_iter(model, "Flags", struct.unpack("<H", data[2:4])[0], 2, 2, "<H")
    add_iter(model, "Size", struct.unpack("<I", data[4:8])[0],  4, 4, "<I")
    add_iter(model, "DataSize", struct.unpack("<I", data[8:12])[0], 8, 4, "<I")

def PDF_Xform(page, model, data, offset):
    XFormMtrx(page, model, data, offset)
    return 24

def PDF_StartCap(page, model, data, offset):
    # FIXME! Add Enum
    add_iter(model, "  StartCap", "%d" % struct.unpack("<I", data[:4])[0], offset, 4, "<I")
    return 4

def PDF_EndCap(page, model, data, offset):
    # FIXME! Add Enum
    add_iter(model, "  EndCap", "%d" % struct.unpack("<I", data[:4])[0], offset, 4, "<I")
    return 4

def PDF_Join(page, model, data, offset):
    # FIXME! Add Enum
    add_iter(model, "  Join", "%d" % struct.unpack("<I", data[:4])[0], offset, 4, "<I")
    return 4

def PDF_MiterLimit(page, model, data, offset):
    add_iter(model, "  Mitter", "%.2f" % struct.unpack("<f", data[:4])[0], offset, 4, "<f")
    return 4

def PDF_LineStyle(page, model, data, offset):
    # FIXME! Add Enum
    add_iter(model, "  LineStyle", "%d" % struct.unpack("<I", data[:4])[0], offset, 4, "<I")
    return 4

def PDF_DashedLineCap(page, model, data, offset):
    # FIXME! Add Enum
    add_iter(model, "  DashLineCap", "%d" % struct.unpack("<I", data[:4])[0], offset, 4, "<I")
    return 4

def PDF_DashedLineOffset(page, model, data, offset):
    add_iter(model, "  DashLineOffset", "%.2f" % struct.unpack("<f", data[:4])[0], offset, 4, "<f")
    return 4

def PDF_DashedLine(page, model, data, offset):
    dlsize = struct.unpack("<I", data[:4])[0]
    add_iter(model, "  DashLineNumElems", "%d" % dlsize, offset, 4, "<I")
    for i in range(dlsize):
        ds = struct.unpack("<I", data[4 + i * 4:8 + i * 4])[0]
        add_iter(model, "  DashElem %d" % i, "%d" % ds, offset + 4 + i * 4, 4, "<I")
    return (dlsize + 1) * 4

def PDF_NonCenter(page, model, data, offset):
    # FIXME! Add Enum
    add_iter(model, "  PenAlignment", "%d" % struct.unpack("<I", data[:4])[0], offset, 4, "<I")
    return 4

def PDF_CompoundLine(page, model, data, offset):
    dlsize = struct.unpack("<I", data[:4])[0]
    add_iter(model, "  CompLineNumElems", "%d" % dlsize, offset, 4, "<I")
    for i in range(dlsize):
        ds = struct.unpack("<I", data[4 + i * 4:8 + i * 4])[0]
        add_iter(model, "  CompLineElem %d" % i, "%d" % ds, offset + 4 + i * 4, 4, "<I")
    return (dlsize + 1) * 4

def PDF_CustomStartCap(page, model, data, offset):
    # FIXME! Add parsing of CapData
    dlsize = struct.unpack("<I", data[:4])[0]
    add_iter(model, "  CustStartCap", "%d" % dlsize, offset, 4, "<I")
    add_iter(model, "  CustStartCapData", "", offset + 4, dlsize, "txt")
    return dlsize + 4

def PDF_CustomEndCap(page, model, data, offset):
    # FIXME! Add parsing of CapData
    dlsize = struct.unpack("<I", data[:4])[0]
    add_iter(model, "  CustEndCap", "%d" % dlsize, offset, 4, "<I")
    add_iter(model, "  CustEndCapData", "", offset + 4, dlsize, "txt")
    return dlsize + 4

def BDF_PresetColors(page, model, data, offset):
    add_iter(model, "PresetColor", "", 0, 0, "")
    pcnt = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "  PosCount", "%d" % pcnt, offset, 4, "<I")
    for i in range(pcnt):
        add_iter(model, "  BlendPos%d" % i, "%.2f" % struct.unpack("<f", data[offset + 4 + i * 4:offset + 8 + i * 4])[0], offset + 4 + i * 4, 4, "<f")
    for i in range(pcnt):
        RGBA(page, model, data, offset + pcnt * 4 + 4 + i * 4, "  Blend%d Clr" % i)
    return 4 + pcnt * 8

def BT_SolidColor(page, model, data, offset):
    RGBA(page, model, data, offset)

def BT_HatchFill(page, model, data, offset):
    hstyle = struct.unpack("<I", data[offset:offset + 4])[0]
    hst = "unknown"
    if hstyle in HatchStyle:
        hst = HatchStyle[hstyle]
    add_iter(model, "  HatchStyle", "0x%02x (%s)" % (hstyle, hst), offset, 4, "<I")
    RGBA(page, model, data, offset + 4, "ForeClr ")
    RGBA(page, model, data, offset + 8, "BackClr ")

def BT_TextureFill(page, model, data, offset):
    bdflags = struct.unpack("<I", data[offset:offset + 4])[0]
    bdf = []
    bdft = ""
    flags = bdflags
    for i in range(8):
        bdf.append(flags & 1)
        flags //= 2
    for i in range(8):
        if bdf[i] == 1:
            bdft += BrushDataFlags[i] + ", "
    bdft = bdft[0:len(bdft) - 2]
    add_iter(model, "  BrushDataFlags", "0x%02x (%s)" % (bdflags, bdft), offset, 4, "<I")
    wmode = struct.unpack("<I", data[offset + 4:offset + 8])[0]
    wmt = ""
    if wmode in WrapMode:
        wmt = WrapMode[wmode]
    add_iter(model, "  WrapMode", "0x%02x (%s)" % (wmode, wmt), offset + 4, 4, "<I")
    offset += 8
    for i in range(8):
        if bdf[i] == 1:
            if i in bdf_ids:
                offset += bdf_ids[i](page, model, data, offset)

def BT_PathGradient(page, model, data, offset):
    bdflags = struct.unpack("<I", data[offset:offset + 4])[0]
    bdf = []
    bdft = ""
    flags = bdflags
    for i in range(8):
        bdf.append(flags & 1)
        flags //= 2
    for i in range(8):
        if bdf[i] == 1:
            bdft += BrushDataFlags[i] + ", "
    bdft = bdft[0:len(bdft) - 2]
    add_iter(model, "  BrushDataFlags", "0x%02x (%s)" % (bdflags, bdft), offset, 4, "<I")
    wmode = struct.unpack("<I", data[offset + 4:offset + 8])[0]
    wmt = ""
    if wmode in WrapMode:
        wmt = WrapMode[wmode]
    add_iter(model, "  WrapMode", "0x%02x (%s)" % (wmode, wmt), offset + 4, 4, "<I")
    RGBA(page, model, data, offset + 8, "CenterClr")
    PointF(model, data, offset + 12, "CenterPoint")
    surclrcnt = struct.unpack("<I", data[offset + 20:offset + 24])[0]
    add_iter(model, "  SurClrCount", "0x%02x" % surclrcnt, offset + 20, 4, "<I")
    for i in range(surclrcnt):
        RGBA(page, model, data, offset + 24 + i * 4, "SurClr#%s" % i)
    if bdf[0]:
        #Boundary path object
        psize = struct.unpack("<I", data[offset + 24 + surclrcnt * 4:offset + 28 + surclrcnt * 4])[0]
        add_iter(model, "Path Size", "0x%02x" % psize, offset + 24, 4, "<I")
        ObjPath(page, model, data, offset + 16 + surclrcnt * 4)
        offset += psize + 28 + surclrcnt * 4
    else:
        #Boundary point object
        numofpts = struct.unpack("<I", data[offset + 28 + surclrcnt * 4:offset + 32 + surclrcnt * 4])[0]
        for i in range(numofpts):
            PointF(model, data, offset + 32 + surclrcnt * 4 + i * 8, "BndrPnt#%s" % i)
        offset += 32 + surclrcnt * 4 + numofpts * 8
    for i in range(7): # have to skip first flag
        if bdf[i + 1] == 1:
            if i + 1 in bdf_ids:
                offset += bdf_ids[i + 1](page, model, data, offset)

def BT_LinearGradient(page, model, data, offset):
    bdflags = struct.unpack("<I", data[offset:offset + 4])[0]
    bdf = []
    bdft = ""
    flags = bdflags
    for i in range(8):
        bdf.append(flags & 1)
        flags //= 2
    for i in range(8):
        if bdf[i] == 1:
            bdft += BrushDataFlags[i] + ", "
    bdft = bdft[:len(bdft) - 2]
    add_iter(model, "  BrushDataFlags", "0x%02x (%s)" % (bdflags, bdft), offset, 4, "<I")
    wmode = struct.unpack("<I", data[offset + 4:offset + 8])[0]
    wmt = ""
    if wmode in WrapMode:
        wmt = WrapMode[wmode]
    add_iter(model, "  WrapMode", "0x%02x (%s)" % (wmode, wmt), offset + 4, 4, "<I")
    PointL(model, data, offset + 8, "s")
    PointL(model, data, offset + 16, "e")
    RGBA(page, model, data, offset + 24, "StartClr")
    RGBA(page, model, data, offset + 28, "EndClr")
    add_iter(model, "Rsrv1", "0x%02x" % struct.unpack("<I", data[offset + 32:offset + 36])[0], offset + 32, 4, "<I")
    add_iter(model, "Rsrv2", "0x%02x" % struct.unpack("<I", data[offset + 36:offset + 40])[0], offset + 36, 4, "<I")
    offset += 40
    for i in range(8):
        if bdf[i] == 1:
            if i in bdf_ids:
                offset += bdf_ids[i](page, model, data, offset)

def RegionNode(page, model, data, offset, txt=""):
    add_iter(model, "%sRegionNode" % txt, "", offset, 0, "")
    rtype = struct.unpack("<I", data[offset:offset + 4])[0]
    rt = "unknown"
    if rtype in RegionNodeDataType:
        rt = RegionNodeDataType[rtype]
    add_iter(model, "  RegionNodeType", rt, offset, 4, "<I")
    if rtype in rnd_ids:
        offset = rnd_ids[rtype](page, model, data, offset, i)
    return offset

def RND_Child(page, model, data, offset, i):
    offset = RegionNode(page, model, data, offset, "Left ")
    offset = RegionNode(page, model, data, offset, "Right ")
    return offset

def RND_Rect(page, model, data, offset, i):
    RectF(model, data, offset, "  ")
    offset += 16
    return offset

def RND_Path(page, model, data, offset, i):
    psize = struct.unpack("<I", data[offset:offset+4])[0]
    add_iter(model, "Path Size", "0x%02x" % psize, offset, 4, "<I")
    ObjPath(page, model, data, offset - 8)
    return offset + psize

def RND_Empty(page, model, data, offset, i):
    return offset

def ObjHeader(page, model, data, offset, title):
    add_iter(model, title, "", offset, 0, "")
    ver = struct.unpack("<I", data[offset + 0xc:offset + 0x10])[0]
    sig = ver & 0xFFFFF000
    graph = ver & 0xFFF
    add_iter(model, "  Ver Sig", "%#03x" % (sig // 4096), offset + 0xd, 2, "<H")
    add_iter(model, "  Ver Graphics", "%#02x" % graph, offset + 0xc, 2, "<H")

def ObjBrush(page, model, data, offset=0):
    ObjHeader(page, model, data, offset, "Brush")
    btype = struct.unpack("<I", data[offset + 0x10:offset + 0x14])[0]
    bt = "unknown"
    if btype in BrushType:
        bt = BrushType[btype]
    add_iter(model, "  Type", "%#02x (%s)" % (btype, bt), offset+0x10, 4, "<I")
    if btype in bt_ids:
        bt_ids[btype](page, model, data, offset + 0x14)

def ObjPen(page, model, data, offset=0):
    ObjHeader(page, model, data, offset, "Pen")
    add_iter(model, "  Type", "%#02x" % struct.unpack("<I", data[0x10:0x14])[0], offset + 0x10, 4, "<I")
    pdflags = struct.unpack("<I", data[0x14:0x18])[0]
    pdf = []
    pdft = ""
    flags = pdflags
    for i in range(13):
        pdf.append(flags & 1)
        flags //= 2
    for i in range(13):
        if pdf[i] == 1:
            pdft += PenDataFlags[i] + ", "
    pdft = pdft[0:len(pdft) - 2]
    add_iter(model, "  PenDataFlags", "%#02x (%s)" % (pdflags, pdft), 0x14, 4, "<I")
    utype = struct.unpack("<I", data[0x18:0x1c])[0]
    ut ="unknown"
    if utype in UnitType:
        ut = UnitType[utype]
    add_iter(model, "  PenUnits", "%#02x (%s)" % (utype, ut), 0x18, 4, "<I")
    add_iter(model, "  PenWidth", "%.2f" % struct.unpack("<f", data[0x1c:0x20])[0], 0x1c, 4, "<f")
    offset = 0x20
    for i in range(13):
        if pdf[i] == 1:
            if i in pdf_ids:
                offset += pdf_ids[i](page, model, data[offset:], offset)
    ObjBrush(page, model, data, offset - 12) # to adjust ObjBrush header

def ObjPath(page, model, data, offset=0):
    ObjHeader(page, model, data, offset, "Path")
    ppcnt = struct.unpack("<I", data[offset + 0x10:offset + 0x14])[0]
    add_iter(model, "  PathPointCount", "%d" % ppcnt, offset + 0x10, 4, "<I")
    ppflags = struct.unpack("<H", data[offset + 0x14:offset + 0x16])[0]
    fc = (ppflags & 0x4000) // 0x4000
    fr = (ppflags & 0x800) // 0x800
    add_iter(model, "  PathPointFlags (c,r)", "%#02x (%d %d)" % (ppflags, fc, fr), offset + 0x14, 2, "<H")
    add_iter(model, "  Reserved", "", offset + 0x16, 2, "<H")
    if fr == 0:
        if fc == 0:
            for i in range(ppcnt):
                PointF(model, data, offset + 0x18 + i * 8, "    Abs Pnt%s " % i)
            offset += 0x18 + i * 8 + 8
        else:
            for i in range(ppcnt):
                PointS(model, data, offset + 0x18 + i * 4, "    Abs Pnt%s " % i)
            offset += 0x18 + i * 4 + 4
        i = 0
        while i < ppcnt:
            hb = data[offset + i]
            if hb & 0x40:
                ptft1 = ""
                if hb & 0x80:
                    ptft1 = "Bezier"
                run = hb & 0x3f
                ptft, pttt = PointType(data, offset + i + 1)
                ptft1 += ptft
                ptft, pttt = PointType(data, offset + i)
                add_iter(model, "    Pnts %d to %d Type" % (i, i + run), "%s %s" % (ptft1, pttt), offset + i, 1, "<B")
                i += 2
            else:
                ptft, pttt = PointType(data ,offset + i)
                add_iter(model, "    Pnt%d Type" % i, "%s %s" % (ptft, pttt), offset + i, 1, "<B")
                i += 1
    else:
        offset += 0x18
        for i in range(ppcnt):
            print(i, offset)
            offset += PointR(model, data, offset, "    Rel Pnt%s " % i)
        for i in range(ppcnt):
            b, runcnt, ptft, pttt = PointTypeRLE(data, offset + i)
            add_iter(model, "    Pnt%d Type" % i, "(b, runcnt, ptft, pttt) %s %s %s %s" % (b, runcnt, ptft, pttt), offset + i, 1, "<BB")

def ObjRegion(page, model, data, offset=0):
    ObjHeader(page, model, data, offset, "Region")
    rncnt = struct.unpack("<I", data[offset + 0x10:offset + 0x14])[0]
    add_iter(model, "  RegionNodeCount", "%d" % rncnt, offset + 0x10, 4, "<I")
    offset += 0x14
    for i in range(rncnt + 1):
        offset = RegionNode(page, model, data, offset)

def ObjImage(page, model, data, offset=0):
    ObjHeader(page, model, data, offset, "Image")
    itype = struct.unpack("<I", data[offset + 0x10:offset + 0x14])[0]
    it = "unknown"
    if itype in ImageType:
        it = ImageType[itype]
    add_iter(model, "  Type", "%#02x (%s)" % (itype, it), offset + 0x10, 4, "<I")
    if itype == 2:  # Metafile
        mtype = struct.unpack("<I", data[offset + 0x14:offset + 0x18])[0]
        msize = struct.unpack("<I", data[offset + 0x18:offset + 0x1c])[0]
        mt = "unknown"
        if mtype in MetafileType:
            mt = MetafileType[mtype]
        add_iter(model, "  MetaFileType", "%#02x (%s)" % (mtype, mt), offset + 0x14, 4, "<I")
        add_iter(model, "  MetaFileSize", "%#02x" % msize, offset + 0x18, 4, "<I")
        #FIXME! send Metafile values to parser

def ObjStringFormat(page, model, data, offset=0):
    ObjHeader(page, model, data, offset, "StringFormat")
    sfflags = struct.unpack("<I", data[offset + 0x10:offset + 0x14])[0]
    sft = ""
    for i in range(10):
        if sfflags&StrFmtFlagValues[i]:
            sft += StrFmtFlagNames[i]
    add_iter(model, "  StringFmt Flags", "%#04x (%s)" % (sfflags, sft), offset + 0x10, 4, "<I")
    langid = struct.unpack("<I", data[offset + 0x14:offset + 0x18])[0]
    lt = "unknown"
    if langid in LangIDnames:
        lt = LangIDnames[langid]
    add_iter(model, "  LangID", "%#04x (%s)" % (langid, lt), offset + 0x14, 4, "<I")
    stral = struct.unpack("<I", data[offset + 0x18:offset + 0x1c])[0]
    sat = "unknown"
    if stral in StrAlign:
        sat = StrAlign[stral]
    add_iter(model, "  StringAlign", "%#04x (%s)" % (stral, sat), offset + 0x18, 4, "<I")
    lineal = struct.unpack("<I", data[offset + 0x1c:offset + 0x20])[0]
    lat = "unknown"
    if lineal in StrAlign:
        lat = StrAlign[lineal]
    add_iter(model, "  LineAlign", "%#04x (%s)" % (lineal, lat), offset + 0x1c, 4, "<I")
    sds = struct.unpack("<I", data[offset + 0x20:offset + 0x24])[0]
    sdst = "unknown"
    if sds in StrDgtSubs:
        sdst = StrDgtSubs[sds]
    add_iter(model, "  StrDigitSubst", "%#04x (%s)" % (sds, sdst), offset + 0x20, 4, "<I")
    langid = struct.unpack("<I", data[offset + 0x24:offset + 0x28])[0]
    lt = "unknown"
    if langid in LangIDnames:
        lt = LangIDnames[langid]
    add_iter(model, "  DigitLang", "%#04x (%s)" % (langid, lt), offset + 0x24, 4, "<I")
    add_iter(model, "  FirstTabOffset", "%.2f" % struct.unpack("<f", data[offset + 0x28:offset + 0x2c])[0], offset + 0x28, 4, "<f")
    hkp = struct.unpack("<i", data[offset + 0x2c:offset + 0x30])[0]
    hkpt = "unknown"
    if hkp in HotPfx:
        hkpt = HotPfx[hkp]
    add_iter(model, "  HotKeyPrefix", "%#04x (%s)" % (hkp, hkpt), offset + 0x2c, 4, "<i")
    add_iter(model, "  LeadMargin", "%.2f" % struct.unpack("<f", data[offset + 0x30:offset + 0x34])[0], offset + 0x30, 4, "<f")
    add_iter(model, "  TrailMargin", "%.2f" % struct.unpack("<f", data[offset + 0x34:offset + 0x38])[0], offset + 0x34, 4, "<f")
    add_iter(model, "  Tracking", "%.2f" % struct.unpack("<f", data[offset + 0x38:offset + 0x3c])[0], offset + 0x38, 4, "<f")
    strim = struct.unpack("<I", data[offset + 0x3c:offset + 0x40])[0]
    st = "unknown"
    if strim in StrTrim:
        st = StrTrim[strim]
    add_iter(model, "  StrTrimming", "%#04x (%s)" % (strim, st), offset + 0x3c, 4, "<I")
    tscnt = struct.unpack("<i", data[offset + 0x40:offset + 0x44])[0]
    add_iter(model, "  TabStopCount", "%#04x" % tscnt, offset + 0x40, 4, "<i")
    rgcnt = struct.unpack("<i", data[offset + 0x44:offset + 0x48])[0]
    add_iter(model, "  RangeCount", "%#04x" % rgcnt, offset + 0x44, 4, "<i")
    for i in range(tscnt):
        add_iter(model, "  TabStop%d" % i, "%.2f" % struct.unpack("<f", data[offset + 0x48 + i * 4:offset + 0x4c + i * 4])[0], offset + 0x48, 4, "<f")
    offset += 0x4c + tscnt * 4
    for i in range(rgcnt):
        add_iter(model, "  Range%d First" % i, "%d" % struct.unpack("<i", data[offset + i * 4:offset + 4 + i * 4])[0], offset + i + 4, 4, "<i")
        add_iter(model, "  Range%d Length" % i, "%d" % struct.unpack("<i", data[offset +4 + i * 4:offset + 8 + i * 4])[0], offset + 4 + i * 4, 4, "<i")

def ObjFont(page, model, data, offset=0):
    ObjHeader(page, model, data, offset, "Font")
    emsize = struct.unpack("<f", data[offset + 0x10:offset + 0x14])[0]
    add_iter(model, "  EmSize", "%.2f" % emsize, offset + 0x10, 4, "<f")
    utype = struct.unpack("<I", data[offset + 0x14:offset + 0x18])[0]
    ut ="unknown"
    if utype in UnitType:
        ut = UnitType[utype]
    add_iter(model, "EmSize Units", "%#02x (%s)" % (utype, ut), offset + 0x14, 4, "<I")
    fflags = struct.unpack("<I", data[offset + 0x18:offset + 0x1c])[0]
    fdft = ""
    flags = fflags
    for i in range(4):
        if flags & 1:
            fdft += FontStyleFlags[i]
        flags //= 2
    add_iter(model, "  Style", "%#02x (%s)" % (fflags, fdft), offset + 0x18, 4, "<I")
    add_iter(model, "Reserved", "", offset + 0x1c, 4, "<I")
    nlen = struct.unpack("<I", data[offset + 0x20:offset + 0x24])[0]
    add_iter(model, "  FontName Len", "%d" % nlen, offset + 0x20, 4, "<I")
    add_iter(model, "  FontName", data[offset + 0x24:].decode("utf-16"), offset + 0x24, 4, "txt")

def ObjImgAttr(page, model, data, offset=0):
    ObjHeader(page, model, data, offset, "ImgAttributes")
    add_iter(model, "Reserved1", "", offset + 0x10, 4, "<I")
    wmode = struct.unpack("<I", data[offset + 0x14:offset + 0x18])[0]
    wmt = ""
    if wmode in WrapMode:
        wmt = WrapMode[wmode]
    add_iter(model, "  WrapMode", "%#02x (%s)" % (wmode, wmt), offset + 0x14, 4, "<I")
    RGBA(page, model, data[offset + 0x18:], offset, "ClampClr ")
    cmode = struct.unpack("<I", data[offset + 0x1c:offset + 0x20])[0]
    cmt = ""
    if cmode in ClampMode:
        cmt = ClampMode[cmode]
    add_iter(model, "  ClampMode", "%#02x (%s)" % (cmode, cmt), offset + 0x1c, 4, "<I")
    add_iter(model, "Reserved2", "", offset + 0x20, 4, "<I")

def ObjCustLineCap(page, model, data, offset=0):
    ObjHeader(page, model, data, offset, "CustLineCap")
    citype = struct.unpack("<I", data[offset + 0x10:offset + 0x14])[0]
    ct = "unknown"
    if ctype in CustLCapDataType:
        ct = CustLCapDataType[ctype]
    add_iter(model, "  CustLineCapDataType", "%#02x (%s)" % (ctype, ct), offset + 0x10, 4, "<I")
    #FIXME! Parse CustLineCapData

#0x4001
def Header(page, model, options, data):
    Record_Header(page, model, data)
    ver = struct.unpack("<I", data[0xc:0x10])[0]
    sig = ver & 0xFFFFF000
    graph = ver & 0xFFF
    add_iter(model, "Ver Sig", "%#03x" % (sig // 4096), 0xd, 2, "<H")
    add_iter(model, "Ver Graphics", "%#02x" % graph, 0xc, 4, "<H")
    add_iter(model, "EMF+ Flags", "%#08x" % struct.unpack("<I", data[0x10:0x14])[0], 0x10, 4, "<I")
    add_iter(model, "LogDpiX (lpi)", "%d" % struct.unpack("<I", data[0x14:0x18])[0], 0x14, 4, "<I")
    add_iter(model, "LogDpiY (lpi)", "%d" % struct.unpack("<I", data[0x18:0x1c])[0], 0x18, 4, "<I")

#0x4002
def EOF(page, model, options, data):
    pass

#0x4004
def GetDC(page, model, options, data):
    pass

#0x4008
def Object(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    c = (flags & 0x8000) // 0x8000
    otf = (flags & 0x7F00) // 256
    ot = "unknown"
    oid = flags & 0xFF
    if otf in ObjectType:
        ot = ObjectType[otf]
    add_iter(model, "Flags (c, type, id)", "%#04x (%d, %s, %02x)" % (flags, c, ot, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    shift = 0
    if c:
        add_iter(model, "Total Object Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
        shift = 4
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8 + shift:0xc + shift])[0], 8 + shift, 4, "<I")
    if otf in obj_ids:
        obj_ids[otf](page, model, data, offset=shift)

#0x4009
def Clear(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    add_iter(model, "Flags", "%#04x" % struct.unpack("<H", data[2:4])[0], 2, 4, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    RGBA(page, model, data, 0xc, "Color ")

#0x4010
def FillPie(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fs = (flags & 0x8000) // 0x8000
    fc = (flags & 0x4000) // 0x4000
    add_iter(model, "Flags (c, s)", "%#04x (%d, %d)" % (flags, fs, fc), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    if fs == 1:
        RGBA(page, model, data[0xc:], 0xc, "Brush Clr")
    else:
        add_iter(model, "Brush ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    add_iter(model, "StartAngle", struct.unpack("<f", data[0x10:0x14])[0], 0x10, 4, "<f")
    add_iter(model, "SweepAngle", struct.unpack("<f", data[0x14:0x18])[0], 0x14, 4, "<I")
    if fc == 1:  # 2bytes rect
        RectS(model, data, 0x18 + i * 8, "Rect ")
    else: # 4bytes rect
        RectF(model, data, 0x18 + i * 16, "Rect ")

#0x400A
def FillRects(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fs = (flags & 0x8000) // 0x8000
    fc = (flags & 0x4000) // 0x4000
    add_iter(model, "Flags (c, s)", "%#04x (%d, %d)" % (flags, fs, fc), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    if fs == 1:
        RGBA(page, model, data[0xc:], 0xc, "Brush Clr")
    else:
        add_iter(model, "Brush ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    cnt = struct.unpack("<I", data[0x10:0x14])[0]
    add_iter(model, "Count", "%#02x" % cnt, 0x10, 4, "<I")
    if fc == 1:  # 2bytes rect
        for i in range(cnt):
            RectS(model, data, 0x14 + i * 8, "Rect%d " % i)
    else: # 4bytes rect
        for i in range(cnt):
            RectF(model, data, 0x14 + i * 16, "Rect%d " % i)

#0x400B
def DrawRects(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fc = (flags & 0x4000) // 0x4000
    oid = data[2]
    add_iter(model, "Flags (c, PenID)", "%#04x (%d, %02x)" % (flags, fc, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    cnt = struct.unpack("<I", data[0xc:0x10])[0]
    add_iter(model, "Count", "%#02x" % cnt, 0xc, 4, "<I")
    if fc == 1:  # 2bytes rect
        for i in range(cnt):
            RectS(model, data, 0x10 + i * 8, "Rect%d " % i)
    else: # 4bytes rect
        for i in range(cnt):
            RectF(model, data, 0x10 + i * 16, "Rect%d " % i)

#0x400C
def FillPolygon(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fs = (flags & 0x8000) // 0x8000
    fc = (flags & 0x4000) // 0x4000
    fp = (flags & 0x800) // 0x800
    add_iter(model, "Flags (s, c, p)", "%#04x (%d, %d, %d)" % (flags, fs, fc, fp), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    if fs == 1:
        RGBA(page, model, data[0xc:], 0xc, "Brush Clr")
    else:
        add_iter(model, "Brush ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    cnt = struct.unpack("<I", data[0x10:0x14])[0]
    add_iter(model, "Count", "%#02x" % cnt, 0x10, 4, "<I")
    if fp == 0:
        if fc == 0:
            for i in range(cnt):
                PointF(model, data, 0x14 + i * 8, "    Abs Pnt%s " % i)
        else:
            for i in range(cnt):
                PointS(model, data, 0x14 + i * 4, "    Abs Pnt%s " % i)
    else:
        offset = 0x14
        for i in range(cnt):
            offset += PointR(model, data, offset, "    Rel Pnt%s " % i)

#0x400D
def DrawLines(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fc = (flags & 0x4000) // 0x4000
    fl = (flags & 0x2000) // 0x2000
    fp = (flags & 0x800) // 0x800
    oid = data[2]
    add_iter(model, "Flags (c, l, p, PenID)", "%#04x (%d, %d, %d, %02x)" % (flags, fc, fl, fp, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    cnt = struct.unpack("<I", data[0xc:0x10])[0]
    add_iter(model, "Count", "%#02x" % cnt, 0xc, 4, "<I")
    if fp == 0:
        if fc == 0:
            for i in range(cnt):
                PointF(model, data, 0x10 + i * 8, "    Abs Pnt%s " % i)
        else:
            for i in range(cnt):
                PointS(model, data, 0x10 + i * 4, "    Abs Pnt%s " % i)
    else:
        offset = 0x10
        for i in range(cnt):
            offset += PointR(model, data, offset, "    Rel Pnt%s " % i)

#0x400E
def FillEllipse(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fs = (flags & 0x8000) // 0x8000
    fc = (flags & 0x4000) // 0x4000
    add_iter(model, "Flags (s, c)", "%#04x (%d, %d)" % (flags, fs, fc), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    if fs == 1:
        RGBA(page, model, data[0xc:], 0xc, "Brush Clr")
    else:
        add_iter(model, "Brush ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    if fc == 1:  # 2bytes rect
        RectS(model, data, 0x10 + i * 8, "Rect ")
    else: # 4bytes rect
        RectF(model, data, 0x10 + i * 16, "Rect ")

#0x400F
def DrawEllipse(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fc = (flags & 0x4000) // 0x4000
    oid = data[2]
    add_iter(model, "Flags (c, PenID)", "%#04x (%d, %02x)" % (flags, fc, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    if fc == 1:  # 2bytes rect
        RectS(model, data, 0xc + i * 8, "Rect ")
    else: # 4bytes rect
        RectF(model, data, 0xc + i * 16, "Rect ")

#0x4011
def DrawPie(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fc = (flags & 0x4000) // 0x4000
    oid = data[2]
    add_iter(model, "Flags (c, PenID)", "%#04x (%d, %02x)" % (flags, fc, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "StartAngle", struct.unpack("<f", data[0xc:0x10])[0], 0xc, 4, "<f")
    add_iter(model, "SweepAngle", struct.unpack("<f", data[0x10:0x14])[0], 0x10, 4, "<I")
    if fc == 1:  # 2bytes rect
        RectS(model, data, 0x14 + i * 8, "Rect ")
    else: # 4bytes rect
        RectF(model, data, 0x14 + i * 16, "Rect ")

#0x4012
def DrawArc(page, model, options, data):
    DrawPie(page, model, data)

#0x4013
def FillRegion(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fs = (flags & 0x8000) // 0x8000
    oid = data[2]
    add_iter(model, "Flags (s, RegionID)", "%#04x (%d, %02x)" % (flags, fs, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    if fs == 1:
        RGBA(page, model, data, 0xc, "Brush Clr")
    else:
        add_iter(model, "Brush ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")

#0x4014
def FillPath(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fs = (flags & 0x8000) // 0x8000
    oid = data[2]
    add_iter(model, "Flags (s, PathID)", "%#04x (%d, %02x)" % (flags, fs, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    if fs == 1:
        RGBA(page, model, data, 0xc, "Brush Clr")
    else:
        add_iter(model, "Brush ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")

#0x4015
def DrawPath(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    oid = data[2]
    add_iter(model, "Flags (PathID)", "%#04x (%02x)" % (flags, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "Pen ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")

#0x4016
def FillClosedCurve(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fs = (flags & 0x8000) // 0x8000
    fc = (flags & 0x4000) // 0x4000
    fw = (flags & 0x2000) // 0x2000
    fp = (flags & 0x800) // 0x800
    add_iter(model, "Flags (s, c, w, p)", "%#04x (%d, %d, %d, %d)" % (flags, fs, fc, fw, fp), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    if fs == 1:
        RGBA(page, model, data[0xc:], 0xc, "Brush Clr")
    else:
        add_iter(model, "Brush ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    add_iter(model, "Tension", struct.unpack("<f", data[0x10:0x14])[0], 0x10, 4, "<f")
    cnt = struct.unpack("<I", data[0x14:0x18])[0]
    add_iter(model, "Count", "%#02x" % cnt, 0x14, 4, "<I")
    if fp == 0:
        if fc == 0:
            for i in range(cnt):
                PointF(model, data, 0x18 + i * 8, "    Abs Pnt%s " % i)
        else:
            for i in range(cnt):
                PointS(model, data, 0x18 + i * 4, "    Abs Pnt%s " % i)
    else:
        offset = 0x18
        for i in range(cnt):
            offset += PointR(model, data, offset, "    Rel Pnt%s " % i)

#0x4017
def DrawClosedCurve(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fc = (flags & 0x4000) // 0x4000
    fp = (flags & 0x800) // 0x800
    oid = data[2]
    add_iter(model, "Flags (c, p, PenID)", "%#04x (%d, %d, %02x)" % (flags, fc, fp, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "Tension", struct.unpack("<f", data[0xc:0x10])[0], 0xc, 4, "<f")
    cnt = struct.unpack("<I", data[0x10:0x14])[0]
    add_iter(model, "Count", "%#02x" % cnt, 0x10, 4, "<I")
    if fp == 0:
        if fc == 0:
            for i in range(cnt):
                PointF(model, data, 0x14 + i * 8, "    Abs Pnt%s " % i)
        else:
            for i in range(cnt):
                PointS(model, data, 0x14 + i * 4, "    Abs Pnt%s " % i)
    else:
        offset = 0x14
        for i in range(cnt):
            offset += PointR(model, data, offset, "    Rel Pnt%s " % i)

#0x4018
def DrawCurve(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fc = (flags & 0x4000) // 0x4000
    oid = data[2]
    add_iter(model, "Flags (c, PenID)", "%#04x (%d, %02x)" % (flags, fc, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "Tension", struct.unpack("<f", data[0xc:0x10])[0], 0xc, 4, "<f")
    add_iter(model, "Offset", struct.unpack("<I", data[0x10:0x14])[0], 0x10, 4, "<I")
    add_iter(model, "NumSegments", struct.unpack("<I", data[0x14:0x18])[0], 0x14, 4, "<I")
    cnt = struct.unpack("<I", data[0x18:0x1c])[0]
    add_iter(model, "Count", "%#02x" % cnt, 0x18, 4, "<I")
    if fc == 1:  # 2bytes rect
        for i in range(cnt):
            PointS(model, data, 0x1c + i * 8, "Point%d " % i)
    else: # 4bytes rect
        for i in range(cnt):
            PointF(model, data, 0x1c + i * 16, "Point%d " % i)

#0x4019
def DrawBeziers(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fc = (flags & 0x4000) // 0x4000
    fp = (flags & 0x800) // 0x800
    oid = data[2]
    add_iter(model, "Flags (c, p, PenID)", "%#04x (%d, %d, %02x)" % (flags, fc, fp, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    cnt = struct.unpack("<I", data[0xc:0x10])[0]
    add_iter(model, "Count", "%#02x" % cnt, 0xc, 4, "<I")
    if fp == 0:
        if fc == 0:
            for i in range(cnt):
                PointF(model, data, 0x14 + i * 8, "    Abs Pnt%s " % i)
        else:
            for i in range(cnt):
                PointS(model, data, 0x14 + i * 4, "    Abs Pnt%s " % i)
    else:
        offset = 0x14
        for i in range(cnt):
            offset += PointR(model, data, offset, "    Rel Pnt%s " % i)

#0x401A
def DrawImage(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fc = (flags & 0x4000) // 0x4000
    oid = data[2]
    add_iter(model, "Flags (c, ImageID)", "%#04x (%d, %02x)" % (flags, fc, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "ImgAttrID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    utype = struct.unpack("<I", data[0x10:0x14])[0]
    ut = "unknown"
    if utype in UnitType:
        ut = UnitType[utype]
    add_iter(model, "SrcUnit", "%#02x (%s)" % (utype, ut), 0x10, 4, "<I")
    RectF(model, data, 0x14, "SrcRect ")
    if fc == 1:  # 2bytes rect
        RectS(model, data, 0x24 + i * 8, "Rect ")
    else: # 4bytes rect
        RectF(model, data, 0x24 + i * 16, "Rect ")

#0x401B
def DrawImagePoints(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fc = (flags & 0x4000) // 0x4000
    fe = (flags & 0x2000) // 0x2000
    fp = (flags & 0x800) // 0x800
    oid = data[2]
    add_iter(model, "Flags (c, e, p, ImageID)", "%#04x (%d, %d, %d, %02x)" % (flags, fc, fe, fp, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "ImgAttrID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    utype = struct.unpack("<I", data[0x10:0x14])[0]
    ut = "unknown"
    if utype in UnitType:
        ut = UnitType[utype]
    add_iter(model, "SrcUnit", "%#02x (%s)" % (utype, ut), 0x10, 4, "<I")
    RectF(model, data, 0x14, "SrcRect ")
    cnt = struct.unpack("<I", data[0x24:0x28])[0]
    add_iter(model, "Count", "%#02x" % cnt, 0x24, 4, "<I")
    if fp == 0:
        if fc == 0:
            for i in range(cnt):
                PointF(model, data, 0x28 + i * 8, "    Abs Pnt%s " % i)
        else:
            for i in range(cnt):
                PointS(model, data, 0x28 + i * 4, "    Abs Pnt%s " % i)
    else:
        offset = 0x28
        for i in range(cnt):
            offset += PointR(model, data, offset, "    Rel Pnt%s " % i)

#0x401C
def DrawString(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fs = (flags & 0x8000) // 0x8000
    oid = data[2]
    add_iter(model, "Flags (s, FontID)", "%#04x (%d, %02x)" % (flags, fs, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    if fs == 1:
        RGBA(page, model, data[0xc:], 0xc, "Brush Clr")
    else:
        add_iter(model, "Brush ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    add_iter(model, "Format ID", "%#02x" % struct.unpack("<I", data[0x10:0x14])[0], 0x10, 4, "<I")
    glcnt = struct.unpack("<I", data[0x14:0x18])[0]
    add_iter(model, "  Length", "%d" % glcnt, 0x14, 4, "<I")
    RectF(model, data, 0x18, "LayoutRect ")
    txt = data[0x28:0x28 + glcnt * 2].decode("utf-16")
    add_iter(model, "  String", txt, 0x28, glcnt * 2, "utxt")

#0x401D
def SetRenderingOrigin(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    add_iter(model, "Flags (s, FontID)", "%#04x" % flags, 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "x", "%d" %  struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    add_iter(model, "y", "%d" %  struct.unpack("<I", data[0x10:0x14])[0], 0x10, 4, "<I")

#0x401E
def SetAntiAliasMode(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    smf = (flags & 0xfe) // 2
    sm = "unknown"
    a = flags & 1
    if smf in SmoothingMode:
        sm = SmoothingMode[smf]
    add_iter(model, "Flags (mode, a)", "%#04x (%s, %d)" % (flags, sm, a), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x401F
def SetTextRenderingHint(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    trh = "unknown"
    if flags in TextRenderingHint:
        trh = TextRenderingHint[flags]
    add_iter(model, "Flags (Txt Rendr hint)", "%#04x (%s)" % (flags, trh), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x4020
def SetTextContrast(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    gamma = (flags & 0xFFF) // 1000
    add_iter(model, "Flags (gamma)", "%#04x (%d)" % (flags, gamma), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x4021
def SetInterpolationMode(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    imode = data[2]
    imt = "unknown"
    if imode in InterpolationMode:
        imt = InterpolationMode[imode]
    add_iter(model, "Flags (IntrpMode)", "%#04x (%s)" % (flags, imt), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x4022
def SetPixelOffsetMode(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    imode = data[2]
    imt = "unknown"
    if imode in PixelOffsetMode:
        imt = PixelOffsetMode[imode]
    add_iter(model, "Flags (PxlOffsetMode)", "%#04x (%s)" % (flags, imt), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x4023
def SetCompositingMode(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    imode = data[2]
    imt = "unknown"
    if imode in CompositingMode:
        imt = CompositingMode[imode]
    add_iter(model, "Flags (ComposMode)", "%#04x (%s)" % (flags, imt), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x4024
def SetCompositingQuality(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    imode = data[2]
    imt = "unknown"
    if imode in CompositingQuality:
        imt = CompositingQuality[imode]
    add_iter(model, "Flags (ComposQly)", "%#04x (%s)" % (flags, imt), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x4025
def Save(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    add_iter(model, "Flags", "%#04x" % flags, 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "StackIdx", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")

#0x4026
def Restore(page, model, options, data):
    Save(page, model, data)

#0x4027
def BeginContainer(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    utype = data[3]
    ut = "unknown"
    if utype in UnitType:
        ut = UnitType[utype]
    add_iter(model, "Flags (PgUnit)", "%#04x (%s)" % (flags, ut), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    RectF(model, data, 0xc, "DestRect ")
    RectF(model, data, 0x1c, "SrcRect ")
    add_iter(model, "StackIdx", "%#02x" % struct.unpack("<I", data[0x2c:0x30])[0], 0x2c, 4, "<I")

#0x4028
def BeginContainerNoParams(page, model, options, data):
    Save(page, model, data)

#0x4029
def EndContainer(page, model, options, data):
    Save(page, model, data)

#0x402A
def SetWorldTransform(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    add_iter(model, "Flags", "%#04x" % flags, 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    XFormMtrx(page, model, data, 0xc)

#0x402B
def ResetWorldTransform(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    add_iter(model, "Flags", "%#04x" % flags, 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x402C
def MultiplyWorldTransform(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fa = (flags & 0x2000) // 0x2000
    add_iter(model, "Flags (a)", "%#04x (%d)" % (flags, fa), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    XFormMtrx(page, model, data, 0xc)

#0x402D
def TranslateWorldTransform(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fa = (flags & 0x2000) // 0x2000
    add_iter(model, "Flags (a)", "%#04x (%d)" % (flags, fa), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "Dx", "%.2f" % struct.unpack("<f", data[0xc:0x10])[0], 0xc, 4, "<f")
    add_iter(model, "Dy", "%.2f" % struct.unpack("<f", data[0x10:0x14])[0], 0xc, 4, "<f")

#0x402E
def ScaleWorldTransform(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fa = (flags & 0x2000) // 0x2000
    add_iter(model, "Flags (a)", "%#04x (%d)" % (flags, fa), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "Sx", "%.2f" % struct.unpack("<f", data[0xc:0x10])[0], 0xc, 4, "<f")
    add_iter(model, "Sy", "%.2f" % struct.unpack("<f", data[0x10:0x14])[0], 0xc, 4, "<f")

#0x402F
def RotateWorldTransform(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fa = (flags & 0x2000) // 0x2000
    add_iter(model, "Flags (a)", "%#04x (%d)" % (flags, fa), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "Angle", "%.2f" % struct.unpack("<f", data[0xc:0x10])[0], 0xc, 4, "<f")

#0x4030
def SetPageTransform(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    utype = data[3]
    ut = "unknown"
    if utype in UnitType:
        ut = UnitType[utype]
    add_iter(model, "Flags (UnitType)", "%#04x (%s)" % (flags, ut), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "Page Scale", "%.2f" % struct.unpack("<f", data[0xc:0x10])[0], 0xc, 4, "<f")

#0x4031
def ResetClip(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    add_iter(model, "Flags", "%#04x" % flags, 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x4032
def SetClipRect(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    combmode = (flags & 0xF00) // 256
    cmt = "unknown"
    if combmode in CombineMode:
        cmt = CombineMode[combmode]
    add_iter(model, "Flags (CM)", "%#04x (%02x (%s))" % (flags, combmode, cmt), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    RectF(model, data, 0xc, "ClipRect ")

#0x4033
def SetClipPath(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    combmode = (flags & 0xF00) // 256
    oid = data[2]
    cmt = "unknown"
    if combmode in CombineMode:
        cmt = CombineMode[combmode]
    add_iter(model, "Flags (CM, PathID)", "%#04x (%02x (%s), %02x)" % (flags, combmode, cmt, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x4034
def SetClipRgn(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    combmode = (flags & 0xF00) // 256
    oid = data[2]
    cmt = "unknown"
    if combmode in CombineMode:
        cmt = CombineMode[combmode]
    add_iter(model, "Flags (CM, RgnID)", "%#04x (%02x (%s), %02x)" % (flags, combmode, cmt, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x4035
def OffsetClip(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    add_iter(model, "Flags", "%#04x" % flags, 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    PointF(model, data, 0xc, "D")

#0x4036
def DrawDriverString(page, model, options, data):
    flags = struct.unpack("<H", data[2:4])[0]
    fs = (flags & 0x8000) // 0x8000
    oid = data[2]
    add_iter(model, "Flags (d, FontID)", "%#04x (%d, %02x)" % (flags, fs, oid), 2, 2, "<H")
    add_iter(model, "Size", "%#02x" % struct.unpack("<I", data[4:8])[0], 4, 4, "<I")
    add_iter(model, "Data Size", "%#02x" % struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

    if fs == 1:
        RGBA(page, model, data[0xc:], 0xc, "Brush Clr")
    else:
        add_iter(model, "Brush ID", "%#02x" % struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    dsof = struct.unpack("<I", data[0x10:0x14])[0]
    dsot = ""
    flags = dsof
    fcm = dsof & 1
    for i in range(5):
        if flags & 1:
            dsot += DrvStrOptFlags[i]
        flags //= 2
    add_iter(model, "  DrvStringOpt Flags", "%#04x (%s)" % (dsof, dsot), 0x10, 4, "<I")
    mp = struct.unpack("<I", data[0x14:0x18])[0]
    add_iter(model, "  Matrix Present", "%d" % mp, 0x14, 4, "<I")
    glcnt = struct.unpack("<I", data[0x18:0x1c])[0]
    add_iter(model, "  Glyph Count", "%d" % glcnt, 0x18, 4, "<I")
    glyphs = data[0x1c:0x1c + glcnt * 2]
    # FIXME! fcm == 1 -- unicode, fcm == 0 -- indexes to glyphs in the font referred by FontID above
    txt = glyphs.decode("utf-16")
    add_iter(model, "  Glyphs", txt, 0x1c, glcnt * 2, "utxt")
    for i in range(glcnt):
        PointF(model, data, 0x1c + glcnt * 2 + i * 8, "Glyph%d Pos " % i)
    if mp:
        XFormMtrx(page, model, data, 0x1c + glcnt * 10)

bdf_ids = {
    0: RND_Path, # will never be used
    #1: BDF_Transform,
    2: BDF_PresetColors,
    #3: BDF_BlendFactorsH,
    #4: BDF_BlendFactorsV,
    #5: BDF_FocusScales,
    #6: BDF_IsGammaCorrected,
    #7: BDF_DoNotTransform
}

pdf_ids = {0: PDF_Xform, 1: PDF_StartCap, 2: PDF_EndCap, 3: PDF_Join, 4: PDF_MiterLimit,
            5: PDF_LineStyle, 6: PDF_DashedLineCap, 7: PDF_DashedLineOffset, 8: PDF_DashedLine,
            9: PDF_NonCenter, 10: PDF_CompoundLine, 11: PDF_CustomStartCap, 12: PDF_CustomEndCap}

bt_ids = {0: BT_SolidColor, 1: BT_HatchFill, 2: BT_TextureFill, 3: BT_PathGradient, 4: BT_LinearGradient}

rnd_ids = {0: RND_Child, 1: RND_Child, 2: RND_Child, 3: RND_Child, 4: RND_Child, 5: RND_Rect,
            6: RND_Path, 7: RND_Empty, 8: RND_Empty}

obj_ids = {1: ObjBrush, 2: ObjPen, 3: ObjPath, 4: ObjRegion, 5: ObjImage, 6: ObjFont,
            7: ObjStringFormat, 8: ObjImgAttr, 9: ObjCustLineCap}

emfplus_ids = {0x4001: Header, 0x4002: EOF, 0x4004: GetDC, # 0x4003: "Comment"
    # 0x4005: "MultiFormatStart", 0x4006: "MultiFormatSection", 0x4007: "MultiFormatEnd", -- MUST NOT BE USED
    0x4008: Object, 0x4009: Clear, 0x400A: FillRects, 0x400B: DrawRects, 0x400C: FillPolygon,
    0x400D: DrawLines, 0x400E: FillEllipse, 0x400F: DrawEllipse, 0x4010:  FillPie, 0x4011: DrawPie,
    0x4012: DrawArc, 0x4013: FillRegion, 0x4014: FillPath, 0x4015: DrawPath, 0x4016: FillClosedCurve,
    0x4017: DrawClosedCurve, 0x4018: DrawCurve, 0x4019: DrawBeziers, 0x401A: DrawImage,
    0x401B: DrawImagePoints, 0x401C: DrawString, 0x401D: SetRenderingOrigin, 0x401E: SetAntiAliasMode,
    0x401F: SetTextRenderingHint, 0x4020: SetTextContrast, 0x4021: SetInterpolationMode,
    0x4022: SetPixelOffsetMode, 0x4023: SetCompositingMode, 0x4024: SetCompositingQuality,
    0x4025: Save, 0x4026: Restore,0x4027: BeginContainer, 0x4028: BeginContainerNoParams,
    0x4029: EndContainer, 0x402A: SetWorldTransform, 0x402B: ResetWorldTransform, 0x402C: MultiplyWorldTransform,
    0x402D: TranslateWorldTransform, 0x402E: ScaleWorldTransform, 0x402F: RotateWorldTransform, 0x4030: SetPageTransform,
    0x4031: ResetClip, 0x4032: SetClipRect, 0x4033 :SetClipPath, 0x4034: SetClipRgn, 0x4035: OffsetClip,
    0x4036: DrawDriverString,
    #0x4037: "StrokeFillPath", #0x4038: "SerializableObject", 0x4039: "SetTSGraphics", 0x403A: "SetTSClip"
}

# END
