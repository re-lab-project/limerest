# (c) frob et all 2007-2021
# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import struct
import zlib

from gi.repository import GdkPixbuf

from utils import *

ri = {0: "Per", 1: "Rel.clr", 2: "Sat", 3: "Abs.clr"}

clr_models = {0: "Invalid", 1: "PANTONE", 2: "CMYK", 3: "CMYK255", 4: "CMY",
    5: "RGB", 6: "HSB", 7: "HLS", 8: "BW", 9: "Gray", 10: "YIQ255", 11: "YIQ",
    12: "LAB", 14: "PANTONE Hex", 17: 'CMYK', #? verify
    18: 'LAB', 20: 'Registration Color', 21: "Roland/PANTONE", 22: "User ink",
    25: "Spot", 26: "Multi-channel",    99: "Mixed"
    }

bmp_clr_models = ('Invalid', 'RGB', 'CMY', 'CMYK255', 'HSB', 'Gray', 'Mono',
    'HLS', 'PAL8', 'Unknown9', 'RGB', 'LAB')

outl_corn_type =('Normal', 'Rounded', 'Cant')

outl_caps_type =('Normal', 'Rounded', 'Out Square')

fild_types = {0: 'Transparent', 1: 'Solid', 2: 'Gradient', 6: 'Postscript',
    7: 'Pattern', 9: 'Bitmap', 10: "Full colour", 11: 'Texture'}

fild_grad_type = ('Unknown', 'Linear', 'Radial', 'Conical', 'Squared')

grad_subtypes = {0: "Line", 1: "CW", 2: "CCW", 3: "Custom"}

wrap_txt_style = {0: "Contour Txt Flow Left", 1: "Contour Txt Flow Right",
    2: "Contour Straddle Txt", 3: "Square Txt Flow Left",
    4: "Square Txt Flow Right", 5: "Square Straddle Txt", 6: "Square Above/Below"
    }

charsets = {0: "Latin", 1: "System default", 2: "Symbol", 77: "Apple Roman",
    128: "Japanese Shift-JIS", #cp932
    129: "Korean (Hangul)", #cp949
    130: "Korean (Johab)", #cp1361
    134: "Chinese Simplified GBK", #cp936
    136: "Chinese Traditional BIG5", #cp950
    161: "Greek", #cp1253
    162: "Turkish", #cp1254
    163: "Vietnamese", #cp1258
    177: "Hebrew", #cp1255
    178: "Arabic", #cp1256
    186: "Baltic", #cp1257
    204: "Cyrillic", #cp1251
    222: "Thai", #cp874
    238: "Latin II (Central European)", #cp1250
    255: "OEM Latin I"
    }

langids = {1025: "Arabic", 1069: "Basque", 1027: "Catalan", 2052: "Chinese (Simplified)",
    1028: "Chinese (Traditional)", 1029: "Czech", 1030: "Danish", 1043: "Dutch",
    1033: "English (United States)", 1035: "Finnish", 1036: "French", 1031: "German",
    1032: "Greek", 1037: "Hebrew", 1038: "Hungarian", 1040: "Italian", 1041: "Japanese",
    1042: "Korean", 1044: "Norwegian", 1045: "Polish", 2070: "Portuguese",
    1046: "Portuguese (Brazil)", 1049: "Russian", 1051: "Slovakian", 1060: "Slovenian",
    3082: "Spanish", 1053: "Swedish", 1055: "Turkish",
    }

corner_types = {0: "Round", 1: "Scalloped", 2: "Clamfered"}

style_ids = {0: "Fild", 1: "Outl", 2: "Font", 3: "Alignment", 4: "Intervals", 5: "Set5s",
        6: "Set11s", 7: "Tabs", 8: "Bullets", 9: "Indents", 10: "Hypens", 11: "Dropcaps"}

nodetypes = {2: "\tChar. start", 4: "\tCan modify", 8: "\tClosed path", 0x10: "\tSmooth", 0x20: "\tSymmetric"}

lcv3styles = {6: "Underline", 4: "Bold", 5: "Italic", 7: "Bold/Italic", 9: "Superscript", 8: "Subscript"}

lcv4styles = {1: "Underline", 2: "Bold", 4: "Italic", 8: "Bold/Italic", 0x11: "Superscript", 0x21: "Subscript"}

lens1_subtypes = {0: "Opacity", 1: "Colour Limit", 2: "Colour Add", 3: "Inverse", 4: "Brighten",
    5: "Tinted Greyscale", 7: "Heat Map", 8: "Custom Colour Map"}

lens5_types = {0: "Uniform", 1: "Linear grad", 2: "Radial grad", 3: "Conical grad", 4: "Square grad",
        7: "Two Colour Patt", 9: "Bitmap Patt", 0xa: "Full Colour Patt", 0xb: "Texture"}

lens5_ops = {0: "Normal", 1: "And", 2: "Or", 3: "Xor", 6: "Invert", 7: "Add",
        8: "Sub", 9: "Mult", 0xa: "Div", 0xb: "If lighter", 0xc: "If darker",
        0xd: "Texturize", 0xf: "Hue", 0x10: "Sat", 0x11: "Lightness",
        0x12: "Red", 0x13: "Green", 0x14: "Blue", 0x18: "Diff"}

lens5_targets = {0: "Fill", 1:"Outl", 2:"All"}

styd_types = {0xc8: "Name", 0xcd: "Fild ID", 0xd2: "Outl ID", 0xdc: "Fonts", 0xe1: "Align", # FIXME! verify with non0
        0xe6: "Bullets", 0xeb: "Intervals", 0xf0: "Tabs", 0xf5: "Idents", 0xfa: "Hypens", 0xff: "Set5s", 0x104: "Dropcaps"}

loda_types = {0: "Layer", 1: "Rectangle", 2: "Ellipse", 3: "Line/Curve", 4: "Artistic Text",
        5: "Bitmap", 6: "Paragraph Text", 0xa: "OLE", 0xb: "Grid", 0xc: "Guides",
        0x10: "Style", #?? ver6-
        0x11: "Desktop", 0x14: "Polygon", 0x20: "Mesh", 0x25: "Path ??", 0x26: "B-Spline"}

loda_types_v3 = {0: "Rectangle", 1: "Ellipse", 2: "Line/Curve", 3: "Text", 4: "Bitmap", # guess
        0xa: "Grid"}

dtypes = {1: "Push", 2:"Zip", 3:"Twist"}

dstflags = {0:"None", 1:"Smooth", 2:"Random", 4:"Local"}

txsm56style = {0x01: "Font ID", 0x02: "Text decoration", 0x04: "Font size", 0x10: "Fill", 0x20: "Outline"}

# -----------------------------------------------------------------------------------------------
def clr_model(page, model, data, offset):
    cmid = struct.unpack('<H', data[offset:offset + 2])[0]
    cmod = "%02x  " % cmid
    if cmid in clr_models:
        cmod += clr_models[cmid]
    cpal = struct.unpack('<H', data[offset + 2:offset + 4])[0]
    add_iter(model, "Color Model", cmod, offset, 2, "<H")
    add_iter(model, "Color Palette", cpal, offset + 2, 2, "<H")
    clr = d2hex(data[offset + 8:offset + 12])
    add_iter(model, "  Color", clr, offset + 8, 4, "<I")

def readfrac(data):
    intp = struct.unpack("<H", data[2:4])[0]
    frp =  struct.unpack("<H", data[0:2])[0] / 0xffff
    return intp + frp

# -----------------------------------------------------------------------------------------------

def arrw(page, model, options, data):
    add_iter (model, "Arrow ID", "%02x" % struct.unpack('<I', data[0:4])[0], 0, 4, "<I")
    add_iter (model, "???", "%02x" % struct.unpack('<I', data[4:8])[0], 4, 4, "<I")
    pnum = struct.unpack('<H', data[8:10])[0]
    add_iter (model, "#ofPts", "%02x" % pnum, 8, 2, "<H")
    coff = 8 + struct.unpack('<I', data[10:14])[0]
    add_iter (model, "Pnt Types", "", 14, pnum, "txt")
    for i in range(pnum):
        x = struct.unpack('<l', data[coff + i * 8:coff + 4 + i * 8])[0]
        y = struct.unpack('<l', data[coff + 4 + i * 8:coff + 8 + i * 8])[0]
        Type = data[14+i]
        NodeType = ''
        if Type & 2 == 2:
            NodeType = '    Char. start'
        if Type & 4 == 4:
            NodeType = NodeType+'  Can modify'
        if Type & 8 == 8:
            NodeType = NodeType+'  Closed path'
        if Type & 0x10 == 0 and Type&0x20 == 0:
            NodeType = NodeType+'  Discontinued'
        if Type & 0x10 == 0x10:
            NodeType = NodeType+'  Smooth'
        if Type & 0x20 == 0x20:
            NodeType = NodeType+'  Symmetric'
        if Type & 0x40 == 0 and Type & 0x80 == 0:
            NodeType = NodeType+'  START'
        if Type & 0x40 == 0x40 and Type & 0x80 == 0:
            NodeType = NodeType+'  Line'
        if Type & 0x40 == 0 and Type & 0x80 == 0x80:
            NodeType = NodeType+'  Curve'
        if Type & 0x40 == 0x40 and Type & 0x80 == 0x80:
            NodeType = NodeType + '  Arc'
        add_iter(model, "X%u/Y%u/Type" % (i + 1, i + 1), "%u/%u mm" % (round(x / 10000.0, 2), round(y / 10000.0, 2)) + NodeType, coff + i * 8, 8, "txt")

def bbox(page, model, options, data):
    offset = 0
    if page.doc_data["version"] < 6:
        for i in range(2):
            varX = struct.unpack('<h', data[offset + i * 4:offset + 2 + i * 4])[0]
            varY = struct.unpack('<h', data[offset + 2 + i * 4:offset + 4 + i * 4])[0]
            add_iter(model, "X%u" % i, "%g in" % round(varX / 1000, 2), offset + i * 2, 2, "<h")
            add_iter(model, "Y%u" % i, "%g in" % round(varY / 1000, 2), offset + i * 2 + 2, 2, "<h")
    else:
        for i in range(2):
            varX = struct.unpack('<l', data[offset + i * 8:offset + 4 + i * 8])[0]
            varY = struct.unpack('<l', data[offset + 4 + i * 8:offset + 8 + i * 8])[0]
            add_iter (model, "X%u" % i, "%u mm" % round(varX / 10000.0, 2), offset + i * 8, 4, "<l")
            add_iter (model, "Y%u" % i, "%u mm" % round(varY / 10000.0, 2), offset + 4 + i * 8, 4, "<l")

def bmp(page, model, options, data):
    add_iter(model, "Image ID", struct.unpack('<I', data[0:4])[0], 0, 4, "txt")
    add_iter(model, "Hdr 1", "", 4, 0x24, "txt")
    hlen = struct.unpack('<I', data[0x32:0x36])[0]
    add_iter (model, "Hdr 2", "", 0x28, hlen, "txt")
    add_iter (model, "\tHdr 2 size", hlen, 0x32, 4, "<I")
    pal = struct.unpack('<I', data[0x36:0x3a])[0]
    bplt = "Unknown"
    if pal < 12:
        bplt = bmp_clr_models[pal]
    add_iter (model, "\tPallete (%02x) %s" % (pal, bplt), struct.unpack('<I', data[0x36:0x3a])[0], 0x36, 4, "<I")
    shift = 0
    if page.doc_data["version"] < 7:
        shift = 4
    imgw = struct.unpack('<I', data[0x3e - shift:0x42 - shift])[0]
    add_iter(model, "\tWidth", imgw, 0x3e - shift, 4, "<I")
    imgh = struct.unpack('<I', data[0x42 - shift:0x46 - shift])[0]
    add_iter(model, "\tHeight", imgh, 0x42 - shift, 4, "<I")
    bpp = struct.unpack('<I', data[0x4a - shift:0x4e - shift])[0]
    add_iter(model, "\tBPP", bpp, 0x4a - shift, 4, "<I")
    # for bpp = 1, cdr aligns by 4 bytes, bits after width are crap
    bmpsize = struct.unpack('<I', data[0x52 - shift:0x56 - shift])[0]
    palsize = 0
    add_iter (model, "\tSize of BMP data", bmpsize, 0x52 - shift, 4, "<I")
    if bpp < 24 and pal != 5 and pal != 6:
        palsize = struct.unpack('<H', data[0x78:0x7a])[0]
        add_iter(model, "Palette Size", palsize, 0x78, 2, "<H")
        add_iter(model, "Palette", "", 0x7a, palsize * 3, "txt")
        add_iter(model, "BMP data", "", 0x7a + palsize * 3, bmpsize, "<I")
        bmpoff = 0x7a + palsize * 3
    else:
        add_iter(model, "BMP data", "", 0x76 - shift, bmpsize, "<I")
        bmpoff = 0x76 - shift
    # FIXME! TODO: add pixbuf-related stuff

def bmpf(page, model, options, data):
    add_iter(model, "Pattern ID", d2hex(data[0:4]), 0, 4, "txt")
    bmp = struct.unpack("<I", data[0x18:0x1c])[0]
    bmpoff = struct.pack("<I", len(data) + 10 - bmp)
    # FIXME! TODO: add pixbuf-related stuff

def DISP(page, model, options, data):
    bmp = struct.unpack("<I", data[0x18:0x1c])[0]
    bmpoff = struct.pack("<I", len(data) + 10 - bmp)
    img = b'BM' + struct.pack("<I", len(data) + 8) + b'\x00\x00\x00\x00' + bmpoff + data[4:]
    pixbufloader = GdkPixbuf.PixbufLoader()
    pixbufloader.write(img)
    pixbufloader.close()
#    imgw = pixbuf.get_width()
#    imgh = pixbuf.get_height()
    img = Gtk.Image.new_from_pixbuf(pixbufloader.get_pixbuf())
    page.hv_disp.add(img)
    page.hv_disp.show_all()


def fild(page, model, options, data):
    add_iter(model, "Fill ID", "%08x" % struct.unpack("<I", data[0:4])[0], 0, 4, "<I")
    ftype_off = 4
    if page.doc_data["version"] > 12:
        v13flag = struct.unpack('<h', data[4:6])[0]
        v13offset = 4
        while v13flag > 0:
            add_iter(model, "Flag: %d" % v13offset, d2hex(data[v13offset:v13offset + 8]), v13offset, 10, "txt")
            v13offset += 10
            v13flag = struct.unpack('<h', data[v13offset:v13offset + 2])[0]

        ftype_off = v13offset - 4
        fill_type = struct.unpack('<h', data[0xc:0xe])[0]
    else:
        fill_type = struct.unpack('<h', data[4:6])[0]

    ft_txt = "%d" % fill_type
    if fill_type in fild_types:
        ft_txt += " " + fild_types[fill_type]
    if page.doc_data["version"] > 12:
        add_iter(model, "Fill Type", ft_txt, 0xc, 2, "txt")
    else:
        add_iter(model, "Fill Type", ft_txt, 4, 2, "txt")

    if fill_type > 0:
        if fill_type == 1:
            clrm_off = 8
            if page.doc_data["version"] > 12:
                clrm_off = 0x1b
            clr_model(page, model, data, clrm_off)

        elif fill_type == 2:
            grd_offset = 0x8
            edge_off = 0x1c
            rot_offset = 0x20
            cx_off = 0x24
            cy_off = 0x28
            steps_off = 0x2c
            mode_off = 0x2e
            mid_offset = 0x32
            pal_len = 16
            pal_off = 0
            prcnt_off = 0
            if page.doc_data["version"] > 12:
                grd_offset = 0x16
                edge_off = 0x28
                rot_offset = 0x2a
                cx_off = 0x2e
                cy_off = 0x32
                steps_off = 0x36
                mode_off = 0x38
                mid_offset = 0x3c
                pal_len = 24
                pal_off = 3
                prcnt_off = 8
                if v13flag >= 0x9e or (page.doc_data["version"] > 15 and v13flag >= 0x96):
                    prcnt_off = 29
                    pal_len = 45
            grdmode = data[grd_offset]
            midpoint = data[mid_offset]
            rot = struct.unpack('<l', data[rot_offset:rot_offset + 4])[0]

            if grdmode < len(fild_grad_type):
                gr_type = "%s" % fild_grad_type[grdmode]
            else:
                gr_type = "Unknown (%X)" % clrmode
            add_iter(model, "Gradient type", gr_type, grd_offset, 1, "B")
            add_iter(model, "Edge offset", struct.unpack('<h', data[edge_off:edge_off + 2])[0], edge_off, 2, "<h")
            add_iter(model, "Rotation", rot / 1000000, rot_offset, 4, "<l")
            add_iter(model, "Center X offset", struct.unpack('<i', data[cx_off:cx_off + 4])[0], cx_off, 4, "<i")
            add_iter(model, "Center Y offset", struct.unpack('<i', data[cy_off:cy_off + 4])[0], cy_off, 4, "<i")
            add_iter(model, "Steps", struct.unpack('<H', data[steps_off:steps_off + 2])[0], steps_off, 2, "<H")
            stid = struct.unpack('<H', data[mode_off:mode_off + 2])[0]
            st = grad_subtypes.get(stid) or "Unknown"
            add_iter(model, "Sub-type", st, mode_off, 2, "<H")
            add_iter(model, "Midpoint", midpoint, mid_offset, 1, "B")

            pal_num = data[mid_offset + 2]
            for i in range(pal_num):
                clr_model(page, model, data, mid_offset + 6 + pal_off + i * pal_len)
                prcnt = data[mid_offset + 18 + prcnt_off + i * pal_len]
                add_iter (model, "  Percent", "%u" % prcnt, mid_offset + 18 + prcnt_off + i * pal_len, 1, "B")

        elif fill_type == 6:
            add_iter(model, "PS fill ID", d2hex(data[8:10]), 8, 2, "<H")

        elif fill_type == 7:
            # Pattern
            patt_off = 8
            w_off = 0xc
            h_off = 0x10
            rcp_off = 0x18
            fl_off = 0x1a
            clr1_off = 0x1c
            clr2_off = 0x28
            if page.doc_data["version"] > 12:
                patt_off = 0x16
                w_off = 0x1a
                h_off = 0x1e
                rcp_off = 0x26
                fl_off = 0x28
                clr1_off = 0x2f
                pal_len = 22
                if v13flag == 0x94 or (page.doc_data["version"] > 15 and v13flag == 0x8c):
                    pal_len = 43
                clr2_off = clr1_off + pal_len

            add_iter(model, "Pattern ID", "%08x" % struct.unpack("<I", data[patt_off:patt_off + 4])[0], patt_off, 4, "txt")
            if page.doc_data["version"] < 6:
                w_off = 0xc
                h_off = 0xe
                rcp_off = 0x14 # FIXME!
                fl_off = 0x16 # FIXME!
                clr1_off = 0x18
                clr2_off = 0x24
                add_iter(model, "Width", struct.unpack("<H", data[w_off:w_off + 2])[0] * 0.0254, w_off, 2, "<I")
                add_iter(model, "Height", struct.unpack("<H", data[h_off:h_off +2])[0] * 0.0254, h_off, 2, "<I")
            else:
                add_iter(model, "Width", struct.unpack("<I", data[w_off:w_off + 4])[0] / 10000., w_off, 4, "<I")
                add_iter(model, "Height", struct.unpack("<I", data[h_off:h_off + 4])[0] / 10000., h_off, 4, "<I")
            add_iter(model, "R/C Offset %", data[rcp_off], rcp_off, 1, "B")
            flag = data[fl_off]
            ftxt = bflag2txt(flag, {1: "Column", 2: "Mirror", 4: "Transform with object"})
            add_iter(model, "Flags", "%02x (%s)" % (flag, ftxt), fl_off, 1, "B")

            # Colors (model + color) started at 0x1c and 0x28
            clr_model(page, model, data, clr1_off)
            clr_model(page, model, data, clr2_off)

        elif fill_type == 9:
            # Bitmap pattern fill
            w_off = 0xc
            h_off = 0x10
            rcp_off = 0x18
            fl_off = 0x1a
            patt_off = 0x30
            if page.doc_data["version"] > 12:
                patt_off = 0x16
                w_off = 0x16
                h_off = 0x1a
                rcp_off = 0x22
                fl_off = 0x24
                patt_off = 0x36

            add_iter (model, "Width", struct.unpack("<I", data[w_off:w_off + 4])[0] / 10000., w_off, 4, "<I")
            add_iter (model, "Height", struct.unpack("<I", data[h_off:h_off + 4])[0] / 10000., h_off, 4, "<I")
            add_iter (model, "R/C Offset %", data[rcp_off], rcp_off, 1, "B")
            flag = data[fl_off]
            ftxt = bflag2txt(flag, {1: "Column", 2: "Mirror", 4: "Transform with object"})
            add_iter(model, "Flags", "%02x (%s)" % (flag, ftxt), fl_off, 1, "B")
            add_iter(model, "Image ID", struct.unpack("<I", data[patt_off:patt_off + 4])[0], patt_off, 4, "<I")

        elif fill_type == 10:
            # Full colour pattern
            if page.doc_data["version"] < 6:
                patt_off = 8
                w_off = 0xa
                h_off = 0xc
                add_iter(model, "Image ID",struct.unpack("<H", data[patt_off:patt_off + 2])[0], patt_off, 2, "<H")
                add_iter(model, "Width", struct.unpack("<H", data[w_off:w_off + 2])[0] * 0.0254, w_off, 2, "<H")
                add_iter(model, "Height", struct.unpack("<H", data[h_off:h_off + 2])[0] * 0.0254, h_off, 2, "<H")

            else:
                w_off = 0xc
                h_off = 0x10
                rcp_off = 0x18
                fl_off = 0x1a
                patt_off = 0x30
                if page.doc_data["version"] > 12:
                    w_off = 0x16
                    h_off = 0x1a
                    rcp_off = 0x22
                    fl_off = 0x24
                    patt_off = 0x36
                add_iter(model, "Width", struct.unpack("<I", data[w_off:w_off + 4])[0] / 10000., w_off, 4, "<I")
                add_iter(model, "Height", struct.unpack("<I", data[h_off:h_off + 4])[0] / 10000., h_off, 4, "<I")
                add_iter(model, "R/C Offset %", data[rcp_off], rcp_off, 1, "B")
                flag = data[fl_off]
                ftxt = bflag2txt(flag, {1: "Column", 2: "Mirror", 4: "Transform with object"})
                add_iter(model, "Flags", "%02x (%s)" % (flag, ftxt), fl_off, 1, "B")
                add_iter(model, "Vect ID", struct.unpack("<I", data[patt_off:patt_off + 4])[0], patt_off, 4, "<I")

        elif fill_type == 11:
            # Texture pattern fill
            if page.doc_data["version"] < 6:
                patt_off = 8
                add_iter (model, "Image ID", struct.unpack("<H", data[patt_off:patt_off + 2])[0], patt_off, 2, "<H")

            else:
                v1_off = 0xc
                rcp_off = 0x18
                fl_off = 0x1a
                v2_off = 0x20
                imgid_off = 0x30
                bmpres_off = 0x38
                maxtw_off = 0x3a

                if page.doc_data["version"] > 12:
                    v1_off = 0x1e
                    rcp_off = 0x22
                    fl_off = 0x24
                    v2_off = 0x32
                    imgid_off = 0x3e
                    bmpres_off = 0x4e
                    maxtw_off = 0x50
                    if v13flag == 0x18e:
                        v1_off = 0x36
                        rcp_off = 0x42
                        fl_off = 0x44
                        v2_off = 0x4a
                        imgid_off = 0x56
                        bmpres_off = 0x66
                        maxtw_off = 0x68

                add_iter(model, "Width", struct.unpack("<I", data[v1_off:v1_off + 4])[0] / 10000., v1_off, 4, "<I")
                add_iter(model, "Height", struct.unpack("<I", data[v1_off + 4:v1_off + 8])[0] / 10000., v1_off + 4, 4, "<I")
                add_iter(model, "R/C Offset %", data[rcp_off], rcp_off, 1, "B")
                flag = data[fl_off]
                ftxt = bflag2txt(flag, {1: "Column", 2: "Mirror", 4: "Transform with object"})
                add_iter(model, "Flags", "%02x (%s)" % (flag, ftxt), fl_off, 1, "B")
                add_iter(model, "v3", struct.unpack("<I", data[v2_off:v2_off + 4])[0] / 10000., v2_off, 4, "<I")
                add_iter(model, "v4", struct.unpack("<I", data[v2_off + 4:v2_off + 8])[0] / 10000., v2_off + 4, 4, "<I")
                add_iter(model, "Image ID", struct.unpack("<I",data[imgid_off:imgid_off + 4])[0], imgid_off, 4, "<I")
                add_iter(model, "BMP resolution", struct.unpack("<h", data[bmpres_off:bmpres_off + 2])[0], bmpres_off, 2, "<h")
                add_iter(model, "Max tile width", struct.unpack("<h", data[maxtw_off:maxtw_off + 2])[0], maxtw_off, 2, "<h")

def font(page, model, options, data):
    add_iter(model, "Font ID", "%02x" % struct.unpack('<H', data[0:2])[0], 0, 2, "<H")
    enc = struct.unpack('<H', data[2:4])[0]
    enctxt = charsets.get(enc) or "Unknown"
    shift = 0
    if page.doc_data["version"] > 5:
        add_iter(model, "Encoding", "%s (%02x)" % (enctxt, enc), 2, 2, "<H")
        shift = 2
    add_iter (model, "Flags", d2hex(data[2 + shift:18], " "), 2 + shift, 16 - shift, "txt")
    if page.doc_data["version"] > 11:
        fontname = data[18:52].decode("utf-16")
    else:
        fontname = data[18:52].decode("utf-8")
    add_iter(model, "FontName", fontname, 18, 34, "txt")

def ftil(page, model, options, data):
    for i in range(6):
        [var] = struct.unpack('<d', data[i * 8:i * 8 + 8])
        add_iter(model, 'Var%d' % i, var, i * 8, 8, "<d")

def guid(page, model, options, data):
    # very EXPERIMENTAL
    num1 = struct.unpack('<I', data[0:4])[0]
    num2 = struct.unpack('<I', data[0:4])[0]
    add_iter(model, "Num 1", num1, 0, 4, "<I")
    add_iter(model, "Num 2", num2, 4, 4, "<I")
    offset = 8
    for i in range(num1):
        piter = add_iter(model, "Rec %02x" % i, d2hex(data[offset + i * 40 + 12:offset + i * 40 + 16]), offset + 40 * i, 40, "txt")
        add_iter(model, "\tNums", "%d\t%d" % struct.unpack("<i", data[offset + i * 40 + 16:offset + i * 40 + 20])[0] / 10000, struct.unpack("<i", data[offset + i * 40 + 20:offset + i * 40 + 24])[0] / 10000, offset + 40 * i + 16, 8, "txt", parent=piter)
        add_iter(model, "\tNums", "%d\t%d" % struct.unpack("<i", data[offset + i * 40 + 24:offset + i * 40 + 28])[0] / 10000, struct.unpack("<i", data[offset + i * 40 + 28:offset + i * 40 + 32])[0] / 10000, offset + 40 * i + 16, 8, "txt", parent=piter)
        add_iter(model, "\tNums", "%d\t%d" % struct.unpack("<i", data[offset + i * 40 + 32:offset + i * 40 + 36])[0] / 10000, struct.unpack("<i", data[offset + i * 40 + 36:offset + i * 40 + 40])[0] / 10000, offset + 40 * i + 16, 8, "txt", parent=piter)

def lnkt(page, model, options, data):
    n_args = struct.unpack('<i', data[4:8])[0]
    s_args = struct.unpack('<i', data[8:0xc])[0]
    for j in range(n_args):
        start = struct.unpack('<L', data[s_args + j * 4:s_args + j * 4 + 4])[0]
        add_iter(model, "???", "%02x" % struct.unpack('<L', data[start:start + 4])[0], start, 4, "<I")
        add_iter(model, "spnd ID1", "%08x" % (struct.unpack("<I",data[start + 4:start + 8])[0]),start + 4, 4, "<I")
        add_iter(model, "spnd ID2", d2hex(data[start + 8:start + 12]), start + 8, 4, "<I")

def loda(page, model, options, data, shift=0, ftype=0):
    if page.doc_data["version"] < 6:
        loda_v5(page, model, data, shift, ftype)
        return
    n_args = struct.unpack('<I', data[4:8])[0]
    s_args = struct.unpack('<I', data[8:0xc])[0]
    s_types = struct.unpack('<I', data[0xc:0x10])[0]
    add_iter(model, "# of args", n_args, 4 + shift, 4, "<I")
    add_iter(model, "Start of args offsets", "%02x" % s_args, 8 + shift, 4, "<I")
    add_iter(model, "Start of arg types", "%02x" % s_types, 0xc + shift, 4, "<I")
    l_type = struct.unpack('<I', data[0x10:0x14])[0]
    t_txt = "%02x" % l_type
    if ftype == 0:
        if l_type in loda_types:
            t_txt += " " + loda_types[l_type]
        add_iter(model, "Type", t_txt, 0x10 + shift, 4, "<I")
    else:
        add_iter(model, "Parent ID", "%02x" % l_type, 0x10 + shift, 4, "<I")

    a_txt = ""
    t_txt = ""
    for i in range(n_args, 0, -1):
        a_txt += " %04x" % struct.unpack('<L', data[s_args + i * 4 - 4:s_args + i * 4])[0]
        t_txt += " %04x" % struct.unpack('<L', data[s_types + (n_args - i) * 4:s_types + (n_args - i) * 4 + 4])[0]
    add_iter(model, "Args offsets", a_txt, s_args + shift, n_args * 4, "txt")
    add_iter(model, "Args types", t_txt, s_types + shift, n_args * 4, "txt")

    for i in range(1, n_args + 1):
        offset = struct.unpack('<L', data[s_args + i * 4 - 4:s_args + i * 4])[0]
        length = struct.unpack('<L', data[s_args + i * 4:s_args + i * 4 + 4])[0]-offset
        argtype = struct.unpack('<L', data[s_types + (n_args - i) * 4:s_types + (n_args - i) * 4 + 4])[0]
        if ftype == 0:
            if argtype in loda_type_func:
                loda_type_func[argtype](page, model, data, offset + shift, l_type, length)
            else:
                add_iter(model, "[%04x]" % argtype, "???", offset + shift, struct.unpack('<L', data[s_args + i * 4:s_args + i * 4 + 4])[0] - offset, "txt")
        else:
            if argtype in styd_types:
                add_iter(model, styd_types[argtype], "...", offset + shift, struct.unpack('<L', data[s_args + i * 4:s_args + i * 4 + 4])[0] - offset, "txt")
            else:
                add_iter(model, "[%04x]" % (argtype), "???", offset + shift, struct.unpack('<L', data[s_args + i * 4:s_args + i * 4 + 4])[0] - offset, "txt")

def loda_contnr(page, model, data, offset, l_type, length):
    add_iter(model, "[1f45] Spnd ID", "%08x" % struct.unpack("<I", data[offset:offset + 4])[0], offset, 4, "txt")

def loda_coords124(page, model, data, offset, l_type):
    # rectangle or ellipse or text
    if page.doc_data["version"] > 14:
        x1 = round(struct.unpack('<d', data[offset:offset + 8])[0] / 10000, 2)
        y1 = round(struct.unpack('<d', data[offset + 8:offset + 16])[0] / 10000, 2)
#        add_iter(model, "[001e] x1/y1", "%.2f/%.2f mm  (corr %.2f/%.2f)"%(x1,y1,x1+hd.width/2,y1+hd.height/2),offset,16,"txt")
        add_iter(model, "[001e] x1/y1", "%.2f/%.2f mm  " % (x1, y1), offset, 16, "txt")

        if l_type == 1:
            scx = struct.unpack('<d', data[offset + 16:offset + 24])[0]
            scy = struct.unpack('<d', data[offset + 24:offset + 32])[0]
            add_iter(model, "[001e] Scale X/Y", "%.2f %.2f " % (round(scx, 2), round(scy, 2)), offset + 16, 16, "txt")
            for i in range(4):
                Ri = struct.unpack('<d', data[offset + 40 + i * 24:offset + 48 + i * 24])[0]
                crnscale = data[offset + 32 + i * 24]
                cs = "Yes"
                if crnscale:
                    cs = "No"
                    Ri = round(Ri / 10000, 2)
                ctype = data[offset + 48 + i * 24]
                ct = key2txt(ctype, corner_types)

                add_iter(model, "[001e] R%d" % (i + 1), "Scale with shape: %s" % cs, offset + 32 + i * 24, 1, "B")
                add_iter(model, "[001e] R%d" % (i + 1), "%.2f" % Ri, offset + 40 + i * 24, 8, "<d")
                add_iter(model, "[001e] R%d" % (i + 1), "Corner type: %s" % ct, offset + 48 + i * 24, 1, "B")
    elif page.doc_data["version"] < 6:
        x1 = round(struct.unpack('<h', data[offset:offset + 2])[0] * 0.0254, 2)
        y1 = round(struct.unpack('<h', data[offset + 2:offset + 4])[0] * 0.0254, 2)
#        add_iter(model, "[001e] x1/y1","%.2f/%.2f mm  (corr %.2f/%.2f)"%(x1,y1,x1+hd.width/2,y1+hd.height/2),offset,4,"txt")
        add_iter(model, "[001e] x1/y1", "%.2f/%.2f mm  " % (x1, y1), offset, 4, "txt")
    else:
        x1 = round(struct.unpack('<l', data[offset:offset + 4])[0] / 10000, 2)
        y1 = round(struct.unpack('<l', data[offset + 4:offset + 8])[0] / 10000, 2)
#        add_iter(model, "[001e] x1/y1","%.2f/%.2f mm  (corr %.2f/%.2f)"%(x1,y1,x1+hd.width/2,y1+hd.height/2),offset,8,"txt")
        add_iter(model, "[001e] x1/y1", "%.2f/%.2f mm  " % (x1, y1), offset, 8, "txt")

        if l_type == 1:
            R1 = struct.unpack('<l', data[offset + 8:offset + 12])[0]
            R2 = struct.unpack('<l', data[offset + 12:offset + 16])[0]
            R3 = struct.unpack('<l', data[offset + 16:offset + 20])[0]
            R4 = struct.unpack('<l', data[offset + 20:offset + 24])[0]
            add_iter(model, "[001e] R1 R2 R3 R4", "%.2f %.2f %.2f %.2f mm" % (round(R1 / 10000, 2), round(R2 / 10000, 2), round(R3 / 10000, 2), round(R4 / 10000, 2)), offset + 8, 16, "txt")

    if l_type == 2:
        a1 = struct.unpack('<L', data[offset + 8:offset + 12])[0]
        a2 = struct.unpack('<L', data[offset + 12:offset + 16])[0]
        a3 = struct.unpack('<L', data[offset + 16:offset + 20])[0]
        add_iter(model, "[001e] Start/End Rot angles; Pie flag", "%.2f %.2f %.2f" % (round(a1 / 1000000, 2), round(a2 / 1000000, 2), round(a3 / 1000000, 2)), offset + 8, 12, "txt")

def loda_coords3(page, model, data, offset, l_type, lt2="001e"):
    pointnum = struct.unpack('<L', data[offset:offset + 4])[0]
    poffset = 4
    if page.doc_data["version"] < 7:
        pointnum = struct.unpack('<H', data[offset:offset + 2])[0]
        poffset = 4
    if page.doc_data["version"] < 6:
        for i in range (pointnum):
            x = 0.02540164 * struct.unpack('<h', data[offset + poffset + i * 4:offset + poffset + 2 + i * 4])[0]
            y = 0.02540164 * struct.unpack('<h', data[offset + poffset + 2 + i * 4:offset + poffset + 4 + i * 4])[0]
            Type = data[offset + poffset + pointnum * 4 + i]
            ntype = bflag2txt(Type, nodetypes)
            if Type & 0x10 == 0 and Type & 0x20 == 0:
                ntype += '  Discontinued'
            if Type & 0x40 == 0 and Type & 0x80 == 0:
                ntype += '  START'
            if Type & 0x40 and Type & 0x80 == 0:
                ntype += '  Line'
            if Type & 0x40 == 0 and Type & 0x80:
                ntype += '  Curve'
            if Type & 0x40 and Type & 0x80:
                ntype +='  Arc'
#            add_iter(model, "[%s] X%u/Y%u/Type" % (lt2, i + 1, i + 1), "%.2f/%.2f mm  (corr. %.2f/%.2f)" % (x, y , x + hd.width / 2, y + hd.height / 2) + ntype, offset + poffset + i * 4, 4, "txt") #, offset + poffset + pointnum * 4 + i, 1)
            add_iter(model, "[%s] X%u/Y%u/Type" % (lt2, i + 1, i + 1), "%.2f/%.2f mm %s" % (x, y, ntype), offset + poffset + i * 4, 4, "txt") #, offset + poffset + pointnum * 4 + i, 1)
        return pointnum

    for i in range (pointnum):
        x = round(struct.unpack('<l', data[offset+poffset+i*8:offset+poffset+4+i*8])[0]/10000.,2)
        y = round(struct.unpack('<l', data[offset+poffset+4+i*8:offset+poffset+8+i*8])[0]/10000.,2)
        Type = data[offset+poffset+pointnum*8+i]
        NodeType = ''
        if Type & 2:
            NodeType = '    Char. start'
        if Type & 4:
            NodeType = NodeType+'  Can modify'
        if Type & 8:
            NodeType = NodeType+'  Closed path'
        if Type & 0x10 == 0 and Type & 0x20 == 0:
            NodeType = NodeType+'  Discontinued'
        if Type & 0x10:
            NodeType = NodeType+'  Smooth'
        if Type & 0x20:
            NodeType = NodeType+'  Symmetric'
        if Type & 0x40 == 0 and Type & 0x80 == 0:
            NodeType = NodeType+'  START'
        if Type & 0x40 and Type & 0x80 == 0:
            NodeType = NodeType+'  Line'
        if Type & 0x40 == 0 and Type & 0x80:
            NodeType = NodeType+'  Curve'
        if Type & 0x40 and Type & 0x80:
            NodeType = NodeType+'  Arc'
#        add_iter(model, "[%s] X%u/Y%u/Type" % (lt2, i + 1, i + 1), "%.2f/%.2f mm  (corr. %.2f/%.2f)"%(x,y,x+hd.width/2,y+hd.height/2)+NodeType,offset+poffset+i*8,8,"txt",offset+poffset+pointnum*8+i,1)
        add_iter(model, "[%s] X%u/Y%u/Type" % (lt2, i + 1, i + 1), "%.2f/%.2f mm  %s" % (x, y, NodeType), offset + poffset + i * 8, 8, "txt") #,offset+poffset+pointnum*8+i,1)

    return pointnum

def loda_coords5v5(page, model, data, offset, l_type):
    x = round(struct.unpack('<h', data[offset:offset + 2])[0] * 0.0254, 2)
    y = round(struct.unpack('<h', data[offset + 2:offset + 4])[0] * 0.0254, 2)
#    add_iter(model, "[001e] X0/Y0", "%.2f/%.2f mm  (corr. %.2f/%.2f)" % (x,y,x+hd.width/2,y+hd.height/2),offset,4,"txt")
    add_iter(model, "[001e] X0/Y0", "%.2f/%.2f mm  " % (x, y), offset, 4, "txt")
    offset += 4
    add_iter(model, "[001e] var1?", struct.unpack("<H", data[offset:offset + 2])[0], offset, 2, "<H")
    add_iter(model, "[001e] BPP?", struct.unpack("<H", data[offset + 2:offset + 4])[0], offset + 2, 2, "<H")
    add_iter(model, "[001e] Img Width (px)", struct.unpack("<h", data[offset + 4:offset + 6])[0], offset + 4, 2, "<h")
    add_iter(model, "[001e] Img Height (px)", struct.unpack("<h", data[offset + 6:offset + 8])[0], offset + 6, 2, "<h")
    offset += 8
    add_iter(model, "[001e] Image ID", struct.unpack("<H", data[offset:offset + 2])[0], offset, 2, "<H")

def loda_coords5(page, model, data, offset, l_type):
    if page.doc_data["version"] < 6:
        loda_coords5v5(page, model, data, offset, l_type)
    else:
        for i in range (4):
            x = round(struct.unpack('<l', data[offset + i * 8:offset + 4 + i * 8])[0] /10000, 2)
            y = round(struct.unpack('<l', data[offset + 4 + i * 8:offset + 8 + i * 8])[0]/10000, 2)
#            add_iter(model, "[001e] X%u/Y%u" % (i + 1, i + 1), "%.2f/%.2f mm  (corr. %.2f/%.2f)"%(x,y,x+hd.width/2,y+hd.height/2),offset+i*8,8,"txt")
            add_iter(model, "[001e] X%u/Y%u" % (i + 1, i + 1), "%.2f/%.2f mm  " % (x, y), offset + i * 8, 8, "txt")
        offset += 32
        add_iter(model, "[001e] var1?", struct.unpack("<H", data[offset:offset + 2])[0], offset, 2, "<H")
        add_iter(model, "[001e] BPP?", struct.unpack("<H", data[offset + 2:offset + 4])[0], offset + 2, 2, "<H")
        add_iter(model, "[001e] Img Width (px)", struct.unpack("<I", data[offset + 4:offset + 8])[0], offset + 4, 4, "<I")
        add_iter(model, "[001e] Img Height (px)", struct.unpack("<I", data[offset + 8:offset + 12])[0], offset + 8, 4, "<I")
        offset += 16
        add_iter(model, "[001e] Image ID", struct.unpack("<I", data[offset:offset + 4])[0], offset, 4, "<I")
        offset += 24
        loda_coords3(page, model, data, offset, l_type)

def loda_coords_poly(page, model, data, offset, l_type):
    pn = loda_coords3(page, model, data, offset, l_type)
    x = round(struct.unpack('<l', data[offset + 4 + pn * 9:offset + 4 + pn * 9 + 4])[0] / 10000, 2)
    y = round(struct.unpack('<l', data[offset + 4 + pn * 9 + 4:offset + 4 + pn * 9 + 8])[0] / 10000, 2)
#    add_iter(model, "[001e] var1/var2 ?","%.2f/%.2f mm  (corr. %.2f/%.2f)"%(x,y,x+hd.width/2,y+hd.height/2),offset+4+pn*9,8,"txt")
    add_iter(model, "[001e] var1/var2 ?", "%.2f/%.2f mm  " % (x, y), offset + 4 + pn * 9, 8, "txt")

def loda_coords_0x6(page, model, data, offset, l_type, length):
    off = offset
    w = round(struct.unpack('<l', data[off + 4:off + 8])[0] / 10000, 2)
    h = round(struct.unpack('<l', data[off + 8:off + 12])[0] / 10000, 2)
    add_iter(model, "[001e] ??", "", off, 4, "<I")
    add_iter(model, "[001e] W/H", "%.2f/%.2f mm" % (w, h), off + 4, 8, "<ll")
    add_iter(model, "[001e] ??", "", off + 12, length - 12, "txt")

def loda_coords_0xa(page, model, data, offset, l_type, length):
    off = offset
    w = round(struct.unpack('<l', data[off:off + 4])[0] / 10000, 2)
    h = round(struct.unpack('<l', data[off + 4:off + 8])[0] / 10000, 2)
    add_iter(model, "[001e] W/H", "%.2f/%.2f mm" % (w, h), off, 8, "<ll")
    add_iter(model, "[001e] ??", "", off + 8, length - 8, "txt")

def loda_coords_0x25(page, model, data, offset, l_type):
    n1 = struct.unpack("<H", data[offset:offset + 2])[0]
    n2 = struct.unpack("<H", data[offset + 2:offset + 4])[0]
    n3 = struct.unpack("<H", data[offset + 4:offset + 6])[0]
    n4 = struct.unpack("<H", data[offset + 6:offset + 8])[0]
    numpts = n3 + n4
    add_iter (model, "[001e] Flags", "%02x %02x %02x %02x" % (n1, n2, n3, n4), offset, 8, "txt")
    off = offset + 8
    xs = round(struct.unpack('<l', data[off:off + 4])[0] / 10000, 2)
    ys = round(struct.unpack('<l', data[off + 4:off + 8])[0] / 10000, 2)
    xe = round(struct.unpack('<l', data[off + 8:off + 12])[0] / 10000, 2)
    ye = round(struct.unpack('<l', data[off + 12:off + 16])[0] / 10000, 2)
#    add_iter(model, "[001e] Xs/Ys","%.2f/%.2f mm  (corr. %.2f/%.2f)"%(xs,ys,xs+hd.width/2,ys+hd.height/2),off,8,"txt")
#    add_iter(model, "[001e] Xe/Ye","%.2f/%.2f mm  (corr. %.2f/%.2f)"%(xe,ye,xe+hd.width/2,ye+hd.height/2),off+8,8,"txt")
    add_iter(model, "[001e] Xs/Ys", "%.2f/%.2f mm  " % (xs, ys), off, 8, "txt")
    add_iter(model, "[001e] Xe/Ye", "%.2f/%.2f mm  " % (xe, ye), off + 8, 8, "txt")
    off += 16
    for i in range (numpts):
        x = round(struct.unpack('<l', data[off + i * 8:off + 4 + i * 8])[0] / 10000, 2)
        y = round(struct.unpack('<l', data[off + 4 + i * 8:off + 8 + i * 8])[0] / 10000, 2)
        Type = data[off + numpts * 8 + i]
        NodeType = ''
        if Type & 2:
            NodeType = '    Char. start'
        if Type & 4:
            NodeType = NodeType+'  Can modify'
        if Type & 8:
            NodeType = NodeType+'  Closed path'
        if Type & 0x10 == 0 and Type & 0x20 == 0:
            NodeType = NodeType+'  Discontinued'
        if Type & 0x10:
            NodeType = NodeType+'  Smooth'
        if Type & 0x20:
            NodeType = NodeType+'  Symmetric'
        if Type & 0x40 == 0 and Type & 0x80 == 0:
            NodeType = NodeType+'  START'
        if Type & 0x40 and Type & 0x80 == 0:
            NodeType = NodeType+'  Line'
        if Type & 0x40 == 0 and Type & 0x80:
            NodeType = NodeType+'  Curve'
        if Type & 0x40 and Type & 0x80:
            NodeType = NodeType+'  Arc'
#        add_iter(model, "[001e] X%u/Y%u/Type"%(i+1,i+1),"%.2f/%.2f mm  (corr. %.2f/%.2f)"%(x,y,x+hd.width/2,y+hd.height/2)+NodeType,off+i*8,8,"txt",off+numpts*8+i,1)
        add_iter(model, "[001e] X%u/Y%u/Type" % (i + 1, i + 1), "%.2f/%.2f mm  %s" % (x, y, NodeType), off + i * 8, 8, "txt")

def loda_coords_v3(page, model, data, offset, l_type, length):
    if l_type == 0: # rectangle
        x = struct.unpack("<h", data[offset:offset + 2])[0] * 0.0254
        y = struct.unpack("<h", data[offset + 2:offset + 4])[0] * 0.0254
        r = struct.unpack("<h", data[offset + 4:offset + 6])[0] * 0.0254
        add_iter(model, "[001e] X/Y/R", "%.2f  %.2f  %.2f" % (x, y, r), offset, 6, "<hhh")
    elif l_type == 1: # ellipse
        x = struct.unpack("<h", data[offset:offset + 2])[0] * 0.0254
        y = struct.unpack("<h", data[offset + 2:offset + 4])[0] * 0.0254
        a1 = struct.unpack("<h", data[offset + 4:offset + 6])[0]  # /10?
        a2 = struct.unpack("<h", data[offset + 6:offset + 8])[0]  # /10?
        f = struct.unpack("<h", data[offset + 8:offset + 10])[0]  # "connect to center" flag
        add_iter(model, "[001e] X/Y/Ang1/Ang2/Flag", "%.2f  %.2f  %.2f %.2f %d" % (x, y, a1, a2, f), offset, 10, "<hhhhh")
    elif l_type == 2: # line/curve
        add_iter(model, "[001e] ", "", offset, length, "txt")
    elif l_type == 3: # text
        num = struct.unpack("<H", data[offset + 11:offset + 13])[0]
        add_iter(model, "[001e] Num of char", num, offset + 11, 2, "<H")
        off = 0x84
        for i in range(num):
            flag = data[offset + off]
            ch = data[offset + off + 1]
            chdesc = ""
            chlen = 3
            if flag:
                chlen = 16
                bf = data[offset + off + 3]
                st = struct.unpack("<h", data[offset + off + 4:offset + off + 6])[0]
                offx = struct.unpack("<h", data[offset + off + 6:offset + off + 8])[0]
                offy = struct.unpack("<h", data[offset + off + 8:offset + off + 10])[0]
                rot = struct.unpack("<h", data[offset + off + 10:offset + off + 12])[0]
                fld = struct.unpack("<H", data[offset + off + 12:offset + off + 14])[0]
                outl = struct.unpack("<H", data[offset + off + 14:offset + off + 16])[0]
                chdesc = "\tFlags: %02x Style: %02x (%s) OffX/Y: %d/%d Rot: %d Fild: %02x Outl: %02x" % (bf, st, key2txt(st, lcv3styles), offx, offy, rot, fld, outl)
            add_iter(model, "\t Char", ch + chdesc, offset + off, chlen, "txt")
            off += chlen

def loda_coords_v4(page, model, data, offset, l_type, length):
    x = struct.unpack("<h", data[offset:offset + 2])[0] * 0.0254
    y = struct.unpack("<h", data[offset + 2:offset + 4])[0]
    off = 4
    clen = struct.unpack("<h", data[offset + off:offset + off + 2])[0]
    off += 2

    if l_type == 4: # artistic text
        add_iter(model, "[001e] # of chars", y, offset + 2, 2, "<h")
        term = 6 + 25 * y
    else:
        add_iter(model, "[001e] X/Y", "%.2f  %.2f" % (x, y * 0.0254), offset, 4, "<hh")
        term = clen - 3
        return

    for _ in range(y):
        flag = data[offset + off]
        ch = data[offset + off + 1]
        chdesc = ""
        chlen = 3
        if flag:
            chlen = 25
            bf = struct.unpack("<h", data[offset + off + 3:offset + off + 5])[0]
            fid = struct.unpack("<h", data[offset + off + 5:offset + off + 7])[0]
            var = struct.unpack("<h", data[offset + off + 7:offset + off + 9])[0]
            st = struct.unpack("<h", data[offset + off + 9:offset + off+ 11])[0]
            offx = struct.unpack("<h", data[offset + off + 11:offset + off + 13])[0]
            offy = struct.unpack("<h", data[offset + off + 13:offset + off + 15])[0]
            rot = struct.unpack("<h", data[offset + off + 15:offset + off + 17])[0]
            fld = struct.unpack("<I", data[offset + off + 17:offset + off + 21])[0]
            outl = struct.unpack("<I", data[offset + off + 21:offset + off + 25])[0]
            chdesc = "\tFlags: %02x Font: %02x ??? %02x Style: %02x (%s) OffX/Y: %d/%d Rot: %d Fild: %02x Outl: %02x" % (bf, fid, var, st, key2txt(st, lcv4styles), offx, offy, rot, fld, outl)
        add_iter(model, "\t Char", ch + chdesc, offset + off, chlen, "txt")
        off += chlen

def loda_v5 (page, model, size, data, shift=0, ftype=0):
    n_args = struct.unpack('<H', data[2:4])[0] # TypeError: 'int' object is not subscriptable
    s_args = struct.unpack('<H', data[4:6])[0]
    s_types = struct.unpack('<H', data[6:8])[0]
    l_type = struct.unpack('<H', data[8:10])[0]
    add_iter(model, "# of args", n_args, 2 + shift, 2, "<H")
    add_iter(model, "Start of args offsets", "%02x" % s_args, 4 + shift, 2, "<H")
    add_iter(model, "Start of arg types", "%02x" % s_types, 6 + shift, 2, "<H")
    t_txt = "%02x" % l_type
    if ftype == 0:
        if page.doc_data["version"] == 3:
            if l_type_ in loda_types_v3:
                t_txt += " " + loda_types_v3[l_type]
        elif l_type in loda_types:
                t_txt += " " + loda_types[l_type]
        add_iter(model, "Type", t_txt, 8, 2, "<H")
    else:
        add_iter(model, "Parent ID", "%02x" % l_type, 8 + shift, 2, "<H")
    a_txt = ""
    t_txt = ""
    for i in range(n_args, 0, -1):
        a_txt += " %02x" % struct.unpack('<H', data[s_args + i * 2 - 2:s_args + i * 2])[0]
        t_txt += " %02x" % struct.unpack('<H', data[s_types + (n_args - i) * 2:s_types + (n_args - i) * 2 + 2])[0]
    add_iter(model, "Args offsets", a_txt, s_args + shift, n_args * 2, "<H")
    add_iter(model, "Args types", t_txt, s_types + shift, n_args * 2, "<H")

    for i in range(n_args, 0, -1):
        offset = struct.unpack('<H', data[s_args + i * 2 - 2:s_args + i * 2])[0]
        length = struct.unpack('<H', data[s_args + i * 2:s_args + i * 2 + 2])[0] - offset
        argtype = struct.unpack('<H', data[s_types + (n_args - i) * 2:s_types + (n_args - i) * 2 + 2])[0]
        if ftype == 0:
            if argtype in loda_type_func:
                loda_type_func[argtype](page, model, data, offset + shift, l_type, length)
            else:
                add_iter(model, "[%02x]" % argtype, "???", offset + shift, struct.unpack('<H', data[s_args + i * 2:s_args + i * 2 + 2])[0] - offset, "<H")
        else:
            if argtype in styd_types:
                add_iter(model, styd_types[argtype], "...", offset + shift, struct.unpack('<H', data[s_args + i * 2:s_args + i * 2 + 2])[0] - offset, "txt")
            else:
                add_iter(model, "[%02x]" % argtype, "???", offset + shift, struct.unpack('<H', data[s_args + i * 2:s_args + i * 2 + 2])[0] - offset, "txt")

def loda_coords(page, model, data, offset, l_type, length):
    if page.doc_data["version"] < 4:
        loda_coords_v3(page, model, data, offset, l_type, length)
    else:
        if l_type == 4 and round(page.doc_data["version"]) == 4:
            loda_coords_v4(page, model, data, offset, l_type, length)
        elif l_type < 5 and l_type != 3:
            loda_coords124(page, model, data, offset, l_type)
        elif l_type == 3:
            loda_coords3(page, model, data, offset, l_type)
        elif l_type == 0x14 or l_type == 0x20:
            loda_coords_poly(page, model, data, offset, l_type)
        elif l_type == 0x25:
            loda_coords_0x25(page, model, data, offset, l_type)
        elif l_type == 5:
            loda_coords5(page, model, data, offset, l_type)
        elif l_type == 0xa:
            loda_coords_0xa(page, model, data, offset, l_type, length)
        elif l_type == 0x6:
            loda_coords_0x6(page, model, data, offset, l_type, length)

    # insert calls to specific coords parsing here
        else:
            add_iter(model, "[001e]", "???", offset, length, "txt")

def loda_fild(page, model, data, offset, l_type, length):
    if page.doc_data["version"] > 3:
        itr = add_iter(model, "[0014] Fild ID", "%08x" % struct.unpack("<I", data[offset:offset + 4])[0], offset, 4, "txt")

        # hd.model.set (iter, 7,("cdr goto",d2hex(data[offset:offset+4])))
    else:
        itr = add_iter (model, "[0014] Fild", "", offset, length, "txt")
        ftype = data[offset]
        # 0 - xparent, 1 - solid, 2 - lin grad, 4 - rad grad,
        # 6 - ps, 7 - pattern, a - bitmap/texture/full clr
        add_iter(model, "\ttype/flag","%02x (%s)"% (ftype, key2txt(ftype, {0: "Xparent", 1: "Solid", 2: "Linear grad", 4: "Radial Grad", 6: "PS", 7: "Pattern", 10: "Bitmap"})), offset, 1, "B", parent=itr)
        if ftype == 1:
            add_iter(model, "\tcolour", d2hex(data[offset + 1:offset + 6]), offset + 1, 5, "txt", parent=itr)
        elif ftype == 2:
            add_iter(model, "\trotation", struct.unpack("<h", data[offset + 1:offset + 3])[0] / 10, offset, 2, "<h", parent=itr)
            add_iter(model, "\tcolour 1", d2hex(data[offset + 3:offset + 8]), offset + 3, 5, "txt", parent=itr)
            add_iter(model, "\tcolour 2", d2hex(data[offset + 8:offset + 13]), offset + 8, 5, "txt", parent=itr)
            add_iter(model, "\tsteps?", struct.unpack("<h", data[offset + 18:offset + 20])[0], offset + 18, 2, "<h", parent=itr)
            add_iter(model, "\tedge offset", struct.unpack("<h", data[offset + 20:offset + 22])[0], offset + 20, 2, "<h", parent=itr)
        elif ftype == 4:
            add_iter(model, "\trotation", struct.unpack("<h", data[offset + 1:offset + 3])[0] / 10, offset, 2, "<h", parent=itr)
            add_iter(model, "\tcolour 1", d2hex(data[offset + 3:offset + 8]), offset + 3, 5, "txt", parent=itr)
            add_iter(model, "\tcolour 2", d2hex(data[offset + 8:offset + 13]), offset + 8, 5, "txt",parent=itr)
#			add_iter(model, "\tsteps?", struct.unpack("<h", data[offset + 18:offset + 20])[0], offset + 18, 2, "<h", parent=itr)
            add_iter(model, "\tedge offset", struct.unpack("<h", data[offset + 20:offset + 22])[0], offset + 20, 2, "<h", parent=itr)
            add_iter(model, "\tcenter X offset", struct.unpack("<h", data[offset + 22:offset + 24])[0], offset + 22, 2, "<h", parent=itr)
            add_iter(model, "\tcenter Y offset", struct.unpack("<h", data[offset + 24:offset + 26])[0], offset + 24, 2, "<h", parent=itr)
        elif ftype == 6:
            add_iter(model, "\tPS ID (usdn)", d2hex(data[offset + 1:offset + 3]), offset + 1, 2, "txt", parent=itr)
            add_iter(model, "\tPS options", d2hex(data[offset + 3:offset + 13]), offset + 3, 10, "txt", parent=itr)
        elif ftype == 7:
            add_iter(model, "\twidth", struct.unpack("<h", data[offset + 5:offset + 7])[0] * 0.0254, offset + 5, 2, "<h", parent=itr)
            add_iter(model, "\theight", struct.unpack("<h", data[offset + 7:offset + 9])[0] * 0.0254, offset + 7, 2, "<h", parent=itr)
            add_iter(model, "\tX tile offset %", struct.unpack("<h", data[offset + 9:offset + 11])[0], offset + 9, 2, "<h", parent=itr)
            add_iter(model, "\tY tile offset %", struct.unpack("<h", data[offset + 11:offset + 13])[0], offset + 11, 2, "<h", parent=itr)
            add_iter(model, "\tRow/Col offset %", struct.unpack("<h", data[offset + 13:offset + 15])[0], offset + 13, 2, "<h", parent=itr)
            add_iter(model, "\tRow/Col (0/1)", data[offset + 15], offset + 15, 1, "B", parent=itr)
            add_iter(model, "\tcolour 1", d2hex(data[offset + 16:offset + 21]), offset + 16, 5, "txt", parent=itr)
            add_iter(model, "\tcolour 2", d2hex(data[offset + 21:offset + 26]), offset + 21, 5, "txt", parent=itr)
        elif ftype == 10:
            add_iter(model, "\tImage ID (spnd)", d2hex(data[offset + 1:offset + 3]), offset + 1, 2, "txt", parent=itr)
            add_iter(model, "\twidth", struct.unpack("<h", data[offset + 3:offset + 5])[0] * 0.0254, offset + 3, 2, "<h", parent=itr)
            add_iter(model, "\theight", struct.unpack("<h", data[offset + 5:offset + 7])[0] * 0.0254, offset + 5, 2, "<h", parent=itr)
            add_iter(model, "\tX tile offset %", struct.unpack("<h", data[offset + 9:offset + 11])[0], offset + 9, 2, "<h", parent=itr)
            add_iter(model, "\tY tile offset %", struct.unpack("<h", data[offset + 11:offset + 13])[0], offset + 11, 2, "<h", parent=itr)
            add_iter(model, "\tRow/Col offset %", struct.unpack("<h", data[offset + 13:offset + 15])[0], offset + 13, 2, "<h", parent=itr)
            add_iter(model, "\tRow/Col (0/1)", data[offset+15], offset + 15, 1, "B", parent=itr)
            add_iter(model, "\tcolour 1", d2hex(data[offset + 16:offset + 21]), offset + 16, 5, "txt", parent=itr)
            add_iter(model, "\tcolour 2", d2hex(data[offset + 21:offset + 26]), offset + 21, 5, "txt", parent=itr)

def loda_grad(page, model, data, offset, l_type, length):
    startx = struct.unpack('<i', data[offset + 8:offset + 12])[0]
    starty = struct.unpack('<i', data[offset + 12:offset + 16])[0]
    endx = struct.unpack('<i', data[offset + 16:offset + 20])[0]
    endy = struct.unpack('<i', data[offset + 20:offset + 24])[0]
    sx = round(startx / 10000, 2)
    sy = round(starty / 10000, 2)
    ex = round(endx / 10000, 2)
    ey = round(endy / 10000, 2)
#    add_iter(model, "[2eea] Gradient Start X", "%.2f  (corr. %.2f)"%(sx,sx+hd.width/2),offset+8,4,"<i")
#    add_iter(model, "[2eea] Gradient Start Y", "%.2f  (corr. %.2f)"%(sy,sy+hd.height/2),offset+12,4,"<i")
#    add_iter(model, "[2eea] Gradient End X", "%.2f  (corr. %.2f)"%(ex,ex+hd.width/2),offset+16,4,"<i")
#    add_iter(model, "[2eea] Gradient End Y", "%.2f  (corr. %.2f)"%(ey,ey+hd.height/2),offset+20,4,"<i")
    add_iter(model, "[2eea] Gradient Start X", "%.2f  " % sx, offset + 8, 4, "<i")
    add_iter(model, "[2eea] Gradient Start Y", "%.2f  " % sy, offset + 12, 4, "<i")
    add_iter(model, "[2eea] Gradient End X", "%.2f  " % ex, offset + 16, 4, "<i")
    add_iter(model, "[2eea] Gradient End Y", "%.2f  " % ey, offset + 20, 4, "<i")

def loda_lens(page, model, data, offset, l_type, length):
    lens_type = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, " [1f40] Lens Type", lens_type, offset, 4, "<I")
    lens_id = d2hex(data[offset + 4:offset + 8])
    add_iter(model, " [1f40] Lens ID", lens_id, offset + 4, 4, "txt")

    if lens_type == 1:
        sub_type = struct.unpack("<H", data[offset + 8:offset + 10])[0]
        add_iter(model, " [1f40] Lens SubType", "%02x (%s)" % (sub_type, key2txt(sub_type, lens1_subtypes)), offset + 8, 2, "<H")
        if sub_type != 3 and sub_type != 5 and sub_type != 8:
            val =  struct.unpack("<h", data[offset + 10:offset + 12])[0] / 10
            add_iter(model, " [1f40] Value", val, offset + 10, 2, "<h")
    elif lens_type == 2:
        val = struct.unpack("<h", data[offset + 8:offset + 10])[0] / 10
        add_iter(model, " [1f40] Magnify Value", value, offset + 8, 2, "<h")
    elif lens_type == 3:
        val = struct.unpack("<h", data[offset + 8:offset + 10])[0] / 10
        add_iter(model, " [1f40] Fish Eye Value", value, offset + 8, 2, "<h")
    elif lens_type == 4:
        add_iter(model, " [1f40] WireFrame", "", offset + 8, 2, "")
    elif lens_type == 5:
        # offsets for version 12
        xy1_off = 0x10  # only for linear?
        xtype_off = 0x28
        xy2_off = 0x2c
        xpar_off = 0x40
        op_off = 0x50
        trg_off = 0x54
        frz_off = 0x58
        fild_off = 0x70
        xt = struct.unpack("<I", data[offset + xtype_off:offset + xtype_off + 4])[0]
        add_iter(model, " [1f40] Xparency Type", "%02x (%s)" % (xt, key2txt(xt, lens5_types)), offset + xtype_off, 4, "<I")
        op = struct.unpack("<I", data[offset + op_off:offset + op_off + 4])[0]
        add_iter(model, " [1f40] Xparency Operation", "%02x (%s)" % (op, key2txt(op, lens5_ops)), offset + op_off, 4, "<I")
        xpar_start = struct.unpack("<d", data[offset + xpar_off:offset + xpar_off + 8])[0]
        xpar_end = struct.unpack("<d", data[offset + xpar_off + 8:offset + xpar_off + 16])[0]
        add_iter(model, " [1f40] Xparency Start", "%02d%%" % xpar_start, offset + xpar_off, 8, "<d")
        add_iter(model, " [1f40] Xparency End", "%02d%%" % xpar_end, offset + xpar_off + 8, 8, "<d")

def loda_mesh(page, model, data, offset, l_type, length):
    add_iter(model, "[4ace]", "", offset, len(data), "txt")
    off = 0
    nrow = struct.unpack("<I", data[offset + 8:offset + 12])[0]
    ncol = struct.unpack("<I", data[offset + 12:offset + 16])[0]
    add_iter(model, "[4ace] Num row", nrow, offset + 8, 4, "<I")
    add_iter(model, "[4ace] Num col", ncol, offset + 12, 4, "<I")
    off = 20
    loda_coords3(page, model, data, offset + off, l_type, "4ace set 1")

def loda_name(page, model, data, offset, l_type, length):
    if page.doc_data["version"] > 11:
        layrname = data[offset:].decode('utf-16')
    else:
        layrname = data[offset:].decode("utf-8")
    add_iter(model, "[03e8] Name", layrname, offset, length - offset, "txt")

def loda_outl(page, model, data, offset, l_type, length):
    if page.doc_data["version"] > 3:
        itr = add_iter(model, "[000a] Outl ID", "%08x" % struct.unpack("<I", data[offset:offset + 4])[0], offset, 4, "txt")

        # FIXME! FIXME! FIXME! FIXME! FIXME! FIXME! FIXME! FIXME! FIXME!
        # hd.model.set (iter, 7,("cdr goto",d2hex(data[offset:offset+4])))
    else:
        itr = add_iter(model, "[000a] Outl", "", offset, length, "txt")
        # type/flag:  1 - solid, 2 -- dashed, 0x40 -- behind fill, 0x80 -- scale with image
        add_iter(model, "\ttype/flag", data[offset], offset, 1, "B", parent=itr)
        add_iter(model, "\twidth", struct.unpack("<h", data[offset + 1:offset + 3])[0] * 0.0254, offset + 1, 2, "<h",parent=itr)
        add_iter(model, "\tstretch", struct.unpack("<h", data[offset + 3:offset + 5])[0], offset + 3, 2, "<h",parent=itr)
        add_iter(model, "\tangle", struct.unpack("<h", data[offset + 5:offset + 7])[0] / 10, offset + 5, 2, "<h", parent=itr)
        add_iter(model, "\tcolour", d2hex(data[offset + 7:offset + 12]), offset + 7, 5, "txt", parent=itr)
        add_iter(model, "\tdashes", d2hex(data[offset + 19:offset + 30]), offset + 19, 11, "txt",parent=itr)
        add_iter(model, "\tjoin type", data[offset + 30], offset + 30, 1, "B", parent=itr)
        add_iter(model, "\tcaps type", data[offset + 32], offset + 32, 1, "B", parent=itr)
        if page.doc_data["version"] == 3:
            add_iter(model, "\tstart arrow", d2hex(data[offset + 34:offset + 38]), offset + 34, 4, "txt",parent=itr)
            add_iter(model, "\tend arrow", d2hex(data[offset + 38:offset + 42]), offset + 38, 4, "txt",parent=itr)
        else:
            # VERIFY!
            add_iter(model, "\tstart arrow", d2hex(data[offset + 34:offset + 36]), offset + 34, 2, "txt",parent=itr)
            add_iter(model, "\tend arrow", d2hex(data[offset + 36:offset + 38]), offset + 36, 2, "txt",parent=itr)

def loda_palt(page, model, data, offset, l_type, length):
    clr_model(page, model, data, offset)

def loda_polygon(page, model, data, offset, l_type, length):
    num = struct.unpack('<L', data[offset + 4:offset + 8])[0]
    add_iter(model, "[2af8] # of angles", num, offset + 4, 4, "<I")
    num = struct.unpack('<L', data[offset + 8:offset + 0xc])[0]
    add_iter(model, "[2af8] next point?", num, offset + 8, 4, "<I")
    var = struct.unpack('<d', data[offset + 0x10:offset + 0x10 + 8])[0]
    add_iter(model, "[2af8] var1 ?", var, offset + 0x10, 8, "<d")
    var = struct.unpack('<d', data[offset + 0x18:offset + 0x18 + 8])[0]
    add_iter(model, "[2af8] var2 ?", var, offset + 0x18, 8, "<d")

    if page.doc_data["version"] > 6:
        for i in range(2):
            varX = struct.unpack('<l', data[offset + 0x18 + 8 + i * 8:offset + 0x1c + 8 + i * 8])[0]
            varY = struct.unpack('<l', data[offset + 0x1c + 8 + i * 8:offset + 0x20 + 8 + i * 8])[0]
            vx = round(varX / 10000, 2)
            vy = round(varY / 10000, 2)
            #add_iter(model, "[2af8] X%u/Y%u" % (i, i), "%.2f/%.2f mm  (corr. %.2f/%.2f)"%(vx,vy,vx+hd.width/2,vy+hd.height/2),offset+0x18+8+i*8,8,"txt")
            add_iter(model, "[2af8] X%u/Y%u" % (i, i), "%.2f/%.2f mm  " % (vx, vy), offset + 0x18 + 8 + i * 8, 8, "txt")
    else:
        # FIXME! could be 1 pair
        for i in range(2):
            varX = struct.unpack('<h', data[offset + 0x18 + 8 + i * 4:offset + 0x1a + 8 + i * 4])[0]
            varY = struct.unpack('<h', data[offset + 0x1a + 8 + i * 4:offset + 0x1c + 8 + i * 4])[0]
            vx = round(varX / 10000, 2)
            vy = round(varY / 10000, 2)
#            add_iter(model, "[2af8] X%u/Y%u" % (i, i), "%.2f/%.2f mm  (corr. %.2f/%.2f)"%(vx,vy,vx+hd.width/2,vy+hd.height/2),offset+0x18+4+i*4,4,"txt")
            add_iter(model, "[2af8] X%u/Y%u" % (i, i), "%.2f/%.2f mm  " % (vx, vy), offset + 0x18 + 4 + i * 4, 4, "txt")

def loda_rot(page, model, data, offset, l_type, length):
    rot = struct.unpack('<l', data[offset:offset + 4])[0]
    add_iter(model, "[2efe] Rotate", "%.2f" % round(rot / 1000000, 2), offset, 4, "txt")

def loda_rot_center(page, model, data, offset, l_type,length):
    rotX = struct.unpack('<l', data[offset:offset + 4])[0]
    rotY = struct.unpack('<l', data[offset + 4:offset + 8])[0]
    rx = round(rotX / 10000, 2)
    ry = round(rotY / 10000, 2)
#    add_iter(model, "[0028] RotCenter X/Y","%.2f/%.2f   (corr. %.2f/%.2f)"%(rx,ry,rx+hd.width/2,ry+hd.height/2),offset,8,"txt")
    add_iter(model, "[0028] RotCenter X/Y","%.2f/%.2f   " % (rx, ry), offset, 8, "txt")

def loda_stlt(page, model, data, offset, l_type, length):
    if page.doc_data["version"] < 6: # probably <5
        add_iter(model, "[00c8] Stlt ID", "%04x" % struct.unpack("<H", data[offset:offset + 2])[0], offset, 2, "<H")
    else:
        add_iter(model, "[00c8] Stlt ID", "%08x" % struct.unpack("<I", data[offset:offset + 4])[0], offset, 4, "<I")

def loda_trfd(page, model, data, offset, l_type, length):
    if page.doc_data["version"] > 3:
        add_iter(model, "[0064] Trfd ID", "%08x" % struct.unpack("<I", data[offset:offset + 4])[0], offset, 4, "txt")
    else:
        add_iter(model, "[0064] Trafo", "", offset, length, "txt")
        t_off = struct.unpack("<h", data[offset + 0xa:offset + 0xc])[0]
        add_iter(model, "\tOffset to values", "%02x" % t_off, offset + 0xa, 2, "<h")
        add_iter(model, "\tvar1", readfrac(data[offset + t_off:offset + t_off + 4]), offset + t_off, 4, "frac")
        add_iter(model, "\tvar2", readfrac(data[offset + t_off + 4:offset + t_off + 8]), offset + t_off + 4, 4, "frac")
        add_iter(model, "\tX0", struct.unpack("<i", data[offset + t_off + 8:offset + t_off + 12])[0] * 0.0254, offset + t_off + 8, 4, "<i")
        add_iter(model, "\tvar3", readfrac(data[offset + t_off + 12:offset + t_off + 16]), offset + t_off + 12, 4, "frac")
        add_iter(model, "\tvar4", readfrac(data[offset + t_off + 16:offset + t_off + 20]), offset + t_off + 16, 4, "frac")
        add_iter(model, "\tY0", struct.unpack("<i", data[offset + t_off + 20:offset + t_off + 24])[0] * 0.0254, offset + t_off + 20, 4, "<i")

def loda_wroff (page, model, data, offset, l_type):
    add_iter(model, "[32c8] Txt Wrap Offset (mm)", struct.unpack("<i", data[offset:offset + 4])[0] / 10000, offset, 4, "<i")

def loda_wrstyle (page, model, data, offset, l_type):
    ws = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter (model, "[32c9] Txt Wrap Style", "%d (%s)" % (ws, key2txt(ws, wrap_txt_style)), offset, 4, "<I")

def obbx(page, model, options, data):
    offset = 0
    for i in range(4):
        varX = struct.unpack('<l', data[offset + i * 8:offset + 4 + i * 8])[0]
        varY = struct.unpack('<l', data[offset + 4 + i * 8:offset + 8 + i * 8])[0]
        add_iter (model, "X%u" % i, "%u mm" % round(varX / 10000.0, 2), offset + i * 8, 4, "<l")
        add_iter (model, "Y%u" % i, "%u mm" % round(varY / 10000.0, 2), offset + 4 + i * 8, 4, "<l")

def outl(page, model, options, data):
    add_iter(model, "Outline ID", "%08x" % struct.unpack("<I", data[0:4])[0], 0, 4, "<I")
    lt = 0x4
    ct = 0x6
    jt = 0x8
    lw = 0xc
    st = 0x10
    ang = 0x14
    varo = 0x1c
    lc = 0x4c
    dash = 0x68
    arrw = 0x80
    if page.doc_data["version"] >= 13:
        flag = struct.unpack('<I', data[4:8])[0]
        off = 0
        if flag == 5:
            off = 107
        elif page.doc_data["version"] >= 16:
            off = 51
        lt = 0x18 + off
        ct = 0x1a + off
        jt = 0x1c + off
        lw = 0x1e + off
        st = 0x22 + off
        ang = 0x24 + off
        varo = 0x28 + off
        lc = 0x58 + off # another place -- 0x55
        dash = 0x74 + off
        arrw = 0x8a + off
    elif page.doc_data["version"] < 6:
        lw = 0xa
        st = 0xc
        ang = 0xe
        lc = 0x10
        dash = 0x26
    ltype = struct.unpack('<H', data[lt:lt + 2])[0]
    ltxt = "Non-scalable"
    if ltype & 0x20 == 0x20:
        ltxt = "Scalable"
    if ltype & 0x10 == 0x10:
        ltxt += ", Behind fill"
    if ltype & 0x80 == 0x80:
        ltxt += ", Share Attrs"
    if ltype & 0x4 == 0x4:
        ltxt += ", Dashed"

    add_iter(model, "Line Type", "%02x %s" % (ltype, ltxt), lt, 2, "<H")
    add_iter(model, "Caps Type", "%02x" % struct.unpack('<H', data[ct:ct + 2])[0], ct, 2, "<H")
    add_iter(model, "Join Type", "%02x" % struct.unpack('<H', data[jt:jt + 2])[0], jt, 2, "<H")
    add_iter(model, "LineWidth", "%.2f mm" % round(struct.unpack('<I', data[lw:lw + 4])[0] / 10000.0, 2), lw, 4, "<I")
    add_iter(model, "Stretch", "%02x" % struct.unpack('<H', data[st:st + 2])[0], st, 2, "<H")
    if page.doc_data["version"] > 5:
        add_iter(model, "Angle", "%.2f" % round(struct.unpack('<i', data[ang:ang + 4])[0] / 1000000.0, 2), ang, 4, "<i")
    else:
        add_iter(model, "Angle", "%.2f" % round(struct.unpack('<h', data[ang:ang + 2])[0] / 1000000.0, 2), ang, 2, "<h")
    if page.doc_data["version"] > 5:
        for i in range(6):
            var = struct.unpack('<d', data[varo + i * 8:varo + 8 + i * 8])[0]
            add_iter (model, "XForm var%d" % (i + 1), "%f" % var, varo + i * 8, 8, "<d")
    clr_model(page, model, data, lc)
    dnum = struct.unpack('<H', data[dash:dash + 2])[0]
    add_iter (model, "Dash num", "%02x" % (dnum // 2), dash, 2, "<H")
    for i in range(dnum // 2):
        add_iter (model, " Dash/Space", "%02x/%02x" % (struct.unpack('<H', data[dash + 2 + i * 4:dash + 4 + i * 4])[0],
            struct.unpack('<H', data[dash + 4 + i * 4:dash + 6 + i * 4])[0]), dash + 2 + i * 4, 4, "txt")
    add_iter(model, "StartArrow ID", "%02x" % struct.unpack('<I', data[arrw:arrw + 4])[0], arrw, 4, "<I") # struct.error: unpack requires a buffer of 4 bytes
    add_iter(model, "EndArrow ID", "%02x" % struct.unpack('<I', data[arrw + 4:arrw + 8])[0], arrw + 4, 4, "<I")

def stlt_s2(page, model, options, data):
    offset = 0x18
    snum = 3
    if page.doc_data["version"] < 10:
        offset = 0x10
        snum = 1
    fid = struct.unpack("<H", data[offset:offset + 2])[0]
    add_iter(model, "Font ID", fid, offset, 2, "<H")
    offset += 2
    enc = struct.unpack("<H", data[offset:offset + 2])[0]
    add_iter(model, "Encoding", enc, offset, 2, "<H")
    offset += 2
    offset += 8 # skip 2 more id/enc
    for i in range(snum):
        fid = round(struct.unpack("<i", data[offset + i * 4:offset + 4 + i * 4])[0] * 72 / 254000)
        add_iter(model, "Size %d" % (i + 1), fid, offset + i * 4, 4, "<I")
    offset += snum * 4
    for i in range(3):
        fid = struct.unpack("<i", data[offset + i * 4:offset + 4 + i * 4])[0]
        add_iter(model, "var %2d" % i, fid, offset + i * 4, 4, "<I")

def stlt_s3(page, model, options, data):
    offset = 8
    fid = struct.unpack("<i", data[offset:offset + 4])[0]
    add_iter(model, "Align", "%d (%s)" % (fid, key2txt(fid, {0:"No", 1:"Left", 2:"Center", 3:"Right", 4:"Full justify", 5:"Force justify"})) , offset, 4, "<I")

def stlt_s(page, model, options, data):
    offset = 8
    for i in range((len(data) - 8) // 4):
        fid = struct.unpack("<i", data[offset + i * 4:offset + 4 + i * 4])[0]
        if fid > 5000 or fid < -5000:
            fid /= 10000
        add_iter(model, "var %2d" % i, fid, offset + i * 4, 4, "<i")

def stlt_s4(page, model, options, data):
    offset = 8
    [fid] = struct.unpack("<I", data[offset:offset + 4])
    add_iter(model, "???", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]/10000.
    add_iter(model, "Char spacing (%)", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]/10000.
    add_iter(model, "Lang spacing (%)", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]/10000.
    add_iter(model, "Word spacing (%)", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]/10000.
    add_iter(model, "Line spacing (%)", fid, offset, 4, "<I")
    offset += 4
    for i in range(2):
        fid = struct.unpack("<i", data[offset + i * 4:offset + 4 + i * 4])[0]/10000.
        add_iter(model, "var %2d" %i, fid, offset + i * 4, 4, "<I")
    offset += 8
    fid = struct.unpack("<I", data[offset:offset + 4])[0]/10000.
    add_iter(model, "Max char spacing (%)", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]/10000.
    add_iter(model, "Min word spacing (%)", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]/10000.
    add_iter(model, "Max word spacing (%)", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<i", data[offset:offset + 4])[0]
    add_iter(model, "var 2", fid, offset, 4, "<I")

def stlt_s5(page, model, options, data):
    stlt_s(page, model, options, data)

def stlt_s7(page, model, options, data):
    offset = 8
    fid0 = struct.unpack("<I",data[offset:offset + 4])[0]
    add_iter(model, "[PT] var5 flag", fid0, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] var1", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] var2", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<H", data[offset:offset + 2])[0]
    add_iter(model, "[PT] Font ID", fid, offset, 2, "<H")
    offset += 2
    enc = struct.unpack("<H", data[offset:offset + 2])[0]
    add_iter(model, "[PT] Encoding", enc, offset, 2, "<H")
    offset += 2
    fid = round(struct.unpack("<i", data[offset:offset + 4])[0] * 72 / 254000)
    add_iter(model, "[PT] var4 (pt)", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] var5", fid, offset, 4, "<I")
    offset += 4
    fid = round(struct.unpack("<i", data[offset:offset + 4])[0] * 72 / 254000)
    add_iter(model, "[PT] Baseline shift (pt)", fid, offset, 4, "<I")
    offset += 4
    for i in range(4):
        fid = struct.unpack("<i", data[offset + i * 4:offset + 4 + i * 4])[0]
        add_iter(model, "var %2d" % (i + 7), fid, offset + i * 4, 4, "<I")
    offset += 16
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Glyph ID", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Bullet to Text spacing", fid, offset, 4, "<I")
    offset += 4

def stlt_s8(page, model, options, data):
    offset = 8
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] var1", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] var2", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Right line indent", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] 1st line indent", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Left line indent", fid, offset, 4, "<I")
    offset += 4

def stlt_s9(page, model, options, data):
    offset = 8
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Autohyphenate ParaText", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Break Cap words", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Break ALL CAPS", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Min word len", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Min char before", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Min char after", fid, offset, 4, "<I")
    offset += 4

def stlt_s10(page, model, options, data):
    offset = 8
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] var0", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] Use hanging indent", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0]
    add_iter(model, "[PT] # of lines", fid, offset, 4, "<I")
    offset += 4
    fid = struct.unpack("<I", data[offset:offset + 4])[0] / 10000.
    add_iter(model, "[PT] Space after", fid, offset, 4, "<I")
    offset += 4

def stlt_s11(page, model, options, data):
    stlt_s(page, model, options, data)

def stlt12_p2(page, model, options, data):
    n = struct.unpack("<I", data[0:4])[0]
    if page.doc_data["version"] > 10:
        offset = 4 + n * 2
    else:
        offset = 4 + n
    for i in range((len(data) - offset) / 4):
        fid = d2hex(data[offset + i * 4:offset + 4 + i * 4])
        st = key2txt(i, style_ids)
        if page.doc_data["version"] < 8 and i > 5:
            st = key2txt(i + 1, style_ids)
        add_iter(model, "ID %d (%s)" % (i, st), fid, offset + i * 4, 4, "<I")

def trfd(page, model, options, data):
    n_args = struct.unpack('<i', data[4:8])[0]
    s_args = struct.unpack('<i', data[8:0xc])[0]
    s_types = struct.unpack('<i', data[0xc:0x10])[0]
    for j in range(n_args):
        start = struct.unpack('<L', data[s_args + j * 4:s_args + j * 4 + 4])[0]
        if page.doc_data["version"] > 12:
            start +=8
        switch = struct.unpack('<H', data[start:start + 2])[0]
        start += 8
        if switch == 8:
            for i in (0, 1):
                var = struct.unpack('<d', data[start + i * 8:start + 8 + i * 8])[0]
                add_iter(model, "var%d" % (i + 1), "%f" % var, start + i * 8, 8, "<d")
            add_iter(model, "x0", "%u" % (struct.unpack('<d', data[start + 16:start + 24])[0] / 10000), start + 16, 8, "<d")
            for i in (3, 4):
                var = struct.unpack('<d', data[start + i * 8:start + 8 + i * 8])[0]
                add_iter(model, "var%d" % (i + 1), "%f" % var, start + i * 8, 8, "<d")
            add_iter(model, "y0", "%u" % (struct.unpack('<d', data[start + 40:start + 48])[0] / 10000), start + 40, 8, "<d")
        elif switch == 0x10:
            # Distortion type
            dtype = struct.unpack('<H', data[start:start + 2])[0]
            dtt = dtypes.get(dtype) or "Unknown"
            add_iter(model, "Distortion type", "%02x (%s)" % (dtype, dtt), start, 2, "<H")

            # Coords of the distortion
            Xd = round(struct.unpack('<i', data[start + 2:start + 6])[0] / 10000., 2)
            Yd = round(struct.unpack('<i', data[start + 6:start + 10])[0] / 10000., 2)

            #add_iter(model, "Distortion Coords", "%.2f/%.2f   (corr. %.2f/%.2f)" % (Xd, Yd, Xd + hd.width / 2, Yd + hd.height / 2), start + 2, 8, "<txt")
            add_iter(model, "Distortion Coords", "%.2f/%.2f   " % (Xd, Yd), start + 2, 8, "<txt")
            # Distiortion sub-type
            dstype = struct.unpack('<I', data[start + 10:start + 14])[0]
            dstt = ""
            if dstype == 0:
                dstt = "None"
            else:
                if dstype & 1:
                    dstt = "Smooth "
                if dstype & 2:
                    dstt += "Random "
                if dstype & 4:
                    dstt = "Local"
            add_iter(model,  "Distortion Subtype", "%02x (%s)" % (dstype, dstt), start + 10, 4, "<I")

            # Options
            dopt1 = struct.unpack('<i', data[start + 14:start + 18])[0]
            dopt2 = struct.unpack('<i', data[start + 18:start + 22])[0]
            add_iter(model, "Distortion Option 1", "%d" % dopt1, start + 14, 4, "<i")
            add_iter(model, "Distortion Option 2", "%d" % dopt2, start + 18, 4, "<i")
        else:
            add_iter(model, "Unknown Type", "", start, 2, "txt")

def txsm16(page, model, options, data):
    off = 0
    frameflag = struct.unpack('<i', data[off:off + 4])[0]
    off += 4
    flag2  = struct.unpack('<i', data[off:off + 4])[0]
    off += 4
    flag3  = struct.unpack('<i', data[off:off + 4])[0]
    off += 4
    flag4  = struct.unpack('<i', data[off:off + 4])[0]
    off = 0x29
    num_frames = struct.unpack("<I", data[off:off + 4])[0]
    add_iter(model, "Num Frames", num_frames, off, 4, "<I")
    off += 4

    for i in range(num_frames):
        fr_iter = add_iter(model, "Frame ID", d2hex(data[off:off + 4]), off, 4, "<I")
        off += 4
        for j in range(6):
            var = struct.unpack('<d', data[off + j * 8:off + 8 + j * 8])[0]
            add_iter(model, "var%d" % (j + 1), "%d" % (var / 10000), off + j * 8, 8, "<d")
        off += 48
        txtonpath = struct.unpack('<I', data[off:off + 4])[0]
        off += 4
        if txtonpath == 1:
            add_iter(model, "tonp var1", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp Orientation", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp var2", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp var3", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp Offset", struct.unpack('<I', data[off:off + 4])[0] / 10000, off, 4, "<I")
            off += 4
            add_iter(model, "tonp var4", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp Distance", struct.unpack('<I', data[off:off + 4])[0] / 10000, off, 4, "<I")
            off += 4
            add_iter(model, "tonp var5", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp Mirror Vert", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp Mirror Hor", struct.unpack('<I', data[off:off+4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp var6", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp var7", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
        else:
            frame_unkn1 = struct.unpack('<I', data[off:off + 4])[0]
            add_iter(model, "frame unknw1", "%d" % frame_unkn1 , off, 4, "<I", parent=fr_iter)
            off += 4
            frame_xfid = struct.unpack('<I', data[off:off + 4])[0]
            add_iter(model, "frame xform ID", "%d" % frame_xfid , off, 4, "<I", parent=fr_iter)
            off += 4
            # palette_frames file has unkn1 == 1 and unkn2 == 3 (ver21), 0x16 (ver16)
            if frame_unkn1 == 1:
                for k in range(6):
                    var = struct.unpack('<d', data[off + k * 8:off + 8 + k * 8])[0]
                    add_iter(model, "frame var%d" % (k + 1), "%d" % var, off + k * 8, 8, "<d", parent=fr_iter)
                off += 48
                off += 4  # extra 4 bytes could be flagged by unkn2 != 0 ???

        if frameflag == 0:
            off += 16
            tlen = struct.unpack('<I', data[off:off + 4])[0]
            off += 4
            if page.doc_data["version"] > 16:
                frametxt = data[off:off + tlen]
                add_iter(model, "FrameTxt", frametxt.decode("utf-8"), off, tlen, "txt", parent=fr_iter)
                off += tlen
            else:
                frametxt = data[off:off+tlen*2]
                add_iter(model, "FrameTxt", frametxt.decode("utf-16"), off, tlen * 2, "txt", parent=fr_iter)
                off += tlen * 2

    num_para = struct.unpack('<I', data[off:off + 4])[0]
    add_iter(model, "# of paragraphs", num_para, off, 4, "<I")
    off += 4
    for _ in range(num_para):
        st_iter = add_iter(model, "style ID", d2hex(data[off:off + 4]), off, 4, "<I")
        off += 5 #!!! one more byte
        if frameflag:
            off += 1 # !!! one extra for txtonpath
        style_len = struct.unpack('<I', data[off:off + 4])[0]
        off += 4
        if page.doc_data["version"] > 16:
            frametxt = data[off:off + style_len]
            add_iter(model, "Style Txt", frametxt.decode("utf-8"), off, style_len,"txt", parent=st_iter)
            off += style_len
        else:
            frametxt = data[off:off + style_len * 2]
            add_iter(model, "Style Txt", frametxt.decode("utf-16"), off, style_len * 2, "txt", parent=st_iter)
            off += style_len * 2
        num_rec = struct.unpack('<I', data[off:off + 4])[0]
        add_iter(model, "#of Rec", num_rec, off, 4, "<I")
        off += 4
        for nr in range(num_rec):
            off += 2 # nchars
            st_flag1 = struct.unpack('<H', data[off:off + 2])[0]
            off += 2
            st_flag2 = struct.unpack('<H', data[off:off + 2])[0]
            off += 2
            rec_iter = add_iter(model, "Rec %#x" % nr, "%#x, %#x" % (st_flag1, st_flag2), off - 6, 6, "txt")
            if st_flag2 & 0x4:
                # encoding
                enc_len = struct.unpack('<I', data[off:off + 4])[0]
                off += 4
                off += enc_len * 2 # skip for now
            if st_flag1 or st_flag2 & 0x4:
                # JSON
                json_len = struct.unpack('<I', data[off:off + 4])[0]
                off += 4
                enc = "utf-8"
                if page.doc_data["version"] < 17:
                    json_len *= 2
                    enc = "utf-16"
                add_iter(model, "Style Txt", data[off:off+json_len].decode(enc), off, json_len, "txt", parent=rec_iter)
                off += json_len

        num2 = struct.unpack('<I', data[off:off + 4])[0]
        add_iter(model, "Num of 'Char'", num2, off, 4, "<I")
        off += 4
        if num2 > 120:
            # catch: most of the samples are shorter
            return
        for i in range(num2):
            add_iter(model, "Char %u" % i, "%s [%s] %s" % (d2hex(data[off:off + 2]), d2hex(data[off + 2:off + 3]), d2hex(data[off + 3:off + 8])), off, 8, "txt")
            off += 8
        txtlen = struct.unpack('<I', data[off:off + 4])[0]
        add_iter(model, "Text length", txtlen, off, 4, "<I")
        off += 4
        enc = "utf-8"
        try:
            txt = data[off:off+txtlen].decode(enc)
        except:
            txt = data[off:off+txtlen].decode("latin-1")
        add_iter(model, "Text", txt, off, txtlen, "txt")
        off += txtlen
        off += 1 # !!! extra 0

def txsm5style(page, model, siter, data, offset):
    flags = txsm56style
    flag1 = data[0]
    add_iter(model, "O/F flag", bflag2txt(flag1, flags, "?"), offset, 1, "B", parent=siter)
    if flag1 & 1:
        add_iter(model, "Font ID", struct.unpack("<H", data[2:4])[0], offset + 2, 2, "<H", parent=siter)
    if flag1 & 2:
        add_iter(model, "Text decorations", "...", offset + 4, 6, "txt", parent=siter)
    if flag1 & 4:
        fsize = struct.unpack("<H", data[10:12])[0] * 72 / 1000
        add_iter(model, "Font Size", fsize, offset + 10, 2, "<H", parent=siter)
    if flag1 & 0x10:
        add_iter(model, "Fill ID", "%08x" % struct.unpack("<I", data[14:18])[0], offset + 14, 4, "<I", parent=siter)
        shift = 4
    if flag1 & 0x20:
        add_iter(model, "Outl ID", "%08x" % struct.unpack("<I", data[18:22])[0], offset + 18, 4, "<I", parent=siter)

def txsm5(page, model, options, data):
    off = 2
    num_frames = struct.unpack("<H", data[off:off + 2])[0]
    add_iter(model, "Num Frames", num_frames, off, 2, "<H")
    off += 2
    for i in range(num_frames):
        add_iter(model, "Frame ID", d2hex(data[off:off + 2]), off, 2, "<H")
        off += 4 # possibly 2 bytes of flags that will then extend it with frame styles, so, length can be variable
    num_para = struct.unpack('<H', data[off:off + 2])[0]
    add_iter(model, "# of paragraphs", num_para, off, 2, "<H")
    off += 2
    for j in range(num_para):
        styleId = struct.unpack('<H', data[off:off + 2])[0]
        add_iter(model, "Style Id", styleId, off, 2, "<H")
        off += 2
        numst = struct.unpack('<H', data[off:off + 2])[0]
        add_iter(model, "# style recs", numst, off, 2, "<H")
        off += 2
        for i in range(numst):
            siter = add_iter(model, "style %d" % i, "...", off, 36, "txt")
            txsm5style(page, model, siter, data[off:off + 36], off)
            off += 36
        numch = struct.unpack('<H', data[off:off + 2])[0]
        txt_iter = add_iter(model, "Text %d" % j, "# of chars: %d" % numch, off, 2, "<H")
        off += 2
        for i in range(numch):
            add_iter(model, "Char %d" % i,
                "%s\t(%d, %d\tstyle %d)" % (data[off + 4], struct.unpack("<H", data[off:off + 2])[0],
                struct.unpack("<H", data[off + 2:off + 4])[0],
                struct.unpack("<H", data[off + 6:off + 8])[0] / 16), off, 8, "txt", parent=txt_iter)
            off += 8

def txsm6style(page, model, siter, data, offset):
    flags = txsm56style
    flag1 = data[0]
    add_iter(model, "O/F flag", bflag2txt(flag1, flags, "?"), offset, 1, "B", parent=siter)
    if flag1&1:
        add_iter(model, "Font ID", struct.unpack("<I", data[4:8])[0], offset + 4, 4, "<I", parent=siter)
    if flag1&2:
        add_iter(model, "Text decorations", "...", offset + 8, 4, "txt", parent=siter)
    if flag1&4:
        fsize = struct.unpack("<I", data[0xc:0x10])[0] * 72 / 254000
        add_iter(model, "Font Size", fsize, offset + 0xc, 4, "<I", parent=siter)
    shift = 0
    if flag1&0x10:
        add_iter(model, "Fill ID", "%08x" % struct.unpack("<I", data[0x3c:0x40])[0], offset + 0x3c, 4, "<I", parent=siter)
        shift = 4
    if flag1&0x20:
        add_iter(model, "Outl ID", "%08x" % struct.unpack("<I", data[0x3c + shift:0x40 + shift])[0], offset + 0x3c + shift, 4, "<I", parent=siter)

def txsm6(page, model, options, data):
    if round(page.doc_data["version"]) == 5:
        txsm5(page, model, options, data)
        return
    elif page.doc_data["version"] < 5:
        return

    frameflag = struct.unpack("<i", data[0:4])[0]
    off = 0x1c
    txtonpath = struct.unpack('<I', data[off:off + 4])[0]
    off += 4
    if txtonpath == 1:
        add_iter(model, "tonp var1", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
        off += 4
        add_iter(model, "tonp var3", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
        off += 4
        add_iter(model, "tonp Offset", struct.unpack('<I', data[off:off + 4])[0] / 10000, off, 4, "<I")
        off += 4
        add_iter(model, "tonp var4", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
        off += 4
        add_iter(model, "tonp Distance", struct.unpack('<I', data[off:off + 4])[0] / 10000, off, 4, "<I")
        off += 4
        # var5 could be "orientation" for ver 12
        add_iter(model, "tonp var5", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
        off += 4
        add_iter(model, "tonp var6", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
        off += 4
        # Mirror Hor could be same as var5 in newer versions
        add_iter(model, "tonp var7", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
        off += 4

    num_frames = struct.unpack("<I", data[off:off + 4])[0]
    add_iter(model, "Num Frames", num_frames, off, 4, "<I")
    off += 4
    for i in range(num_frames):
        fr_iter = add_iter(model, "Frame ID", d2hex(data[off:off + 4]), off, 4, "<I")
        off += 4
        for i in range(6):
            var = struct.unpack('<d', data[off + i * 8:off + 8 + i * 8])[0]
            add_iter(model, "var%d" % (i + 1), "%d" % (var / 10000), off + i * 8, 8, "<d", parent=fr_iter)
        off += 48
        if not frameflag:
            off += 8

    num_para = struct.unpack('<I', data[off:off + 4])[0]
    add_iter(model, "# of paragraphs", num_para, off, 4, "<I")
    off += 4

    for i in range(num_para):
        stlId = struct.unpack('<I', data[off:off + 4])[0]
        add_iter(model, "Style ID", stlId, off, 4, "<I")
        off += 4
        numst = struct.unpack('<I', data[off:off + 4])[0]
        add_iter(model, "# style recs", numst, off, 4, "<I")
        off += 4
        for k in range(numst):
            stlen = 60
            if data[off] & 0x10:
                stlen += 4
            if data[off] & 0x20:
                stlen += 4
            siter = add_iter(model, "style %d" % k, "...", off, stlen, "txt")
            txsm6style(page, model, siter, data[off:off + stlen], off)
            off += stlen
        numch = struct.unpack('<I', data[off:off + 4])[0]
        txt_iter = add_iter(model, "Text %d" % i, "# of chars: %d" % numch, off, 4, "<I")
        off += 4
        off += 4 # txt ID?
        for k in range(numch):
            add_iter(model, "Char %d" % k, "%s\t(%#x, style %d)" % data[off].decode("latin-1"),
                struct.unpack("<H", data[off + 8:off + 10])[0],
                struct.unpack("<H", data[off + 6:off + 8])[0], off, 12, "txt", parent=txt_iter)
            off += 12

def txsm(page, model, options, data):
    if page.doc_data["version"] > 15:
        txsm16(page, model, options, data)
        return
    elif page.doc_data["version"] < 7:
        txsm6(page, model, options, data)
        return

    # 6 -- 0/3, all other versions are 8/x
    # 7 -- 2, 8 -- 3, 8bidi,9 -- 4
    # 10,11 -- 5; 12 -- 6; 13 -- 8; 14 -- 9; 15 -- b; 16 -- c; 17-21 -- d;
    # 22, 23 -- e

    off = 0
    frameflag = struct.unpack('<i', data[off:off + 4])[0]

    off = 0x24
    if page.doc_data["version"] == 15:
        off += 1
    if page.doc_data["version"] < 8:
        # ver7 has it here
        txtonpath = struct.unpack('<I', data[off:off + 4])[0]
        off += 4
        if txtonpath == 1:
            add_iter(model, "tonp var1", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp var3", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp Offset", struct.unpack('<I', data[off:off + 4])[0] / 10000, off, 4, "<I")
            off += 4
            add_iter(model, "tonp var4", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp Distance", struct.unpack('<I', data[off:off + 4])[0] / 10000, off, 4, "<I")
            off += 4
            # var5 could be "orientation" for ver 12
            add_iter(model, "tonp var5", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            add_iter(model, "tonp var6", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4
            # Mirror Hor could be same as var5 in newer versions
            add_iter(model, "tonp var7", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
            off += 4

    num_frames = struct.unpack("<I", data[off:off + 4])[0]
    add_iter(model, "Num Frames", num_frames, off, 4, "<I")
    off += 4

    if num_frames > 5:
        # catch: we don't have samples with more than 4 frames
        return

    for i in range(num_frames):
        fr_iter = add_iter(model, "Frame ID", d2hex(data[off:off + 4]), off, 4, "<I")
        off += 4
        for i in range(6):
            var = struct.unpack('<d', data[off + i * 8:off + 8 + i * 8])[0]
            add_iter(model, "var%d" % (i + 1), "%d" % (var / 10000), off + i * 8, 8, "<d", parent=fr_iter)
        off += 48
        if page.doc_data["version"] > 7:
            txtonpath = struct.unpack('<I', data[off:off + 4])[0]
            off += 4
            if txtonpath == 1:
                add_iter(model, "tonp var1", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                off += 4
                if page.doc_data["version"] > 12:
                    add_iter(model, "tonp Orientation", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                    off += 4
                    add_iter(model, "tonp var2", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                    off += 4
                add_iter(model, "tonp var3", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                off += 4
                add_iter(model, "tonp Offset", struct.unpack('<I', data[off:off + 4])[0] / 10000, off, 4, "<I")
                off += 4
                add_iter(model, "tonp var4", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                off += 4
                add_iter(model, "tonp Distance", struct.unpack('<I', data[off:off + 4])[0] / 10000, off, 4, "<I")
                off += 4
                # var5 could be "orientation" for ver 12
                add_iter(model, "tonp var5", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                off += 4
                add_iter(model, "tonp Mirror Vert", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                off += 4
                # Mirror Hor could be same as var5 in newer versions
                add_iter(model, "tonp Mirror Hor", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                off += 4
                if page.doc_data["version"] == 15:
                    add_iter(model, "tonp var6", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                    off += 4
                    add_iter(model, "tonp var7", struct.unpack('<I', data[off:off + 4])[0], off, 4, "<I")
                    off += 4
            else:
                if page.doc_data["version"] == 15:
                    frame_unkn1 = struct.unpack('<I', data[off:off + 4])[0]
                    add_iter(model, "frame unknw1", "%d" % frame_unkn1, off, 4, "<I", parent=fr_iter)
                    off += 4
                    frame_xfid = struct.unpack('<I', data[off:off + 4])[0]
                    add_iter(model, "frame xform ID", "%d" % frame_xfid, off, 4, "<I", parent=fr_iter)
                    off += 4
                    # palette_frames file has unkn1 == 1 and unkn2 == 3 (ver21), 0x16 (ver16)
                    if frame_unkn1 == 1:
                        for k in range(6):
                            var = struct.unpack('<d', data[off + k * 8:off + 8 + k * 8])[0]
                            add_iter(model, "frame var%d" % (k + 1), "%d" % var, off + k * 8, 8, "<d", parent=fr_iter)
                        off += 48
                        off += 20

        if frameflag == 0:
            if page.doc_data["version"] == 15:
                off += 40
            if page.doc_data["version"] == 14:
                off += 36
            if page.doc_data["version"] in [8.01, 9, 10, 11, 12, 13]: # 8.01 -- 8bidi
                off += 34
            if page.doc_data["version"] == 8:
                off += 32
            if page.doc_data["version"] == 7:
                off += 36 # !!! txt-on-path is before frame, hence things are rearranged
        else:
            if page.doc_data["version"] == 15:
                off += 4
    num_para = struct.unpack('<I', data[off:off + 4])[0]
    add_iter(model, "# of paragraphs", num_para, off, 4, "<I")
    off += 4

    if num_para > 40:
        # catch: we don't have samples with that many para
        return

    for _ in range(num_para):
        para_iter = add_iter(model, "Para ID", d2hex(data[off:off + 4]), off, 4, "<I")
        off += 5 # !!! one more byte
        if page.doc_data["version"] > 12 and frameflag != 0:
            off += 1 # ??? extra 00
        numst = struct.unpack('<I', data[off:off + 4])[0]
        add_iter(model, "# style recs", numst, off, 4, "<I", parent=para_iter)
        off += 4

        for sti in range(numst):
            off += 1 # num of chars using the style
            flag1 = data[off]
            off += 1
            flag2 = data[off]
            off += 1
            stflags = 3 # no need for parsing, just visuals
            if page.doc_data["version"] > 7:
                flag3 = data[off] # seems to be 8 all the time
                off += 1
                stflags = 4
            st_iter = add_iter(model, "Style %#02x" % (sti * 2), "", off - stflags, stflags, "txt", parent=para_iter)
            if flag2 & 1:
                # Font
                enctxt = "Unknown"
                enc = struct.unpack("<H", data[off + 2:off + 4])[0]
                if enc in ms_charsets:
                    enctxt = ms_charsets[enc]
                add_iter(model, "\tFont ID, Charset", "%s, %s (%02x)" % (d2hex(data[off:off + 2]), enctxt, enc), off, 4, "txt", parent=st_iter)
                off += 4
            if flag2 & 2:
                # Bold/Italic etc
                add_iter(model, "\tFont Style", d2hex(data[off:off + 4]), off, 4, "txt", parent=st_iter)
                off += 4
            if flag2&4:
                # Font Size
                add_iter(model, "\tFont Size", struct.unpack("<I", data[off:off + 4])[0] * 72 / 254000, off, 4, "txt", parent=st_iter)
                off += 4
            if flag2&8:
                # assumption
                add_iter(model, "\tRotate", struct.unpack("<i", data[off:off + 4])[0] / 1000000, off, 4, "txt", parent=st_iter)
                off += 4
            if flag2&0x10:
                # Offset X
                add_iter(model, "\tOffsetX", struct.unpack("<i", data[off:off + 4])[0], off, 4, "txt", parent=st_iter)
                off += 4
            if flag2 & 0x20:
                # Offset Y
                add_iter(model, "\tOffsetY", struct.unpack("<i",data[off:off + 4])[0], off, 4, "txt", parent=st_iter)
                off += 4
            if flag2&0x40:
                # Fild ID (font colour)
                add_iter(model, "\tFild ID", d2hex(data[off:off + 4]), off, 4, "txt", parent=st_iter)
                off += 4
                if page.doc_data["version"] > 12:
                    off += 48 # ftil
            if flag2&0x80:
                # Outl ID (colour of the text outline)
                add_iter(model, "\tOutl ID", d2hex(data[off:off + 4]), off, 4, "txt", parent=st_iter)
                off += 4
            if page.doc_data["version"] > 7:
                if flag3&8 == 8:
                    if page.doc_data["version"] > 12:
                        tlen = struct.unpack("<I", data[off:off + 4])[0]
                        txt = data[off+4:off+4+tlen*2].decode("utf-16")
                        add_iter(model, "\tEncoding", txt, off, 4 + tlen * 2, "txt", parent=st_iter)
                        off += 4 + tlen * 2
                    else:
                        enc = data[off:off + 2]
                        add_iter(model, "\tEncoding", enc, off, 4, "txt", parent=st_iter)
                        off += 4
                if flag3&0x20:
                    flag = data[off]
                    add_iter(model, "\tFild/ftil flag", flag , off, 1, "<B", parent=st_iter)
                    if flag:
                        add_iter(model, "\tFild ID", d2hex(data[off:off + 4]), off, 4, "txt", parent=st_iter)
                        off += 4
                        if page.doc_data["version"] > 14:
                            off += 48 # ftil

        chars_num = struct.unpack('<I', data[off:off + 4])[0]
        add_iter(model, "# of characters", chars_num, off, 4, "<I", parent=para_iter)
        off += 4
        char_len = 4
        if page.doc_data["version"] > 11:
            char_len = 8
        for i in range(chars_num):
            add_iter(model, "char %#02x" % i, d2hex(data[off:off + char_len]), off, char_len, "txt", parent=para_iter)
            off += char_len
        # FIXME! I'm ignoring encoding...
        if page.doc_data["version"] > 11:
            off += 4 # chars_num value is repeated here
        add_iter(model, "text", data[off:off + chars_num].decode("latin-1"), off, chars_num, "txt", parent=para_iter)
        off += chars_num
        off += 1

def user(page, model, options, data):
    add_iter(model, "PS fill ID", d2hex(data[0:2]), 0, 2, "<H")
    if page.doc_data["version"] > 11:
        psname = data[2:].decode("utf-16")
        pslen = len(psname) * 2
    else:
        psname = data[2:].decode("utf-8")
        pslen = len(psname)
    add_iter(model, "PS fill name", psname, 2, pslen, "txt")

def vpat(page, model, options, data):
    add_iter(model, "Vect ID", "%08x" % struct.unpack("<I", data[0:4])[0], 0, 4, "<I")


loda_type_func = {
    0xa: loda_outl,
    0x14: loda_fild,
    0x1e:loda_coords,
    0x28:loda_rot_center,
    0x64:loda_trfd,
    0xc8:loda_stlt,
    #0xc9 loda_description,
    0x3e8:loda_name,
    0x7d0:loda_palt,
    0x1f40:loda_lens,
    0x1f45:loda_contnr,
    0x2af8:loda_polygon,
    0x2eea:loda_grad,
    0x2efe:loda_rot,
    0x32c8:loda_wroff,
    0x32c9:loda_wrstyle,
    0x4ace:loda_mesh
}

# ===============================================================================================
def version(data):
    v = 0
    if data[:4] == b"RIFF" and data[8:0xb] == b"CDR":
        v = data[0xb]
        if v < 0x21:
            v = 3
        elif v < 0x41:
            v = v - 48
        elif v < 0x4a:
            v = v - 55
        else:
            v = v - 56
    return v

def parse_stlt(page, parent, data, **kwargs):
    # tmpcache -- probably will use to cross reference between stlt and fild/outl etc
    tmpcache = {}
    # FIXME! ver 13 and newer is different
    offset = 4
    d1 = struct.unpack("<I", data[offset:offset + 4])[0]
    # this num matches with num of (un)named records in "set 11" below
    page.pgi_add(parent=parent, name="Num of style entries [%d]" % d1, type="cdr/stlt_d1", data=data[offset:offset + 4])
    offset += 4
    d2 = struct.unpack("<I", data[offset:offset + 4])[0]
    s_iter = page.pgi_add(parent=parent, name="fild list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
    offset += 4
    for i in range(d2):
        t_iter = page.pgi_add(parent=s_iter, name="%s | %s | %s" % (d2hex(data[offset:offset + 4]), d2hex(data[offset + 4:offset + 8]), d2hex(data[offset + 8:offset + 12])), type="cdr/stlt_ds0", data=data[offset:offset + 12])
        tmpcache[d2hex(data[offset:offset + 4])] = t_iter
#        add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]),"stlt fill")
        offset += 12
        if page.doc_data["version"] > 12:
            page.pgi_add(parent=s_iter, name="\tTrafo?", type="cdr/stlt_d1", data=data[offset:offset + 48])
            offset += 48
    d2 = struct.unpack("<I", data[offset:offset + 4])[0]
    s_iter = page.pgi_add(parent=parent, name="outl list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
    offset += 4
    for i in range(d2):
        t_iter = page.pgi_add(parent=s_iter, type="cdr/stlt_s1", name="%s | %s | %s" % (d2hex(data[offset:offset + 4]), d2hex(data[offset + 4:offset + 8]), d2hex(data[offset + 8:offset + 12])), data=data[offset:offset + 12])
        tmpcache[d2hex(data[offset:offset + 4])] = t_iter
        # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt outl")
        offset += 12
    size = 60
    if page.doc_data["version"] < 10:
        size = 44
    d2 = struct.unpack("<I", data[offset:offset + 4])[0]
    s_iter = page.pgi_add(parent=parent, name="font list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
    offset += 4
    for i in range(d2):
        t_iter = page.pgi_add(parent=s_iter, name="ID %s" % d2hex(data[offset:offset + 4]), type="cdr/stlt_s2", data=data[offset:offset + size])
        tmpcache[d2hex(data[offset:offset + 4])] = t_iter
        # add_dictiter(page, t_iter,d2hex(data[offset:offset + 4]), "stlt font")
        offset += size
    d2 = struct.unpack("<I", data[offset:offset + 4])[0]
    s_iter = page.pgi_add(parent=parent, name="align list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
    offset += 4
    for i in range(d2):
        t_iter =page.pgi_add(parent=s_iter, name="ID %s" % d2hex(data[offset:offset + 4]), type="cdr/stlt_s3", data=data[offset:offset + 12])
        tmpcache[d2hex(data[offset:offset + 4])] = t_iter
        # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt algn")
        offset += 12
    size = 52
    d2 = struct.unpack("<I",data[offset:offset + 4])[0]
    s_iter = page.pgi_add(parent=parent, name="interval list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
    offset += 4
    for i in range(d2):
        t_iter = page.pgi_add(parent=s_iter, name="ID %s" % d2hex(data[offset:offset + 4]), type="cdr/stlt_s4", data=data[offset:offset + size])
        tmpcache[d2hex(data[offset:offset + 4])] = t_iter
        # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt intr")
        offset += size
    size = 152
    d2 = struct.unpack("<I", data[offset:offset + 4])[0]
    set5d2 = d2
    s_iter = page.pgi_add(parent=parent, name="set5 [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
    offset += 4
    for i in range(d2):
        t_iter = page.pgi_add(parent=s_iter, name="ID %s" % d2hex(data[offset:offset + 4]), type="cdr/stlt_s5", data=data[offset:offset + size])
        tmpcache[d2hex(data[offset:offset + 4])] = t_iter
        # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt set5")
        offset += size
    size = 784
    d2 = struct.unpack("<I", data[offset:offset + 4])[0]
    s_iter = page.pgi_add(parent=parent, name="Tabs list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
    offset += 4
    for i in range(d2):
        t_iter = page.pgi_add(parent=s_iter, name="ID %s" % d2hex(data[offset:offset + 4]), type="cdr/stlt_s6", data=data[offset:offset + size])
        tmpcache[d2hex(data[offset:offset + 4])] = t_iter
        # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt tabs")
        offset += size
    bkpoff = offset
    try:
        d2 = struct.unpack("<I", data[offset:offset + 4])[0]
        s_iter = page.pgi_add(parent=parent, name="bullet list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
        offset += 4

        size = 80
        shift = 0
        if page.doc_data["version"] < 10:  # VERIFY in what version it was changed
            size = 72
            shift = -8

        for i in range(d2):
            flag = struct.unpack("<I", data[offset + 68 + shift:offset + 72 + shift])[0]
            if flag:
                inc = 8
            else:
                inc = 0
            if page.doc_data["version"] > 12:
                if struct.unpack("<I", data[offset + 0x2c:offset + 0x30])[0]:
                    inc = 32
                    if page.doc_data["version"] > 13:
                        inc = 36
                else:
                    inc = -24
                    if page.doc_data["version"] > 13:
                        inc = -20
            t_iter = page.pgi_add(parent=s_iter, name="ID %s" % d2hex(data[offset:offset + 4]), type="cdr/stlt_s7", data=data[offset:offset + size + inc])
            tmpcache[d2hex(data[offset:offset + 4])] = t_iter
            # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt bult")
            offset += size + inc

        size = 28
        d2 = struct.unpack("<I", data[offset:offset + 4])[0]
        s_iter = page.pgi_add(parent=parent, name="Indent list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
        offset += 4
        for i in range(d2):
            t_iter = page.pgi_add(parent=s_iter, name="ID %s (pID %s)" % (d2hex(data[offset:offset + 4]), d2hex(data[offset + 4:offset + 8])), type="cdr/stlt_s8", data=data[offset:offset + size])
            tmpcache[d2hex(data[offset:offset + 4])] = t_iter
            # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt indn")
            offset += size
        d2 = struct.unpack("<I", data[offset:offset + 4])[0]
        size = 32
        if page.doc_data["version"] > 12:
            size = 36
        s_iter = page.pgi_add(parent=parent, name="Hypen list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
        offset += 4
        for i in range(d2):
            t_iter = page.pgi_add(parent=s_iter, name="ID %s" % d2hex(data[offset:offset + 4]), type="cdr/stlt_s9", data=data[offset:offset + size])
            tmpcache[d2hex(data[offset:offset + 4])] = t_iter
            # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt hypn")
            offset += size
        d2 = struct.unpack("<I", data[offset:offset + 4])[0]
        size = 28
        s_iter = page.pgi_add(parent=parent, name="Dropcap list [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
        offset += 4
        for i in range(d2):
            t_iter = page.pgi_add(parent=s_iter, name="ID %s" % d2hex(data[offset:offset + 4]), type="cdr/stlt_s10", data=data[offset:offset+size])
            tmpcache[d2hex(data[offset:offset + 4])] = t_iter
            # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt drcp")
            offset += size
        set11flag = 0
        if page.doc_data["version"] > 8:
            size = 12
            set11flag = 1
            d2 = struct.unpack("<I", data[offset:offset + 4])[0]
            s_iter = page.pgi_add(parent=parent, name="set11 [%u]" % d2, type="cdr/stlt_d2", data=data[offset:offset + 4])
            offset += 4
            for i in range(d2):
                t_iter = page.pgi_add(parent=s_iter, name="ID %s" % d2hex(data[offset:offset + 4]), type="cdr/stlt_s11", data=data[offset:offset + size])
                tmpcache[d2hex(data[offset:offset + 4])] = t_iter
                # add_dictiter(page, t_iter, d2hex(data[offset:offset + 4]), "stlt st11")
                offset += size
    # size after name
    # dword 1:	3		2		1
    # ver <10:	44	24	8
    # ver 10+:	48	28	8

        s_iter = page.pgi_add(parent=parent, name="set12", type="cdr/", data=b"")
        # based on the latest idea -- parse to end
        while offset < len(data):
            num = struct.unpack("<I", data[offset:offset + 4])[0]
            num2 = struct.unpack("<I", data[offset + 12:offset + 16])[0]
            t_iter = page.pgi_add(parent=s_iter, name="num %d ID %s (pID %s) %s" % (num, d2hex(data[offset + 4:offset + 8]), d2hex(data[offset + 8:offset + 12]), d2hex(data[offset + 12:offset + 16])), type="cdr/stlt_s12", data=data[offset:offset + 20])
            tmpcache[d2hex(data[offset + 4:offset + 8])] = t_iter
            # add_dictiter(page, t_iter, d2hex(data[offset + 4:offset + 8]), "stlt st12")
            if num == 3:
                asize = 48
            elif num == 2:
                asize = 28
            else: # num == 1
                asize = 8
            offset += 20
            if page.doc_data["version"] <= 8 and num > 1 and set11flag ==0: # was PV 10
                asize -= 4
            namelen = struct.unpack("<I", data[offset:offset + 4])[0]
            if page.doc_data["version"] < 12:
                # ended with \0
                # FIXME! I don't know where to take encoding for style names, set <s>Russian</s> UTF8 just for now
                name = data[offset + 4:offset + 4 + namelen - 1].decode("utf-8")
            else:
                namelen *= 2
                name = data[offset + 4:offset + 4 + namelen].decode("utf-16")
            page.pgi_add(parent=s_iter, name="\t[%s]" % name, type="cdr/stlt_s12_p2", data=data[offset:offset + 4 + namelen + asize])
            offset += 4 + asize + namelen
    except Exception as e:
        page.pgi_add(parent=parent, name="Tail", type="cdr/", data=data[bkpoff:])
        debug("stlt exception, see 'tail'", e)

def unpack(page, parent, data, blocksizes=(), **kwargs):
    decomp = zlib.decompressobj()
    uncmprdata = decomp.decompress(data)
    offset = 0
    while offset < len(uncmprdata):
        size = parse(page, parent, uncmprdata[offset:], blocksizes=blocksizes, **kwargs)
        offset += 8 + size

def cmpr(page, parent, data, blocksizes=(), fmttype="cdr", **kwargs):
    cmprsize = struct.unpack('<I', data[4:8])[0]
    uncmprsize = struct.unpack('<I', data[8:12])[0]
    blcks = struct.unpack('<I', data[12:16])[0]
    blcksdata = zlib.decompress(data[28 + cmprsize:])
    blocksizes = []
    for i in range(0, len(blcksdata), 4):
        blocksizes.append(struct.unpack('<I', blcksdata[i:i + 4])[0])
    unpack(page, parent, data[28:], blocksizes=blocksizes, **kwargs)

def parse_init(page, parent, data, kw):
    # need that to put version into kwargs
    ver = version(data)
    kw['version'] = ver
    page.doc_data["version"] = ver
    parse(page, parent, data, **kw)

    page.main_tview.freeze_child_notify()
    page.on_load_complete()

def parse(page, parent, data, *, off=0, blocksizes=(), fmttype="cdr", **kwargs):
    rec_frcc = data[off:off + 4].decode("utf-8")
    rec_len = struct.unpack('<I', data[off + 4:off + 8])[0]
    if blocksizes:
        rec_len = blocksizes[rec_len]
    if rec_len & 1:
        rec_len += 1
    if rec_frcc in ["RIFF", "LIST"]:
        rec_name = data[off + 8:off + 12].decode("utf-8")
        f_iter = page.pgi_add(parent=parent, name="%s" % rec_name, type="%s" % rec_frcc, data=data[off + 8: off + 8 + rec_len])
        if rec_name == 'vect':
            parse(page, f_iter, data[16:], fmttype="cmx", **kwargs)
        if rec_name == 'stlt' and kwargs["version"] > 6:
            try:
                parse_stlt(page, f_iter, data[8:8 + rec_len], **kwargs)
            except Exception as e:
                debug("Something failed in 'stlt'.", e)
        elif rec_name == 'cmpr':
            cmpr(page, f_iter, data[8:8 + rec_len], blocksizes=blocksizes, **kwargs)
        else:
            off = 12
            while off < 8 + rec_len:
                size = parse(page, f_iter, data[off:], blocksizes=blocksizes, **kwargs)
                off += 8 + size
#==========================================================================================================
#  need to get "wdata" from pkzip
#=============
    elif page.doc_data["version"] > 15:
        try:
            rdata = data[off + 8:off + 8 + rec_len]
            strid = struct.unpack("<i", rdata[:4])[0]
            off1 = struct.unpack("<I", rdata[8:12])[0]
            off2 = off1 + struct.unpack("<I", rdata[4:8])[0]
            if strid != -1:
                ci = page.doc_data["wdata"][page.doc_data["wtable"][strid]]
                dddata = page.model.get_value(ci, 3)[off1:off2]
                p_iter = page.pgi_add(parent=ci, name="%s [%04x - %04x]" % (rec_frcc, off1, off2), type="cdr/%s" % rec_frcc, data=dddata)
                ##page.model.set_value(p_iter, 8, ("path", page.model.get_string_from_iter(f_iter)))
                ##page.model.set_value(f_iter, 8, ("path", page.model.get_string_from_iter(p_iter)))

                if 0: #if rec_frcc == "outl" or rec_frcc == "fild" or rec_frcc == "arrx" or rec_frcc == "bmpf":
                    ##d_iter = add_dictiter(page, p_iter, d2hex(data[0:4]), rec_frcc)
                    if rec_frcc == "fild":
                        off = 4
                        if page.doc_data["version"] > 12:
                            off = 12
                        t = struct.unpack("<H", dddata[off:off + 2])[0]
                        ttxt = key2txt(t, fild_types)
                        #page.dictmod.set_value(d_iter, 3, "0x%02x (%s)" % (t, ttxt))
                if rec_frcc == 'stlt':
                    try:
                        parse_stlt(page, p_iter, b"stlt" + dddata)
                    except:
                        debug("Something failed in 'stlt'.")

                if 0: # rec_frcc == 'mcfg':
                    if page.doc_data["version"] == 6:
                        page.hd.width = struct.unpack("<I", dddata[0x1c:0x20])[0] / 10000
                        page.hd.height = struct.unpack("<I", dddata[0x20:0x24])[0] / 10000
                    elif page.doc_data["version"] < 6:
                        page.hd.width = struct.unpack("<H", dddata[0x1c:0x20])[0] * 0.0254
                        page.hd.height = struct.unpack("<H", dddata[0x20:0x24])[0] * 0.0254
                    else:
                        page.hd.width = struct.unpack("<I", dddata[4:8])[0] / 10000
                        page.hd.height = struct.unpack("<I", dddata[8:12])[0] / 10000
        except Exception as e:
            debug("Failed in v16+ dat", e)

#==========================================================================================================

    else:
        page.pgi_add(parent=parent, name="%s" % rec_frcc, type="cdr/%s" % rec_frcc, data=data[off + 8: off + 8 + rec_len])
    return rec_len
