# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import struct
import sys

from formats.mf import *
from utils import *


TernaryRasterOperation = {
    0x00: "BLACKNESS",
    0x01: "DPSoon",
    0x02: "DPSona",
    0x03: "PSon",
    0x04: "SDPona",
    0x05: "DPon",
    0x06: "PDSxnon",
    0x07: "PDSaon",
    0x08: "SDPnaa",
    0x09: "PDSxon",
    0x0a: "DPna",
    0x0B: "PSDnaon",
    0x0C: "SPna",
    0x0D: "PDSnaon",
    0x0E: "PDSonon",
    0x0F: "Pn",
    0x10: "PDSona",
    0x11: "NOTSRCERASE",
    0x12: "SDPxnon",
    0x13: "SDPaon",
    0x14: "DPSxnon",
    0x15: "DPSaon",
    0x16: "PSDPSanaxx",
    0x17: "SSPxDSxaxn",
    0x18: "SPxPDxa",
    0x19: "SDPSanaxn",
    0x1a: "PDSPaox",
    0x1B: "SDPSxaxn",
    0x1C: "PSDPaox",
    0x1D: "DSPDxaxn",
    0x1E: "PDSox",
    0x1F: "PDSoan",
    0x20: "DPSnaa",
    0x21: "SDPxon",
    0x22: "DSna",
    0x23: "SPDnaon",
    0x24: "SPxDSxa",
    0x25: "PDSPanaxn",
    0x26: "SDPSaox",
    0x27: "SDPSxnox",
    0x28: "DPSxa",
    0x29: "PSDPSaoxxn",
    0x2a: "DPSana",
    0x2B: "SSPxPDxaxn",
    0x2C: "SPDSoax",
    0x2D: "PSDnox",
    0x2E: "PSDPxox",
    0x2F: "PSDnoan",
    0x30: "PSna",
    0x31: "SDPnaon",
    0x32: "SDPSoox",
    0x33: "NOTSRCCOPY",
    0x34: "SPDSaox",
    0x35: "SPDSxnox",
    0x36: "SDPox",
    0x37: "SDPoan",
    0x38: "PSDPoax",
    0x39: "SPDnox",
    0x3a: "SPDSxox",
    0x3B: "SPDnoan",
    0x3C: "PSx",
    0x3D: "SPDSonox",
    0x3E: "SPDSnaox",
    0x3F: "PSan",
    0x40: "PSDnaa",
    0x41: "DPSxon",
    0x42: "SDxPDxa",
    0x43: "SPDSanaxn",
    0x44: "SRCERaSE",
    0x45: "DPSnaon",
    0x46: "DSPDaox",
    0x47: "PSDPxaxn",
    0x48: "SDPxa",
    0x49: "PDSPDaoxxn",
    0x4a: "DPSDoax",
    0x4B: "PDSnox",
    0x4C: "SDPana",
    0x4D: "SSPxDSxoxn",
    0x4E: "PDSPxox",
    0x4F: "PDSnoan",
    0x50: "PDna",
    0x51: "DSPnaon",
    0x52: "DPSDaox",
    0x53: "SPDSxaxn",
    0x54: "DPSonon",
    0x55: "DSTINVERT",
    0x56: "DPSox",
    0x57: "DPSoan",
    0x58: "PDSPoax",
    0x59: "DPSnox",
    0x5a: "PATINVERT",
    0x5B: "DPSDonox",
    0x5C: "DPSDxox",
    0x5D: "DPSnoan",
    0x5E: "DPSDnaox",
    0x5F: "DPan",
    0x60: "PDSxa",
    0x61: "DSPDSaoxxn",
    0x62: "DSPDoax",
    0x63: "SDPnox",
    0x64: "SDPSoax",
    0x65: "DSPnox",
    0x66: "SRCINVERT",
    0x67: "SDPSonox",
    0x68: "DSPDSonoxxn",
    0x69: "PDSxxn",
    0x6a: "DPSax",
    0x6B: "PSDPSoaxxn",
    0x6C: "SDPax",
    0x6D: "PDSPDoaxxn",
    0x6E: "SDPSnoax",
    0x6F: "PDxnan",
    0x70: "PDSana",
    0x71: "SSDxPDxaxn",
    0x72: "SDPSxox",
    0x73: "SDPnoan",
    0x74: "DSPDxox",
    0x75: "DSPnoan",
    0x76: "SDPSnaox",
    0x77: "DSan",
    0x78: "PDSax",
    0x79: "DSPDSoaxxn",
    0x7a: "DPSDnoax",
    0x7B: "SDPxnan",
    0x7C: "SPDSnoax",
    0x7D: "DPSxnan",
    0x7E: "SPxDSxo",
    0x7F: "DPSaan",
    0x80: "DPSaa",
    0x81: "SPxDSxon",
    0x82: "DPSxna",
    0x83: "SPDSnoaxn",
    0x84: "SDPxna",
    0x85: "PDSPnoaxn",
    0x86: "DSPDSoaxx",
    0x87: "PDSaxn",
    0x88: "SRCAND",
    0x89: "SDPSnaoxn",
    0x8a: "DSPnoa",
    0x8B: "DSPDxoxn",
    0x8C: "SDPnoa",
    0x8D: "SDPSxoxn",
    0x8E: "SSDxPDxax",
    0x8F: "PDSanan",
    0x90: "PDSxna",
    0x91: "SDPSnoaxn",
    0x92: "DPSDPoaxx",
    0x93: "SPDaxn",
    0x94: "PSDPSoaxx",
    0x95: "DPSaxn",
    0x96: "DPSxx",
    0x97: "PSDPSonoxx",
    0x98: "SDPSonoxn",
    0x99: "DSxn",
    0x9a: "DPSnax",
    0x9B: "SDPSoaxn",
    0x9C: "SPDnax",
    0x9D: "DSPDoaxn",
    0x9E: "DSPDSaoxx",
    0x9F: "PDSxan",
    0xa0: "DPa",
    0xa1: "PDSPnaoxn",
    0xa2: "DPSnoa",
    0xa3: "DPSDxoxn",
    0xa4: "PDSPonoxn",
    0xa5: "PDxn",
    0xa6: "DSPnax",
    0xa7: "PDSPoaxn",
    0xa8: "DPSoa",
    0xa9: "DPSoxn",
    0xaa: "D",
    0xaB: "DPSono",
    0xaC: "SPDSxax",
    0xaD: "DPSDaoxn",
    0xaE: "DSPnao",
    0xaF: "DPno",
    0xB0: "PDSnoa",
    0xB1: "PDSPxoxn",
    0xB2: "SSPxDSxox",
    0xB3: "SDPanan",
    0xB4: "PSDnax",
    0xB5: "DPSDoaxn",
    0xB6: "DPSDPaoxx",
    0xB7: "SDPxan",
    0xB8: "PSDPxax",
    0xB9: "DSPDaoxn",
    0xBa: "DPSnao",
    0xBB: "MERGEPAINT",
    0xBC: "SPDSanax",
    0xBD: "SDxPDxan",
    0xBE: "DPSxo",
    0xBF: "DPSano",
    0xC0: "MERGECOPY",
    0xC1: "SPDSnaoxn",
    0xC2: "SPDSonoxn",
    0xC3: "PSxn",
    0xC4: "SPDnoa",
    0xC5: "SPDSxoxn",
    0xC6: "SDPnax",
    0xC7: "PSDPoaxn",
    0xC8: "SDPoa",
    0xC9: "SPDoxn",
    0xCa: "DPSDxax",
    0xCB: "SPDSaoxn",
    0xCC: "SRCCOPY",
    0xCD: "SDPono",
    0xCE: "SDPnao",
    0xCF: "SPno",
    0xD0: "PSDnoa",
    0xD1: "PSDPxoxn",
    0xD2: "PDSnax",
    0xD3: "SPDSoaxn",
    0xD4: "SSPxPDxax",
    0xD5: "DPSanan",
    0xD6: "PSDPSaoxx",
    0xD7: "DPSxan",
    0xD8: "PDSPxax",
    0xD9: "SDPSaoxn",
    0xDa: "DPSDanax",
    0xDB: "SPxDSxan",
    0xDC: "SPDnao",
    0xDD: "SDno",
    0xDE: "SDPxo",
    0xDF: "SDPano",
    0xE0: "PDSoa",
    0xE1: "PDSoxn",
    0xE2: "DSPDxax",
    0xE3: "PSDPaoxn",
    0xE4: "SDPSxax",
    0xE5: "PDSPaoxn",
    0xE6: "SDPSanax",
    0xE7: "SPxPDxan",
    0xE8: "SSPxDSxax",
    0xE9: "DSPDSanaxxn",
    0xEa: "DPSao",
    0xEB: "DPSxno",
    0xEC: "SDPao",
    0xED: "SDPxno",
    0xEE: "SRCPAINT",
    0xEF: "SDPnoo",
    0xF0: "PATCOPY",
    0xF1: "PDSono",
    0xF2: "PDSnao",
    0xF3: "PSno",
    0xF4: "PSDnao",
    0xF5: "PDno",
    0xF6: "PDSxo",
    0xF7: "PDSano",
    0xF8: "PDSao",
    0xF9: "PDSxno",
    0xFa: "DPo",
    0xFB: "PATPAINT",
    0xFC: "PSo",
    0xFD: "PSDnoo",
    0xFE: "DPSoo",
    0xFF: "WHITENESS",
}

def RecHdr(model, data):
    size = struct.unpack("<I", data[:4])[0]
    add_iter(model, "Size", size, 0, 4, "<I")
    rtype = struct.unpack("<H", data[4:6])[0]
    add_iter(model, "Type", "%#x" % rtype, 4, 2, "<H")
    return size, rtype

def Bitmap16(model, data, off=0):
    add_iter(model, "Type", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Width", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Height", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "WidthBytes", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Planes", struct.unpack("<B", data[off])[0], off, 1, "<B")
    off += 1
    add_iter(model, "BPP", struct.unpack("<B", data[off])[0], off, 1, "<B")

#?
def Aldus_Header(page, model, options, data):
    add_iter(model, "Signature", struct.unpack("<i", data[:4])[0], 0, 4, "<i")
    add_iter(model, "Handle", struct.unpack("<h", data[4:6])[0], 4, 2, "<h")
    PointS (model, data, 6, 'S', "wmf")
    PointS (model, data, 10, 'E', "wmf")
    add_iter(model, "Inch", struct.unpack("<h", data[14:16])[0], 14, 2, "<h")
    add_iter(model, "Reserved", struct.unpack("<i", data[16:20])[0], 16, 4, "<i")
    add_iter(model, "Checksum", struct.unpack("<h", data[20:22])[0], 20, 2, "<h")

#1
def Header(page, model, options, data):
    add_iter(model, "Type", struct.unpack("<H", data[:2])[0], 0, 2, "<H")
    add_iter(model, "HdrSize", struct.unpack("<H", data[2:4])[0], 2, 2, "<H")
    add_iter(model, "Version", struct.unpack("<H", data[4:6])[0], 4, 2, "<H")
    add_iter(model, "Size", struct.unpack("<I", data[6:10])[0], 6, 4, "<I")
    add_iter(model, "#Objects", struct.unpack("<H", data[10:12])[0], 10, 2, "<H")
    add_iter(model, "MaxRecord", struct.unpack("<I", data[12:16])[0], 12, 4, "<I")
    add_iter(model, "#Members", struct.unpack("<H", data[16:18])[0], 16, 2, "<H")

#30
def SaveDC(page, model, options, data):
    RecHdr(model, data)

#258
def SetBKMode(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "Mode", struct.unpack("<H", data[6:8])[0], 6, 2, "<h")

#259
def SetMapMode(page, model, options, data):
    SetBKMode(page, model, data)

#260
def SetROP2(page, model, options, data):
    SetBKMode(page, model, data)

#262
def SetPolyfillMode(page, model, options, data):
    SetBKMode(page, model, data)

#263
def SetStretchBltMode(page, model, options, data):
    SetBKMode(page, model, data)

#264
def SetTextCharExtra(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "Extra", struct.unpack("<i", data[6:10])[0], 6, 4, "<i")

#295
def RestoreDC(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "RestoreDC", struct.unpack("<h", data[6:8])[0], 6, 2, "<h")

#298
def InvertRegion(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "Region ID", struct.unpack("<i", data[6:10])[0], 6, 4, "<i")

#299
def PaintRegion(page, model, options, data):
    InvertRegion(page, model, data)

#300
def SelectClipRegion(page, model, options, data):
    InvertRegion(page, model, data)

#301
def SelectObject(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "Obj ID", struct.unpack("<H", data[6:8])[0], 6, 2, "<H")

#302
def SetTextAlign(page, model, options, data):
    SetBKMode(page, model, data)

#496
def DeleteObject(page, model, options, data):
    SelectObject(page, model, data)

#513
def SetBKColor(page, model, options, data):
    RecHdr(model, data)
    clr = "%02X%02X%02X" % (data[6], data[7], data[8])
    add_iter(model, "RGB", clr, 6, 3, "clr")

#521
def SetTextColor(page, model, options, data):
    SetBKColor(page, model, data)

#522
def SetTextJustification(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "Extra", struct.unpack("<i", data[6:10])[0], 6, 4, "<i")
    add_iter(model, "Count", struct.unpack("<i", data[10:14])[0], 10, 4, "<i")

#523
def SetWindowExtEx(page, model, options, data):
    RecHdr(model, data)
    PointS(model, data, 6, "", "wmf")

#524
def SetWindowOrgEx(page, model, options, data):
    SetWindowExtEx(page, model, data)

#525
def SetViewportExtEx(page, model, options, data):
    SetWindowExtEx(page, model, data)

#526
def SetViewportOrgEx(page, model, options, data):
    SetWindowExtEx(page, model, data)

#527
def OffsetWindowOrg(page, model, options, data):
    SetWindowExtEx(page, model, data)

#529
def OffsetViewportOrgEx(page, model, options, data):
    SetWindowExtEx(page, model, data)

#531
def LineTo(page, model, options, data):
    SetWindowExtEx(page, model, data)

#532
def MoveTo(page, model, options, data):
    SetWindowExtEx(page, model, data)

#544
def OffsetClipRgn(page, model, options, data):
    RecHdr(model, data)
    PointS(model, data, 6, 'off', "wmf")

#552
def FillRegion(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "Region", struct.unpack("<H", data[6:8])[0], 6, 2, "<H")
    add_iter(model, "Brush", struct.unpack("<H", data[8:10])[0], 8, 2, "<H")

#561
def SetMapperFlags(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "Flag", struct.unpack("<i", data[6:10])[0], 6, 4, "<i")

#762
def CreatePenIndirect(page, model, options, data):
    RecHdr(model, data)
    off = 6
    add_iter(model, "Style", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Width", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Height", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    clr = "%02X%02X%02X" % (data[12], data[13], data[14])
    add_iter(model, "RGB", clr, 12, 3, "clr")

#763
def CreateFontIndirect(page, model, options, data):
    RecHdr(model, data)
    off = 6
    add_iter(model, "Height", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Width", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Escapement", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Orientation", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Weight", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Italic", data[off], off, 1, "<B")
    off += 1
    add_iter(model, "Underline", data[off], off, 1, "<B")
    off += 1
    add_iter(model, "StrikeOut", data[off], off, 1, "<B")
    off += 1
    add_iter(model, "Charset", data[off], off, 1, "<B")
    off += 1
    add_iter(model, "OutPrecision", data[off], off, 1, "<B")
    off += 1
    add_iter(model, "ClipPrecision", data[off], off, 1, "<B")
    off += 1
    add_iter(model, "Quality", data[off], off, 1, "<B")
    off += 1
    add_iter(model, "Pitch&Family", data[off], off, 1, "<B")

#764
def CreateBrushIndirect(page, model, options, data):
    RecHdr(model, data)
    off = 6
    add_iter(model, "Style", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    clr = "%02X%02X%02X" % (data[off], data[off + 1], data[off + 2])
    add_iter(model, "RGB", clr, off, 3, "clr")
    add_iter(model, "Hatch", struct.unpack("<h", data[12:14])[0], 12, 2, "<h")

#804
def Polygon(page, model, options, data):
    RecHdr(model, data)
    off = 6
    [count] = struct.unpack("<H", data[6:8])
    add_iter(model, "Count", struct.unpack("<H", data[off:off + 2])[0], off, 2, "<H")
    for i in range(count):
        PointS(model, data, i * 4 + 8, str(i), "wmf")

#805
def Polyline(page, model, options, data):
    Polygon(page, model, data)

#1040
def ScaleViewportExtEx(page, model, options, data):
    RecHdr(model, data)
    off = 6
    add_iter(model, "xNum", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "xDenom", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "yNum", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "yDenom", struct.unpack("<h", data[off:off + 2])[0], off, 2, "<h")

#1042
def ScaleWindowExtEx(page, model, options, data):
    ScaleViewportExtEx(page, model, data)

#1045
def ExcludeClipRect(page, model, options, data):
    Rectangle(page, model, data)

#1046
def IntersectClipRect(page, model, options, data):
    Rectangle(page, model, data)

#1048
def Ellipse(page, model, options, data):
    Rectangle(page, model, data)

#1051
def Rectangle(page, model, options, data):
    RecHdr(model, data)
    PointS(model, data, 6, "S", "wmf")
    PointS(model, data, 10, "E", "wmf")

#1336
def PolyPolygon(page, model, options, data):
    RecHdr(model, data)
    off = 6
    [numpoly] = struct.unpack("<h", data[6:8]) #24/28
    add_iter(model, "NumOfPoly", numpoly, off, 2, "<h")
    counts = {}
    for i in range(numpoly): #32
        cnt = struct.unpack("<H", data[i * 2 + 8:i * 2 + 10])[0]
        add_iter(model, "PolyPnt %d" % i, cnt, i * 2 + 8, 2, "<H")
        counts[i]=cnt
    offset = 8 + numpoly * 2
    for i in counts:
        for j in range(counts[i]):
            PointS(model, data, offset, str(j) + " (pg%d)" % i, "wmf")
            offset += 4

#1564
def RoundRect(page, model, options, data):
    Rectangle(page, model, data)
    PointS(model, data, 14, "Diam", "wmf")

def Esc_EMF(page, model, data, offset):
    add_iter(model, "CommentId", "%#x" % struct.unpack("<I", data[offset:offset + 4])[0], offset, 4, "<I")  # 0x43464d57
    add_iter(model, "CommentType", struct.unpack("<I", data[offset + 4:offset + 8])[0], offset + 4, 4, "<I")  # 0x00000001
    add_iter(model, "Version", struct.unpack("<I", data[offset + 8:offset + 12])[0], offset + 8, 4, "<I")  # 0x00010000
    add_iter(model, "Checksum", struct.unpack("<H", data[offset + 12:offset + 14])[0], offset + 12, 2, "<H")
    add_iter(model, "Flags", struct.unpack("<I", data[offset + 14:offset + 18])[0], offset + 14, 4, "<I")  # 0x00000000
    add_iter(model, "CommentRecCount", struct.unpack("<I", data[offset + 18:offset + 22])[0], offset + 18, 4, "<I")
    add_iter(model, "CurRecSize", struct.unpack("<I", data[offset + 22:offset + 26])[0], offset + 22, 4, "<I")
    add_iter(model, "RemainingBytes", struct.unpack("<I", data[offset + 26:offset + 30])[0], offset + 26, 4, "<I")
    add_iter(model, "EMFDataSize", struct.unpack("<I", data[offset + 30:offset + 34])[0], offset + 30, 4, "<I")  # 0x00010000


#1574
def Escape(page, model, options, data):
    RecHdr(model, data)
    efunc = struct.unpack("<H", data[6:8])[0]
    if efunc in esc_func_names:
        efuncstr = "%#x (%s)" % (efunc, esc_func_names[efunc])
    else:
        efuncstr = "%#x" % efunc
    add_iter(model, "EscFunction", efuncstr, 6, 2, "<H")
    add_iter(model, "ByteSize", struct.unpack("<H", data[8:10])[0], 8, 2, "<H")
    if efunc in escape_funcs:
        escape_funcs[efunc](page, model, data, 10)

#1791
def CreateRegion(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "nextInChain", struct.unpack("<H", data[6:8])[0], 6, 2, "<H")
    add_iter(model, "ObjectType", struct.unpack("<H", data[8:10])[0], 8, 2, "<H")
    add_iter(model, "ObjectCount", struct.unpack("<I", data[10:14])[0], 10, 4, "<I")
    add_iter(model, "RegionSize", struct.unpack("<h", data[14:16])[0], 14, 2, "<h")
    sc = struct.unpack("<h", data[16:18])[0]
    add_iter(model, "ScanCount", sc, 16, 2, "<h")
    add_iter(model, "MaxScan", struct.unpack("<h", data[18:20])[0], 18, 2, "<h")
    Rect(model, data, offset=20)
    add_iter(model, "Count", struct.unpack("<H", data[28:30])[0], 28, 2, "<H")
    add_iter(model, "Top", struct.unpack("<H", data[30:32])[0], 30, 2, "<H")
    add_iter(model, "Bottom", struct.unpack("<H", data[32:34])[0], 32, 2, "<H")
    add_iter(model, "ScanLines", "BLOB", 34, len(data)-36, "blob")
    add_iter(model, "Count2", struct.unpack("<H", data[-2:])[0], len(data) - 2, 2, "<H")

#2071
def Arc(page, model, options, data):
    RecHdr(model, data)
    PointS(model, data, 6, "Rs", "wmf")
    PointS(model, data, 10, "Re", "wmf")
    PointS(model, data, 14, "S", "wmf")
    PointS(model, data, 18, "E", "wmf")

#2074
def Pie(page, model, options, data):
    Arc(page, model, data)

#2096
def Chord(page, model, options, data):
    Arc(page, model, data)

#2338
def BitBlt(page, model, options, data):
    size, rtype = RecHdr(model, data)
    off = 6
    rop = struct.unpack("<I", data[off:off + 4])[0]
    add_iter(model, "ROP", "%#04x" % rop , off, 4, "<I")
    off += 4
    add_iter(model, "YSrc", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "XSrc", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    if (rtype >> 8) + 3 == size:
        add_iter(model, "Reserved", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
        off += 2
    add_iter(model, "Height", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Width", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "YDst", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "XDst", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    Bitmap16(model, data, off=off)

#2610
def ExtTextOut(page, model, options, data):
    RecHdr(model, data)
    PointS(model, data, 6, "", "wmf")
    count = struct.unpack("<h", data[10:12])[0]
    add_iter(model, "Count", count, 10, 2, "<h")
    flags = struct.unpack("<h", data[12:14])[0]
    add_iter(model, "Flags", flags, 12, 2, "<h")
    off = 0xe
    if flags & 6:
        PointS(model, data, off, "S", "wmf")
        PointS(model, data, off + 4, "E", "wmf")
        off += 8
    add_iter(model, "Text", data[off:off + count], off, count, "txt")
    off += count
    if not flags & 0x10:
        if flags & 0x2000:
            for i in range(count):
                add_iter(model, "Dx%d" % i, struct.unpack("<H", data[off + i * 4:off + 2 + i * 4])[0], off + i * 4, 2, "<H")
                add_iter(model, "Dy%d" % i, struct.unpack("<H", data[off + 2 + i * 4:off + 4 + i * 4])[0], off + 2 + i * 4, 2, "<H")
        else:
            for i in range(count):
                add_iter(model, "Dx%d" % i, struct.unpack("<h", data[off + i * 2:off + 2 + i * 2])[0], off + i * 2, 2, "<H")
        # Fixme! DX/DY depends on flags

#2368
def DibBitblt(page, model, options, data):
    size, rtype = RecHdr(model, data)
    off = 6
    rop = struct.unpack("<I", data[off:off + 4])[0]
    add_iter(model, "ROP", "%#04x" % rop , off, 4, "<I")
    off += 4
    add_iter(model, "YSrc", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "XSrc", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    if (rtype >> 8) + 3 == size:
        add_iter(model, "Reserved", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
        off += 2
    add_iter(model, "Height", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "Width", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "YDst", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "XDst", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")

#2851
def StretchBlt(page, model, data, dib=False):
    size, rtype = RecHdr(model, data)
    off = 6
    rop = struct.unpack("<I", data[off:off + 4])[0]
    add_iter(model, "ROP", "%#04x" % rop , off, 4, "<I")
    off += 4
    add_iter(model, "SrcHeight", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "SrcWidth", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "YSrc", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "XSrc", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    bmflag = True
    if (rtype >> 8) + 3 == size:
        add_iter(model, "Reserved", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
        off += 2
        bmflag = False
    add_iter(model, "DstHeight", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "DstWidth", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "YDst", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "XDst", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    if bmflag and not dib:
        Bitmap16(model, data, off=off)
    return bmflag

#2881
def DibStretchBlt(page, model, options, data):
    bmflag = StretchBlt(page, model, data, dib=True)
    if bmflag:
        hsize = struct.unpack("<I", data[26:30])[0]
        if hsize == 0xc:
            BitmapCoreHeader(model, data, off=26)
        else:
            BitmapInfoHeader(model, data, off=26)

def StretchDIB(page, model, options, data):
    RecHdr(model, data)
    off = 6
    rop = struct.unpack("<I", data[off:off + 4])[0]
    add_iter(model, "ROP", "%#04x" % rop , off, 4, "<I")
    off += 4
    cu = struct.unpack("<H", data[off:off + 2])[0]
    add_iter(model, "ColorUsage", cu, off, 2, "<H")
    off += 2
    add_iter(model, "SrcHeight", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "SrcWidth", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "YSrc", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "XSrc", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "DstHeight", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "DstWidth", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "YDst", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")
    off += 2
    add_iter(model, "XDst", struct.unpack("<h", data[off: off + 2])[0], off, 2, "<h")


def file_save(fname, model, *args, **kwargs):
    root = model.get_iter("0")
    data = model["0:0"][3]
    ch = model.iter_children(root)
    hdrpath = "0:0"
    if data[:4] == b"\xd7\xcd\xc6\x9a":
        # AP
        chks = 0
        for i in range(10):
            chks ^= struct.unpack("<H", data[i * 2:i * 2 + 2])[0]
        model["0:0"][3] = data[:20] + struct.pack("<H", chks)
        ch = model.iter_next(ch)
        hdrpath = "0:1"
    acc = 0
    maxrec = 0
    while ch:
        rlen = len(model[ch][3])
        acc += rlen
        ch = model.iter_next(ch)
        if rlen > maxrec:
            maxrec = rlen
    num_ch = model.iter_n_children(root)
    acc //= 2
    maxrec //= 2
    data = model[hdrpath][3]
    model[hdrpath][3] = data[:6] + struct.pack("<I", acc) + data[10:12] + struct.pack("<IH", maxrec, num_ch)
    formats.mf.dump_mf_tree(fname, model, "0")

esc_func_names = {
    0x0001: "NEWFRAME", 0x0002: "ABORTDOC", 0x0003: "NEXTBAND",
    0x0004: "SETCOLORTABLE", 0x0005: "GETCOLORTABLE",
    0x0006: "FLUSHOUT", 0x0007: "DRAFTMODE", 0x0008: "QUERYESCSUPPORT",
    0x0009: "SETABORTPROC", 0x000A: "STARTDOC", 0x000B: "ENDDOC",
    0x000C: "GETPHYSPAGESIZE", 0x000D: "GETPRINTINGOFFSET",
    0x000E: "GETSCALINGFACTOR",
    0x000F: "EMF",
    0x0010: "SETPENWIDTH",
    0x0011: "SETCOPYCOUNT", 0x0012: "SETPAPERSOURCE", 0x0013: "PASSTHROUGH",
    0x0014: "GETTECHNOLOGY",
    0x0015: "SETLINECAP", 0x0016: "SETLINEJOIN", 0x0017: "SETMITERLIMIT",
    0x0018: "BANDINFO",
    0x0019: "DRAWPATTERNRECT",
    0x001A: "GETVECTORPENSIZE", 0x001B: "GETVECTORBRUSHSIZE", 0x001C: "ENABLEDUPLEX",
    0x001D: "GETSETPAPERBINS", 0x001E: "GETSETPRINTORIENT", 0x001F: "ENUMPAPERBINS",
    0x0020: "SETDIBSCALING", 0x0021: "EPSPRINTING",
    0x0022: "ENUMPAPERMETRICS", 0x0023: "GETSETPAPERMETRICS",
    0x0025: "POSTSCRIPT_DATA", 0x0026: "POSTSCRIPT_IGNORE",
    0x002A: "GETDEVICEUNITS", 0x0100: "GETEXTENDEDTEXTMETRICS", 0x0102: "GETPAIRKERNTABLE",
    0x0200: "EXTTEXTOUT",
    0x0201: "GETFACENAME", 0x0202: "DOWNLOADFACE", 0x0801: "METAFILE_DRIVER", 0x0C01: "QUERYDIBSUPPORT",
    0x1000: "BEGIN_PATH", 0x1001: "CLIP_TO_PATH", 0x1002: "END_PATH",
    0x100E: "OPENCHANNEL", 0x100F: "DOWNLOADHEADER", 0x1010: "CLOSECHANNEL", 0x1013: "POSTSCRIPT_PASSTHROUGH",
    0x1014: "ENCAPSULATED_POSTSCRIPT", 0x1015: "POSTSCRIPT_IDENTIFY, 0x1016: Esc_POSTSCRIPT_INJECTION",
    0x1017: "CHECKJPEGFORMAT", 0x1018: "CHECKPNGFORMAT", 0x1019: "GET_PS_FEATURESETTING",
    0x101A: "MXDC_ESCAPE",
    0x11D8: "SPCLPASSTHROUGH2"
}

escape_funcs = {
    ## 0x0001: Esc_NEWFRAME, 0x0002: Esc_ABORTDOC, 0x0003: Esc_NEXTBAND,

    #0x0004: Esc_SETCOLORTABLE, 0x0005: Esc_GETCOLORTABLE,

    ## 0x0006: Esc_FLUSHOUT, 0x0007: Esc_DRAFTMODE, 0x0008: Esc_QUERYESCSUPPORT,
    ## 0x0009: Esc_SETABORTPROC, 0x000A: Esc_STARTDOC, 0x000B: Esc_ENDDOC,
    ## 0x000C: Esc_GETPHYSPAGESIZE, 0x000D: Esc_GETPRINTINGOFFSET,
    ## 0x000E: Esc_GETSCALINGFACTOR,

    0x000F: Esc_EMF,

    #0x0010: Esc_SETPENWIDTH,
    ## 0x0011: Esc_SETCOPYCOUNT, 0x0012: Esc_SETPAPERSOURCE, 0x0013: Esc_PASSTHROUGH,
    ## 0x0014: Esc_GETTECHNOLOGY,

    #0x0015: Esc_SETLINECAP, 0x0016: Esc_SETLINEJOIN, 0x0017: Esc_SETMITERLIMIT,

    ## 0x0018: Esc_BANDINFO,

    #0x0019: Esc_DRAWPATTERNRECT,

    ## 0x001A: Esc_GETVECTORPENSIZE, 0x001B: Esc_GETVECTORBRUSHSIZE,    0x001C: Esc_ENABLEDUPLEX,
    ## 0x001D: Esc_GETSETPAPERBINS, 0x001E: Esc_GETSETPRINTORIENT, 0x001F: Esc_ENUMPAPERBINS,

    #0x0020: Esc_SETDIBSCALING, 0x0021: Esc_EPSPRINTING,

    ##0x0022: Esc_ENUMPAPERMETRICS, 0x0023: Esc_GETSETPAPERMETRICS,

    #0x0025: Esc_POSTSCRIPT_DATA, 0x0026: Esc_POSTSCRIPT_IGNORE,

    ## 0x002A: Esc_GETDEVICEUNITS, 0x0100: Esc_GETEXTENDEDTEXTMETRICS, 0x0102: Esc_GETPAIRKERNTABLE,

    #0x0200: Esc_EXTTEXTOUT,

    ##0x0201: Esc_GETFACENAME, 0x0202: Esc_DOWNLOADFACE, 0x0801: Esc_METAFILE_DRIVER, 0x0C01: Esc_QUERYDIBSUPPORT,

    #0x1000: Esc_BEGIN_PATH, 0x1001: Esc_CLIP_TO_PATH, 0x1002: Esc_END_PATH,

    ## 0x100E: Esc_OPENCHANNEL, 0x100F: Esc_DOWNLOADHEADER, 0x1010: Esc_CLOSECHANNEL, 0x1013: Esc_POSTSCRIPT_PASSTHROUGH,
    ## 0x1014: Esc_ENCAPSULATED_POSTSCRIPT, 0x1015: Esc_POSTSCRIPT_IDENTIFY, 0x1016: Esc_POSTSCRIPT_INJECTION,
    ## 0x1017: Esc_CHECKJPEGFORMAT, 0x1018: Esc_CHECKPNGFORMAT, 0x1019: Esc_GET_PS_FEATURESETTING,
    ## 0x101A: Esc_MXDC_ESCAPE,
    ## 0x11D8: Esc_SPCLPASSTHROUGH2
}

wmr_ids = {1: Aldus_Header,
    #2:'CLP_Header16',3:'CLP_Header32',
    1: Header, 30: SaveDC,
    #53: 'RealizePalette', 55: 'SetPalEntries', 247: 'CreatePalette',
    #313: 'ResizePalette', 564:'SelectPalette', 1078: 'AnimatePalette',
    #79: 'StartPage', 80: 'EndPage', 82: 'AbortDoc', 94: 'EndDoc', 333: 'StartDoc',
    #248: 'CreateBrush', 322: 'DibCreatePatternBrush', 505: 'CreatePatternBrush',
    258: SetBKMode, 259: SetMapMode, 260: SetROP2, 262: SetPolyfillMode, 263: SetStretchBltMode,
    264: SetTextCharExtra, 295: RestoreDC, 298: InvertRegion, 299: PaintRegion,
    300: SelectClipRegion, 301: SelectObject, 302: SetTextAlign,
    #332: 'ResetDc',
    496: DeleteObject, 513: SetBKColor, 521: SetTextColor, 522: SetTextJustification,
    523: SetWindowOrgEx, 524: SetWindowExtEx, 525: SetViewportOrgEx,
    526: SetViewportExtEx, 527: OffsetWindowOrg, 529: OffsetViewportOrgEx,
    531: LineTo, 532: MoveTo, 544: OffsetClipRgn, 552: FillRegion, 561: SetMapperFlags,
    762: CreatePenIndirect, 763: CreateFontIndirect, 764: CreateBrushIndirect,
    #765:'CreateBitmapIndirect',
    804: Polygon, 805: Polyline, 1040: ScaleWindowExtEx, 1042:ScaleViewportExtEx,
    1045: ExcludeClipRect, 1046: IntersectClipRect, 1048: Ellipse, 1051: Rectangle,
    #1065: 'FrameRegion', 1049: 'FloodFill', 1352: 'ExtFloodFill',
    1574: Escape,
    # 1055: 'SetPixel',
    1336: PolyPolygon, 1564: RoundRect, 1791: CreateRegion, 2071: Arc, 2074: Pie, 2096: Chord,
    #1313: 'TextOut', 1583: 'DrawText',
    2610: ExtTextOut,
    #1790: 'CreateBitmap', 1565: 'PatBlt',
    2338: BitBlt, 2368: DibBitblt, 2851: StretchBlt,
    2881: DibStretchBlt, # 3379: 'SetDibToDev',
    3907: StretchDIB
}

#END
