# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import math
import struct
import sys

from formats.mf import *
from utils import *

ArcDirection = {
    1: "COUNTERCLOCKWISE",
    2: "CLOCKWISE"
    }

PAN_ArmStyle = {
    0: "ANY",
    1: "NO_FIT",
    2: "STRAIGHT_ARMS_HORZ",
    3: "STRAIGHT_ARMS_WEDGE",
    4: "STRAIGHT_ARMS_VERT",
    5: "STRAIGHT_ARMS_SINGLE_SERIF",
    6: "STRAIGHT_ARMS_DOUBLE_SERIF",
    7: "BENT_ARMS_HORZ",
    8: "BENT_ARMS_WEDGE",
    9: "BENT_ARMS_VERT",
    10: "BENT_ARMS_SINGLE_SERIF",
    11: "BENT_ARMS_DOUBLE_SERIF"
    }

BackgroundMode = {
    1: "TRANSPARENT",
    2: "OPAQUE"
    }

ColorAdjustment = {
    1: "NEGATIVE",
    2: "LOG_FILTER"
    }

ColorMatchToTarget = {0: "NOTEMBEDDED", 1: "EMBEDDED"}

ColorSpace = {1: "ENABLE", 2: "DISABLE", 3: "DELETE_TRANSFORM"}

PAN_Contrast = {
    0: "ANY",
    1: "NO_FIT",
    2: "NONE",
    3: "VERY_LOW",
    4: "LOW",
    5: "MEDIUM_LOW",
    6: "MEDIUM",
    7: "MEDIUM_HIGH",
    8: "HIGH",
    9: "VERY_HIGH"
    }

DIBColors = {
    0: "RGB_COLORS",
    1: "PAL_COLORS",
    2: "PAL_INDICES"
    }

EmrComment = {
    0x80000001: "WINDOWS_METAFILE",
    2: "BEGINGROUP",
    3: "ENDGROUP",
    0x40000004: "MULTIFORMATS",
    0x40: "UNICODE_STRING",
    0x80: "UNICODE_END"
    }

ExtTextOutOptions = {
    2: "OPAQUE",
    4: "CLIPPED",
    0x10: "GLYPH_INDEX",
    0x80: "RTLREADING",
    0x100: "NO_RECT",
    0x200: "SMALL_CHARS",
    0x400: "NUMERICSLOCAL",
    0x800: "NUMERICSLATIN",
    0x1000: "IGNORELANGUAGE",
    0x2000: "PDY",
    0x10000: "REVERSE_INDEX_MAP"
    }

PAN_FamilyType = {
    0: "ANY",
    1: "NO_FIT",
    2: "TEXT_DISPLAY",
    3: "SCRIPT",
    4: "DECORATIVE",
    5: "PICTORIAL"
    }

FloodFill = {0: "Border", 1: "Surface"}

GradientFill = {
    0: "RECT_H",
    1: "RECT_V",
    2: "TRIANGLE"
    }

GraphicsMode = {1: "Compatible", 2: "Advanced"}

HatchStyle = {
    6: "SOLIDCLR",
    7: "DITHEREDCLR",
    8: "SOLIDTEXTCLR",
    9: "DITHEREDTEXTCLR",
    10: "SOLIDBKCLR",
    11: "DITHEREDBKCLR"
    }

ICMMode = {
    1: "OFF",
    2: "ON",
    3: "QUERY",
    4: "DONE_OUTSIDEDC"
    }

Illuminant = {
    0: "DEVICE_DEFAULT",
    1: "TUNGSTEN",
    2: "B",
    3: "DAYLIGHT",
    4: "D50",
    5: "D55",
    6: "D65",
    7: "D75",
    8: "FLUORESCENT"
    }

PAN_Letterform = {
    0: "ANY",
    1: "NO_FIT",
    2: "NORMAL_CONTACT",
    3: "NORMAL_WEIGHTED",
    4: "NORMAL_BOXED",
    5: "NORMAL_FLATTENED",
    6: "NORMAL_ROUNDED",
    7: "NORMAL_OFF_CENTER",
    8: "NORMAL_SQUARE",
    9: "OBLIQUE_CONTACT",
    10: "OBLIQUE_WEIGHTED",
    11: "OBLIQUE_BOXED",
    12: "OBLIQUE_FLATTENED",
    13: "OBLIQUE_ROUNDED",
    14: "OBLIQUE_OFF_CENTER",
    15: "OBLIQUE_SQUARE"
    }

MapMode = {
    1: "TEXT",
    2: "LOMETRIC",
    3: "HIMETRIC",
    4: "LOENGLISH",
    5: "HIENGLISH",
    6: "TWIPS",
    7: "ISOTROPIC",
    8: "ANISOTROPIC"
    }

PAN_MidLine = {
    0: "ANY",
    1: "NO_FIT",
    2: "STANDARD_TRIMMED",
    3: "STANDARD_POINTED",
    4: "STANDARD_SERIFED",
    5: "HIGH_TRIMMED",
    6: "HIGH_POINTED",
    7: "HIGH_SERIFED",
    8: "CONSTANT_TRIMMED",
    9: "CONSTANT_POINTED",
    10: "CONSTANT_SERIFED",
    11: "LOW_TRIMMED",
    12: "LOW_POINTED",
    13: "LOW_SERIFED"
    }

ModifyWorldTransformMode = {
    1: "IDENTITY",
    2: "LEFTMULTIPLY",
    3: "RIGHTMULTIPLY",
    4: "SET"
    }

PenStyle = {
    "COSMETIC": 0x00000000,
    "ENDCAP_ROUND": 0x00000000,
    "JOIN_ROUND": 0x00000000,
    "SOLID": 0x00000000,
    "DASH": 0x00000001,
    "DOT": 0x00000002,
    "DASHDOT": 0x00000003,
    "DASHDOTDOT": 0x00000004,
    "NULL": 0x00000005,
    "INSIDEFRAME": 0x00000006,
    "USERSTYLE": 0x00000007,
    "ALTERNATE": 0x00000008,
    "ENDCAP_SQUARE": 0x00000100,
    "ENDCAP_FLAT": 0x00000200,
    "JOIN_BEVEL": 0x00001000,
    "JOIN_MITER": 0x00002000,
    "GEOMETRIC": 0x00010000
    }

Point = {1: "CLOSEFIGURE", 2: "LINETO", 4: "BEZIERTO", 6: "MOVETO"}

PolygonFillMode = {1: "ALTERNATE", 2: "WINDING"}

StockObject = {
    0x80000000: "WHITE_BRUSH",  # BS_SOLID 0x00FFFFFF
    0x80000001: "LTGRAY_BRUSH", # BS_SOLID 0x00C0C0C0
    0x80000002: "GRAY_BRUSH",   # BS_SOLID 0x00808080
    0x80000003: "DKGRAY_BRUSH", # BS_SOLID 0x00404040
    0x80000004: "BLACK_BRUSH",  # BS_SOLID 0x00000000
    0x80000005: "NULL_BRUSH",   # BS_NULL
    0x80000006: "WHITE_PEN",    # PS_COSMETIC + PS_SOLID 0x00FFFFFF
    0x80000007: "BLACK_PEN",    # PS_COSMETIC + PS_SOLID 0x00000000
    0x80000008: "NULL_PEN",     # PS_NULL
    0x8000000A: "OEM_FIXED_FONT",
    0x8000000B: "ANSI_FIXED_FONT",
    0x8000000C: "ANSI_VAR_FONT",
    0x8000000D: "SYSTEM_FONT",
    0x8000000E: "DEVICE_DEFAULT_FONT",
    0x8000000F: "DEFAULT_PALETTE",
    0x80000010: "SYSTEM_FIXED_FONT",
    0x80000011: "DEFAULT_GUI_FONT",
    0x80000012: "DC_BRUSH",     # WHITE_BRUSH
    0x80000013: "DC_PEN"        # BLACK_PEN
    }

StretchMode = {
    1: "ANDSCANS",
    2: "ORSCANS",
    3: "DELETESCANS",
    4: "HALFTONE"
    }

PAN_Proportion = {
    0: "ANY",
    1: "NO_FIT",
    2: "OLD_STYLE",
    3: "MODERN",
    4: "EVEN_WIDTH",
    5: "EXPANDED",
    6: "CONDENSED",
    7: "VERY_EXPANDED",
    8: "VERY_CONDENSED",
    9: "MONOSPACED"
    }

PAN_SerifType = {
    0: "ANY",
    1: "NO_FIT",
    2: "COVE",
    3: "OBTUSE_COVE",
    4: "SQUARE_COVE",
    5: "OBTUSE_SQUARE_COVE",
    6: "SQUARE",
    7: "THIN",
    8: "BONE",
    9: "EXAGGERATED",
    10: "TRIANGLE",
    11: "NORMAL_SANS",
    12: "OBTUSE_SANS",
    13: "PERP_SANS",
    14: "FLARED",
    15: "ROUNDED"
    }

PAN_StrokeVariation = {
    0: "ANY",
    1: "NO_FIT",
    2: "GRADUAL_DIAG",
    3: "GRADUAL_TRAN",
    4: "GRADUAL_VERT",
    5: "GRADUAL_HORZ",
    6: "RAPID_VERT",
    7: "RAPID_HORZ",
    8: "INSTANT_VERT"
    }

PAN_Weight = {
    0: "ANY",
    1: "NO_FIT",
    2: "VERY_LIGHT",
    3: "LIGHT",
    4: "THIN",
    5: "BOOK",
    6: "MEDIUM",
    7: "DEMI",
    8: "BOLD",
    9: "HEAVY",
    10: "BLACK",
    11: "NORD"
    }

PAN_XHeight = {
    0: "ANY",
    1: "NO_FIT",
    2: "CONSTANT_SMALL",
    3: "CONSTANT_STD",
    4: "CONSTANT_LARGE",
    5: "DUCKING_SMALL",
    6: "DUCKING_STD",
    7: "DUCKING_LARGE"
    }


def LogPalette(model, data, offset):
    add_iter(model, "Version", struct.unpack("<H", data[offset:offset + 2])[0], offset, 2, "<H")
    [num] = struct.unpack("<H", data[offset + 2:offset + 4])
    add_iter(model, "NumOfEntries", num, offset + 2, 2, "<H")
    for i in range(num):
        clr = "%02X%02X%02X" % (data[offset + 6 + i * 4], data[offset + 5 + i * 4], data[offset + 4 + i * 4])
        add_iter(model, "RGB %d" % i, clr, offset + 4 + i * 4, 3, "clr")

def Poly(model, data):
    RectL(model, data)
    [count] = struct.unpack("<i", data[24:28])
    add_iter(model, "Count", count, 24, 4, "<i")
    for i in range(count):
        PointL(model, data, i * 8 + 28, str(i))
    return count

def Poly16(model, data):
    RectL(model, data)
    [count] = struct.unpack("<i", data[24:28])
    add_iter(model, "Count", count, 24, 4, "<i")
    for i in range(count):
        PointS(model, data, i * 4 + 28, str(i))
    return count

def SetWinExt(model, data):
    RecHdr(model, data)
    PointL(model, data, 8)

def RecHdr(model, data):
    add_iter(model, "Type", "%#x" % struct.unpack("<I", data[:4])[0], 0, 4, "<I")
    add_iter(model, "Size", struct.unpack("<I", data[4:8])[0], 4, 4, "<I")

def GC_BeginGroup(model, data):
    PointL(model, data, 0x14, "S")
    PointL(model, data, 0x1c, "E")
    nlen = struct.unpack("<I", data[0x24:0x28])[0]
    add_iter(model, "DescLength", nlen, 0x24, 4, "<I")
    txt = unicode(data[0x28:0x28 + nlen * 2], "utf-16")
    add_iter(model, "Description", txt, 0x28, nlen * 2, "txt")

def GC_EndGroup(model, data):
    return


#1
def Header(page, model, options, data):
    RecHdr(model, data)
    PointL(model, data, 8, "S")
    PointL(model, data, 16, "E")
    PointL(model, data, 24, "S (mm)")
    PointL(model, data, 32, "E (mm)")
    [sig] = struct.unpack("<i", data[40:44])
    add_iter(model, "Signature", "%#08X" % sig, 40, 4, "<i")
    add_iter(model, "Version", "%#08X" % struct.unpack("<i", data[44:48]), 44, 4, "<i")
    add_iter(model, "Size", struct.unpack("<I", data[48:52])[0], 48, 4, "<I")
    add_iter(model, "Records", struct.unpack("<I", data[52:56])[0], 52, 4, "<I")
    add_iter(model, "Objects", struct.unpack("<H", data[56:58])[0], 56, 2, "<H")
    add_iter(model, "Reservd", struct.unpack("<H", data[58:60])[0], 58, 2, "<H")
    [descsize] = struct.unpack("<I", data[60:64])
    add_iter(model, "DescSize", descsize, 60, 4, "<I")
    [descoff] = struct.unpack("<I", data[64:68])
    add_iter(model, "DescOffset","%#02x" % descoff, 64, 4, "<I")
    [palnum] = struct.unpack("<I", data[68:72])
    add_iter(model, "PalEntries", palnum, 68, 4, "<I")
    PointL(model, data, 72, "Dev")
    PointL(model, data, 80, "Dev (mm)")
    add_iter(model, "cbPxlFmt", struct.unpack("<I", data[88:92])[0], 88, 4, "<I")
    add_iter(model, "offPxlFmt", struct.unpack("<I", data[92:96])[0], 92, 4, "<I")
    add_iter(model, "bOpenGL", struct.unpack("<I", data[96:100])[0], 96, 4, "<I")
    PointL(model, data, 100, " (micrometers)")
    add_iter(model, "Description", data[descoff:descoff + descsize * 2].decode("utf-16"), descoff, descsize * 2, "txt")

#2
def Polybezier(page, model, options, data):
    RecHdr(model, data)
    Poly(model, data)

#3
def Polygon(page, model, options, data):
    RecHdr(model, data)
    Poly(model, data)

#4
def Polyline(page, model, options, data):
    RecHdr(model, data)
    Poly(model, data)

#5
def PolybezierTo(page, model, options, data):
    RecHdr(model, data)
    Poly(model, data)

#6
def PolylineTo(page, model, options, data):
    RecHdr(model, data)
    Poly(model, data)

#7
def PolyPolyline(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    [numpoly] = struct.unpack("<i", data[24:28])
    add_iter(model, "NumOfPoly", numpoly, 24, 4, "<i")
    [count] = struct.unpack("<i", data[28:32])
    add_iter(model, "Count", count, 28, 4, "<i")
    for i in range(numpoly):
        add_iter(model, "PolyPnt %d" % i, struct.unpack("<I", data[32 + i * 4:36 + i * 4])[0], 32 + i * 4, 4, "<I")
    for i in range(count):
        PointL(model, data, i * 8 + 32 + numpoly * 4, str(i))

#8
def PolyPolygon(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    [numpoly] = struct.unpack("<i", data[24:28])
    add_iter(model, "NumOfPoly", numpoly, 24, 4, "<i")
    [count] = struct.unpack("<i", data[28:32])
    add_iter(model, "Count", count, 28, 4, "<i")
    for i in range(numpoly):
        add_iter(model, "PolyPnt %d" % i, struct.unpack("<I", data[32 + i * 4:36 + i * 4])[0], 32 + i * 4, 4, "<I")
    for i in range(count):
        PointL(model, data, i * 8 + 32 + numpoly * 4, str(i))

#9
def SetWindowExtEx(page, model, options, data):
    SetWinExt(model, data)

#0xa
def SetWindowOrgEx(page, model, options, data):
    SetWinExt(model, data)

#0xb
def SetViewportExtEx(page, model, options, data):
    SetWinExt(model, data)

#0xc
def SetViewportOrgEx(page, model, options, data):
    SetWinExt(model, data)

#0xd
def SetBrushOrgEx(page, model, options, data):
    RecHdr(model, data)
    PointL(model, data, 8, "Org")

#0xe
def EOF(page, model, options, data):
    RecHdr(model, data)
    [nPE] = struct.unpack("<I", data[8:12])
    add_iter(model, "nPalEntries", nPE, 8, 4, "<I")
    add_iter(model, "offPalEntries", struct.unpack("<I", data[12:16])[0], 12, 4, "<I")
    if nPE:
        add_iter(model, "PaletteBufer", data[16:-4], 16, len(data) - 20, "blob")
    add_iter(model, "SizeLast", struct.unpack("<I", data[-4:])[0], len(data) - 4, 4, "<I")

#0xf
def SetPixelV(page, model, options, data):
    RecHdr(model, data)
    PointL(model, data, 8)
    add_iter(model, "Color", data[16:20], 16, 4, "clra")

def SetMode(model, data):
    RecHdr(model, data)
    add_iter(model, "Mode", struct.unpack("<I", data[8:12])[0], 8, 4,"<I")

#0x10
def SetMapperFlags(page, model, options, data):
    SetMode(model, data)

#0x11
def SetMapMode(page, model, options, data):
    SetMode(model, data)

#0x12
def SetBKMode(page, model, options, data):
    SetMode(model, data)

#0x13
def SetPolyfillMode(page, model, options, data):
    SetMode(model, data)

#0x14
def SetRop2(page, model, options, data):
    SetMode(model, data)

#0x15
def SetStretchBltMode(page, model, options, data):
    SetMode(model, data)

#0x16
def SetTextAlign(page, model, options, data):
    SetMode(model, data)

#0x17
def SetColorAdjustment(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "Size", struct.unpack("<i", data[8:10])[0], 8, 4, "<i")
    add_iter(model, "Values", struct.unpack("<i", data[10:12])[0], 10, 4, "<i")
    add_iter(model, "IllumIdx", struct.unpack("<i", data[12:14])[0], 12, 4, "<i")
    add_iter(model, "RedGamma", struct.unpack("<i", data[14:16])[0], 14, 4, "<i")
    add_iter(model, "GreenGamma", struct.unpack("<i", data[16:18])[0], 16, 4, "<i")
    add_iter(model, "BlueGamma", struct.unpack("<i", data[18:20])[0], 18, 4, "<i")
    add_iter(model, "RefBlack", struct.unpack("<i", data[20:22])[0], 20, 4, "<i")
    add_iter(model, "RefWhite", struct.unpack("<i", data[22:24])[0], 22, 4, "<i")
    add_iter(model, "Contrast", struct.unpack("<i", data[24:26])[0], 24, 4, "<i")
    add_iter(model, "Brightness", struct.unpack("<i", data[26:28])[0], 26, 4, "<i")
    add_iter(model, "Colorfull", struct.unpack("<i", data[28:30])[0], 28, 4, "<i")
    add_iter(model, "RedGreenTint", struct.unpack("<i", data[30:32])[0], 30, 4, "<i")

#0x18
def SetTextColor(page, model, options, data):
    RecHdr(model, data)
    clr = "%02X%02X%02X" % (data[10], data[9], data[8])
    add_iter(model, "RGB", clr, 8, 3, "clr")

#0x19
def SetBKColor(page, model, options, data):
    RecHdr(model, data)
    clr = "%02X%02X%02X" % (data[10], data[9], data[8])
    add_iter(model, "RGB", clr, 8, 3, "clr")

#0x1a
def OffsetClipRgn(page, model, options, data):
    SetWinExt(model, data)

#0x1b
def MoveToEx(page, model, options, data):
    SetWinExt(model, data)

#0x1c
def SetMetaRgn(page, model, options, data):
    RecHdr(model, data)

#0x1d
def ExcludeClipRect(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)

#0x1e
def IntersectClipRect(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)

#0x1f
def ScaleViewportExtEx(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "xNum", struct.unpack("<i", data[8:12])[0], 8, 4, "<i")
    add_iter(model, "xDenom", struct.unpack("<i", data[12:16])[0],12, 4, "<i")
    add_iter(model, "yNum", struct.unpack("<i", data[16:20])[0],16, 4, "<i")
    add_iter(model, "yDenom", struct.unpack("<i", data[20:24])[0],20, 4, "<i")

#0x20
def ScaleWindowExtEx(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "xNum", struct.unpack("<i", data[8:12])[0], 8, 4, "<i")
    add_iter(model, "xDenom", struct.unpack("<i", data[12:16])[0],12, 4, "<i")
    add_iter(model, "yNum", struct.unpack("<i", data[16:20])[0],16, 4, "<i")
    add_iter(model, "yDenom", struct.unpack("<i", data[20:24])[0],20, 4, "<i")

#0x21
def SaveDC(page, model, options, data):
    RecHdr(model, data)

#0x22
def RestoreDC(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "SavedDC", struct.unpack("<i", data[8:12])[0], 8, 4, "<i")

#0x23
def SetWorldTransform(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "m11", struct.unpack("<f", data[8:12])[0], 8, 4, "<f")
    add_iter(model, "m12", struct.unpack("<f", data[12:16])[0], 12, 4, "<f")
    add_iter(model, "m21", struct.unpack("<f", data[16:20])[0], 16, 4, "<f")
    add_iter(model, "m22", struct.unpack("<f", data[20:24])[0], 20, 4, "<f")
    add_iter(model, "Dx", struct.unpack("<f", data[24:28])[0], 24, 4, "<f")
    add_iter(model, "Dy", struct.unpack("<f", data[28:32])[0], 28, 4, "<f")

#0x24
def ModifyWorldTransform(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "m11", struct.unpack("<f", data[8:12])[0], 8, 4, "<f")
    add_iter(model, "m12", struct.unpack("<f", data[12:16])[0], 12, 4, "<f")
    add_iter(model, "m21", struct.unpack("<f", data[16:20])[0], 16, 4, "<f")
    add_iter(model, "m22", struct.unpack("<f", data[20:24])[0], 20, 4, "<f")
    add_iter(model, "Dx", struct.unpack("<f", data[24:28])[0], 24, 4, "<f")
    add_iter(model, "Dy", struct.unpack("<f", data[28:32])[0], 28, 4, "<f")
    add_iter(model, "Mode", struct.unpack("<I", data[32:36])[0], 32, 4, "<I")

#0x25
def SelectObject(page, model, options, data):
    RecHdr(model, data)
    objid = struct.unpack("<I", data[8:12])[0]
    if objid in StockObject:
        objid = "%#x (%s)" % (objid, StockObject[objid])
    else:
        objid = "%#x" % objid
    add_iter(model, "ObjID", objid , 8, 4, "<I")

#0x26
def CreatePen(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ObjID", "%#x" % struct.unpack("<I", data[8:12])[0], 8, 4, "<I")
    add_iter(model, "PenStyle", struct.unpack("<i", data[12:16])[0], 12, 4, "<i")
    add_iter(model, "Width", struct.unpack("<i", data[16:20])[0], 16, 4, "<i")
    # skip 4 bytes
    clr = "%02X%02X%02X" % (data[26], data[25], data[24])
    add_iter(model, "RGB", clr, 24, 3, "clr")

#0x27
def CreateBrushIndirect(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ObjID", "%#x" % struct.unpack("<I", data[8:12])[0], 8, 4, "<I")
    add_iter(model, "BrushStyle", struct.unpack("<i", data[12:16])[0], 12, 4, "<i")
    clr = "%02X%02X%02X" % (data[18], data[17], data[16])
    add_iter(model, "RGB", clr, 16, 3, "clr")
    add_iter(model, "Hatch", struct.unpack("<i", data[20:24])[0], 20, 4, "<i")

#0x28
def DeleteObject(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ObjID", "%#x" % struct.unpack("<I", data[8:12])[0], 8, 4, "<I")

#0x29
def AngleArc(page, model, options, data):
    RecHdr(model, data)
    PointL(model, data, 8, "C")
    add_iter(model, "Radius", struct.unpack("<I", data[16:20])[0], 16, 4, "<I")
    add_iter(model, "StartAng", struct.unpack("<f", data[20:24])[0], 20, 4, "<f")
    add_iter(model, "SweepAng", struct.unpack("<f", data[24:28])[0], 24, 4, "<f")

#0x2a
def Ellipse(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)

#0x2b
def Rectangle(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)

#0x2c
def RoundRect(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    PointL(model, data, 24, "R")

#0x2d
def Arc(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    PointL(model, data, 24, "S")
    PointL(model, data, 32, "E")

#0x2e
def Chord(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    PointL(model, data, 24, "S")
    PointL(model, data, 32, "E")

#0x2f
def Pie(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    PointL(model, data, 24, "S")
    PointL(model, data, 32, "E")

#0x30
def SelectPalette(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ObjID", "%#x" % struct.unpack("<I", data[8:12])[0], 8, 4, "<I")

#0x31
def CreatePalette(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ihPal", struct.unpack("<I", data[8:12])[0], 8, 4, "<I")
    LogPalette(model, data, 12)

#0x32
def SetPaletteEntries(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ihPal", struct.unpack("<I", data[8:12])[0], 8, 4, "<I")
    add_iter(model, "Start", struct.unpack("<I", data[12:16])[0], 12, 4, "<I")
    [num] = struct.unpack("<I", data[16:20])
    add_iter(model, "NumOfEntries", num, 16, 4, "<I")
    for i in range(num):
        clr = "%02X%02X%02X" % (data[24 + i * 4], data[23 + i * 4], data[22 + i * 4])
        add_iter(model, "RGB %d" % i, clr, 22 + i * 4, 3, "clr")

#0x33
def ResizePalette(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "lhPal", struct.unpack("<i", data[8:0xc])[0], 8, 4, "<i")
    add_iter(model, "NumOfEntries", struct.unpack("<i", data[0xc:0x10])[0], 0xc, 4, "<i")

#0x34
def RealizePalette(page, model, options, data):
    RecHdr(model, data)

#0x35
def ExtFloodFill(page, model, options, data):
    RecHdr(model, data)
    PointL(model, data, 8, "Start ")
    add_iter(model, "Color", struct.unpack("<I", data[0x10:0x14])[0], 0x10, 4, "<I")
    ffm = struct.unpack("<I", data[0x14:0x18])[0]
    ft = "unknown"
    if ffm in FloodFill:
        ft = FloodFill(ffm)
    add_iter(model, "FloodFillMode", "%d (%s)" % (ffm, ft), 0x14, 4, "<I")

#0x36
def LineTo(page, model, options, data):
    SetWinExt(model, data)

#0x37
def ArcTo(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    PointL(model, data, 24, "S")
    PointL(model, data, 32, "E")

#0x38
def Polydraw(page, model, options, data):
    RecHdr(model, data)
    count = Poly(model, data)
    for i in range(count):
        add_iter(model, "abType %d" % i, str(data[count * 8 + 28 + i]), count * 8 + 28 + i, 1, "b")

#0x39
def SetArcDirection(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ArcDirection",  struct.unpack("<I", data[8:12])[0], 8, 4, "<I")

#0x3a
def SetMiterLimit(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "MitterLimit", struct.unpack("<I", data[8:12])[0], 8, 4, "<I")

#0x3b
def BeginPath(page, model, options, data):
    RecHdr(model, data)

#0x3c
def EndPath(page, model, options, data):
    RecHdr(model, data)

#0x3d
def CloseFigure(page, model, options, data):
    RecHdr(model, data)

#0x3e
def FillPath(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)

#0x3f
def StrokeAndFillPath(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)

#0x40
def StrokePath(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)

#0x41
def FlattenPath(page, model, options, data):
    RecHdr(model, data)

#0x42
def WidenPath(page, model, options, data):
    RecHdr(model, data)

#0x43
def SelectClipPath(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "RegionMode", struct.unpack("<I", data[8:12])[0], 8, 4, "<I")

#0x44
def AbortPath(page, model, options, data):
    RecHdr(model, data)

#0x46
def GDIComment(page, model, options, data):
    RecHdr(model, data)
    gctype = data[0xC:0x10]
    add_iter(model, "Comment Type", gctype, 0xc, 4, "txt")
    if gctype == b"\x45\x4d\x46\x2b":
        # EMF+
        [emfptype] = struct.unpack("<H", data[0x10:0x12])
        ettxt = emrplus_ids.get(emfptype) or "Unknown"
        add_iter(model, "EMF+ Type", ettxt, 0x10, 2, "<H")
    elif gctype == '\x47\x44\x49\x43':
        ctype = struct.unpack("<I", data[0x10:0x14])[0]
        ct = "unknown"
        if ctype in gc_ids:
            ct = gc_ids[ctype]
        add_iter(model, "PubComment ID", "%d (%s)" % (ctype, ct), 0x10, 4, "<I")
        # FIXME!
        #if ctype in gcfunc_ids:
        #    gcfunc_ids[ctype](model, data)

#0x47
def FillRgn(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    add_iter(model, "RgnDataSize", struct.unpack("<I", data[0x18:0x1c])[0], 0x18, 4, "<I")
    add_iter(model, "ihBrush", struct.unpack("<I", data[0x1c:0x20])[0], 0x1c, 4, "<I")
    # FIXME! add RegionData object
    add_iter(model, "RgnData", data[0x20:], 0x20, len(data) - 0x20, "blob")

#0x48
def FrameRgn(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    add_iter(model, "RgnDataSize", struct.unpack("<I", data[0x18:0x1c])[0], 0x18, 4, "<I")
    add_iter(model, "ihBrush", struct.unpack("<I", data[0x1c:0x20])[0], 0x1c, 4, "<I")
    add_iter(model, "Width", struct.unpack("<I", data[0x20:0x24])[0], 0x20, 4, "<I")
    add_iter(model, "Height", struct.unpack("<I", data[0x24:0x28])[0], 0x24, 4, "<I")
    # FIXME! add RegionData object
    add_iter(model, "RgnData", data[0x28:], 0x28, len(data) - 0x28, "blob")

#0x49
def InvertRgn(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    rds = struct.unpack("<I", data[0x18:0x1c])[0]
    add_iter(model, "RgnDataSize", rds, 0x18, 4, "<I")
    # FIXME! add RegionData object
    add_iter(model, "RgnData", data[0x1c:], 0x1c, len(data) - 0x1c, "blob")

#0x4a
def PaintRgn(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    add_iter(model, "RgnDataSize", struct.unpack("<I", data[0x18:0x1c])[0], 0x18, 4, "<I")
    # FIXME! add RegionData object
    add_iter(model, "RgnData", data[0x1c:], 0x1c, len(data) - 0x1c, "blob")

#0x4b
def ExtSelectClipRgn(page, model, options, data):
    RecHdr(model, data)
    rds = struct.unpack("<I", data[0x8:0xc])[0]
    add_iter(model, "RgnDataSize", rds, 0x8, 4, "<I")
    add_iter(model, "RegionMode", struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")
    off = 0
    i = " BBox"
    while off < rds:
        #add_iter(model, "RgnData", data[0x10:0x10 + rds], 0x10, rds, "blob")
        RectL(model, data, offset=0x10 + off, i="%s" % i)
        off += 16
        i = i + 1 if i != " BBox" else 0


#0x4c
def BitBlt(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
# FIXME!
#xDest "<I"
#yDest "<I"
#cxDest "<I"
#cyDest "<I"
#BitBltROP "<I"
#xSrc "<I"
#ySrc "<I"
#XformSrc XForm (24 bytes)
#BkColorSrc (ColorRef obj)
#UsageSrc "<I"
#offBmiSrc "<I"
#cbBmiSrc "<I"
#offBitsSrc "<I"
#cbBitsSrc "<I"
# <can have unused bytes>
# BmiSrc
# <can have unused bytes>
# BitsSrc



#0x4d
def StretchBlt(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
# FIXME! check <I vs <i for xDest etc
#xDest "<I"
#yDest "<I"
#cxDest "<I"
#cyDest "<I"
#BitBltROP "<I"
#xSrc "<I"
#ySrc "<I"
#XformSrc XForm (24 bytes)
#BkColorSrc (ColorRef obj)
#UsageSrc "<I"
#offBmiSrc "<I"
#cbBmiSrc "<I"
#offBitsSrc "<I"
#cbBitsSrc "<I"
#cxSrc "<i"
#cySrc "<i"
# <can have unused bytes>
# BmiSrc
# <can have unused bytes>
# BitsSrc


#0x4e
def MaskBlt(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
# FIXME!
#xDest "<i"
#yDest "<i"
# ... ...


#0x4f PlgBlt

#0x50
def SetDIBitsToDevice(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data, i="BBox")
    off = 24
    add_iter(model, "xDest", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "yDest", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "xSrc", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "ySrc", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "cxSrc", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "cySrc", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "offBmiSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "cbBmiSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "offBitsSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "cbBitsSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "UsageSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "iStartScan", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "cScans", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    hsize = struct.unpack("<I", data[off:off + 4])[0]
    if hsize == 0xc:
        BitmapCoreHeader(model, data, off=off)
    else:
        BitmapInfoHeader(model, data, off=off)

#0x51
def StretchDIBits(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data, i="BBox")
    off = 24
    add_iter(model, "xDest", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "yDest", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "xSrc", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "ySrc", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "cxSrc", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "cySrc", struct.unpack("<i", data[off: off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "offBmiSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "cbBmiSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "offBitsSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "cbBitsSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "UsageSrc", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "BitBltROP", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "cxDest", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "cyDest", struct.unpack("<I", data[off: off + 4])[0], off, 4, "<I")


#0x52
def ExtCreateFontIndirectW(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ObjID", "%#x" % struct.unpack("<I", data[8:12])[0], 8, 4, "<I")
    add_iter(model, "Height", "%d" % struct.unpack("<i", data[12:16])[0], 12, 4, "<i")
    add_iter(model, "Width", "%d" % struct.unpack("<i", data[16:20])[0], 16, 4, "<i")
    add_iter(model, "Escapement", "%d" % struct.unpack("<i", data[20:24])[0], 20, 4, "<i")
    add_iter(model, "Orientation", "%d" % struct.unpack("<i", data[24:28])[0], 24, 4, "<i")
    add_iter(model, "Weight", "%d" % struct.unpack("<i", data[28:32])[0], 28, 4, "<i")
    add_iter(model, "Italic", "%d" % data[32], 32, 1, "<B")
    add_iter(model, "Underline", "%d" % data[33], 33, 1, "<B")
    add_iter(model, "StrikeOut", "%d" % data[34], 34, 1, "<B")
    add_iter(model, "CharSet", "%d" % data[35], 35, 1, "<B")
    add_iter(model, "OutPrecision", "%d" % data[36], 36, 1, "<B")
    add_iter(model, "ClipPrecision", "%d" % data[37], 37, 1, "<B")
    add_iter(model, "Quality", "%d" % data[38], 38, 1, "<B")
    add_iter(model, "PitchAndFamily", "%d" % data[39], 39, 1, "<B")
    fn_end = math.ceil(data[40:104].find(b"\x00\x00") / 2) * 2
    fontname = data[40:fn_end].decode("utf-16")
    add_iter(model, "FontName", fontname, 40, 64, "txt")
    #FIXME!

#0x53
def ExtTextOutA(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data, offset=8)
    igm = struct.unpack("<I", data[24:28])[0]
    add_iter(model, "iGraphicsMode", igm, 24, 4, "<I")
    add_iter(model, "exScale", "%d" % struct.unpack("<f", data[28:32])[0], 28, 4, "<f")
    add_iter(model, "eyScale", "%d" % struct.unpack("<f", data[32:36])[0], 32, 4, "<f")
    PointL(model, data, offset=36, i="ref")
    add_iter(model, "Chars", "%d" % struct.unpack("<I", data[44:48])[0], 44, 4, "<I")
    add_iter(model, "offString", "%#x" % struct.unpack("<I", data[48:52])[0], 48, 4, "<I")
    options = struct.unpack("<I", data[52:56])[0]
    add_iter(model, "Options", "%#x" % options, 52, 4, "<I")
    off = 56
    # if not options & 0x100 -- looks like that would be correct, but MS is weird as always
    RectL(model, data, offset=56)
    off += 16
    add_iter(model, "offDx", "%#x" % struct.unpack("<I", data[off:off + 4])[0], off, 4, "<I")

#0x54
def ExtTextOutW(page, model, options, data):
    ExtTextOutA(page, model, options, data)

#0x55
def Polybezier16(page, model, options, data):
    RecHdr(model, data)
    Poly16(model, data)

#0x56
def Polygon16(page, model, options, data):
    RecHdr(model, data)
    Poly16(model, data)

#0x57
def Polyline16(page, model, options, data):
    RecHdr(model, data)
    Poly16(model, data)

#0x58
def PolybezierTo16(page, model, options, data):
    RecHdr(model, data)
    Poly16(model, data)

#0x59
def PolylineTo16(page, model, options, data):
    RecHdr(model, data)
    Poly16(model, data)

#0x5a
def PolyPolyline16(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    [numpoly] = struct.unpack("<i", data[24:28])
    add_iter(model, "NumOfPoly", numpoly, 24, 4, "<i")
    [count] = struct.unpack("<i", data[28:32])
    add_iter(model, "Count", count, 28, 4, "<i")
    for i in range(numpoly):
        add_iter(model, "PolyPnt %d"%i, struct.unpack("<I", data[i * 4 + 32:i * 4 + 36])[0], i * 4 + 32, 4, "<I")
    for i in range(count):
        PointS(model, data, i * 4 + 32 + numpoly * 4, str(i))

#0x5b
def PolyPolygon16(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    [numpoly] = struct.unpack("<i", data[24:28])
    add_iter(model, "NumOfPoly", numpoly, 24, 4, "<i")
    [count] = struct.unpack("<i", data[28:32])
    add_iter(model, "Count", count, 28, 4, "<i")
    for i in range(numpoly):
        add_iter(model, "PolyPnt %d"%i, struct.unpack("<I", data[i * 4 + 32:i * 4 + 36])[0], i * 4 + 32, 4, "<I")
    for i in range(count):
        PointS(model, data, i * 4 + 32 + numpoly * 4, str(i))

#0x5c
def Polydraw16(page, model, options, data):
    RecHdr(model, data)
    count = Poly16(model, data)
    for i in range(count):
        add_iter(model, "abType %d" % i, str(data[count * 4 + 28 + i]), count * 4 + 28 + i, 1, "b")

#0x5d
def CreateMonoBrush(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ObjID", "%#x" % struct.unpack("<I", data[8:12])[0], 8, 4, "<I")
    # DIBColors enum
    add_iter(model, "Usage", "%d" % struct.unpack("<I", data[12:16])[0], 12, 4, "<I")
    add_iter(model, "offBmi", struct.unpack("<I", data[12:16])[0], 12, 4, "<I")
    add_iter(model, "cbBmi", struct.unpack("<I", data[16:20])[0], 16, 4, "<I")
    add_iter(model, "offBits", struct.unpack("<I", data[20:24])[0], 20, 4, "<I")
    add_iter(model, "cbBits", struct.unpack("<I", data[24:28])[0], 24, 4, "<I")

#0x5e
def CreateDIBPatternBrushPT(page, model, options, data):
    CreateMonoBrush(page, model, data)
    # FIXME! DIB as format?

#0x5f
def ExtCreatePen(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ObjID", "%#x" % struct.unpack("<I", data[8:12])[0], 8, 4, "<I")
    add_iter(model, "offBmi", struct.unpack("<I", data[12:16])[0], 12, 4, "<I")
    add_iter(model, "cbBmi", struct.unpack("<I", data[16:20])[0], 16, 4, "<I")
    add_iter(model, "offBits", struct.unpack("<I", data[20:24])[0], 20, 4, "<I")
    add_iter(model, "cbBits", struct.unpack("<I", data[24:28])[0], 24, 4, "<I")
    add_iter(model, "PenStyle", struct.unpack("<I", data[28:32])[0], 28, 4, "<I")
    add_iter(model, "Width", struct.unpack("<I", data[32:36])[0], 32, 4, "<I")
    add_iter(model, "BrushStyle", struct.unpack("<I", data[36:40])[0], 36, 4, "<I")
    clr = "%02X%02X%02X" % (data[42], data[41], data[40])
    add_iter(model, "RGB", clr, 40, 3, "clr")
    add_iter(model, "BrushHatch", struct.unpack("<I", data[44:48])[0], 44, 4, "<I")
    [numstyle] = struct.unpack("<I", data[48:52])
    add_iter(model, "NumEntryStyle", numstyle, 48, 4, "<I")
    for i in range(numstyle):
        add_iter(model, "Dash/Gap %d" % i, struct.unpack("<I", data[52 + i * 4:56 + i * 4])[0], 52 + i * 4, 4 ,"<I")
    # Optional BitmapBuffer
    add_iter(model, "BitmapBuffer", "(Optional)", 56 + numstyle * 4, len(data) - (56 + numstyle * 4), "blob")

#0x60 PolyTextOutA
def PolyTextOutA(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data, offset=8)
    igm = struct.unpack("<I", data[24:28])[0]
    add_iter(model, "iGraphicsMode", igm, 24, 4, "<I")
    add_iter(model, "exScale", "%d" % struct.unpack("<f", data[28:32])[0], 28, 4, "<d")
    add_iter(model, "eyScale", "%d" % struct.unpack("<f", data[32:36])[0], 32, 4, "<d")
    cStrings = struct.unpack("<I", data[36:40])[0]
    add_iter(model, "cStrings", cStrings, 36, 4, "<I")
    offset = 40
    for _ in range(cStrings):
        PointL(model, data, offset=offset, i="ref")
        offset += 8
        Chars = struct.unpack("<I", data[offset:offset + 4])[0]
        add_iter(model, "Chars", Chars, offset, 4, "<I")
        offset += 4
        add_iter(model, "offString", "%#x" % struct.unpack("<I", data[offset:offset + 4])[0], offset, 4, "<I")
        offset += 4
        options = struct.unpack("<I", data[offset:offset + 4])[0]
        add_iter(model, "Options", "%#x" % options, offset, 4, "<I")
        offset += 4
        # if not options & 0x100 -- looks like that would be correct, but MS is weird as always
        RectL(model, data, offset=offset)
        offset += 16
        offDx = struct.unpack("<I", data[offset:offset + 4])[0]
        add_iter(model, "offDx", "%#x" % offDx, offset, 4, "<I")
        offset = offDx + Chars * 4

#0x61
def PolyTextOutW(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data, offset=8)
    igm = struct.unpack("<I", data[24:28])[0]
    add_iter(model, "iGraphicsMode", igm, 24, 4, "<I")
    add_iter(model, "exScale", "%d" % struct.unpack("<f", data[28:32])[0], 28, 4, "<d")
    add_iter(model, "eyScale", "%d" % struct.unpack("<f", data[32:36])[0], 32, 4, "<d")
    cStrings = struct.unpack("<I", data[36:40])[0]
    add_iter(model, "cStrings", cStrings, 36, 4, "<I")
    offset = 40
    for _ in range(cStrings):
        PointL(model, data, offset=offset, i="ref")
        offset += 8
        Chars = struct.unpack("<I", data[offset:offset + 4])[0]
        add_iter(model, "Chars", Chars, offset, 4, "<I")
        offset += 4
        add_iter(model, "offString", "%#x" % struct.unpack("<I", data[offset:offset + 4])[0], offset, 4, "<I")
        offset += 4
        options = struct.unpack("<I", data[offset:offset + 4])[0]
        add_iter(model, "Options", "%#x" % options, offset, 4, "<I")
        offset += 4
        # if not options & 0x100 -- looks like that would be correct, but MS is weird as always
        RectL(model, data, offset=offset)
        offset += 16
        offDx = struct.unpack("<I", data[offset:offset + 4])[0]
        add_iter(model, "offDx", "%#x" % offDx, offset, 4, "<I")
        offset = offDx + Chars * 4


#0x62
def SetICMMode(page, model, options, data):
    SetMode(model, data)

#0x63
def CreateColorSpace(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "lhCS", struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")

#0x64
def SetColorSpace(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ObjID", "%#x" % struct.unpack("<I", data[8:12])[0], 8, 4, "<I")

#0x65
def DeleteColorSpace(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ObjID", "%#x" % struct.unpack("<I", data[8:12])[0], 8, 4, "<I")

#0x6c
def SmallTextOut(page, model, options, data):
    RecHdr(model, data)
    PointL(model, data, 8)
    add_iter(model, "cChars", struct.unpack("<I", data[0x10:0x14])[0], 0x10, 4, "<I")
    fuOptions = struct.unpack("<I", data[0x14:0x18])[0]
    add_iter(model, "fuOptions", fuOptions, 0x14, 4, "<I")
    add_iter(model, "iGraphicsMode", struct.unpack("<I", data[0x14:0x18])[0], 0x14, 4, "<I")
    add_iter(model, "exScale", struct.unpack("<f", data[0x18:0x1c])[0], 0x18, 4, "<f")
    add_iter(model, "eyScale", struct.unpack("<f", data[0x1c:0x20])[0], 0x1c, 4, "<f")
    if fuOptions & 0x100:
        RectL(model, data, offset=0x20)

#0x6d
def ForceUFIMapping(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "ChkSum", struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    add_iter(model, "Idx", struct.unpack("<I", data[0xc:0x10])[0], 0xc, 4, "<I")

#0x70
def SetICMProfileA(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "dwFlags", struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    cbName = struct.unpack("<I", data[0xc:0x10])[0]
    add_iter(model, "cbName", cbName, 0xc, 4, "<I")
    cbData = struct.unpack("<I", data[0x10:0x14])[0]
    add_iter(model, "cbData", cbName, 0x10, 4, "<I")
    add_iter(model, "Name", data[0x14:0x14 + cbName], 0x14, cbName, "txt")
    add_iter(model, "Data", 0x14 + cbName, cbData, "txt")

#0x71
def SetICMProfileW(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "dwFlags", struct.unpack("<I", data[8:0xc])[0], 8, 4, "<I")
    cbName = struct.unpack("<I", data[0xc:0x10])[0]
    add_iter(model, "cbName", cbName, 0xc, 4, "<I")
    cbData = struct.unpack("<I", data[0x10:0x14])[0]
    add_iter(model, "cbData", cbName, 0x10, 4, "<I")
    add_iter(model, "Name", data[0x14:0x14 + cbName * 2].decode("utf-16"), 0x14, cbName * 2, "utxt")
    add_iter(model, "Data", 0x14 + cbName * 2, cbData, "txt")

#0x73
def SetLayout(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "LayoutMode", struct.unpack("<I", data[8:12])[0], 8, 4, "<I")

#0x76
def GradientFill(page, model, options, data):
    RecHdr(model, data)
    RectL(model, data)
    nver = struct.unpack("<I", data[24:28])[0]
    add_iter(model, "nVer", nver, 24, 4, "<I")
    ntri = struct.unpack("<I", data[28:32])[0]
    add_iter(model, "nTri", ntri, 28, 4, "<I")
    ulmode = struct.unpack("<I", data[32:36])[0]
    add_iter(model, "ulMode", ulmode, 32, 4, "<I")
    for i in range(nver):
        PointL(model, data, 36 + i * 16, "%d" % i)
        add_iter(model, "  Red", struct.unpack("<H", data[44 + i * 16:46 + i * 16])[0], 44 + i * 16, 2, "<H")
        add_iter(model, "  Green", struct.unpack("<H", data[46 + i * 16:48 + i * 16])[0], 46 + i * 16, 2, "<H")
        add_iter(model, "  Blue", struct.unpack("<H", data[48 + i * 16:50 + i * 16])[0], 48 + i * 16, 2, "<H")
        add_iter(model, "  Alpha", struct.unpack("<H", data[50 + i * 16:52 + i * 16])[0], 50 + i * 16, 2, "<H")
    for i in range(ntri):
        if ulmode < 2:
            add_iter(model, "UpperLeft %d" % i, struct.unpack("<I", data[36 + nver * 16 + i * 8:40 + nver * 16 + i * 8])[0], 36 + nver * 16 + i * 8, 4, "<I")
            add_iter(model, "LowerRight %d" % i, struct.unpack("<I", data[40 + nver * 16 + i * 8:44 + nver * 16 + i * 8])[0], 40 + nver * 16 + i * 8, 4, "<I")
        else:
            add_iter(model, "Vertex1 %d" % i, struct.unpack("<I", data[36 + nver * 16 + i * 12:40 + nver * 16 + i * 12])[0], 36 + nver * 16 + i * 12, 4, "<I")
            add_iter(model, "Vertex2 %d" % i, struct.unpack("<I", data[40 + nver * 16 + i * 12:44 + nver * 16 + i * 12])[0], 40 + nver * 16 + i * 12, 4, "<I")
            add_iter(model, "Vertex3 %d" % i, struct.unpack("<I", data[44 + nver * 16 + i * 12:48 + nver * 16 + i * 12])[0], 44 + nver * 16 + i * 12, 4, "<I")
    if ulmode < 2:
        add_iter(model, "Padding (nTri * 4), if ulmode < 2", "padding", 36 + nver * 16 + ntri * 8, ntri * 4, "blob")

#0x78
def SetTextJustification(page, model, options, data):
    RecHdr(model, data)
    add_iter(model, "nBreakExtra", struct.unpack("<i", data[8:0xc])[0], 8, 4, "<i")
    add_iter(model, "nBreakCount", struct.unpack("<i", data[0xc:0x10])[0], 0xc, 4, "<i")

#0x79
def ClrMatchToTargetW(page, model, options, data):
    RecHdr(model, data)
    dwAction = struct.unpack("<I", data[0x8:0xc])[0]
    dt = "unknown"
    if dwAction in ColorSpace:
        dt = ColorSpace[dwAction]
    add_iter(model, "dwAction", "%d (%s)" % (dwAction, dt), 8, 4, "<I")
    dwFlags = struct.unpack("<I", data[0xc:0x10])[0]
    dt = "unknown"
    if dwFlags in ColorMatchToTarget:
        dt = ColorMatchToTarget[dwFlags]
    add_iter(model, "dwFlags", "%d (%s)" % (dwFlags, dt), 0xc, 4, "<I")
    cbName = struct.unpack("<I", data[0x10:0x14])[0]
    add_iter(model, "cbName", "%d" % cbName, 0x10, 4, "<I")
    cbData = struct.unpack("<I", data[0x14:0x18])[0]
    add_iter(model, "cbData", "%d"%cbData, 0x14, 4, "<I")
    txt = data[0x18:0x18+cbName*2].decode("utf-16")
    add_iter(model, "Name", txt, 0x18, cbName * 2, "utxt")
    add_iter(model, "Data", 0x18 + cbName * 2, cbData, "txt")

gc_ids = {0x80000001:"WindowsMetafile", 2:"BeginGroup", 3:"EndGroup",
    0x40000004:"MultiFormats", 0x00000040:"UNICODE_STRING", 0x00000080:"UNICODE_END"} #last two must not be used

gcfunc_ids = {
    #0x80000001:"WindowsMetafile",
    2:GC_BeginGroup,
    3:GC_EndGroup,
    #0x40000004:"MultiFormats"
    }

emr_ids = {1: Header, 2: Polybezier, 3: Polygon, 4: Polyline, 5: PolybezierTo, 6: PolylineTo,
    7: PolyPolyline, 8: PolyPolygon, 9: SetWindowExtEx, 0xa: SetWindowOrgEx, 0xb: SetViewportExtEx,
    0xc: SetViewportOrgEx, 0xd: SetBrushOrgEx, 0xe:EOF, 0xf: SetPixelV, 0x10: SetMapperFlags,
    0x11: SetMapMode, 0x12: SetBKMode, 0x13: SetPolyfillMode, 0x14: SetRop2, 0x15: SetStretchBltMode,
    0x16: SetTextAlign, 0x17: SetColorAdjustment, 0x18: SetTextColor, 0x19: SetBKColor,
    0x1a: OffsetClipRgn, 0x1b: MoveToEx, 0x1c: SetMetaRgn, 0x1d: ExcludeClipRect, 0x1e: IntersectClipRect,
    0x1f: ScaleViewportExtEx, 0x20: ScaleWindowExtEx, 0x21: SaveDC, 0x22: RestoreDC,
    0x23: SetWorldTransform, 0x24: ModifyWorldTransform, 0x25: SelectObject, 0x26: CreatePen,
    0x27: CreateBrushIndirect, 0x28: DeleteObject, 0x29: AngleArc, 0x2a:Ellipse, 0x2b: Rectangle,
    0x2c: RoundRect, 0x2d: Arc, 0x2e: Chord, 0x2f: Pie, 0x30: SelectPalette, 0x31:CreatePalette,
    0x32: SetPaletteEntries, 0x33: ResizePalette, 0x34: RealizePalette, 0x35: ExtFloodFill, 0x36: LineTo,
    0x37: ArcTo, 0x38: Polydraw, 0x39: SetArcDirection, 0x3a: SetMiterLimit, 0x3b: BeginPath, 0x3c: EndPath,
    0x3d: CloseFigure, 0x3e: FillPath, 0x3f: StrokeAndFillPath, 0x40: StrokePath, 0x41: FlattenPath,
    0x42: WidenPath, 0x43: SelectClipPath, 0x44: AbortPath, 0x46:GDIComment, # 0x45 is missing
    0x47: FillRgn, 0x48: FrameRgn, 0x49: InvertRgn,
    # 0x4a: PaintRgn,
    0x4b: ExtSelectClipRgn,
    # 0x4c: BitBlt, 0x4d: StretchBlt, 0x4e: MaskBlt,
    # 0x4f: PlgBlt, 0x50: SetDIBitsToDevice,
    0x51: StretchDIBits, 0x52: ExtCreateFontIndirectW, 0x53: ExtTextOutA, 0x54: ExtTextOutW,
    0x55: Polybezier16, 0x56: Polygon16, 0x57: Polyline16, 0x58: PolybezierTo16, 0x59: PolylineTo16,
    0x5a: PolyPolyline16, 0x5b: PolyPolygon16, 0x5c: Polydraw16, 0x5d: CreateMonoBrush,
    0x5e: CreateDIBPatternBrushPT,
    0x5f: ExtCreatePen,
    0x60: PolyTextOutA,
    0x61: PolyTextOutW,
    0x62: SetICMMode, 0x63: CreateColorSpace, 0x64: SetColorSpace, 0x65: DeleteColorSpace,
    # 0x66: GLSRecord , 0x67: GLSBoundedRecord,
    # 0x68: PixelFormat, 0x69: DrawEscape , 0x6a: ExtEscape , 0x6b: StartDoc,
    0x6c: SmallTextOut,
    0x6d: ForceUFIMapping, # 0x6e: NamedEscape, 0x6f: ColorCorrectPalette,
    0x70: SetICMProfileA, 0x71: SetICMProfileW, #0x72: AlphaBlend,
    0x73: SetLayout, # 0x74: TransparentBlt,
    0x76: GradientFill, # 0x77: SetLinkedUFI,
    0x78: SetTextJustification, 0x79: ClrMatchToTargetW, #0x7a: CreateColorSpaceW
    }

def file_save(fname, model, *args, **kwargs):
    root = model.get_iter("0")
    ch = model.iter_children(root)
    acc = 0
    while ch:
        if model[ch][0] == "GDIComment" and model.iter_n_children(ch):
            acc += 16 # "header"
            gch = model.iter_children(ch)
            while gch:
                acc += len(model[gch][3])
                gch = model.iter_next(gch)
        else:
            acc += len(model[ch][3])
        ch = model.iter_next(ch)
    num_ch = model.iter_n_children(root)
    data = model["0:0"][3]
    model["0:0"][3] = data[:0x30] + struct.pack("<II", acc, num_ch) + data[0x38:]
    formats.mf.dump_mf_tree(fname, model, "0")

#END
