#!/usr/bin/env python3
#
# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.


import math
import re
import struct
import traceback
from gi.repository import Gdk

import formats.utils
import utils
from utils import debug

DEBUG = False

chunks = {
    #0x: "ParentStory",
    0x0117: "Color ID", # Fill/Stroke in 6e64, 6e68
    0x0119: "Image ID",
    0x013c: "CLR Profile Name", # in 0x7d03
    0x0151: "XForm", # last two ieee754 are X and Y
    0x0154: "Page Size",
    0x015b: "Spread/Parent/Children IDs",
    #0x01bb: screen resolution? and few other ieee754
    0x01d8: "Saves History",
    0x0210: "TxtBlk2Page",
    0x021f: "BBox?", # text?
    0x0222: "List of Story IDs",
    0x0230: "Style Name",
    0x023f: "Style Values (?)", # ?H for rec nums, typical [?I id + ?H len + data] records
    0x02c8: "Parent/?/Children", # text
    0x027e: "BBox?", # text?
    0x02d6: "BBox?", # text?
    0x02de: "XForm", # text?
    0x0301: "List of Layer IDs",
    0x0302: "Layer Props ID",
    0x0303: "Layer Item IDs",
    0x0313: "Active Layer", # in 0x0e01
    0x040d: "XForm", # Group?
    0x0501: "List of Spread IDs", # in 0x0e01
    0x0503: "List of Layer IDs",
    0x051a: "Page Margins", # Inset/Top/Outset/Bottom
    0x0528: "Margin Column",
    0x056c: "Active Spread",
    0x056e: "XForm",
    0x05cc: "XForm", # page
    0x05dd: "GeometricBounds",
    0x0e02: "Document (cont.) ID",
    0x0a6b: "Page AppliedTrapPreset",
    0x1401: "List of MasterSpread IDs", # in 0x0e01
    0x140f: "Page AppliedMaster/MasterPageXForm",
    0x162b: "PathPoints", # dword: num_paths, dword: num points, dword: format. 2 -- x(f), y(f) per point; 0,1 -- triples for point/before/after
    0x1633: "GraphicBounds",
    #0x1708: "Image Size", # subrec '64' Width (dword), subrec '65' Height (dword)
    0x172e: "List of Image IDs",
    0x177a: "GraphicLayerOption",
    0x1f01: "CLR Values",
    0x1f02: "List of CLR IDs",
    0x1f05: "List of RGB CLR IDs", # ?I for num of stream IDs, ?I*x -- IDs
    0x1f0c: "List of Proc CLR IDs", # ?I for num of stream IDs, ?I*x -- IDs
    0x1f10: "CLR Name",
    0x1f30: "CLR Name",
    0x1f39: "List of root CLR IDs",
    0x1f60: "CLR List",  # dword (len in dwords), dword ('parent'), list of strm IDs of colors
    0x1f61: "Root CLR Group ID",
    0x288a: "List of 0xcab4 IDs", # ?I for num of stream IDs, ?I*x -- IDs
    0x2c1a: "ClipPath Settings",
    0x2c6b: "List of Item BBoxes",
    0x2d10: "List of Lang ID/Name pairs",
    0x3e06: "List of Font IDs", # ?I for num of stream IDs, ?I*x -- IDs
    0x4c01: "List of Section IDs", # in 0x0e01
    #0x4c02: includes name of the page format ("A4")?
    0x6e03: "ShapeProps",
    0x6e64: "Stroke",
    0x6e65: "LineWidth",
    0x6e68: "Fill",
    0x6e70: "InvRounded Corners",
    0x8c3f: "List of Link IDs",
    0x8c91: "Links (?)", # in 0x0e01
    0xa443: "Username", # in 0x0e01
    0xbf14: "Ref MasterPage?", # in 0x0e01
    0xcd02: "Page GridDataInformation",
    #0x13501: "List S-1355e", # in 0x0e01
    0x14534: "Button Name",
    0x1b916: "Frame Type ID",
    #0x1b92b: "List of recs", # ?H for rec nums, typical [?I id + ?H len + data] records
    #0x1b92c: "List of recs", # ?H for rec nums, typical [?I id + ?H len + data] records
    #0x1b92e: "List of rec IDs", # ?I for rec num, ?I*x IDs
    #0x1b956: "List of recs", # ?H for rec nums, typical [?I id + ?H len + data] records
    0x1be13: "List of Assignment IDs", # in 0x0e01
}


def add_master_page(parent, pgi_add, data, current):
    name = "Master Page"
    if current:
        name += " (current)"
    mp_iter = pgi_add(parent=parent, name=name, type="indd/MasterPage", data=data)
    # 16 bytes GUID
    # 8 bytes type of 'database' ["DOCUMENT", "BOOK   " (?) etc]
    # 1 byte LE(1)/BE(2) flag for "contignuos objects" (XMP) length
    # "70 0f 00 00" (LE) or "00 00 0f 70" (BE)
    # looks like a size that DB actualy uses from each 4K blocks
    #
    # last 0x80 bytes of the blocks sometimes used for some info that looks like IDs/lengths
    # but probably not needed for parsing
    # no samples were found (yet?) with other 'reserved' size

def add_xmp(parent, pgi_add, data, LE):
    off = 0
    while off < len(data):
        hdr = data[off:off + 0x24]
        '''
        header is 0x20 bytes. adding 4 bytes of XMP length to it.
        checksum can be calculated for 4 bytes of XMP length + XMP data
        with zlib.adler32(data, 0)
        '''
        pgi_add(parent=parent, name="XMP hdr", type="", data=hdr)

        xmp_len = struct.unpack("%sI" % LE, data[0x20 + off:0x24 + off])[0]
        trailer_start = 0x24 + off + xmp_len
        blk = data[off + 0x24:trailer_start]
        pgi_add(parent=parent, name="XMP", type="XMP", data=blk)
        trailer_end = math.ceil(trailer_start/0x1000) * 0x1000
        blk = data[trailer_start:trailer_end]
        pgi_add(parent=parent, name="XMP trailer", type="", data=blk)
        off += trailer_end


def parse_records(parent, page, rtype, LE, data, *args, **kwargs):
    '''
    Records seems to require to consider LE flag
    '''
    '''
    0xe01
      0x1d8
        dword of 'num_entries'
        each entry is 26 bytes + length of version string + 1
        strlen is at offset 0xe (same at 0x10)
        string is prepended with 0x40 (--> +1 byte)

        Sample (ver4):
            07 00
            01 00 05 00 02 00 00
            01 01 02 07 09 00 09 <-- length
            40 <-- single byte encoding  '80' -- utf16
            34 2e 30 2e 30 2e 34 32 31 <-- "@4.0.0.421"
            a5 01
            37 02 d7 01 26 d3 73 85 <-- x-timestamp (100 nanosec since Jan 1st 1600)

      0x222
        dword for the number of 'page' stream IDs,
        dwords of page IDs
        dword for the number of 'master page (?)' IDs
        dwords of 'master page' IDs
        03 00 00 00
            bf 00 00 00   d6 00 00 00   ec 00 00 00
        01 00 00 00
            75 00 00 00
    '''
    '''
    0xb667
        xx xx  <-- count of sub-recs
        sub-rec:
          xx xx xx xx <-- sub-rec type ID
          xx xx <-- len of the rest
          01 00 <-- num of sub-sub-recs
          xx xx xx xx <-- sub-sub-rec ID
          0x 00 <-- len of value
          [.. ..] <-- value; usually 2, 4, 8 bytes; couple of 8bytes*6 (ieee754)
    '''
    '''
    0x2d10: ?I num recs
        records:
            ?I stream ID
            [typical indd string (01 02 00 LL 00 LL 40 <LL bytes>]
        ?I stream ID for default???
    '''

    if rtype == "rec_0262":
        r262_type = struct.unpack("%sI" % LE, data[:4])[0]
        off = 4
        r262_parent = struct.unpack("%sI" % LE, data[off:off + 4])[0]
        off += 4
        num_recs = struct.unpack("%sH" % LE, data[off:off + 2])[0]
        off += 2
        page.pgi_add(parent=parent, name="", type="indd/Type/Parent/#Recs: %#x %#x %d" % (r262_type, r262_parent, num_recs), data=data[:0xa])
        for i in range(num_recs):
            rec_len = struct.unpack("%sI" % LE, data[off:off + 4])[0]
            page.pgi_add(parent=parent, name="", type="  Rec %d" % i , data=data[off:off + 4 + rec_len])
            off += 4 + rec_len
        if off < len(data):
            r262_tail = struct.unpack("%sI" % LE, data[off:off + 4])[0]
            page.pgi_add(parent=parent, name="", type="  Tail %#x" % r262_tail , data=data[off:])
    elif rtype == "rec_1708":
        '''
        dword: rec_num
            rec_num of [dword: subrec_id?, dword: rec_len,  byte: ??, rec_len bytes: data]
        '''
        num_recs = struct.unpack("%sI" % LE, data[:4])[0]
        off = 4
        for i in range(num_recs):
            rec_id, rec_len, flag = struct.unpack("%sIIB" % LE, data[off:off + 9])
            rectype = "Rec: %d Type/Flag: %#x/%x" % (i, rec_id, flag)
            if rec_len == 4:
                rectype += " [%d %d]" % struct.unpack("%sHH" % LE, data[off+9:off + 9 + 4])
            page.pgi_add(parent=parent, name="", type=rectype, data=data[off:off+9+rec_len])
            off += 9 + rec_len
    elif rtype == "rec_2c5d":
        '''
        dword: rec_num1
            rec_num1 of [dword: num?, dword: rec_id,  ieee754]
        dword: rec_num2
            rec_num2 of [dword: num?, dword: rec_id]
        '''
        # MAKEME
        pass
    elif rtype in ["rec_6e30", "rec_6e31"]:
        '''
        until the end:
            dword: num_rec
                num_rec of [dword: rec_id?]
        '''
        # MAKEME
        pass
    elif rtype == "path_points":
        # num of paths (dword)
        #   num of points (dword)
        #     point type (dword):
        #     2 -- 2*8 bytes for x,y; 0 or 1 -- 3*2*8 bytes for xy pairs of left/anchor/right
        #       (2 - plain, 1 - corner, 0 - smooth)
        # word: open (1) or closed (0)

        num_paths = struct.unpack("%sI" % LE, data[:4])[0]
        off = 4
        for i in range(num_paths):
            piter = page.pgi_add(parent=parent, name="", type="Path: %d" % i, data=b"")
            pnt_num = struct.unpack("%sI" % LE, data[off:off + 4])[0]
            off += 4
            for _ in range(pnt_num):
                pnt_type = struct.unpack("%sI" % LE, data[off:off + 4])[0]
                rsize = 0 if pnt_type == 2 else 16
                rdata = data[off: off + 20 + rsize * 2]
                off += 4
                if pnt_type < 2:
                    off += 16
                x = struct.unpack("%sd" % LE, data[off:off + 8])[0]
                off += 8
                y = struct.unpack("%sd" % LE, data[off:off + 8])[0]
                off += 8
                page.pgi_add(parent=piter, name="", type="  x: %d y: %d" % (x, y), data=rdata)
                off += rsize
                if pnt_type not in [0, 1, 2]:
                    print("New point type!", pnt_type)
            path_open = struct.unpack("%sH" % LE, data[off: off + 2])[0]
            page.pgi_add(parent=piter, name="", type="  %s path" % ("Open" if path_open else "Closed"), data=data[off:off + 2])
            off += 2

    elif rtype in ["reclist_h", "reclist_i"]:
        #"reclist_h"
        #?H for rec nums, typical [?I id + ?H len + data] records
        #"reclist_i"
        #?I for rec nums, typical [?I id + ?H len + data] records
        if rtype == "reclist_h":
            rec_num = struct.unpack("%sH" % LE, data[:2])[0]
            off = 2
        else:
            rec_num = struct.unpack("%sI" % LE, data[:4])[0]
            off = 4
        try:
            for _ in range(rec_num):
                rid = struct.unpack("%sI" % LE, data[off:off + 4])[0]
                rtype = "  SubRec %05x" % rid
                if rid in chunks:
                    rtype += " [%s]" % chunks[rid]
                off += 4
                rlen = struct.unpack("%sH" % LE, data[off:off + 2])[0]
                off += 2
                rdata = data[off:off + rlen]
                off += rlen
                riter = page.pgi_add(parent=parent, name="", type=rtype, data=rdata)
                rr_num = struct.unpack("%sH" % LE, rdata[:2])[0]
                roff = 2
                for i in range(rr_num):
                    rrid = struct.unpack("%sI" % LE, rdata[roff:roff + 4])[0]
                    rrtype = "    SubRec %05x  " % rrid
                    if rrid in chunks:
                        rrtype += " [%s]" % chunks[rrid]
                    roff += 4
                    rrlen = struct.unpack("%sH" % LE, rdata[roff:roff + 2])[0]
                    roff += 2
                    rrdata = rdata[roff:roff + rrlen]
                    roff += rrlen
                    if rrlen == 2:
                        rrtype += "%s" % struct.unpack("%sH" % LE, rrdata)[0]
                    elif rrlen == 4:
                        v = struct.unpack("%sI" % LE, rrdata)[0]
                        rrtype += "%s (%#x)" % (v, v)
                    elif rrlen == 8:
                        rrtype += "%s" % struct.unpack("%sd" % LE, rrdata)[0]
                    if rrlen > 5 and rrdata[:3] == b'\x02\x02\x00':
                        strlen = struct.unpack("%sH" % LE, rrdata[3:5])[0]
                        if strlen > 0:
                            txt = rrdata[7:7 + strlen].decode("utf-8")
                            rrtype += " [%s]" % txt
                    page.pgi_add(parent=riter, name="", type=rrtype, data=rrdata)
        except Exception as e:
            traceback.print_exc()
            print("Failed in subrec", kwargs["rectype"], rtype, rrtype)

def version(data):
    LE = "<"
    if data[0x18] == 2:
        LE = ">"
    return struct.unpack("%sI" % LE, data[0x1d:0x21])[0]

def parse(page, parent, data, kw):
    fname = page.path
    '''In XMP Specification part 3
    Adobe described INDD as a dump of DB with 4K blocks.
    First two blocks are 'master page', current one identified by greater
    'sequence number'.
    At the end of the file could be a dump of XMP stored.
    All values except size of XMP are in LE format.
    XMP size can be in BE, defined by the flag in the master page.
    '''
    master_pg_seq0 = struct.unpack("<Q", data[0x108:0x110])[0]
    master_pg_seq1 = struct.unpack("<Q", data[0x1108:0x1110])[0]
    xpb_off = 0 if master_pg_seq0 > master_pg_seq1 else 0x1000
    LE = "<"
    if data[0x18 + xpb_off] == 2:
        LE = ">"
    page.doc_data["LE"] = LE
    xp_block = struct.unpack("<H", data[0x118 + xpb_off:0x11a + xpb_off])[0]
    add_master_page(parent, page.pgi_add, data[:0x1000], not bool(xpb_off))
    add_master_page(parent, page.pgi_add, data[0x1000:0x2000], bool(xpb_off))

    biter = page.pgi_add(parent=parent, name="DB Blocks", type="indd/Blocks", data=b"")
    itr0 = page.pgi_add(parent=biter, name="Type 00", type="indd/Type0", data=b"")
    itr25 = page.pgi_add(parent=biter, name="Type 25", type="indd/Type25", data=b"")
    '''
    File can contain two blocks of each type 2, 3, 4 and 5.
    These come in consecuent pairs.
    Sometimes the 'second copy' of the block is completely filled with 0s.
    Current hypotesis is that it should be treated the same as 'Master Page': pick one with higher ID.

    type 5 seems to list dwords with IDs of type 6, 7 and 9 blocks.
    It can have gaps (00 00 00 00) between values.
    Probably have to be read till the last 12 bytes and drop all zeroes.
    Looks like file can easily include some blocks which are not in use anymore.
    Therefore the "current" type 5 block could be used to identify other "good" blocks.
    '''
    itr6 = page.pgi_add(parent=biter, name="Type 6", type="indd/Type6", data=b"")
    '''
    type 6: first dword defines num of 16 bytes records in the block.
        type 6 records:
            There are two types of type 6 records
            Type 6A
               '4bytes block seq ID', '4 bytes block ID', '2 bytes length', '2+4 bytes the place to take data'
                if 2 bytes in 'place to take data' is 0, then next 4 bytes is DB block ID
                otherwise it's a record ID (counted from 1) for type 9 block and 4 bytes is 'ID' of that type 9 block.
                Example A:
                  01 00 00 00   05 00 00 00   70 0f  00 00   0e 00 00 00
                  02 00 00 00   05 00 00 00   70 0f  00 00   10 00 00 00
                  03 00 00 00   05 00 00 00   20 0a  00 00   1a 00 00 00
                You need to take 0xf70 bytes from DB block 0x0e
                Concatenate 0xf70 bytes from DB block 0x10
                Concatenate 0xa20 bytes from DB block 0x1a
                This will be "stream #5".

                Example B:
                  01 00 00 00   08 00 00 00   20 02  01 00   05 00 00 00
                You need to take 0x220 bytes of the "first" record in the type 9 block with ID 5.
                This will be "stream #8".

                It's possible to have a stream splitted into one or more type 8 blocks and completed by a part from type 9 block.
                Looks like type 9 is used to store multiple 'small' records and type 8 for more efficient storage for 'large' streams.

                Type 6 could be used to reconstruct all the streams/recprds from the DB blocks.
                Streams start with ID of their content.
                Records seem to have an ID for the content type inside.
            Type 6B
                00 00 00 00   '4bytes stream/record ID'  '4bytes of type ID (???)'  00 00 00 00
    '''
    itr7 = page.pgi_add(parent=biter, name="Type 7", type="indd/Type7", data=b"")
    '''
    type 7: first dword defines num of 12-bytes records in the block.
        Records are '4bytes of type 6 ID', '4bytes of SeqNum' , '4bytes of stream/record num'
        SeqNum and Stream/Record num are for the last entry in the type 6 block

        Sample (from offset 0):
            13 00 00 00  => 0x13 records in the block
            01 00 00 00  0c 00 00 00  70 00 00 00   => block type 6 ID 01 ends with a record for stream/record 0x70 sequence 0x0c
            09 00 00 00  8b 00 00 00  70 00 00 00   => block type 6 ID 09 ends with a record for stream/record 0x70 sequence 0x8b
            11 00 00 00  01 00 00 00  aa 05 00 00   => block type 6 ID 11 ends with a record for stream/record 0x5aa sequence 0x01
    '''
    itr8 = page.pgi_add(parent=biter, name="Type 08", type="indd/Type8", data=b"")
    '''
    First type 8 block in the sequence (see type 6 above) starts with dword of the stream type (?).
    Next dword is the total size of the stream.
    !!! If less than 0xf70 bytes are used, for the first (and the only) type 8 block,
    then stream doesn't have type/length at the start !!!

    If the stream data (+8 bytes for type/size) is greater than 0xf70 bytes, other type 8 blocks concatenated 'as is'.
    The last type 8 block could contain less than 0xf70 bytes of data. How much to read from that could be figured
    from the stream size.
    '''
    itr9 = page.pgi_add(parent=biter, name="Type 09", type="indd/Type9", data=b"")
    '''
    type 9:
        consists of 'records' that starts with 2bytes length and 2 bytes ID.
        records could be out of order (by ID).
        Sample:
            3c 00           -- record length
            14 00           -- record ID
            62 02 00 00     -- ??
            30 00 00 00     -- size for the rest of the record
            02 02 00 00     -- record type? "text"
            ef 00 00 00     -- ??
            01 00           -- ??
            16 00 00 00     -- size for the next part till end of text
            10 00 00 00 10  -- dword of text length and byte of it???
            40 54 68 69 72
            64 20 70 61 67
            65 20 74 65 78
            74 0d           -- text prepended with '0x40' (encoding?)
            10 00 00 00     -- ??
            00 00 00 00     -- ??
            00 00 00 00     -- ??

        Special case:
            68 02
            2f 80    <-- record ID has MSB set
            00 00
            0c 00   50 00 00 00    <-- need to append at the end of this record content of record 0xc from block '09 50'

        Trailer of the type 9 has a ToC for the records. Need to read it "backward".
        Sample:

        Offset  <-- dwords with record's offsets -->
                00 00 00 00   => NA (only 7 records are here)
                10 00 00 00   => NA (only 7 records are here)
                0f 00 00 00   => NA (only 7 records are here)
                0e 00 00 00   => NA (only 7 records are here)
                0d 00 00 00   => NA (only 7 records are here)
                0c 00 00 00   => NA (only 7 records are here)
                0b 00 00 00   => NA (only 7 records are here)
                0a 00 00 00   => NA (only 7 records are here)
                09 00 00 00   => NA (only 7 records are here)
                fc 07 00 00   => record 7 offset
                d8 07 00 00   => record 6 offset
                50 07 00 00   => record 5 offset
                f0 06 00 00   => record 4 offset
        0xfd0   94 06 00 00   => record 3 offset
        0xfd4   34 06 00 00   => record 2 offset
        0xfd8   00 00 00 00   => record 1 offset
                <-- record's offsets end  -->
        0xfdc   10 00 00 00                              => how many dwords are reserved from here backward
        0xfe0   07 00 00 00                              => num of records with data
        0xfe4   08 00 00 00                              => "placeholder" 'ID'
        0xfe8   48 0e 00 00   54 01 00 00                => offset and size of "placeholder"
                00 00 00 00
                09 00 00 00   07 00 00 00   2b 20 6c bb  => block type 9 id 7, checksum?
                <-- end of block -->

        Can read dwords from (0xfdc - 4*num_records) as offsets for records with IDs from num_records to 1.

    '''
    itr_types = {0: itr0, 2:itr25, 3:itr25, 4:itr25, 5:itr25, 6:itr6, 7:itr7, 8: itr8, 9:itr9}

    # let's find which type 5 is current
    off5_8 = 0x9000 - 8
    off5_9 = 0xa000 - 8
    t5_8_seq = struct.unpack("<I", data[off5_8:off5_8 + 4])[0]
    t5_9_seq = struct.unpack("<I", data[off5_9:off5_9 + 4])[0]
    off5 = 0x8084
    if t5_8_seq < t5_9_seq:
        off5 = 0x9084

    # single type 5 could contain up to 988 dwords for IDs from 0x84 till 0xff4
    # collect 'current' blocks
    cur_blocks = []
    for i in range(988):
        blk_id = struct.unpack("<I", data[off5 + 4 * i:off5 + 4 * i + 4])[0]
        if not blk_id:
            continue
        cur_blocks.append(blk_id)

    blocks = {}
    block9s = {}
    streams = {}
    stream_0156 = None
    for i in range(2, xp_block):
        blk = data[i*4096:(i+1)*4096]
        t0 = struct.unpack("<I", blk[-12:-8])[0]    # block type
        t1 = struct.unpack("<I", blk[-8:-4])[0]     # block ID for types except 8
        t2 = struct.unpack("<I", blk[-4:])[0]       # checksum? doesn't seem to be adler32 as for XMP, could be just "unique ID"
        piter = parent
        if t0 in itr_types:
            piter = itr_types[t0]
        itype = "  %02x %02x    %02x" % (t0, t1, t2)
        if i in cur_blocks:
            itype += " *"
        t_iter = page.pgi_add(parent=piter, name="%02x" % i, type=itype, data=blk)
        blocks[i] = (t0, t1)
        if t0 == 9 and i in cur_blocks:
            # caching info on where is the specific type 9 block is stored
            # check for presense in cur_blocks because it's possible to have "obsolete" type 9 blocks with the same IDs
            block9s[t1] = i
        if t0 == 6:
            size = struct.unpack("<I", blk[:4])[0]
            b_off = 4
            for k in range(size):
                seqnum = struct.unpack("<I", blk[b_off + k * 0x10: b_off + k * 0x10 + 4])[0]
                if seqnum:
                    streamnum = struct.unpack("<I", blk[b_off + k * 0x10 + 4: b_off + k * 0x10 + 8])[0]
                    streamsize = struct.unpack("<H", blk[b_off + k * 0x10 + 8: b_off + k * 0x10 + 0xa])[0]
                    recnum = struct.unpack("<H", blk[b_off + k * 0x10 + 0xa: b_off + k * 0x10 + 0xc])[0]
                    blknum = struct.unpack("<I", blk[b_off + k * 0x10 + 0xc: b_off + k * 0x10 + 0x10])[0]
                    name = "Stream"
                    srtype = "SN %02x Seq %02x Blk %02x Size %02x" % (streamnum, seqnum, blknum, streamsize)
                    if recnum:
                        name = "Record"
                        srtype = "RN %02x Seq %02x Blk %02x %02x Size %02x" % (streamnum, seqnum, blknum, recnum, streamsize)
                    page.pgi_add(parent=t_iter, name=name, type=srtype, data=blk[b_off + k * 0x10:b_off + k * 0x10 + 0x10])
                    if i in cur_blocks:
                        if not streamnum in streams:
                            streams[streamnum] = {"data_refs":{}, "type":None}
                        streams[streamnum]["data_refs"][seqnum] = (blknum, recnum, streamsize)
                else:
                    streamnum = struct.unpack("<I", blk[b_off + k * 0x10 + 4: b_off + k * 0x10 + 8])[0]
                    streamtype = struct.unpack("<I", blk[b_off + k * 0x10 + 8: b_off + k * 0x10 + 0xc])[0]
                    if streamtype == 0x156:
                        stream_0156 = streamnum
                    page.pgi_add(parent=t_iter, name="Str/Rec Type", type="indd/SN %08x Type %04x" % (streamnum, streamtype), data=blk[b_off + k * 0x10:b_off + k * 0x10 + 0x10])
                    if i in cur_blocks:
                        if not streamnum in streams:
                            streams[streamnum] = {"data_refs":{}, "type":None}
                        streams[streamnum]["type"] = streamtype

    add_xmp(parent, page.pgi_add, data[xp_block * 4096:], LE)

    # here I should have all info on streams to rebuild them

    # for now will extract good parts in that iter
    streams_iter = page.pgi_add(parent=parent, name="Streams", type="indd/SR", data=b"")
    stypes = {
        0x0129: "Image",
        0x0201: "---- Story ----",
        0x0205: "Style",
        0x0301: "Layer",
        0x0302: "Layer Props",
        0x0501: "Spread",
        0x050f: "Page",
        0x0e01: "Document",
        0x1401: "MasterSpread",
        0x1f05: "Color",
        0x1f07: "Color",   # Process Color
        0x1f11: "RGB Color",
        0x2202: "Document (cont.)",
        0x2d07: "Lang",
        0x3e03: "Font Family",
        0x4c01: "Section",
        0x6201: "Shape",
        0x7d06: "CLR Profile",
        0xca17: "TxtProp",  # size: 0x1b28, 0x1b03; color (rec#): 0x1b01,0x1b05
        0xca18: "Text",
        0xcb03: "Charset",
        0x1450c: "Button",
    }
    no_recs = [0x0129,]

    def reconstruct_stream(sdata_refs, blocks9s, sk):
        # 'sk' for debug only
        sdata = b""
        for sdk in sorted(sdata_refs):
            blknum, recnum, streamsize = sdata_refs[sdk]
            if not recnum:
                # data from type 8 block
                # need to skip 8 bytes of the first block
                skip = 0
                if not sdata and streamsize > 0xf70:
                    skip = 8
                b_off = blknum * 0x1000
                sdata += data[b_off + skip:b_off + streamsize]
            else:
                # data from type 9 block
                # get DB block ID for specific type 9 block ID and calculate offset to the block
                # FIXME! need to move it to a function to call for end_appending if needed
                b_off = block9s[blknum] * 0x1000
                # offset to the specific record
                r_off = b_off + 0xfdc - 4 * recnum
                d_off = b_off + struct.unpack("<I", data[r_off:r_off + 4])[0]
                r9_id = struct.unpack("<H", data[d_off + 2:d_off + 4])[0]
                if r9_id & 0x8000:
                    # need to append something to the end
                    # WARN! Assume that appending record will not have &0x8000 ID
                    ea_recnum = struct.unpack("<H", data[d_off + 6:d_off + 8])[0]
                    ea_blknum = struct.unpack("<I", data[d_off + 8:d_off + 0xc])[0]
                    n_streamsize = struct.unpack("<H", data[d_off:d_off + 2])[0]
                    if DEBUG:
                        print("BN, RN: %02x %02x, r_id %02x EABN/RN: 09 %02x %02x (%02x)" % (blknum, recnum, r9_id, ea_blknum, ea_recnum, sk), "%x %x" % (streamsize, len(sdata)))
                    sdata += data[d_off + 0xc:d_off + n_streamsize] # skip record length/id and end_append bytes
                    ea_b_off = block9s[ea_blknum] * 0x1000
                    ea_r_off = ea_b_off + 0xfdc - 4 * ea_recnum
                    ea_d_off = ea_b_off + struct.unpack("<I", data[ea_r_off:ea_r_off + 4])[0]
                    ea_streamsize = min(streamsize - n_streamsize + 0xc, struct.unpack("<H", data[ea_d_off:ea_d_off + 2])[0])
                    sdata += data[ea_d_off + 4:ea_d_off + 4 + ea_streamsize] # skip record length/id
                else:
                    sdata += data[d_off + 4:d_off + 4 + streamsize] # skip record length/id
        return sdata

    # here I want to reconstruct stream with type 0x0156, extract record 0x0191 and parse it for stream/record references to RPLNs
    stream_0156_data = b""
    if stream_0156:
        stream_0156_data = reconstruct_stream(streams[stream_0156]["data_refs"], block9s, stream_0156)
        if stream_0156_data:
            # INDD Rec 0191
            # [:0x15] -- crap
            # ?H -- length, LE/BE depends on the flag
            # -- 0x40
            # string of length above
            # ?I -- num of records
            #   record
            #    ?I -- ID
            #    03 02 00 xx [?H len] 40
            #    [string of len]
            #    27 bytes of crap
            # ... repeat num of recs time
            # repeat 2 times, first -- streams, second -- records
            # ?I -- num of 2*dwords mappings

            l = struct.unpack("%sH" % LE, stream_0156_data[0x15 + 8:0x17 + 8])[0]  # +8 to skip record type/length
            off = 0x19 + 8 + l
            nrecs = struct.unpack("%sI" % LE, stream_0156_data[off:off + 4])[0]
            off += 4
            recs = {}
            try:
                for _ in range(nrecs):
                    rid = struct.unpack("%sI" % LE, stream_0156_data[off:off+4])[0]
                    off += 7
                    l = struct.unpack("%sH" % LE, stream_0156_data[off:off+2])[0]
                    off += 4
                    rname = stream_0156_data[off:off+l].decode("utf-8")
                    off += l
                    n1 = struct.unpack("%sI" % LE, stream_0156_data[off:off+4])[0]
                    off += 4
                    n2 = struct.unpack("%sI" % LE, stream_0156_data[off:off+4])[0]
                    off += 23
                    if rname[-15:] == ".InDesignPlugin":
                        rname = rname[:-15]
                    elif rname[-5:] == ".RPLN":
                        rname = rname[:-5]
                    recs[rid] = rname
                for dest in (stypes, chunks):
                    nrecs2 = struct.unpack("%sI" % LE, stream_0156_data[off:off+4])[0]
                    off += 4
                    for _ in range(nrecs2):
                        pid = struct.unpack("%sI" % LE, stream_0156_data[off:off+4])[0]
                        off += 4
                        cid = struct.unpack("%sI" % LE, stream_0156_data[off:off+4])[0]
                        off += 4
                        if pid not in dest:
                            dest[pid] = "\t%s@156" % recs[cid]
            except Exception as e:
                print("Failed in 0x156 parsing", e)


    rec_types = {
        0x0023f: "reclist_h",
        0x06e03: "reclist_i",
        0x0b667: "reclist_h",
        0x1b92b: "reclist_h",
        0x1b92c: "reclist_h",
        0x1b956: "reclist_h",

        0x00262: "rec_0262",
        0x01708: "rec_1708",

        0x162b: "path_points",
    }

    # store stream num to model iter mapping
    page.doc_data["indd_streams"] = {}

    for sk in sorted(streams):
        stype = streams[sk]["type"]
        sstype = "%05x" % stype
        if stype in stypes:
            sstype += " [%s]" % stypes[stype]
        if sk == stream_0156:
            sdata = stream_0156_data
        else:
            sdata_refs = streams[sk]["data_refs"]
            sdata = reconstruct_stream(sdata_refs, block9s, sk)

        if len(sdata): # sometimes streams seems to not exist, but yet referenced in type 6B blocks
            s_iter = page.pgi_add(parent=streams_iter, name="Stream %04x" % sk, type="indd/Stream %s" % sstype, data=sdata)
            page.doc_data["indd_streams"][sk] = s_iter
            if stype in no_recs or stype & 0x80000000:
                continue
            # let's try to split to records in the stream (and sort them)
            rec_off = 0
            recs = {}
            while rec_off < len(sdata):
                try:
                    rec_id = struct.unpack("%sI" % LE, sdata[rec_off:rec_off + 4])[0]
                    rec_off += 4
                    rec_len = struct.unpack("%sI" % LE, sdata[rec_off:rec_off + 4])[0]
                    rec_off += 4
                    rec_data = sdata[rec_off:rec_off + rec_len]
                    rec_off += rec_len
                    recs[rec_id] = rec_off, rec_data
                except:
                    print("Failed to split stream into records!", "%02x" % sk, sstype, "Len/Off", len(sdata), rec_off)
                    break
            for rid in sorted(recs):
                rdata = recs[rid][1]
                rtype = "%04x" % rid
                if rid in chunks:
                    rtype += " [%s]" % chunks[rid]
                if rid == 0x1f01:
                    pal = struct.unpack("%sI" % LE, rdata[0:4])[0]
                    if pal == 6:
                        # cmyk
                        c = struct.unpack("%sd" % LE, rdata[0x6:0xe])[0]
                        m = struct.unpack("%sd" % LE, rdata[0xe:0x16])[0]
                        y = struct.unpack("%sd" % LE, rdata[0x16:0x1e])[0]
                        k = struct.unpack("%sd" % LE, rdata[0x1e:0x26])[0]
                        clr = formats.utils.cmyk2rgb(c, m, y, k)
                    elif pal == 5:
                        r = int(255 * struct.unpack("%sd" % LE, sdata[0x0e:0x16])[0])
                        g = int(255 * struct.unpack("%sd" % LE, sdata[0x16:0x1e])[0])
                        b = int(255 * struct.unpack("%sd" % LE, sdata[0x1e:0x26])[0])
                        clr = (r, g, b)
                    pn = stypes[stype] if stype in stypes else "###"
                    lum = 0.2126 * r + 0.7152 * g + 0.0722 * b
                    fgclr = "black" if lum > 127 else "white"
                    prtype = "%05x <span background='#%02x%02x%02x' foreground='%s'>[%s]</span>" % (stype, *clr, fgclr, pn)
                    page.pgi_mod(s_iter, type="indd/Stream %s" % prtype)
                riter = page.pgi_add(parent=s_iter, name="Records %04x" % recs[rid][0], type="indd/Rec %s" % rtype, data=rdata)
                if rid == 0x501: # spreads
                    page.doc_data["indd_spreads"] = riter
                if rid in rec_types:
                    parse_records(riter, page, rec_types[rid], LE, rdata, rectype=rtype)

    page.main_tview.freeze_child_notify()
    page.on_load_complete()

# END
