#!/usr/bin/env python3
#
# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import re
import struct

import formats.cdr
import formats.indd
import formats.mf
import formats.ole
import formats.pkzip
from utils import debug


def cmyk2rgb(c, m, y, k):
    c1 = (c * (1 - k) + k)
    m1 = (m * (1 - k) + k)
    y1 = (y * (1 - k) + k)
    r = int((1 - c1) * 255)
    g = int((1 - m1) * 255)
    b = int((1 - y1) * 255)
    return r, g, b


def parse(path, model, parent=None, data=None):
    if path:
        with open(path, "rb") as f:
            data = f.read()
    elif not data:
        print("No path && no data == no parse")
        return
    off = 0
    if data[0:4] == "RIFF":
        dsize = struct.unpack("<I",data[4:8])
        if data[8:12] == "MGX ":
            off = 12
    liter = parent
    while off < len(data)-8:
        fourcc = data[off:off+4]
        length = struct.unpack("<I",data[off+4:off+8])[0]
        if fourcc == "LIST":
            off += 8
            listname = data[off:off+4]
            niter = model.append(liter, ["%s"%listname, length + 8])
            parse(None, model, niter, data[off+4:off+length])
        else:
            niter = model.append(liter, ["%s"%fourcc, length+8])
            off += 8
        off += length

signatures = [
    (b"\x06\x06\xed\xf5\xd8\x1d\x46\xe5\xbd\x31\xef\xe7\xfe\x74\xb7\x1d", "INDD", formats.indd.parse),
    (b"Kaydara FBX Binary  ", "FBX", None),
    (b"\xd0\xcf\x11\xe0\xa1\xb1\x1a\xe1", "OLE", formats.ole.detect),
    (b"\x1aWLF10", "VFB", None),
    (b"CPT9FILE", "CPT", None),
    (b"VjCD0100", "CHDRAW", None),
    (b"\x4c\x00\x52\x00\x46\x00\x00\x00", "LRF", None),
    (b"VCLMTF", "SVM", None),
    (b"EVHD", "YEP", None),
    (b"\x12\x90\xa8\x7f", "NKI", None),
    (b"8BGR", "BGR", None),
    (b"\xd7\xcd\xc6\x9a", "WMF", formats.mf.parse, {"type": "WMF"}), # Aldus Placeable WMF
    (b"\x01\x00\x09\x00\x00\x03", "WMF", formats.mf.parse, {"type": "WMF"}),
    (b"ITOLITLS", "LIT", None),
    (b"BOOKDOUG", "IMP", None),
    (b"\x50\x4b\x03\x04", "PKZIP", formats.pkzip.parse),
    (b".{40}\x20\x45\x4d\x46", "EMF", formats.mf.parse, {"type": "EMF"}),
    (b"\x50\xc3", "CLP", None),
    (b"WL", "CDRv2", None),
    (b"\xcc\xdc", "CPL", None),
    (b"KF[^\x00]", "CDW", None),
    (b"(?i){\\rtf", "RTF", None),  # ignore case
    (b"<\\?xml |\xff\xfe<\0\\?\0x\0m\0l\0 \0|\xfe\xff\0<\0\\?\0x\0m\0l\0 ", "XML", None),
    (b"FHD2|acf3", "FH12", None),
    (b"AGD", "FH", None),
#   (b"|".join(pdb.pdb_types.keys()), 0x3c): "PDB", None),   # add later
    (b".{2}IIXPR3", "QXP", None),
    (b".{4}8BIM", "ABR", None),
    (b".{8}xV4", "ZMF", None),
    (b".{4}Standard (Jet|ACE) DB", "MDB", None),
    (b"RIFF.{4}[Cc][Dd][Rr]", "CDR", formats.cdr.parse_init),
    (b"RIFF.{4}CMX", "CMX", None),
    (b"CAT .{4}REX2", "REX2", None),
    (b"\x9a\x02(\01|\02|\03)\0", "ZBR", None),
    (b"\x01\xff\x02", "DRW", None), # unreliable

    # need full file...
#   (b".*Freehand.*AGD", "FH9", None),
#   (b".*FHDocHeader" && b"FH", "FH34", None)

]

sigs_compiled = []

def detect(data, size):
    global signatures, sigs_compiled
    if len(signatures) > len(sigs_compiled):
        sigs_compiled = []
        for (s, *e) in signatures:
            sigs_compiled.append((re.compile(s), *e))

    for sig in sigs_compiled:
        r, fmt, action = sig[:3]
        if re.match(r, data):
            kwargs = {}
            if len(sig) > 3:
                kwargs = sig[3]
            return fmt, kwargs, action

    # check special
    csize = (data[1] | data[2] << 8) + 4
    if data[0] == "\0" and (csize == size or (csize < size and data[4:7] == "\x80\x80\x04")):
        print("Detected: IWA")
        return "IWA", None, None

    return "", None, None

# END
