# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import gi
gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GObject

import struct
import sys

from utils import *

emrplus_ids = {
    0x4001: "Header", 0x4002: "EOF", 0x4003: "Comment", 0x4004: "GetDC",
    0x4005: "MultiFormatStart", 0x4006: "MultiFormatSection",
    0x4007: "MultiFormatEnd", 0x4008: "Object", 0x4009: "Clear",
    0x400A: "FillRects", 0x400B: "DrawRects", 0x400C: "FillPolygon",
    0x400D: "DrawLines", 0x400E: "FillEllipse", 0x400F: "DrawEllipse",
    0x4010: "FillPie", 0x4011: "DrawPie", 0x4012: "DrawArc",
    0x4013: "FillRgn", 0x4014: "FillPath", 0x4015: "DrawPath",
    0x4016: "FillClosedCurve", 0x4017: "DrawClosedCurve", 0x4018: "DrawCurve",
    0x4019: "DrawBeziers", 0x401A: "DrawImage", 0x401B: "DrawImagePoints",
    0x401C: "DrawString", 0x401D: "SetRenderingOrigin", 0x401E: "SetAntiAliasMode",
    0x401F: "SetTextRenderingHint", 0x4020: "SetTextContrast", 0x4021: "SetInterpolationMode",
    0x4022: "SetPixelOffsetMode", 0x4023: "SetCompositingMode",
    0x4024: "SetCompositingQuality", 0x4025: "Save", 0x4026: "Restore",
    0x4027: "BeginContainer", 0x4028: "BeginContainerNoParams",
    0x4029: "EndContainer", 0x402A: "SetWorldTransform",
    0x402B: "ResetWorldTransform", 0x402C: "MultiplyWorldTransform",
    0x402D: "TranslateWorldTransform", 0x402E: "ScaleWorldTransform",
    0x402F: "RotateWorldTransform", 0x4030: "SetPageTransform",
    0x4031: "ResetClip", 0x4032: "SetClipRect", 0x4033: "SetClipPath",
    0x4034: "SetClipRgn", 0x4035: "OffsetClip", 0x4036: "DrawDriverString",
    0x4037: "StrokeFillPath", 0x4038: "SerializableObject", 0x4039: "SetTSGraphics",
    0x403A: "SetTSClip"
    }

emr_ids = {0:'Unknown', 1: 'Header', 2: 'Polybezier', 3: 'Polygon', 4: 'Polyline', 5: 'PolybezierTo',
    6: 'PolylineTo', 7: 'PolyPolyline', 8: 'PolyPolygon', 9: 'SetWindowExtEx', 10: 'SetWindowOrgEx',
    11: 'SetViewportExtEx', 12:'SetViewportOrgEx', 13: 'SetBrushOrgEx', 14: 'EOF', 15: 'SetPixelV',
    16: 'SetMapperFlags', 17: 'SetMapMode', 18: 'SetBKMode', 19: 'SetPolyfillMode', 20: 'SetRop2',
    21: 'SetStretchBltMode', 22: 'SetTextAlign', 23: 'SetColorAdjustment', 24: 'SetTextColor',
    25: 'SetBKColor', 26: 'OffsetClipRgn', 27: 'MoveToEx', 28: 'SetMetaRgn', 29: 'ExcludeClipRect',
    30: 'IntersectClipRect', 31: 'ScaleViewportExtEx', 32: 'ScaleWindowExtEx', 33: 'SaveDC',
    34: 'RestoreDC', 35: 'SetWorldTransform', 36: 'ModifyWorldTransform', 37: 'SelectObject',
    38: 'CreatePen', 39: 'CreateBrushIndirect', 40: 'DeleteObject', 41: 'AngleArc', 42: 'Ellipse',
    43: 'Rectangle', 44: 'RoundRect', 45: 'Arc', 46: 'Chord', 47: 'Pie', 48: 'SelectPalette',
    49: 'CreatePalette', 50: 'SetPaletteEntries', 51: 'ResizePalette', 52: 'RealizePalette',
    53: 'ExtFloodFill', 54: 'LineTo', 55: 'ArcTo', 56: 'Polydraw', 57: 'SetArcDirection', 58: 'SetMiterLimit',
    59: 'BeginPath', 60: 'EndPath', 61: 'CloseFigure', 62: 'FillPath', 63: 'StrokeAndFillPath',
    64: 'StrokePath', 65: 'FlattenPath', 66: 'WidenPath', 67: 'SelectClipPath', 68: 'AbortPath', ##69 is missed
    70: 'GDIComment', 71: 'FillRgn', 72: 'FrameRgn', 73: 'InvertRgn', 74: 'PaintRgn', 75: 'ExtSelectClipRgn',
    76: 'BitBlt', 77: 'StretchBlt', 78: 'MaskBlt', 79: 'PlgBlt', 80: 'SetDIBitsToDevice', 81: 'StretchDIBits',
    82: 'ExtCreateFontIndirectW', 83:'ExtTextOutA', 84: 'ExtTextOutW', 85: 'Polybezier16', 86: 'Polygon16',
    87: 'Polyline16', 88: 'PolybezierTo16', 89: 'PolylineTo16', 90: 'PolyPolyline16', 91: 'PolyPolygon16',
    92: 'Polydraw16', 93: 'CreateMonoBrush', 94: 'CreateDIBPatternBrushPT', 95:'ExtCreatePen',
    96: 'PolyTextOutA', 97: 'PolyTextOutW', 98: 'SetICMMode', 99: 'CreateColorSpace', 100: 'SetColorSpace',
    101: 'DeleteColorSpace', 102: 'GLSRecord', 103:'GLSBoundedRecord', 104:'PixelFormat', 105: 'DrawEscape',
    106: 'ExtEscape', 107: 'StartDoc', 108: 'SmallTextOut', 109: 'ForceUFIMapping', 110: 'NamedEscape',
    111: 'ColorCorrectPalette', 112: 'SetICMProfileA', 113: 'SetICMProfileW', 114: 'AlphaBlend',
    115: 'SetLayout', 116: 'TransparentBlt', 117: 'Reserved_117', 118: 'GradientFill', 119: 'SetLinkedUFI',
    120: 'SetTextJustification', 121: 'ColorMatchToTargetW', 122: 'CreateColorSpaceW'
    }

wmr_ids = {0: 'EOF', 1: 'Header', 2: 'CLP_Header16', 3: 'CLP_Header32', #4: 'Header',
    30: 'SaveDC', 295: 'RestoreDC', 332: 'ResetDC',
    53: 'RealizePalette', 55: 'SetPalEntries', 247: 'CreatePalette', 313: 'ResizePalette', 564: 'SelectPalette', 1078: 'AnimatePalette',
    79: 'StartPage', 80: 'EndPage', 82: 'AbortDoc', 94: 'EndDoc', 333: 'StartDoc',
    248: 'CreateBrush', 322: 'DibCreatePatternBrush', 505: 'CreatePatternBrush',
    762: 'CreatePenIndirect', 763: 'CreateFontIndirect', 764: 'CreateBrushIndirect', 765: 'CreateBitmapIndirect',
    496: 'DeleteObject', 301: 'SelectObject',
    258: 'SetBKMode', 259: 'SetMapMode', 260: 'SetROP2', 261: 'SetRelabs', 262: 'SetPolyfillMode', 263: 'SetStretchBltMode',
    561: 'SetMapperFlags', 329: 'SetLayout',
    264: 'SetTextCharExtra', 302: 'SetTextAlign', 513: 'SetBKColor', 521: 'SetTextColor', 522: 'SetTextJustification',
    298: 'InvertRegion', 299: 'PaintRegion', 300: 'SelectClipRegion', 544: 'OffsetClipRgn', 552: 'FillRegion', 1065: 'FrameRegion', 1791: 'CreateRegion',
    1045: 'ExcludeClipRect', 1046: 'IntersectClipRect',
    523: 'SetWindowOrgEx', 524: 'SetWindowExtEx', 525: 'SetViewportOrgEx', 526: 'SetViewportExtEx', 527: 'OffsetWindowOrg', 529: 'OffsetViewportOrgEx',
    1040: 'ScaleWindowExtEx', 1042: 'ScaleViewportExtEx',
    1049: 'FloodFill', 1352: 'ExtFloodFill', 1574: 'Escape',
    531: 'LineTo', 532: 'MoveTo', 804: 'Polygon', 805: 'Polyline', 1048: 'Ellipse', 1051: 'Rectangle', 1055: 'SetPixel',
    1336: 'PolyPolygon', 1564: 'RoundRect', 2071: 'Arc', 2074: 'Pie', 2096: 'Chord',
    1313: 'TextOut', 1583: 'DrawText', 2610: 'ExtTextOut',
    1790: 'CreateBitmap', 1565: 'PatBlt', 2338: 'BitBlt', 2368: 'DibBitblt', 2851: 'StretchBlt', 2881: 'DibStretchBlt',
    3379: 'SetDibToDev', 3907: 'StretchDIB'
    }


def BitmapCoreHeader(model, data, off=0):
    add_iter(model, "HeaderSize", struct.unpack("<I", data[off:off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "Width", struct.unpack("<H", data[off:off + 2])[0], off, 2, "<H")
    off += 2
    add_iter(model, "Height", struct.unpack("<H", data[off:off + 2])[0], off, 2, "<H")
    off += 2
    add_iter(model, "Planes", struct.unpack("<H", data[off:off + 2])[0], off, 2, "<H")
    off += 2
    bce = struct.unpack("<H", data[off:off + 2])[0]
    add_iter(model, "BitCount", bce, off, 2, "<H")

def BitmapInfoHeader(model, data, off=0):
    add_iter(model, "HeaderSize", struct.unpack("<I", data[off:off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "Width", struct.unpack("<i", data[off:off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "Height", struct.unpack("<i", data[off:off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "Planes", struct.unpack("<H", data[off:off + 2])[0], off, 2, "<H")
    off += 2
    bce = struct.unpack("<H", data[off:off + 2])[0]
    add_iter(model, "BitCount", bce, off, 2, "<H")
    off += 2
    add_iter(model, "Compression", struct.unpack("<I", data[off:off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "ImageSize", struct.unpack("<I", data[off:off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "XPelsPerMeter", struct.unpack("<i", data[off:off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "YPelsPerMeter", struct.unpack("<i", data[off:off + 4])[0], off, 4, "<i")
    off += 4
    add_iter(model, "ColorUsed", struct.unpack("<I", data[off:off + 4])[0], off, 4, "<I")
    off += 4
    add_iter(model, "ColorImportant", struct.unpack("<I", data[off:off + 4])[0], off, 4, "<I")

def PointS(model, data, offset, i="", fmt=None):
    x, y = ("y", "x") if fmt == "wmf" else ("x", "y")
    add_iter(model, x + i, struct.unpack("<h", data[offset:offset+2])[0], offset, 2, "<h")
    add_iter(model, y + i, struct.unpack("<h", data[offset+2:offset+4])[0], offset + 2, 2, "<h")

def PointL(model, data, offset, i=""):
    add_iter(model, "x" + i, struct.unpack("<i", data[offset:offset + 4])[0], offset, 4, "<i")
    add_iter(model, "y" + i, struct.unpack("<i", data[offset + 4:offset + 8])[0], offset + 4, 4, "<i")

def PointF(model, data, offset, i=""):
    add_iter(model, "%sX" % i, struct.unpack("<f", data[offset:offset + 4])[0], offset, 4, "<f")
    add_iter(model, "%sY" % i, struct.unpack("<f", data[offset + 4:offset + 8])[0], offset + 4, 4, "<f")

def GetCoordR(data, offset):
    hb = data[offset]
    if hb & 1: # 2 bytes coord
        off = 2
        hb = struct.unpack("<H", data[offset:offset + 2])[0]
        val = hb // 2
        if val > 16383:
            val = -val - 1
    else:
        off = 1
        val = hb // 2
        if val > 63:
            val = -val - 1
    return off, val

def PointR(model, data, offset, i=""):
    offx, x = GetCoordR(data, offset)
    offy, y = GetCoordR(data, offset + offx)
    add_iter(model, "xR%s" % i, x, offset, offx, "int7" if offx == 1 else "int15")
    add_iter(model, "yR%s" % i, y, offset + offx, offy, "int7" if offy == 1 else "int15")
    return offx + offy

def Rect(model, data, offset=8):
    add_iter(model, "xS", struct.unpack("<h", data[offset:offset + 2])[0], offset, 2, "<h")
    offset += 2
    add_iter(model, "yS", struct.unpack("<h", data[offset:offset + 2])[0], offset, 2, "<h")
    offset += 2
    add_iter(model, "xE", struct.unpack("<h", data[offset:offset + 2])[0], offset, 2, "<h")
    offset += 2
    add_iter(model, "yE", struct.unpack("<h", data[offset:offset + 2])[0], offset, 2, "<h")

def RectL(model, data, offset=8, i=""):
    add_iter(model, "xS" + i, struct.unpack("<i", data[offset:offset + 4])[0], offset, 4, "<i")
    offset += 4
    add_iter(model, "yS" + i, struct.unpack("<i", data[offset:offset + 4])[0], offset, 4, "<i")
    offset += 4
    add_iter(model, "xE" + i, struct.unpack("<i", data[offset:offset + 4])[0], offset, 4, "<i")
    offset += 4
    add_iter(model, "yE" + i, struct.unpack("<i", data[offset:offset + 4])[0], offset, 4, "<i")

def RectF(model, data, offset, i=""):
    PointF(model, data, offset, i)
    add_iter(model, "%sWidth" % i, struct.unpack("<f", data[offset + 8:offset + 12])[0], offset + 8, 4, "<f")
    add_iter(model, "%sHeight" % i, struct.unpack("<f", data[offset + 12:offset + 16])[0], offset + 12, 4, "<f")

def RectS(model, data, offset, i=""):
    PointS(model, data, offset, i)
    add_iter(model, "%sWidth" % i, struct.unpack("<h", data[offset + 4:offset + 6])[0], offset + 4, 2, "<h")
    add_iter(model, "%sHeight" % i, struct.unpack("<h", data[offset + 6:offset + 8])[0], offset + 6, 2, "<h")


def make_tree():
    # Record/Group Name, Rec.Type, Min.Length String, Template
    model = Gtk.TreeStore(GObject.TYPE_STRING, GObject.TYPE_INT, GObject.TYPE_STRING, GObject.TYPE_PYOBJECT)
    view = Gtk.TreeView(model)
    cell = Gtk.CellRendererText()
    col = Gtk.TreeViewColumn('Group/Record', cell, text=0)
    view.append_column(col)
    col = Gtk.TreeViewColumn('Length', cell, text=2)
    view.append_column(col)
    return model, view

def on_row_activated(tv, path, tvc, nb, wname):
    pn = nb.get_current_page()
    if pn < 0:
        return
    rname, rid, rec_len, data = tv.get_model()[path]
    if not rec_len:
        # has to be 'group title'
        if tv.row_expanded(path):
            tv.collapse_row(path)
        else:
            tv.expand_row(path, False)
        return
    rlen = int(rec_len)
    page = nb.get_nth_page(pn)
    treeSel = page.main_tview.get_selection()
    _, parent = treeSel.get_selected_rows()
    if parent:
        parent = parent[0]
    root = page.model.get_iter("0")

    if wname == "add_record_wmf":
        if not data:
            data = struct.pack("<I", rlen // 2) + struct.pack("<H", rid)
        if len(data) < rlen:
            data += b"\x00" * (rlen - len(data))
        if not parent or str(parent) == "0":
            addedrec = page.pgi_add(parent=root, name=rname, type="wmf/%s" % rname, data=data)
        else:
            addedrec = page.pgi_ins(sibling=page.model.get_iter(parent), name=rname, type="wmf/%s" % rname, data=data)
        page.main_tview.set_cursor(page.model[addedrec].path, None, False)
    elif wname == "add_record_emf":
        if not data:
            data = struct.pack("<I", rid) + struct.pack("<I", rlen)
        if len(data) < rlen:
            data += b"\x00" * (rlen - len(data))
        if not parent or str(parent) == "0":
            addedrec = page.pgi_add(parent=root, name=rname, type="emf%s/%s" % ("plus" if rid > 0x4000 else "", rname), data=data)
        else:
            addedrec = page.pgi_ins(sibling=page.model.get_iter(parent), name=rname, type="emf%s/%s" % ("plus" if rid > 0x4000 else "", rname), data=data)
        page.main_tview.set_cursor(page.model[addedrec].path, None, False)

def add_groups(model, rec_groups, ids):
    for rgname, rg in rec_groups:
        parent = model.append(None, [rgname, -1, "", None])
        for i in range(len(rg)):
            data = rg[i][2] if len(rg[i]) > 2 else None
            model.append(parent, [ids[rg[i][0]], rg[i][0], "%d" % rg[i][1], data])

def emf_gentree ():
    model, view = make_tree()

    # Bitmap Record Types
    bmprec = ((0x4c, 100), (0x4d, 108), (0x4e, 132), (0x4f, 140), (0x50, 76), (0x51, 80), (0x72, 112), (0x74, 108))
    # Clipping Record Types
    cliprec = ((0x1a, 16), (0x1c, 8), (0x1d, 24), (0x1e, 24), (0x43, 12), (0x4b, 16))
    # Comment and Control Record Types
    ctrlrec = ((0x46, 16), (0x1, 108), (0xE, 20))
    # Drawing Record Types
    drawrec= ((0x02, 28), (0x03, 28), (0x04, 28), (0x05, 28), (0x06, 28), (0x07, 32), (0x08, 28),
              (0x0F, 20), (0x29, 28), (0x2a, 24), (0x2b, 24), (0x2c, 32), (0x2d, 40), (0x2e, 40),
              (0x2f, 40), (0x35, 24), (0x36, 16), (0x37, 40), (0x38, 28), (0x3e, 24), (0x3f, 24),
              (0x40, 24), (0x47, 32), (0x48, 40), (0x4a, 28), (0x53, 36), (0x54, 36), (0x55, 28),
              (0x56, 28), (0x57, 28), (0x58, 28), (0x59, 28), (0x5a, 32), (0x5b, 32), (0x5c, 28),
              (0x60, 36), (0x61, 36), (0x6c, 52), (0x76, 36))
    # Escape Record Types
    escrec = ((0x69, 12), (0x6a, 12), (0x6e, 16))
    # Object Creation Record Types
    objcrec = ((0x5d, 32),  (0x5e, 32), (0x5f, 32), (0x7a, 20), (0x26, 28), (0x27, 24), (0x31, 12), (0x52, 16), (0x63, 12))
    # Object Manipulation Record Types
    objmrec = ((0x25, 12), (0x28, 12), (0x30, 12), (0x32, 20), (0x33, 16), (0x64, 12), (0x65, 12), (0x6f, 24))
    # OpenGL Record Types
    oglrec = ((0x66, 12), (0x67, 28))
    # Path Bracket Record Types
    pathrec = ((0x3b, 8), (0x3c, 8), (0x3d, 8), (0x41, 8), (0x42, 8), (0x44, 8))
    # State Record Types
    staterec = ((0x09, 16), (0x0a, 16), (0x0b, 16), (0x0c, 16), (0x0d, 16), (0x10, 12), (0x11, 12),
                (0x12, 12), (0x13, 12), (0x14, 12), (0x15, 12), (0x16, 12), (0x17, 32), (0x18, 12),
                (0x19, 12), (0x1b, 16), (0x1f, 24), (0x20, 24), (0x21, 8), (0x22, 12), (0x34, 8),
                (0x39, 12), (0x3a, 12), (0x49, 28), (0x62, 12), (0x68, 48), (0x6d, 16),
                (0x70, 20), (0x71, 20), (0x73, 12), (0x77, 20), (0x78, 16), (0x79, 24)
        )
    # Transform Record Types
    xformrec = ((0x23, 32), (0x24, 36))
    # EMF+ Record Templates
    plusrec = (
      ("Clipping Records", (
          # OffsetClip
          (0x4035, 0x14, b"\x35\x40\x00\x00\x14\x00\x00\x00\x08" + b"\x00" * 11),
          # ResetClip
          (0x4031, 0x0c, b"\x31\x40\x00\x00\x0c" + b"\x00" * 7),
          # SetClipPath
          (0x4033, 0x0c, b"\x33\x40\x00\x01\x0c" + b"\x00" * 7),
          # SetClipRect
          (0x4032, 0x1c, b"\x32\x40\x00\x00\x1c\x00\x00\x00\x10" + b"\x00" * 19),
          # SetClipRegion
          (0x4034, 0x0c, b"\x34\x40\x04\x00\x0c" + b"\x00" * 7),
        )),
      ("Comment Records", (
        # Comment
        (0x4003, 0xc, b"\x03\x40\x00\x00\x0c" + b"\x00" * 7),
        )),
      ("Control Records", (
          # Header
          (0x4001, 0x1c, b"\x01\x40\x01\x00\x1c\x00\x00\x00\x10\x00\x00\x00\x02\x10\xc0\xdb\x01\x00\x00\x00" + b"\x60\x00\x00\x00" * 2),
          # EOF
          (0x4002, 0x0c, b"\x02\x40\x00\x00\x0c" + b"\x00" * 7),
          # GetDC
          (0x4004, 0x0c, b"\x04\x40\x00\x00\x0c" + b"\x00" * 7),
        )),
      ("Drawing Records", (
          # Clear
          (0x4009, 0x10, b"\x09\x40\x00\x00\x10\x00\x00\x00\x04\x00\x00\x00\x00\x00\x00\x00"),
          # DrawArc
          (0x4012, 0x1c, b"\x12\x40\x00\x00\x1c\x00\x00\x00\x10" + b"\x00" * 19),
          # DrawBeziers
          (0x4019, 0x18, b"\x19\x40\x00\x00\x18\x00\x00\x00\x0c\x00\x00\x00\x04" + b"\x00" * 11),
          # DrawClosedCurve
          (0x4017, 0x1c, b"\x17\x40\x00\x00\x1c\x00\x00\x00\x10" + b"\x00" * 7 + b"\x03" + b"\x00" * 11),
          # DrawCurve
          (0x4018, 0x24, b"\x18\x40\x00\x00\x24\x00\x00\x00\x18" + b"\x00" * 15 + b"\x02" + b"\x00" * 11),
          # DrawDriverstring
          (0x4036, 0x5c, b"\x36\x40\x02\x80\x5c\x00\x00\x00\x50\x00\x00\x00\xff\xff\xff\xff" + b"\x01\x00\x00\x00" * 2\
            + b"\x04\x00\x00\x00\x54\x00\x45\x00\x53\x00\x54\x00" + (b"\x00" * 7 + b"\x3f" + b"\x00\x00\x80\x3f\x00\x00\xc0\x3f") * 2\
            + b"\x00\x00\x80\x3f" + b"\x00" * 10 + b"\x80\x3f" + b"\x00" * 8),
          # DrawEllipse
          (0x400f, 0x14, b"\x0f\x40\x00\x00\x14\x00\x00\x00\x08" + b"\x00" * 11),
          # DrawImage
          (0x401a, 0x2c, b"\x1a\x40\x00\x00\x2c\x00\x00\x00\x20" + b"\x00" * 35),
          # DrawImagePoints
          (0x401b, 0x30, b"\x1b\x40\x00\x00\x30\x00\x00\x00\x24" + b"\x00" * 27 + b"\x03" + b"\x00" * 11),
          # DrawLines
          (0x400d, 0x14, b"\x0d\x40\x00\x00\x14\x00\x00\x00\x08\x00\x00\x00\x02" + b"\x00" * 7),
          # DrawPath
          (0x4015, 0x10, b"\x15\x40\x00\x00\x10\x00\x00\x00\x04" + b"\x00" * 7),
          # DrawPie
          (0x4011, 0x1c, b"\x11\x40\x00\x00\x1c\x00\x00\x00\x10" + b"\x00" * 19),
          # DrawRects
          (0x400b, 0x18, b"\x0b\x40\x00\x00\x18\x00\x00\x00\x0c" + b"\x00\x00\x00\x01" * 2 + b"\x00" * 7),
          # DrawString
          (0x401c, 0x2a, b"\x1c\x40\x00\x00\x2a\x00\x00\x00\x1e" + b"\x00" * 33),
          # FillClosedCurve
          (0x4016, 0x20, b"\x16\x40\x00\x00\x20\x00\x00\x00\x14" + b"\x00" * 11 + b"\x03" + b"\x00" * 11),
          # FillEllipse
          (0x400e, 0x18, b"\x0e\x40\x00\x00\x18\x00\x00\x00\x0c" + b"\x00\x00\x00\x01" * 2 + b"\x00" * 7),
          # FillPath
          (0x4014, 0x10, b"\x14\x40\x00\x80\x10\x00\x00\x00\x04\x00\x00\x00\xff\xff\xff\x00"),
          # FillPie
          (0x4010, 0x20, b"\x10\x40\x00\x00\x20\x00\x00\x00\x14\x00\x00\x00\x01" + b"\x00" * 19),
          # FillPolygon
          (0x400c, 0x24, b"\x0c\x40\x00\x00\x1c\x00\x00\x00\x10\x00\x00\x00\x01\x00\x00\x00\x03\x00\x00\x00" + b"\x00" * 16),
          # FillRects
          (0x400a, 0x24, b"\x0a\x40\x00\x00\x24\x00\x00\x00\x18" + b"\x00\x00\x00\x01" * 2 + b"\x00" * 13 + b"\x20\x41\x00\x00\x80\x40"),
          # FillRegion
          (0x4013, 0x10, b"\x13\x40\x00\x00\x10\x00\x00\x00\x04" + b"\x00" * 7),
          # StrokeFillPath (0x4037): Not documented in [MS-EMFPLUS]
        )),
      # MultiFormatStart (0x4005) reserved
      # MultiFormatSection (0x4006) reserved
      # MultiFormatEnd (0x4007) reserved
      ("Object Records", (
          # Objects (0x4008 and ObjectType enum)
            # 0: Invalid, # 1: Brush
            # 2: Pen
          (0x0002, 0x30, b"\x08\x40\x01\x02\x30\x00\x00\x00\x24\x00\x00\x00\x02\x10\xc0\xdb" + b"\x00" * 4 + b"\x80\x00\x00\x00\x02"\
                + b"\x00" * 5 + b"\x80\x3f" + b"\x00" * 4 + b"\x02\x10\xc0\xdb" + b"\x00" * 8),
            # 3: Path
          (0x0003, 0x28, b"\x08\x40\x00\x03\xc4\x00\x00\x00\xb8\x00\x00\x00\x02\x10\xc0\xdb\x04" + b"\x00" * 40 + b"\x01\x01\x81"),
            # 4: Region, # 5: Image, # 6: Font
          (0x0006, 0x30, b"\x08\x40\x02\x06\x30\x00\x00\x00\x24\x00\x00\x00\x02\x10\xc0\xdb\x00\x22\xbc\x3d" + b"\x00" * 12\
                + b"\x05\x00\x00\x00\x41\x00\x52\x00\x49\x00\x41\x00\x4c\x00\x00\x00"),
            # 7: StringFormat, # 8: ImageAttreibutes, # 9: CustomLineCap
          # SerializableObject
          (0x4038, 0x20, b"\x38\x40\x00\x00\x20\x00\x00\x00\x14" + b"\x00" * 23),
        )),
      ("Property Records", (
          # SetAntiAliasMode
          (0x401e, 0x0c, b"\x1e\x40\x0b\x00\x0c" + b"\x00" * 7),
          # SetCompositingMode
          (0x4023, 0x0c, b"\x23\x40\x00\x00\x0c" + b"\x00" * 7),
          # SetCompositingQuality
          (0x4024, 0x0c, b"\x24\x40\x02\x00\x0c" + b"\x00" * 7),
          # SetInterpolationMode
          (0x4021, 0x0c, b"\x21\x40\x07\x00\x0c" + b"\x00" * 7),
          # SetPixelOffsetMode
          (0x4022, 0x0c, b"\x22\x40\x03\x00\x0c" + b"\x00" * 7),
          # SetRenderingOrigin
          (0x401d, 0x14, b"\x1d\x40\x00\x00\x14\x00\x00\x00\x08" + b"\x00" * 11),
          # SetTextContrast
          (0x4020, 0x0c, b"\x20\x40\x00\x00\x0c" + b"\x00" * 7),
          # SetTextRenderingHint
          (0x401f, 0x0c, b"\x1f\x40\x05\x00\x0c" + b"\x00" * 7),
        )),
      ("State Records", (
          # BeginContainer
          (0x4027, 0x30, b"\x27\x40\x00\x00\x30\x00\x00\x00\x24" + b"\x00" * 39),
          # BeginContainerNoParams
          (0x4028, 0x10, b"\x28\x40\x00\x00\x10\x00\x00\x00\x04" + b"\x00" * 7),
          # EndContainer
          (0x4029, 0x10, b"\x29\x40\x00\x00\x10\x00\x00\x00\x04" + b"\x00" * 7),
          # Restore
          (0x4026, 0x10, b"\x26\x40\x00\x00\x10\x00\x00\x00\x04" + b"\x00" * 7),
          # Save
          (0x4025, 0x10, b"\x25\x40\x00\x00\x10\x00\x00\x00\x04" + b"\x00" * 7),
        )),
      ("Terminal Server Records", (
          # SetTSClip
          (0x403a, 0x0c, b"\x3a\x40\x00\x00\x0c" + b"\x00" * 7),
          # SetTSGraphics
          (0x4039, 0x30, b"\x39\x40\x00\x00\x30\x00\x00\x00\x24" + b"\x00" * 39),
        )),
      ("Transform Records", (
          # MultiplyWorldTransform
          (0x402c, 0x24, b"\x2c\x40\x00\x00\x24\x00\x00\x00\x18" + b"\x00" * 5 + b"x80\x3f" + b"\x00" * 10 + b"\x80\x3f" + b"\x00" * 8),
          # ResetWorldTransform
          (0x402b, 0x0c, b"\x2b\x40\x00\x00\x0c" + b"\x00" * 7),
          # RotateWorldTransform
          (0x402f, 0x10, b"\x2f\x40\x00\x00\x10\x00\x00\x00\x04" + b"\x00" * 7),
          # ScaleWorldTransform
          (0x402e, 0x14, b"\x2e\x40\x00\x00\x14\x00\x00\x00\x08" + b"\x00" * 11),
          # SetPageTransform
          (0x4030, 0x10, b"\x30\x40\x02\x00\x10\x00\x00\x00\x04" + b"\x00" * 5 + b"\x80\x3f"),
          # SetWorldTransform
          (0x402a, 0x24, b"\x2a\x40\x00\x00\x24\x00\x00\x00\x18" + b"\x00" * 5 + b"x80\x3f" + b"\x00" * 10 + b"\x80\x3f" + b"\x00" * 8),
          # TranslateWorldTransform
          (0x402d, 0x14, b"\x2d\x40\x00\x00\x14\x00\x00\x00\x08" + b"\x00" * 11),
        )),
    )
    emfplus_ObjectType = {0: "Invalid", 1: "Brush", 2: "Pen", 3: "Path", 4: "Region", 5: "Image", 6: "Font", 7: "StringFormat",
              8: "ImageAttributes", 9: "CustomLineCap"}

    parent = model.append(None, ["EMF+ Records", -1, "", None])
    for title, precs in plusrec:
        cparent = model.append(parent, [title, -1, "", None])
        for i in range(len(precs)):
            if precs[i][0] > 0x4000:
                model.append(cparent, [emrplus_ids[precs[i][0]], precs[i][0], "%d" % precs[i][1], precs[i][2]])
            else:
                model.append(cparent, ["Obj%s" % emfplus_ObjectType[precs[i][0]], 0x4008, "%d" % precs[i][1], precs[i][2]])

    rec_groups = [
        ("Bitmap Records", bmprec),
        ("Clipping Records", cliprec),
        ("Comment & Control Records", ctrlrec),
        ("Drawing Records", drawrec),
        ("Escape Records", escrec),
        ("Object Creation Records", objcrec),
        ("Object Modification Records", objmrec),
        ("OpenGL Records", oglrec),
        ("Path Bracket Records", pathrec),
        ("State Records", staterec),
        ("Transform Records", xformrec),
        ]

    add_groups(model, rec_groups, emr_ids)
    return model, view


def wmf_gentree ():
    model, view = make_tree()

    # Bitmap Records
    bmprec = (
        (0x0922, 24), # BITBLT
        (0x0940, 24), # DIBBITBLT
        (0x0b41, 28), # DIBSTRETCHBLT
        (0x0d33, 24), # SETDIBTODEV
        (0x0B23, 28), # STRETCHBLT
        (0x0f43, 28), # STRETCHDIB
    )
    # Control Records
    ctrlrec = (
        (0, 6), # EOF
        (1, 18, b"\x01\x00\x09\x00\x00\x03\x0c\x00\x00\x00\x01\x00\x0c"),
        (2, 18), # FIXME!
        (3, 18), # FIXME!
    )
    # Drawing Record Types
    #
    drawrec = (
        (0x817, 22), # Arc
        (0x830, 22), # Chord
        (0x418, 14), # Ellipse
        (0x548, 16), # EXTFLOODFILL
        (0xa32, 22), # ExtTextOut
        (0x228, 10), # FILLREGION
        (0x419, 14), # FLOODFILL
        (0x429, 14), # FRAMEREGION
        (0x12A, 8), # INVERTREGION
        (0x213, 10), # LineTo
        (0x12B, 8), # PAINTREGION
        (0x61D, 18), # PATBLT
        (0x81a, 22), # Pie
        (0x325, 8), # POLYLINE
        (0x324, 8), # POLYGON
        (0x538, 8), # POLYPOLYGON
        (0x41b, 14), # Rectangle
        (0x61c, 18), # RoundRect
        (0x41F, 14), # SETPIXEL
        (0x521, 20), # TEXTOUT
    )
    # Object Records
    objcrec = (
        (0x2FC, 14), # CREATEBRUSHINDIRECT
        (0x2FB, 8), # CREATEFONTINDIRECT
        (0x0f7, 8), # CREATEPALETTE
        (0x1F9, 42), # CREATEPATTERNBRUSH
        (0x2FA, 16), # CREATEPENINDIRECT
        (0x6FF, 8), # CREATEREGION
        (0x1f0, 8), # DELETEOBJECT
        (0x142, 10), # DIBCREATEPATTERNBRUSH
        (0x12C, 8), # SELECTCLIPREGION
        (0x12D, 8), # SELECTOBJECT
        (0x234, 8), # SELECTPALETTE
    )
    # Object Manipulation Record Types

    #objmrec = (0x25, 0x28, 0x30, 0x32, 0x33, 0x64, 0x65, 0x6f)
    #(0x5d, 32), (0x5e, 32), (0x5f, 32), (0x7a, 20), (0x26, 28), (0x27, 24), (0x31, 12), (0x52, 16), (0x63, 12)

    # State Record Types
    staterec = (
        (0x436, 8), # ANIMATEPALETTE
        (0x415, 14), #EXCLUDECLIPRECT
        (0x416, 14), # INTERSECTCLIPRECT
        (0x214, 10), # MOVETO
        (0x220, 10), # OFFSETCLIPRGN
        (0x211, 10), # OFFSETVIEWPORTORG
        (0x20F, 10), # OFFSETWINDOWORG
        (0x035, 6), # REALIZEPALETTE
        (0x139, 8), # RESIZEPALETTE
        (0x127, 8), # RESTOREDC
        (0x01E, 6), # SAVEDC
        (0x412, 14), # SCALEVIEWPORTEXT
        (0x410, 14), # SCALEWINDOWEXT
        (0x201, 10), # SETBKCOLOR
        (0x102, 10), # SETBKMODE
        (0x149, 10), # SETLAYOUT
        (0x103, 8), # SETMAPMODE
        (0x231, 10), # SETMAPPERFLAGS
        (0x037, 8), # SETPALENTRIES
        (0x106, 10), # SETPOLYFILLMODE
        (0x105, 6), # SETRELABS
        (0x104, 10), # SETROP2
        (0x107, 10), # SETSTRETCHBLTMODE
        (0x12E, 10), # SETTEXTALIGN
        (0x108, 8), # SETTEXTCHAREXTRA
        (0x209, 10), # SETTEXTCOLOR
        (0x20A, 10), # SETTEXTJUSTIFICATION
        (0x20E, 10), # SETVIEWPORTEXT
        (0x20D, 10), # SETVIEWPORTORG
        (0x20C, 10), # SETWINDOWEXT
        (0x20B, 10), # SETWINDOWORG
    )

    rec_groups = [
        ("Bitmap Records", bmprec),
        ("Control Records", ctrlrec),
        ("Drawing Records", drawrec),
        ("Object Records", objcrec),
        ("State Records", staterec),
        ]

    add_groups(model, rec_groups, wmr_ids)
    return model, view


def parse_gdiplus (data, offset, page, parent):
    try:
        eprid = struct.unpack('<H', data[offset:offset + 2])[0]
        eprlen = struct.unpack('<I', data[offset + 4:offset + 8])[0]
        eprname = "%02x" % eprid
        if eprid in emrplus_ids:
            eprname = emrplus_ids[eprid]

        page.pgi_add(parent=parent, name=eprname, type="emfplus/%s" % eprname, data=data[offset:offset + eprlen])

        return eprlen
    except:
        print("Oops")
        return 4


def parse(page, parent, data, kw):
    offset = page.kwargs["offset"]
    page.main_tview.freeze_child_notify()
    cnt = 0
    if page.kwargs["type"] == 'EMF':
        while offset < len(data) - 8:
            newT = struct.unpack('<I', data[offset:offset + 4])[0]
            newL = struct.unpack('<I', data[offset + 4:offset + 8])[0]
            newV = data[offset:offset + newL]
            rname = emr_ids[newT] if newT in emr_ids else "%#x" % newT
            if newT:
                iter1 = page.pgi_add(parent=parent, name=rname, type="emf/%s" % rname, data=newV)
                if newT == 0x46: # GDIComment
                    eptype = data[offset + 0xc:offset + 0x10]
                    if eptype == b"\x45\x4d\x46\x2b":  # EMF+
                        # eplen = struct.unpack("<I", data[offset + 0x14:offset + 0x18])[0]
                        i = 0
                        while i < newL - 16:
                            i += parse_gdiplus (data, offset + 0x10 + i, page, iter1)
            offset += newL
            if newL == 0:
                offset += 8
            cnt += 1
            if not cnt % 100:
                pb = page.kwargs.get("progressbar")
                if not pb:
                    pb = page.add_progressbar()
                pb.set_fraction((offset + 1) / (len(data) + 1))
                page.main_tview.thaw_child_notify()
                page.kwargs["offset"] = offset
                return True
        page.on_load_complete()
        return False

    elif page.kwargs["type"] == 'WMF':
        if offset == 0:
            if data[:4] == b"\xd7\xcd\xc6\x9a":
                page.pgi_add(parent=parent, name="AP Header", type="wmf/Aldus_Header", data=data[:22])
                offset += 22

            page.pgi_add(parent=parent, name="WMF Header", type="wmf/Header", data=data[offset:offset + 18])
            offset += 18

        while offset < len(data) - 5:
            [newL] = struct.unpack('<I', data[offset:offset + 4])
            [newT] = struct.unpack('<H', data[offset + 4:offset + 6])
            newV = data[offset:offset + newL * 2]
            rname = wmr_ids[newT] if newT in wmr_ids else "%#x" % newT
            page.pgi_add(parent=parent, name=rname, type="wmf/%s" % rname, data=newV)
            offset = offset + newL * 2
            if rname == 'Unknown':
                nlen = len(buf) - offset
                nval = data[offset:]
                page.pgi_add(parent=parent, name="Leftover", type="wmf/-1", data=nval)
                offset += nlen
            if rname == "EOF":
                break

            cnt += 1
            if not cnt % 100:
                pb = page.kwargs.get("progressbar")
                if not pb:
                    pb = page.add_progressbar()
                pb.set_fraction((offset + 1) / (len(data) + 1))
                page.kwargs["offset"] = offset
                page.main_tview.thaw_child_notify()
                return True
        if offset < len(data):
            nlen = len(data) - offset
            page.pgi_add(parent=parent, name="After EOF", ype="wmf/-1", data=data[offset:])
        page.on_load_complete()
        return False


def dump_mf_tree (fname, model, path):
    with open(fname, "wb") as f:
        root = model.get_iter(path)
        ch = model.iter_children(root)
        while ch:
            if model[ch][0] == "GDIComment" and model.iter_n_children(ch):
                gch = model.iter_children(ch)
                data = b""
                while gch:
                    data += model[gch][3]
                    gch = model.iter_next(gch)
                data = model[ch][3][:4] + struct.pack("<II", len(data) + 16, len(data) + 4) + model[ch][3][12:16] + data
                f.write(data)
            else:
                f.write(model[ch][3])
            ch = model.iter_next(ch)

# END
