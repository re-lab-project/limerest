# (c) frob et all 2007-2021
# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import struct

from utils import *
from utils import debug

psd_color_modes = {
    0: "Bitmap",
    1: "Grayscale",
    2: "Indexed",
    3: "RGB",
    4: "CMYK",
    7: "Multichannel",
    8: "Duotone",
    9: "Lab"
    }

psd_image_resource_ids = {
    # 0x03E8	(Obsolete--Photoshop 2.0 only ) Contains five 2-byte values: number of channels, rows, columns, depth, and mode
    # 0x03E9	Macintosh print manager print info record
    # 0x03EA	Macintosh page format information. No longer read by Photoshop. (Obsolete)
    # 0x03EB	(Obsolete--Photoshop 2.0 only ) Indexed color table
    # 0x03ED	ResolutionInfo structure. See Appendix A in Photoshop API Guide.pdf.
    # 0x03EE	Names of the alpha channels as a series of Pascal strings.
    # 0x03EF	(Obsolete) See ID 1077DisplayInfo structure. See Appendix A in Photoshop API Guide.pdf.
    # 0x03F0	The caption as a Pascal string.
    # 0x03F1	Border information. Contains a fixed number (2 bytes real, 2 bytes fraction) for the border width, and 2 bytes for border units (1 = inches, 2 = cm, 3 = points, 4 = picas, 5 = columns).
    # 0x03F2	Background color. See See Color structure.
    # 0x03F3	Print flags. A series of one-byte boolean values (see Page Setup dialog): labels, crop marks, color bars, registration marks, negative, flip, interpolate, caption, print flags.
    # 0x03F4	Grayscale and multichannel halftoning information
    # 0x03F5	Color halftoning information
    # 0x03F6	Duotone halftoning information
    # 0x03F7	Grayscale and multichannel transfer function
    # 0x03F8	Color transfer functions
    # 0x03F9	Duotone transfer functions
    # 0x03FA	Duotone image information
    # 0x03FB	Two bytes for the effective black and white values for the dot range
    # 0x03FC	(Obsolete)
    # 0x03FD	EPS options
    # 0x03FE	Quick Mask information. 2 bytes containing Quick Mask channel ID; 1- byte boolean indicating whether the mask was initially empty.
    # 0x03FF	(Obsolete)
    # 0x0400	Layer state information. 2 bytes containing the index of target layer (0 = bottom layer).
    # 0x0401	Working path (not saved). See See Path resource format.
    # 0x0402	Layers group information. 2 bytes per layer containing a group ID for the dragging groups. Layers in a group have the same group ID.
    # 0x0403	(Obsolete)
    # 0x0404	IPTC-NAA record. Contains the File Info... information. See the documentation in the IPTC folder of the Documentation folder.
    # 0x0405	Image mode for raw format files
    # 0x0406	JPEG quality. Private.
    # 0x0408	(Photoshop 4.0) Grid and guides information. See See Grid and guides resource format.
    # 0x0409	(Photoshop 4.0) Thumbnail resource for Photoshop 4.0 only. See See Thumbnail resource format.
    # 0x040A	(Photoshop 4.0) Copyright flag. Boolean indicating whether image is copyrighted. Can be set via Property suite or by user in File Info...
    # 0x040B	(Photoshop 4.0) URL. Handle of a text string with uniform resource locator. Can be set via Property suite or by user in File Info...
    # 0x040C	(Photoshop 5.0) Thumbnail resource (supersedes resource 1033). See See Thumbnail resource format.
    # 0x040D	(Photoshop 5.0) Global Angle. 4 bytes that contain an integer between 0 and 359, which is the global lighting angle for effects layer. If not present, assumed to be 30.
    # 0x040E	(Obsolete) See ID 1073 below. (Photoshop 5.0) Color samplers resource. See See Color samplers resource format.
    # 0x040F	(Photoshop 5.0) ICC Profile. The raw bytes of an ICC (International Color Consortium) format profile. See ICC1v42_2006-05.pdf in the Documentation folder and icProfileHeader.h in Sample Code\Common\Includes .
    # 0x0410	(Photoshop 5.0) Watermark. One byte.
    # 0x0411	(Photoshop 5.0) ICC Untagged Profile. 1 byte that disables any assumed profile handling when opening the file. 1 = intentionally untagged.
    # 0x0412	(Photoshop 5.0) Effects visible. 1-byte global flag to show/hide all the effects layer. Only present when they are hidden.
    # 0x0413	(Photoshop 5.0) Spot Halftone. 4 bytes for version, 4 bytes for length, and the variable length data.
    # 0x0414	(Photoshop 5.0) Document-specific IDs seed number. 4 bytes: Base value, starting at which layer IDs will be generated (or a greater value if existing IDs already exceed it). Its purpose is to avoid the case where we add layers, flatten, save, open, and then add more layers that end up with the same IDs as the first set.
    # 0x0415	(Photoshop 5.0) Unicode Alpha Names. Unicode string
    # 0x0416	(Photoshop 6.0) Indexed Color Table Count. 2 bytes for the number of colors in table that are actually defined
    # 0x0417	(Photoshop 6.0) Transparency Index. 2 bytes for the index of transparent color, if any.
    # 0x0419	(Photoshop 6.0) Global Altitude. 4 byte entry for altitude
    # 0x041A	(Photoshop 6.0) Slices. See See Slices resource format.
    # 0x041B	(Photoshop 6.0) Workflow URL. Unicode string
    # 0x041C	(Photoshop 6.0) Jump To XPEP. 2 bytes major version, 2 bytes minor version, 4 bytes count. Following is repeated for count: 4 bytes block size, 4 bytes key, if key = 'jtDd' , then next is a Boolean for the dirty flag; otherwise it's a 4 byte entry for the mod date.
    # 0x041D	(Photoshop 6.0) Alpha Identifiers. 4 bytes of length, followed by 4 bytes each for every alpha identifier.
    # 0x041E	(Photoshop 6.0) URL List. 4 byte count of URLs, followed by 4 byte long, 4 byte ID, and Unicode string for each count.
    # 0x0421	(Photoshop 6.0) Version Info. 4 bytes version, 1 byte hasRealMergedData , Unicode string: writer name, Unicode string: reader name, 4 bytes file version.
    # 0x0422	(Photoshop 7.0) EXIF data 1. See http://www.kodak.com/global/plugins/acrobat/en/service/digCam/exifStandard2.pdf
    # 0x0423	(Photoshop 7.0) EXIF data 3. See http://www.kodak.com/global/plugins/acrobat/en/service/digCam/exifStandard2.pdf
    # 0x0424	(Photoshop 7.0) XMP metadata. File info as XML description. See http://www.adobe.com/devnet/xmp/
    # 0x0425	(Photoshop 7.0) Caption digest. 16 bytes: RSA Data Security, MD5 message-digest algorithm
    # 0x0426	(Photoshop 7.0) Print scale. 2 bytes style (0 = centered, 1 = size to fit, 2 = user defined). 4 bytes x location (floating point). 4 bytes y location (floating point). 4 bytes scale (floating point)
    # 0x0428	(Photoshop CS) Pixel Aspect Ratio. 4 bytes (version = 1 or 2), 8 bytes double, x / y of a pixel. Version 2, attempting to correct values for NTSC and PAL, previously off by a factor of approx. 5%.
    # 0x0429	(Photoshop CS) Layer Comps. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure)
    # 0x042A	(Photoshop CS) Alternate Duotone Colors. 2 bytes (version = 1), 2 bytes count, following is repeated for each count: [ Color: 2 bytes for space followed by 4 * 2 byte color component ], following this is another 2 byte count, usually 256, followed by Lab colors one byte each for L, a, b. This resource is not read or used by Photoshop.
    # 0x042B	(Photoshop CS)Alternate Spot Colors. 2 bytes (version = 1), 2 bytes channel count, following is repeated for each count: 4 bytes channel ID, Color: 2 bytes for space followed by 4 * 2 byte color component. This resource is not read or used by Photoshop.
    # 0x042D	(Photoshop CS2) Layer Selection ID(s). 2 bytes count, following is repeated for each count: 4 bytes layer ID
    # 0x042E	(Photoshop CS2) HDR Toning information
    # 0x042F	(Photoshop CS2) Print info
    # 0x0430	(Photoshop CS2) Layer Group(s) Enabled ID. 1 byte for each layer in the document, repeated by length of the resource. NOTE: Layer groups have start and end markers
    # 0x0431	(Photoshop CS3) Color samplers resource. Also see ID 1038 for old format. See See Color samplers resource format.
    # 0x0432	(Photoshop CS3) Measurement Scale. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure)
    # 0x0433	(Photoshop CS3) Timeline Information. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure)
    # 0x0434	(Photoshop CS3) Sheet Disclosure. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure)
    # 0x0435	(Photoshop CS3) DisplayInfo structure to support floating point clors. Also see ID 1007. See Appendix A in Photoshop API Guide.pdf .
    # 0x0436	(Photoshop CS3) Onion Skins. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure)
    # 0x0438	(Photoshop CS4) Count Information. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure) Information about the count in the document. See the Count Tool.
    # 0x043A	(Photoshop CS5) Print Information. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure) Information about the current print settings in the document. The color management options.
    # 0x043B	(Photoshop CS5) Print Style. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure) Information about the current print style in the document. The printing marks, labels, ornaments, etc.
    # 0x043C	(Photoshop CS5) Macintosh NSPrintInfo. Variable OS specific info for Macintosh. NSPrintInfo. It is recommened that you do not interpret or use this data.
    # 0x043D	(Photoshop CS5) Windows DEVMODE. Variable OS specific info for Windows. DEVMODE. It is recommened that you do not interpret or use this data.
    # 0x043E	(Photoshop CS6) Auto Save File Path. Unicode string. It is recommened that you do not interpret or use this data.
    # 0x043F	(Photoshop CS6) Auto Save Format. Unicode string. It is recommened that you do not interpret or use this data.
    # 0x0440	(Photoshop CC) Path Selection State. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure) Information about the current path selection state.
    # 0x07D0-0x0BB6	Path Information (saved paths). See See Path resource format.
    # 0x0BB7	Name of clipping path. See See Path resource format.
    # 0x0BB8	(Photoshop CC) Origin Path Info. 4 bytes (descriptor version = 16), Descriptor (see See Descriptor structure) Information about the origin path data.
    # 0x0FA0-0x1387	Plug-In resource(s). Resources added by a plug-in. See the plug-in API found in the SDK documentation
    # 0x1B58	Image Ready variables. XML representation of variables definition
    # 0x1B59	Image Ready data sets
    # 0x1B5A	Image Ready default selected state
    # 0x1B5B	Image Ready 7 rollover expanded state
    # 0x1B5C	Image Ready rollover expanded state
    # 0x1B5D	Image Ready save layer settings
    # 0x1B5E	Image Ready version
    # 0x1F40	(Photoshop CS3) Lightroom workflow, if present the document is in the middle of a Lightroom workflow.
    # 0x2710	Print flags information. 2 bytes version ( = 1), 1 byte center crop marks, 1 byte ( = 0), 4 bytes bleed width value, 2 bytes bleed width scale.
    }

psd_blend_mode_key = {
    'pass': "pass through",
    'norm': "normal",
    'diss': "dissolve",
    'dark': "darken",
    'mul ': "multiply",
    'idiv': "color burn",
    'lbrn': "linear burn",
    'dkCl': "darker color",
    'lite': "lighten",
    'scrn': "screen",
    'div ': "color dodge",
    'lddg': "linear dodge",
    'lgCl': "lighter color",
    'over': "overlay",
    'sLit': "soft light",
    'hLit': "hard light",
    'vLit': "vivid light",
    'lLit': "linear light",
    'pLit': "pin light",
    'hMix': "hard mix",
    'diff': "difference",
    'smud': "exclusion",
    'fsub': "subtract",
    'fdiv': "divide",
    'hue ': "hue",
    'sat ': "saturation",
    'colr': "color",
    'lum ': "luminosity",
    }


# Unicode string consist of:
# A 4-byte length field, representing the number of UTF-16 code units in the string (not bytes).
# The string of Unicode values, two bytes per character and a two byte null for the end of the string.

def header(page, model, options, data):
    '''
    PSD Header

    Length/Data
    4     Signature: always equal to '8BPS' . Do not try to read the file if the signature does not match this value.
    2     Version: always equal to 1. Do not try to read the file if the version does not match this value. (**PSB** version is 2.)
    6     Reserved: must be zero.
    2     The number of channels in the image, including any alpha channels. Supported range is 1 to 56.
    4     The height of the image in pixels. Supported range is 1 to 30,000.  (**PSB** max of 300,000.)
    4     The width of the image in pixels. Supported range is 1 to 30,000.   (*PSB** max of 300,000)
    2     Depth: the number of bits per channel. Supported values are 1, 8, 16 and 32.
    2     The color mode of the file. (psd_color_modes)
    '''

    add_iter(model, 'Signature', data[:4], 0, 4, "4b")
    off = 4
    ver = struct.unpack(">H", data[off:off + 2])[0]
    add_iter(model, 'Version', ver, off, 2, ">H")
    off += 2
    add_iter(model, 'Reserved', None, off, 6, "6b")
    off += 6
    numch = struct.unpack(">H", data[off:off + 2])[0]
    add_iter(model, '#ofChannels', numch, off, 2, ">H")
    off += 2
    h = struct.unpack(">I", data[off:off + 4])[0]
    add_iter(model, 'ImgHeight', h, off, 4, ">I")
    off += 4
    w = struct.unpack(">I", data[off:off + 4])[0]
    add_iter(model, 'ImgWidth', w, off, 4, ">I")
    off += 4
    depth = struct.unpack(">H", data[off:off + 2])[0]
    add_iter(model, 'Depth', depth, off, 2, ">H")
    off += 2
    clrmode = struct.unpack(">H", data[off:off + 2])[0]
    if clrmode in psd_color_modes:
        clrmode = psd_color_modes[clrmode]
    add_iter(model, 'ColorMode', clrmode, off, 2, ">H")
    off += 2


def lrec(page, model, options, data):
    # 4*4   TLBR rect for layer content
    # 2     num of channels in the layer
    # 6 * num_of_chan
    #       channel info:
    #           2 bytes for Channel ID: 0 = red, 1 = green, etc.;
    #           -1 = transparency mask; -2 = user supplied layer mask, -3 real user supplied layer mask
    #               (when both a user mask and a vector mask are present)
    #           4 bytes for length of corresponding channel data. (**PSB** 8 bytes for length of corresponding channel data.)
    # 4     "8BIM" (blend mode signature)
    # 4     blend mode key
    # 1     opacity: 0 xparent, 255 opaque
    # 1     clipping: 0 base, 1 non-base
    # 1     flags: &1 - xparency protected
    #               &2 - visible
    #               &3 - 1 for Photoshop 5.0 and later, tells if bit 4 has useful information
    #               &4 - pixel data irrelevant to appearance of document
    # 1     filter (zero)
    # 4     length of extra data
    # Extras:
    #   layer mask data
    #   layer blending ranges
    #   layer name
    t, l, b, r = struct.unpack(">iiii", data[:16])
    add_iter(model, 'Top', t, 0, 4, ">i")
    add_iter(model, 'Left', l, 4, 4, ">i")
    add_iter(model, 'Bottom', b, 8, 4, ">i")
    add_iter(model, 'Right', r, 12, 4, ">i")
    off = 16
    num_ch = struct.unpack(">H", data[off:off + 2])[0]
    ch_iter = add_iter(model, 'Channels', "", off, 2 + num_ch * 6, "blob")
    off += 2
    for i in range(num_ch):
        ch_id = struct.unpack(">h", data[off:off + 2])[0]
        ch_len = struct.unpack(">I", data[off + 2:off + 6])[0]
        add_iter(model, 'Channel %s id/len' % i, "%s/%s" % (ch_id, ch_len), off, 6, ">hI", parent=ch_iter)
        off += 6
    off += 4 # no need for 8BIM sig
    blend_modkey = data[off:off + 4].decode("utf-8")
    if blend_modkey in psd_blend_mode_key:
        blend_modkey = psd_blend_mode_key[blend_modkey]
    add_iter(model, 'Blend mode', blend_modkey, off, 4, "str")
    off += 4
    add_iter(model, 'Opacity', data[off], off, 1, "B")
    off += 1
    add_iter(model, 'Clipping', data[off], off, 1, "B")
    off += 1
    add_iter(model, 'Flags', data[off], off, 1, "B")
    off += 2  # + filling zero
    xtra_len = struct.unpack(">I", data[off:off + 4])[0]
    add_iter(model, 'Len of extras', xtra_len, off, 4, ">I")
    off += 4
    end_off = off + xtra_len
    mask_len = struct.unpack(">I", data[off:off + 4])[0]
    mask_iter = add_iter(model, 'Layer Mask', "" if mask_len else "(empty)", off, 4 + mask_len, "blob")
    off += 4
    if mask_len:
        t, l, b, r = struct.unpack(">iiii", data[off :off + 16])
        add_iter(model, 'Top', t, off, 4, ">i", parent=mask_iter)
        add_iter(model, 'Left', l, off + 4, 4, ">i", parent=mask_iter)
        add_iter(model, 'Bottom', b, off + 8, 4, ">i", parent=mask_iter)
        add_iter(model, 'Right', r, off + 12, 4, ">i", parent=mask_iter)
        off += 16
        add_iter(model, 'Default Clr', data[off], off, 1, "B", parent=mask_iter)
        off += 1
        flags = data[off]
        add_iter(model, 'Flags', flags, off, 1, "B", parent=mask_iter)
        off += 1
        off += mask_len - 18  # FIXME!
    blend_len = struct.unpack(">I", data[off:off + 4])[0]
    blend_iter = add_iter(model, 'Blending Ranges', "" if blend_len else "(empty)", off, 4 + blend_len, "blob")
    off += 4
    for i in range(blend_len // 8):
        src = struct.unpack("BBBB", data[off:off + 4])
        add_iter(model, 'Chan %d source' % i, src, off, 4, "BBBB", parent=blend_iter)
        off += 4
        dst = struct.unpack("BBBB", data[off:off + 4])
        add_iter(model, 'Chan %d dest' % i, dst, off, 4, "BBBB", parent=blend_iter)
        off += 4
    name_len = data[off]
    off += 1
    name = data[off:off + name_len].decode("utf-8")
    add_iter(model, 'Layer name', name, off, name_len, "")
    off += name_len + (3 - name_len) % 4  # round-up len+str to 4 bytes
    if end_off > off:
        xtra_iter = add_iter(model, 'Extra', "", off, end_off - off, "blob")
        while off < end_off:
            sig = data[off:off + 4]
            off += 4
            key = data[off:off + 4].decode("utf-8")
            off += 4
            kdlen = struct.unpack(">I", data[off:off + 4])[0]
            off += 4
            kdlen += kdlen % 2
            add_iter(model, "Key: %s" % key, "", off - 8, 8 + kdlen, "blob", parent=xtra_iter)
            off += kdlen


def color_mode_data(page, parent, data, off, *args, **kwargs):
    # 4     length of the data (may be 0)
    # var   color data
    #
    # data depends on color mode
    #   indexed: 768 bytes
    #   duotone: duotone spec (not documented)
    #   others -- empty data, only length is presented
    dlen = struct.unpack(">I", data[off:off + 4])[0]
    dlen += 4 + dlen % 2
    page.pgi_add(parent=parent, name="ColorMode", type="psd/clr_mode", data=data[off:off + dlen])
    return dlen


def image_resource_block(page, parent, data, off, *args, **kwargs):
    # 4     signature "8BIM"
    # 2     UniqueID (psd_image_resource_ids)
    # var   Name (pascal string padded to even size (empty string --> 00 00)
    # 4     Actual size of following resource data
    # var   resource data (type defined by UniqueID), padded to even size
    uid = struct.unpack(">H", data[off + 4:off + 6])[0]
    name_len = data[off + 6]
    name = data[off + 7:off + 7 + name_len].decode("utf-8") if name_len else "no name"
    name_len += 1 + (name_len + 1) % 2
    data_len =  struct.unpack(">I", data[off + 6 + name_len:off + 10 + name_len])[0]
    dlen = 10 + name_len + data_len
    dlen += dlen % 2
    page.pgi_add(parent=parent, name="ImgResBlock %#02x (%s)" % (uid, name), type="psd/irb_%02x" % uid, data=data[off:off + dlen])
    return dlen


def image_resources_section(page, parent, data, off, *args, **kwargs):
    # 4     length (may be 0)
    # var   img resource data
    #
    # data are image resource blocks
    dlen = struct.unpack(">I", data[off:off + 4])[0]
    dlen += 4 + dlen % 2
    end_off = off + dlen
    ir_iter = page.pgi_add(parent=parent, name="ImgResources", type="psd/img_rsrc", data=data[off:end_off])
    off += 4
    while off < end_off:
        off += image_resource_block(page, ir_iter, data, off)

    return dlen


def layer_record(page, parent, data, off, layer_num):
    num_ch = struct.unpack(">H", data[off + 16:off + 18])[0]
    extras_len = struct.unpack(">I", data[off + 6 * num_ch + 30:off + 6 * num_ch + 34])[0]
    dlen = 6 * num_ch + 34 + extras_len
    t, l, b, r = struct.unpack(">iiii", data[off :off + 16])
    num_ch = struct.unpack(">H", data[off + 16:off + 18])[0]
    channel_info = []
    for i in range(num_ch):
        ch_id = struct.unpack(">h", data[off + 18 + i * 6:off + 20 + i * 6])[0]
        ch_len = struct.unpack(">I", data[off + 20 + i * 6:off + 24 + i * 6])[0]
        channel_info.append((ch_id, ch_len))

    page.doc_data["layers"][layer_num] = t, l, b, r, channel_info # this is needed for channel image info parsing
    page.pgi_add(parent=parent, name="LayerRec %s" % layer_num, type="psd/lrec", data=data[off:off + dlen])
    return dlen


def channel_image_data(page, parent, data, off, layer_num):
    t, l, b, r, channel_info = page.doc_data["layers"][layer_num]
    dlen = sum([x[1] for x in channel_info])
    ch_iter = page.pgi_add(parent=parent, name="Layer %s ChannelImgData" % layer_num, type="psd/chd", data=data[off:off + dlen])
    ch_off = 0
    for i in channel_info:
        page.pgi_add(parent=ch_iter, name="Channel %s" % i[0], type="psd/chimgd", data=data[off + ch_off:off + ch_off + i[1]])
        ch_off += i[1]
    return dlen


def layer_info(page, parent, data, off, *args, **kwargs):
    # 4     length
    # 2     layer count (negative if the 1st alpha channel is xparency data for the merged result)
    # var   info about each layer (layer records)
    # var   channel image data (1+ image data records)
    dlen = struct.unpack(">I", data[off:off + 4])[0]
    dlen += 4 + dlen % 2
    cnt = struct.unpack(">H", data[off + 4:off + 6])[0]
    li_iter = page.pgi_add(parent=parent, name="LayerInfo [#Ch: %s]" % cnt, type="psd/li", data=data[off:off + 6])
    # FIXME! need to pass it to the next level for parsing
    off += 6
    page.doc_data["layers"] = {}
    for i in range(cnt):
        off += layer_record(page, li_iter, data, off, i)
    for i in range(cnt):
        res = channel_image_data(page, li_iter, data, off, i)
        if res > 0:
            off += res
        else:
            break
    return dlen


def global_layer_mask_info(page, parent, data, off, *args, **kwargs):
    dlen = struct.unpack(">I", data[off:off + 4])[0]
    dlen += 4 + dlen % 2
    page.pgi_add(parent=parent, name="GlobalLayerMaskInfo", type="psd/glmi", data=data[off:off + dlen])
    return dlen


def additional_layer_info(page, parent, data, off, dend, *args, **kwargs):
    debug("ALI", off, dend)
    al_iter = page.pgi_add(parent=parent, name="AdditionalLayerInfo", type="psd/ali", data=data[off:dend])
    while off < dend:
        sig = data[off:off + 4]
        off += 4
        key = data[off:off + 4].decode("utf-8")
        off += 4
        kdlen = struct.unpack(">I", data[off:off + 4])[0]
        off += 4
        page.pgi_add(parent=al_iter, name="Key: %s" % key, type="psd/key", data=data[off:off + kdlen])
        off += kdlen
    return 0


def layer_and_mask_section(page, parent, data, off, *args, **kwargs):
    # 4     length
    # var   layer info
    # var   global layer mask info
    # var   additional layer info (tagged blocks)
    dlen = struct.unpack(">I", data[off:off + 4])[0]
    dlen += 4 + dlen % 2
    dend = off + dlen
    lms_iter = page.pgi_add(parent=parent, name="LayerMaskSection", type="psd/lms", data=data[off:off + dlen])
    off += 4
    off += layer_info(page, lms_iter, data, off)
    off += global_layer_mask_info(page, lms_iter, data, off)
    off += additional_layer_info(page, lms_iter, data, off, dend)

    return dlen


def image_data_section(page, parent, data, off, *args, **kwargs):
    # 2     compression
    # var   data
    compr = struct.unpack(">H", data[off:off + 2])[0]
    page.pgi_add(parent=parent, name="ImgData", type="psd/img_data", data=data[off:])


def parse(page, parent, data, *args, **kwargs):
    page.pgi_add(parent=parent, name="Header", type="psd/header", data=data[:26])
    off = 26
    psd_version = struct.unpack(">H", data[4:6])[0]
    # FIXME! pass psd_version around as ver2 uses 8 bytes for lengths
    off += color_mode_data(page, parent, data, off)
    off += image_resources_section(page, parent, data, off)
    off += layer_and_mask_section(page, parent, data, off)
    image_data_section(page, parent, data, off)

    page.main_tview.freeze_child_notify()
    page.on_load_complete()

# END
