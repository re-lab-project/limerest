# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.


import zipfile

import formats.utils
from utils import debug

def find_dir(page, cur_dirs, diter, fullname):
    spl = fullname.split("/")
    for pn in spl[:-1]:
        if pn not in cur_dirs:
            diter = page.pgi_add(parent=diter, name=pn, type="", data=b"")
            cur_dirs[pn] = {42:diter}
            cur_dirs = cur_dirs[pn]
        else:
            cur_dirs = cur_dirs[pn]
            diter = cur_dirs[42]
    return diter, spl[-1]

def parse(page, parent, data, kw):
    fname = page.path
    try:
        z = zipfile.ZipFile(fname, "r")
        dirs = {}
        for i in z.filelist:
            fn = i.filename
            zdata = z.read(fn)
            diter = parent
            cur_dirs = dirs
            diter, fname = find_dir(page, cur_dirs, diter, fn)
            if "wdata" in page.doc_data and fname in page.doc_data["wdata"]:
                continue
            if fn == "content/dataFileList.dat":
                page.doc_data["wtable"] = zdata.decode("utf-8").split("\n")
                if "wdata" not in page.doc_data:
                    page.doc_data["wdata"] = {}
                for n in page.doc_data["wtable"]:
                    if n in page.doc_data["wdata"]:
                        continue
                    wname = "content/data/%s" % n
                    wzdata = z.read(wname)
                    diter, _ = find_dir(page, cur_dirs, diter, wname)
                    itr = page.pgi_add(parent=diter, name="%s" % n, type="", data=wzdata)
                    page.doc_data["wdata"][n] = itr

            fmt = ""
            action = None
            if len(zdata) > 0:
                fmt, kw, action = formats.utils.detect(zdata, len(zdata))
                ver = kw and kw.get("version")
                if fmt and ver:
                    fmt = "%s:%s" % (fmt, ver)
            itr = page.pgi_add(parent=diter, name="%s" % fname, type=fmt, data=zdata)
            if fn[-4:] == ".dat" and fn[:13] == "content/data/":
                if "wdata" not in page.doc_data:
                    page.doc_data["wdata"] = {}
                page.doc_data["wdata"][fname] = itr
            if action:
                action(page, itr, zdata, kw)

    except zipfile.BadZipfile:
        print ("Open as PKZIP failed")
    except zipfile.LargeZipFile:
        print ("Open as PKZIP failed")
