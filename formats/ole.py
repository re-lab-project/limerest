#!/usr/bin/env python3
#
# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.


import re
import struct
import subprocess
import formats.vsd
from utils import debug

"""
signatures = [
    # most of the detection can be done from the 'filename' itself
    {fn:r"^DesignerDoc", type:"dsf"},
    {fn:r"^VisioDocument", type:"vsd"},
    {fn:r"^PageMaker", type:"pm"},
    {fn:r"^NativeContent_MAIN", type:"qpw"},
    {fn:r"^PowerPoint Document|Pictures", type:"ppt"},
    {fn:r"^Escher(Delay|)Stm", type:"escher"},
    {fn:r"^(Visio|Summary|DocumentSummary)Information", type:"ole/propset"},
    {fn:r"^(Workb|B)ook", type:"xls"},
    {fn:r".zmf$", type:'zmf'},
    # in some cases I have to set type for future check or only set type
    {fn:r"^WordDocument", type:"doc"}, # side effect of setting page.type to DOC
    {fn:r"^VBA", type:"vba", flags:"set_only"},
    # need to check type
    {fn:r"^dir", set_type:"vba", flags:"store:vba_data"}, # store iter to parse it later (need to get all other files first?)
    {fn:r"^Data", set_type:"doc", flags:"store:w_data"},
    {fn:r"^(0|1)Table", flags:"store:wtable"},
    {fn:r"^contents", set_type:"ppp", flags:"parse"},
    {fn:r"^SCFFPreview", set_type:"ppp", flags:"parse"},
    # have to check for data inside the file
    # signature for 'quill' is less specific and has to be checked after wt602 one
    {fn:r"^CONTENTS", data:r".{6}WT602", type:"wt602"},
    {fn:r"^CONTENTS", type:"quill"},
    # check for data seems to be optional as there is no competing formats
    {fn:r"^Contents", data:r"^\xe8\xac", type:"pub"},
    {fn:r"^Signature", data:r"^\x60\x67\x01\x00", set:"ppp"},  #PagePlus OLE version (9.x?)
    {fn:r"^Header", data:r"^.{12}xV4", type:"zmf"}
]
"""

def cdir_to_treeiter(page, parent, cdir, dircache):
    dirspl = cdir.split("/")
    pn = parent
    pnn = ""
    for fn in dirspl:
        jn = pnn+"/"+fn
        if not jn in dircache.keys():
            page.pgi_add(parent=pn, name=fn, type="OLE/%s" % jn, data=b"")
            dircache[jn] = pn
        pnn = jn

def detect(page, parent, data, kw):
    fname = page.path
    dircache = {}
    ftype = None

    try:
        gsfout = subprocess.check_output(["gsf", "list", fname])
    except:
        print("Failed to run gsf")
        return

    for lsr in gsfout.decode("utf-8").split("\n")[1:-1]:
        if lsr[0] != "f":
            continue
        fullname = lsr[35:]
        if "/" in fullname:
            fns = fullname.split("/")
            cdir = "/".join(fns[:-1])
            fn = fns[-1]
        else:
            fn = fullname
            cdir = ""
        if ord(fn[0]) < 32:
            fn = fn[1:]
        pn = parent
        if cdir:
            cdir_to_treeiter(page, parent, cdir, dircache)
            pn = dircache["/"+cdir]

        data = subprocess.check_output(["gsf", "cat", fname, fullname])
        if fn == "VisioDocument":
            ftype = "VSD"
            vsditer = page.pgi_add(parent=pn, name=fn, type="VSD", data=data)
            formats.vsd.parse(page, vsditer, data, kw)
        else:
            oleiter = page.pgi_add(parent=pn, name=fn, type="OLE", data=data)
