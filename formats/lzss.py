# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import struct

def lm318(data, padding=False):
    '''
    data -- something to compress
    offset -- current position in the processing data
    winoff -- offset in the data for the dictionary window
    winlen -- current size of the window, grows up to 4K
    flags -- byte with ctrl/literal flags; for now "1" is ctrl, 0 is literal, LSB first
    fcnt -- counter for bits in flags
    buf -- place to collect ctrl/literal bytes before flushing to result
    chunk -- 1 or 3 to 18 bytes piece to encode
    '''

    result = b''
    winoff, winlen = 0, 0
    flags = 0
    buf, chunk = b'', b''

    # first byte always goes as-is
    buf += data[0:1]
    fcnt = 1
    offset = 1
    winlen = 1

    while offset < len(data):
        chunk = data[offset:offset + 3]
        pos = data[winoff:winoff + max(winlen, offset + 2)].find(chunk)
        if pos == -1:
            buf += data[offset:offset+1]
            offset += 1
            if winlen < 4095:
                winlen += 1
            else:
                winoff += 1
        else:
            x = 8
            cnt = 4 # to avoid math.log(x, 2)
            while offset + x + 3 > len(data):
                x = x>>1
                cnt -= 1
            dx = x>>1
            for _ in range(cnt):
                chunk = data[offset:offset + x + 3]
                res = data[winoff:winoff + max(winlen, offset + x +3 - 1)].find(chunk)
                if res == -1:
                    x -= dx
                    shift = -1
                else:
                    pos = res
                    x += dx
                    shift = 0
                dx = dx>>1
            x += shift
            # here x is the longest [3:18] match
            buf += struct.pack("<H", (pos << 4) + x )
            flags += 1 << fcnt
            offset += x + 3
            if winlen + x + 3 > 4095:
                winoff += winlen + x + 3 - 4095
            else:
                winlen += x + 3
        fcnt += 1

        if fcnt == 8:
            result += struct.pack("B", flags) + buf
            buf = b''
            fcnt = 0
            flags = 0

    if fcnt > 0:
        result += struct.pack("B", flags) + buf
        if padding:
            result += b'\x00'*(8-fcnt)

    return result