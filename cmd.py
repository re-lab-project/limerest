#!/usr/bin/env python3
#
# Copyright (C) 2007,2010,2011,2021 Valek Filippov (frob@df.ru)
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of version 3 of the GNU General Public
# License as published by the Free Software Foundation.
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

import importlib
import os
import struct
import sys
from gi.repository import Gdk, Gtk

import utils
from utils import debug


def str2num(txt):
    base = 10
    txt = txt.strip()
    if txt[0] in "+-":
        txt = txt[1:]
    if not len(txt):
        return
    if txt[:2] == "0x":
        base = 16
        txt = txt[2:]
    try:
        addr = int(txt, base)
    except:
        addr = None
    return addr

def search_in_buffer(buf, data, result_model):
    test = -1
    try:
        while test < len(buf):
            test = buf.find(data, test+1)
            if test != -1:
                result_model.append(["%02x" % (len(result_model)+1), "", "", "%#x" % test, True, False])
            else:
                break
    except Exception as e:
        print("Search failed.", e)


def search_in_iter(modelrow, data, result_model, typesearch=False, parents=None):
    name, itype, _, buf = modelrow[:]
    newparents = []
    if not parents:
        parents = []
    if typesearch:
        if itype and data in itype:
            result_model.append(["%02x" % (len(result_model)+1), str(modelrow.path), name, itype, True, False])
    else:
        test = -1
        try:
            while test < len(buf):
                test = buf.find(data, test+1)
                if test != -1:
                    #parent = modelrow.model.iter_n_children(modelrow.model.get_iter(path))
                    for p in parents:
                        result_model.set_value(p, 5, True)
                    parents = []
                    np = result_model.append(["%02x" % (len(result_model)+1), str(modelrow.path), name, "%#x" % test, True, False])
                    newparents.append(np)
                else:
                    break
        except Exception as e:
            print("Search failed.", e)
    for i in modelrow.iterchildren():
        search_in_iter(i, data, result_model, typesearch, newparents)

def execute(cmd, appwin):
    appname = appwin.app.appname
    # find current page, selected iter etc
    pn = appwin.main_nb.get_current_page()
    if pn < 0:
        return "No opened docs to work with..."
    page = appwin.main_nb.get_nth_page(pn)
    if appname == "limerest":
        model = page.model
        treeSel = page.main_tview.get_selection()
        _, spaths = treeSel.get_selected_rows()
        spath, itr = (spaths[0], model.get_iter(spaths[0])) if spaths else (None, None)
    if cmd[0] == "?":
        # search
        # if there is selected iter will search in it and every child deep-first
        # for matches print iter path, name, type and offset to the match
        # ?x0123 - hexvalues
        # ?aTEXT - utf8
        # ?uTEXT - utf16
        # ?XYdata - X is < or >, Y is one of the struct format letters;
        #    pack 'data' according to spec, data is dec or hex number, only dec for floats
        # ?tdata - search in row type
        typesearch = False
        if cmd[1] in "tT" and appname == "limerest":
            # search for iters with
            data = cmd[2:]
            typesearch = True
            if cmd[1] == "T":
                spath = "0" # "global" search
        elif cmd[1].lower() == "x":
            data = utils.hex2d(cmd[2:])
        elif cmd[1].lower() == "a":
            data = cmd[2:].encode("utf-8")
        elif cmd[1].lower() == "u":
            data = cmd[2:].encode("utf-16")
        elif cmd[1] in "<>":
            if "." in cmd[3:] and cmd[2] in "def":
                try:
                    num = float(cmd[3:])
                except Exception as e:
                    return "Failed to read the number. %s" % e
            else:
                num = str2num(cmd[3:])
            try:
                data = struct.pack(cmd[1:3], num)
            except Exception as e:
                return "Failed to pack number. %s" % e
        else:
            return "Missed or wrong search type"
        if appname == "limerest":
            path = spath or "0"
            if spath:
                msc = model.iter_n_children(model[spath].iter)
                if not msc:
                    # search in parent not in the end leaf
                    pspl = str(spath).split(":")
                    if len(pspl) > 2:
                        path = (":").join(pspl[:len(pspl) - 1])
            sw = SearchWindow(appwin, page, cmd, len(data))
            search_in_iter(model[path], data, sw.model, typesearch)
        else:
            sw = SearchWindow(appwin, page, cmd, len(data))
            search_in_buffer(page.hexview.data, data, sw.model)
        if len(sw.model):
            sw.show_all()
        else:
            del sw
            page.app.set_stbar_msg("<span foreground='red'>Not found</span>", 3)
    elif cmd[:6] == "reload":
        mods = [x.strip() for x in cmd[6:].split(",")]
        for m in mods:
            if m[0] == ".":
                m = "formats" + m
            if m in sys.modules:
                importlib.reload(sys.modules[m])
                print("Reloading module %s" % m)
    elif cmd[:4] == "goto":
        path = cmd[4:].strip()
        if appname != "limerest":
            path = "#" + path
        if path[0] == "#":
            # scroll hexview
            path = path[1:]
            addr = str2num(path)
            if addr is None:
                return "String was not recognized as valid address"
            off = addr
            if path[0] == "+":
                off += page.hexview.cur_pos
            elif path[0] == "-":
                off = max(0, page.hexview.cur_pos - off)
            page.hexview.cur_pos = min(off, len(page.hexview.data) - 1)
            page.hexview.scroll_to(page.hexview.cur_pos, True)
            return
        if appname == "limerest":
            spl = path.split("#")
            if len(spl) > 1:
                path, off = spl[:2]
                addr = str2num(off)
                off = addr or 0
            else:
                off = 0
            try:
                path = Gtk.TreePath.new_from_string(path)
                page.main_tview.expand_to_path(path)
                page.main_tview.set_cursor(path, None, False)
                page.main_tview.row_activated(path, page.main_tview.get_column(0))
                page.hexview.cur_pos = min(off, len(page.hexview.data) - 1)
                page.hexview.scroll_to(page.hexview.cur_pos, True)
            except Exception as e:
                print("Failed to change row", e)
    elif cmd == "join" and appname == "limerest" and len(spaths) > 1:
        # join selected iters into the top one, remove all but top
        data = model[spath][3]
        for sp in spaths[1:]:
            data += model[sp][3]
        model[spath][3] = data
        for sp in reversed(spaths[1:]):
            del model[sp]
        page.main_tview.row_activated(spath, page.main_tview.get_column(0))
    elif cmd == "delete" and appname == "limerest" and spath:
        for sp in reversed(spaths):
            del model[sp]
        # modify the command to prevent accidental removal
        # to remove multiple iters it would require to press 'd' before 'enter'
        appwin.cmd_entry.set_text("elete")
        treeSel = page.main_tview.get_selection()
        model, nspaths = treeSel.get_selected_rows()
        if nspaths:
            page.main_tview.row_activated(nspaths[0], page.main_tview.get_column(0))
        return "Commandline was modified to prevent accidental deletes."
    elif cmd[:4] == "name":
        if appname != "limerest":
            # FIXME! rename tab
            pass
        elif spaths:
            # rename iter
            # 'name X@Y' X for name, Y for type
            xy = cmd[4:].strip().split("@", 1)
            if len(xy) > 1:
                x, y = xy
            else:
                x, y = xy[0], None
            for sp in spaths:
                if x:
                    model[sp][0] = x
                if y:
                    model[sp][1] = y
    elif appname == "limerest" and spath and cmd[:5] in ("child", "split"):
        # 'child A,B,C...' append to the iter children made of its parts
        # 'split A,B,C...' split the iter into smaller parts inserted on the same level
        # values could be a mix of offsets and lengths
        data = model[spath][3]
        values = [x.strip() for x in cmd[5:].split(",")]
        addrs = []
        last = 0
        for v in values:
            addr = str2num(v)
            if v[0] == "+":
                addr += last
            addrs.append((last, addr))
            if addr > len(data):
                break
            last = addr
        if addrs[-1][1] < len(data):
            addrs.append((addrs[-1][1], len(data)))
        # here there is a list of start/end tupples with offsets
        if cmd[:5] == "child":
            for i, (s,e) in enumerate(addrs):
                cdata = data[s:e]
                page.pgi_add(parent=itr, name="Child %02x" % i, type="???", data=cdata)
        if cmd[:5] == "split":
            name_pfx = model[spath][0]
            type_pfx = model[spath][0]
            cdata = data[:addrs[0][1]]
            model[spath][0] = "%s Part 0" % name_pfx
            model[spath][3] = cdata
            page.hexview.set_data(cdata)
            page.hexview.set_size()
            addrs = addrs[1:]
            for i, (s,e) in enumerate(addrs):
                cdata = data[s:e]
                itr = page.pgi_ins(sibling=itr, name="%s Part %02x" % (name_pfx, i + 1), type=type_pfx, data=cdata)
        else:
            return "Command was not recognized."
    elif appname == "limerest" and spath and cmd[:4] == "dump":
        # save iter data or its part
        # 'dump' - save a whole iter
        # in all cases A and B can be dec or hex (start with 0x)
        data = model[spath][3]
        addr = cmd[4:].strip().lower()
        if  len(addr) > 0:
            pos = addr.find(":")
            s_addr = 0
            e_addr = len(data)
            if pos > -1:
                # 'dump A:B' - save from A to B, same rules as in python for skipped A/B
                # 'dump A:+B' - save B bytes starting from A
                s_a = addr[:pos].strip() or 0
                e_a = addr[pos+1:].strip() or None
            if s_a:
                s_addr = str2num(s_a)
            if e_a:
                e_addr = str2num(e_a)
                if s_a and len(e_a) > 1 and e_a[0] == "+":
                    # 'dump +B' - save B bytes from the start
                    e_addr += s_addr
            data = data[s_addr:e_addr]

        appwin.app.save(data)

def on_cmd_entry_activate(w, appwin):
    cmd = w.get_text()
    if len(cmd) > 0:
        err = execute(cmd, appwin)
        if not err and cmd not in appwin.cmd_history:
            appwin.cmd_history.append(cmd)
            appwin.cmd_cur = -1

def on_cmd_entry_keypressed(w, e, appwin):
    if len(appwin.cmd_history) > 0:
        kn = Gdk.keyval_name(e.keyval)
        if kn == "Up":
            if appwin.cmd_cur == -1:
                if len(appwin.cmd_history) > 1:
                    appwin.cmd_cur = len(appwin.cmd_history) - 2
                else:
                    appwin.cmd_cur = 0
            elif appwin.cmd_cur > 0:
                appwin.cmd_cur -= 1
            appwin.cmd_entry.set_text(appwin.cmd_history[appwin.cmd_cur])
            return True
        elif kn == "Down":
            if appwin.cmd_cur == -1:
                appwin.cmd_cur = len(appwin.cmd_history) - 1
            elif appwin.cmd_cur < len(appwin.cmd_history) - 1:
                appwin.cmd_cur += 1
            appwin.cmd_entry.set_text(appwin.cmd_history[appwin.cmd_cur])
            return True

@Gtk.Template(filename="ui/search.ui")
class SearchWindow(Gtk.Window):
    __gtype_name__ = "SearchWindow"

    search_tview = Gtk.Template.Child()
    search_store = Gtk.Template.Child()
    deep_chkbox = Gtk.Template.Child()

    def __init__(self, appwin, page, cmd, length, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.model = self.search_store
        self.appwin = appwin
        self.page = page
        self.cmd = cmd
        self.length = length
        self.filter = self.search_store.filter_new()
        self.search_tview.set_model(self.filter)
        self.filter.set_visible_func(self.visible_func)
        self.search_tview.connect("row-activated", self.on_row_activated)
        self.set_title("%s : %s" % (os.path.split(self.page.path)[1], self.cmd))
        self.deep_chkbox.connect("toggled", self.on_deep_toggled)

    def on_deep_toggled(self, w):
        self.filter.refilter()

    def visible_func(self, model, iter, data):
        if self.deep_chkbox.get_active():
            return not model[iter][5]
        else:
            return True

    def on_row_activated(self, tv, spath, tvc):
        pn = self.appwin.main_nb.page_num(self.page)
        if pn < 0:
            return
        self.appwin.main_nb.set_current_page(pn)
        mr = self.model[self.filter.convert_iter_to_child_iter(self.filter.get_iter(spath))]
        if self.appwin.app.appname == "limerest":
            path = Gtk.TreePath.new_from_string(mr[1])
            self.page.main_tview.expand_to_path(path)
            self.page.main_tview.set_cursor(path, None, False)
            self.page.main_tview.row_activated(path, self.page.main_tview.get_column(0))
        try:
            off = int(mr[3], 16)
            redraw = (self.page.hexview.cur_pos != off) or (self.page.hexview.cur_len != self.length - 1)
            self.page.hexview.cur_pos = off
            self.page.hexview.cur_len = self.length - 1
            self.page.hexview.scroll_to(off, redraw)
        except:
            pass # in case of type search mr[3] stores type name and not an offset

# END
